/**
 * @apiDefinePermission admin This title is visible in version 0.1.0 and 0.2.0
 * @apiVersion 0.1.0
 */


/**
 * @api {post} /auth Авторизация системы
 * @apiVersion 0.1.0
 * @apiName auth
 * @apiGroup Object
 * @apiPermission всем системам
 *
 * @apiDescription Запрос на авторизацию и получение системой рабочего токена.  До авторизации невозможны операции с объектами.
 *
 * @apiParam {String} id* Id системы.
 * @apiParam {String} token* Токен авторизации.
 *
 *
 * @apiErrorExample {json} Search-Error-Response:
 *    HTTP/1.1 404 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "404",
 *       "error_text": "System is not exist"
 *     }
 *
 * @apiErrorExample {json} Save-Error-Response:
 *    HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "500",
 *       "error_text": "Processing error"
 *     }
 *

 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "success": "true",
 *       "work_token": "1231asasqbdfs32324bfsd"
 *     }
 */


/**
 * @api {post} /get Получить объекты
 * @apiVersion 0.1.0
 * @apiName Get
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на получение объектов системы и их атрибутов. Условие для поиска составляется в зависимости от значения флага поиска search внутри параметра data.  Если search равен 1: поиск производится по всем полученным параметрам поиска их логическим умножением. Если search равен 0: поиск производится по полученным параметрам их логическим сложением.
 *
 * @apiParam {String} id* Id системы.
 * @apiParam {String} token* рабочий токен полученный при авторизации.
 * @apiParam {Json} data* Данные для фильтра по объектам.
 *
 *
 * @apiSuccessExample {json} Request-example:
 *     data =
 *     {
 *       "search": "0", - Флаг поиска (обязательный)
 *       "id": "12", - Id объектов
 *       "created_at_before": "22.02.2017" - Дата создания, от
 *       "created_at_after": "22.02.2016" - Дата создания, до
 *       "updated_at_before": "22.02.2016" - Дата изменения, от
 *       "updated_at_after": "22.02.2016" - Дата изменения, до
 *       "like": "Text" - Вхождение
 *       "limit" : 10 - количество записей в выборке
 *       "offset" : 0 - количество пропускаемых записей
 *     }
 *
 * @apiSuccessExample {json} Success-Response-example:
 *      [
 *          {
 *              "id":55, - id объекта
 *              "name":"ЗКО (Завод котельного оборудования)", - название объекта
 *              "description":"", - описание
 *              "latitude":56.717241, - широта
 *              "longitude":37.538879, - долгота
 *              "status":2, - статус
 *              "address_address":"г. Талдом", - адрес
 *              "owner":3, - форма собственности
 *              "bargain_type":null, - тип сделки
 *              "bargain_price":null, - сумма сделки
 *              "square":2.48, - площадь, га
 *              "square_sqm":null, - площадь, кв. м
 *              "cadastral_number":"50:01:0031105:24", - кадастровый номер
 *              "distance_to_moscow":null, - дистанция до Москвы
 *              "square_target":"Промышленное производство", - назначение площади
 *              "vri":"123", - ВРИ
 *              "has_electricity_supply":1, - электроснабжение
 *              "has_gas_supply":1, - газоснабжение
 *              "has_water_supply":1, - водоснабжение
 *              "has_water_removal_supply":1, - водоотведение
 *              "has_heat_supply":1, - теплоснабжение
 *              "electricity_supply_capacity":"0.65", - мощность электроснабжения
 *              "gas_supply_capacity":"", - мощность газоснабжения
 *              "water_supply_capacity":"", - мощность водоснабжения
 *              "water_removal_supply_capacity":"", - мощность водоотведения
 *              "heat_supply_capacity":"", - мощность темплоснабжения
 *              "land_category":"1", - категория земель
 *              "has_buildings":1, - наличие строений
 *              "buildings_count":null, - количество строений
 *              "has_road_availability":0, - доступность дорог
 *              "distance_to_nearest_road":null, - расстояние до ближайшей дороги
 *              "has_railway_availability":1, - доступность железнодорожного транспорта
 *              "has_charge":0, - наличие обременения
 *              "charge_type":"", - тип обременения
 *              "is_special_ecological_zone":null, - специальная экологическая зона
 *              "danger_class":1, - класс опасности производства
 *              "phone":"", - контактный телефон
 *              "parent_id":null, - родительский объект
 *              "object_type_id":4, - тип объекта
 *              "system_id":1, - идентификатор системы
 *              "okved":"", - ОКВЭД
 *              "owner_name":"", - наименование владельца
 *              "flat":null, - этажность
 *              "flat_height":null, - высота этажа
 *              "industry_type":1, - тип промышленности
 *              "municipality":"", - муниципальное образование
 *              "land_status":null, - статус земель
 *              "created_at":"2017-07-07 13:11:50", - всемя
 *              "updated_at":"2017-08-14 10:24:55"
 *          },
 *          ...
 *     ]
 *
 * @apiErrorExample {json} Limit-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "The Search Flag can not be empty"
 *     }
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Auth error"
 *     }
 *
 */

/**
 * @api {post} /get-object-container Поиск контейнера по объекту
 * @apiVersion 0.1.0
 * @apiName GetObjectContainer
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на получение контейнера по id объекта. Метод возвращает обязательные параметры объекта, необходимые для его идентификации,
 *
 * @apiParam {String} id Id системы.
 * @apiParam {String} token рабочий токен полученный при авторизации.
 * @apiParam {Json} data Данные для фильтра по контейнерам.
 *
 * @apiSuccessExample {json} Requset-example:
 *      data =
 *      {
 *          "object_id": 1 - идентификатор объекта (обязательный)
 *      }
 *
 * @apiSuccessExample {json} Success-Response-example:
 *      {
 *          "id":74,
 *          "name":"Ступино-1",
 *          "address_address":"Ступинский район",
 *          "object_type_id":1
 *      }
 *
 *
 *
 *
 * @apiErrorExample {json} Not-Found-Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "success": "false",
 *       "error_code": "404",
 *       "error_text": "There is no object with that id"
 *     }
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Auth error"
 *     }
 *
 * @apiErrorExample {json} No-parent-Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "success": "false",
 *       "error_code": "404,
 *       "error_text": "The object has no parent"
 *     }
 *
 * @apiErrorExample {json} Bad-Request-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Request error: the object_id param is empty"
 *     }
 */


/**
 * @api {post} /get-objects-by-container Получить объекты по контейнеру
 * @apiVersion 0.1.0
 * @apiName GetObjectsByContainer
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на получение вложенных объектов по id контейнера. Метод возвращает обязательные параметры объектов, необходимые для их идентификации, включая id объектов.
 *
 * @apiParam {String} id Id системы.
 * @apiParam {String} token рабочий токен полученный при авторизации.
 * @apiParam {Json} data Данные для фильтра по контейнерам.
 *
 * @apiSuccessExample {json} Requset-example:
 *     data =
 *     {
 *       "parent_id": 1 - идентификатор контейнера (обязательный)
 *     }
 *
 * @apiSuccessExample {json} Success-Response-example:
 *      [
 *          {
 *              "id":74,
 *              "name":"Ступино-1",
 *              "address_address":"Ступинский район",
 *              "object_type_id":1
 *          },
 *          ...
 *      ]
 *
 * @apiErrorExample {json} No-Children-Objects-Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "success": "false",
 *       "error_code": "404",
 *       "error_text": "No child objects available"
 *     }
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Auth error"
 *     }
 *
 *
 * @apiErrorExample {json} Bad-Request-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Request error: the parent_id param is empty"
 *     }
 */


/**
 * @api {post} /create Создать объект
 * @apiVersion 0.1.0
 * @apiName Create
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на создание объектов с указанием их параметров.
 *
 * @apiParam {String} id Id системы.
 * @apiParam {String} token рабочий токен полученный при авторизации.
 * @apiParam {Json} data Данные для фильтра по контейнерам.
 *
 * @apiSuccessExample {json} Request-example:
 *     data =
 *     [
 *         {
 *              "name":"ЗКО (Завод котельного оборудования)", - название объекта (обязательный)
 *              "object_type_id":4, - тип объекта (обязательный)
 *              "description":"", - общие сведения
 *              "address_address":"г. Талдом", - адрес (обязательный)
 *              "latitude":56.717241, - широта (обязательный)
 *              "longitude":37.538879, - долгота (обязательный)
 *              "owner":3, - форма собственности
 *              "municipality":"", - муниципальное образование
 *              "owner_name":"", - наименование владельца
 *              "bargain_type":null, - форма реализации
 *              "bargain_price":null, - стоимость, руб
 *              "square":2.48, - площадь, га
 *              "square_sqm":null, - площадь, кв. м
 *              "cadastral_number":"50:01:0031105:24", - кадастровый номер
 *              "industry_type":1, - сектор промышленности
 *              "distance_to_moscow":null, - расстояние до Москвы, км
 *              "square_target":"Промышленное производство", - назначение площади
 *              "has_electricity_supply":1, - наличие электроснабжения
 *              "has_gas_supply":1, - наличие газоснабжения
 *              "has_water_supply":1, - наличие водоснабжения
 *              "has_water_removal_supply":1, - наличие водоотведения
 *              "has_heat_supply":1, - наличие теплоснабжения
 *              "electricity_supply_capacity":"0.65", - мощность электроснабжения, МВт
 *              "gas_supply_capacity":"", - мощность газоснабжения, м3/час
 *              "water_supply_capacity":"", - мощность водоснабжения, м3/день
 *              "water_removal_supply_capacity":"", - мощность водоотведения, м3/день
 *              "heat_supply_capacity":"", - мощность темплоснабжения, ГКал/час
 *              "land_category":"1", - категория земель
 *              "land_status":null, - статус земель
 *              "vri":"123", - ВРИ
 *              "has_buildings":1, - наличие строений
 *              "buildings_count":0, - число строений
 *              "has_road_availability":0, - наличие доступа к автодороге
 *              "distance_to_nearest_road":null, - расстояние до автодороги
 *              "has_railway_availability":1, - возможность присоединения к грузовой ж/д станции
 *              "has_charge":0, - наличие обременения
 *              "charge_type":"", - вид обременения
 *              "danger_class":1, - класс опасности производства
 *              "okved":"", - ОКВЭД
 *              "phone":"", - контактный телефон
 *              "parent_id":null, - родительский объект
 *              "flat":null, - этажность
 *              "flat_height":null, - высота этажа
 *              "photos" : [ - массив фотографий, каждое фото представлено в виде base64-строки
 *                  "==Photo_file_in_base64"
 *              ]
 *         },
 *         ...
 *    ]
 *
 * @apiSuccessExample {json} Success-Response-example:
 *      [
 *          1,
 *          2,
 *          3,
 *          ...
 *      ]
 *
 *
 *
 * @apiErrorExample {json} Validating-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" : "Validating error",
 *       "errors": [
 *           {
 *              "name": "can not be blank"
 *           },
 *           ...
 *       ]
 *     }

 * @apiErrorExample {json} Saving-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" : "Saving error",
 *       "errors": [
 *           {
 *              "name": "can not be blank"
 *           },
 *           ...
 *       ]
 *     }
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" => "Auth error"
 *     }
 *
 */


/**
 * @api {post} /update Изменить объект
 * @apiVersion 0.1.0
 * @apiName Update
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на изменение созданных объектов с указанием параметров, которые необходимо изменить. Значения атрибутов должны быть пустыми, если их необходимо очистить.
 *
 * @apiParam {String} id Id системы.
 * @apiParam {String} token рабочий токен полученный при авторизации.
 * @apiParam {Json} data Данные для фильтра по контейнерам.
 *
 * @apiSuccessExample {json} Request-example:
 *     data =
 *     [
 *         "112233" : { - 112233 - идентификатор объекта для изменения
 *              "name":"ЗКО (Завод котельного оборудования)", - название объекта (обязательный)
 *              "object_type_id":4, - тип объекта (обязательный)
 *              "description":"", - общие сведения
 *              "address_address":"г. Талдом", - адрес (обязательный)
 *              "latitude":56.717241, - широта (обязательный)
 *              "longitude":37.538879, - долгота (обязательный)
 *              "owner":3, - форма собственности
 *              "municipality":"", - муниципальное образование
 *              "owner_name":"", - наименование владельца
 *              "bargain_type":null, - форма реализации
 *              "bargain_price":null, - стоимость, руб
 *              "square":2.48, - площадь, га
 *              "square_sqm":null, - площадь, кв. м
 *              "cadastral_number":"50:01:0031105:24", - кадастровый номер
 *              "industry_type":1, - сектор промышленности
 *              "distance_to_moscow":null, - расстояние до Москвы, км
 *              "square_target":"Промышленное производство", - назначение площади
 *              "has_electricity_supply":1, - наличие электроснабжения
 *              "has_gas_supply":1, - наличие газоснабжения
 *              "has_water_supply":1, - наличие водоснабжения
 *              "has_water_removal_supply":1, - наличие водоотведения
 *              "has_heat_supply":1, - наличие теплоснабжения
 *              "electricity_supply_capacity":"0.65", - мощность электроснабжения, МВт
 *              "gas_supply_capacity":"", - мощность газоснабжения, м3/час
 *              "water_supply_capacity":"", - мощность водоснабжения, м3/день
 *              "water_removal_supply_capacity":"", - мощность водоотведения, м3/день
 *              "heat_supply_capacity":"", - мощность темплоснабжения, ГКал/час
 *              "land_category":"1", - категория земель
 *              "land_status":null, - статус земель
 *              "vri":"123", - ВРИ
 *              "has_buildings":1, - наличие строений
 *              "buildings_count":0, - число строений
 *              "has_road_availability":0, - наличие доступа к автодороге
 *              "distance_to_nearest_road":null, - расстояние до автодороги
 *              "has_railway_availability":1, - возможность присоединения к грузовой ж/д станции
 *              "has_charge":0, - наличие обременения
 *              "charge_type":"", - вид обременения
 *              "danger_class":1, - класс опасности производства
 *              "okved":"", - ОКВЭД
 *              "phone":"", - контактный телефон
 *              "parent_id":null, - родительский объект
 *              "flat":null, - этажность
 *              "flat_height":null, - высота этажа
 *              "photos" : [ - массив фотографий, каждое фото представлено в виде base64-строки
 *                  "==Photo_file_in_base64"
 *              ]
 *              или
 *              "photos" : "delete" - удалить фото, ассоциированные с данным объектом
 *         },
 *         ...
 *    ]
 *
 * @apiSuccessExample {json} Success-Response-example:
 *      [
 *          1,
 *          2,
 *          3,
 *          ...
 *      ]
 *
 *
 *
 * @apiErrorExample {json} Validating-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" : "Validating error at object:  123",
 *       "errors": [
 *           {
 *              "name": "can not be blank"
 *           },
 *           ...
 *       ]
 *     }

 * @apiErrorExample {json} Saving-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" : "Saving at object: 321",
 *       "errors": [
 *           {
 *              "name": "can not be blank"
 *           },
 *           ...
 *       ]
 *     }
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Auth error"
 *     }

  * @apiErrorExample {json} Child-Saving-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text": "Сhild saving error",
 *       "errors": [...]
 *     }
 *
 */


/**
 * @api {post} /delete Удалить объект
 * @apiVersion 0.1.0
 * @apiName Delete
 * @apiGroup Object
 * @apiPermission авторизованным системам
 *
 * @apiDescription Запрос на удаление созданных объектов
 *
 * @apiParam {String} id Id системы.
 * @apiParam {String} token рабочий токен полученный при авторизации.
 * @apiParam {Json} data Данные для фильтра по контейнерам.
 *
 * @apiSuccessExample {json} Requset-example:
 *     data =
 *     [
 *          123,
 *          321,
 *          ...
 *     ]
 *
 *
 * @apiErrorExample {json} Auth-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" => "Auth error"
 *     }
 *
 * @apiErrorExample {json} Search-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "404",
 *       "error_text" => "Object with ID 123 is not found"
 *     }
 *
 * @apiErrorExample {json} Delete-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "500",
 *       "error_text" => "Deleting error"
 *     }
 *
 *
 * @apiErrorExample {json} Save-Children-Error-Response:
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "success": "false",
 *       "error_code": "400",
 *       "error_text" => "Child saving error"
 *     }
 *
 */

/**
 * @api {} тип объекта (OBJECT_TYPE_ID)
 * @apiVersion 0.1.0
 * @apiName OBJECT_TYPE_ID
 * @apiGroup Dictionaries
 *
 * @apiParam {TYPE_INDUSTRIAL_PARK}     1     Индустриальные парки
 * @apiParam {TYPE_TECHNOLOGICAL_PARK}  2     Технопарки
 * @apiParam {TYPE_SEZ}                 3     ОЭЗ
 * @apiParam {TYPE_BROWNFIELDS}         4     Браунфилды
 * @apiParam {TYPE_RESIDENT}            5     Резиденты
 * @apiParam {TYPE_FREE_LAND}           6     Свободные земли
 * @apiParam {TYPE_FREE_SPACE}          7     Свободные площади
 * @apiParam {TYPE_COWORKING}           8     Коворкинги
 * @apiParam {TYPE_LAND}                9     Земельные участки
 * @apiParam {TYPE_PROPERTY}            10    Объекты имущества
 *
 */

/**
 * @api {} форма собственности (OWNER)
 * @apiVersion 0.1.0
 * @apiName OWNER
 * @apiGroup Dictionaries
 *
 * @apiParam {OWNER_GOVERNMENT} 1     Государственная
 * @apiParam {OWNER_MUNICIPAL}  2     Муниципальная
 * @apiParam {OWNER_PRIVATE}    3     Частная
 */

/**
 * @api {} форма реализации (BARGAIN_TYPE)
 * @apiVersion 0.1.0
 * @apiName BARGAIN_TYPE
 * @apiGroup Dictionaries
 *
 * @apiParam {BARGAIN_TYPE_SALE}    1     Продажа
 * @apiParam {BARGAIN_TYPE_RENT}    2     Аренда
 */

/**
 * @api {} сектор промышленности (INDUSTRY_TYPE)
 * @apiVersion 0.1.0
 * @apiName INDUSTRY_TYPE
 * @apiGroup Dictionaries
 *
 * @apiParam {INDUSTRY_TYPE_ELECTRICAL}             1     Электроэнергетика
 * @apiParam {INDUSTRY_TYPE_FUEL}                   2     Топливная промышленность
 * @apiParam {INDUSTRY_TYPE_IRON}                   3     Черная металлургия
 * @apiParam {INDUSTRY_TYPE_NONFERROUS}             4     Цветная металлургия
 * @apiParam {INDUSTRY_TYPE_CHEMISTRY}              5     Химия и нефтехимия
 * @apiParam {INDUSTRY_TYPE_METAL_FABRICATION}      6     Машиностроение и металлообработка
 * @apiParam {INDUSTRY_TYPE_WOOD}                   7     Лесообрабатывающая промышленность
 * @apiParam {INDUSTRY_TYPE_CONSTRUCTION_MATERIALS} 8     Промышленность стройматериалов
 * @apiParam {INDUSTRY_TYPE_GLASS}                  9     Стекольная промышленность
 * @apiParam {INDUSTRY_TYPE_CONSUMERS_GOODS}        10    Легкая промышленность
 * @apiParam {INDUSTRY_TYPE_FOOD}                   11    Пищевая промышленность
 * @apiParam {INDUSTRY_TYPE_MICROBIOLOGICAL}        12    Микробиологическая промышленность
 * @apiParam {INDUSTRY_TYPE_AGRICULTURAL}           13    Сельскохозяйственная промышленность
 * @apiParam {INDUSTRY_TYPE_MEDICINE}               14    Медицинская промышленность
 * @apiParam {INDUSTRY_TYPE_POLYGRAPHIC}            15    Полиграфическая промышленность
 *
 */

/**
 * @api {} категория земель (LAND_CATEGORY)
 * @apiVersion 0.1.0
 * @apiName LAND_CATEGORY
 * @apiGroup Dictionaries
 *
 * @apiParam {LAND_CATEGORY_SETTLEMENTS}        1     Населенный пункт
 * @apiParam {LAND_CATEGORY_INDUSTRIAL_PURPOSE} 2     Промышленного назначения
 * @apiParam {LAND_CATEGORY_AGRICULTURE}        3     Сельскохозяйственного назначения
 * @apiParam {LAND_CATEGORY_RESERVE}            4     Заповедник
 */

/**
 * @api {} статус участка (LAND_STATUS)
 * @apiVersion 0.1.0
 * @apiName LAND_STATUS
 * @apiGroup Dictionaries
 *
 * @apiParam {LAND_STATUS_FREE}             1     Свободен
 * @apiParam {LAND_STATUS_PLANNED_BARGAIN}  2     Планируется на торги
 * @apiParam {LAND_STATUS_STARTED_BARGAIN}  3     Торги объявлены
 * @apiParam {LAND_STATUS_FINISHED_BARGAIN} 4     Торги прошли
 */