$(document).on('submit','.form_ajax',function () {
    var formData = new FormData($('.form_ajax')[0]);
    var url = $(this).attr('action');
    var type = $(this).attr('method');
    $.ajax({
        url: url,
        contentType: false,
        processData: false,
        data: formData,
        type: type,
        success: function (res) {
            var data = JSON.parse(res);
            var handler = data.handler;
            if(handler){
                if(handler == 'alert')
                    alert(data.message);
                else
                    $('.' + handler).html(data.message);
            }
            if(data.location){
                location.href = data.location;
            }
        }
    });
    return false;
});