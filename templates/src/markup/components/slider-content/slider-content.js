import Component from 'helpers-js/Component';
import 'slick-carousel';
import { forEach } from 'helpers-js/for-each';

const SliderContent = Component.create('slider-content', class {
    static init($block) {
        if (!$block.length) {
            return;
        }
    }

    constructor($block) {
        this.block = $block[0];

        this.parent = this.block.closest('.slider-content__wrapper');
        this.thumb = this.parent.querySelector('.slider-content__thumb');

        this.init();
    }


    init() {

        $(this.block).on('init reInit afterChange', (event, slick, currentSlide, nextSlide) => {
            let i = (currentSlide ? currentSlide : 0) + 1;

            if (i <= 9) {
                i = `0${i}`;
            }

            if (slick.slideCount === 1) {

                slick.$slider[0].classList.add('slider-content_one-element');
                return;
            }

            this.thumb.innerHTML = `<span class="slider-content__current-slide">${i}</span> <sup class="slider-content__count"> <span class="slider-content__separator">/</span> ${slick.slideCount}</sup>`;
        });

        $(this.block).slick({
            // autoplay: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
        });
    }

});

export default SliderContent;

