var data = {social: {
    default: {
        title: 'Соцсети:',
        items: [
            {
                icon: 'vk'
            },
            {
                icon: 'fb'
            },
            {
                icon: 'in'
            }
        ]
    },
    topBox: {
        title: false,
        items: [
            {
                icon: 'vk'
            },
            {
                icon: 'fb'
            },
            {
                icon: 'in'
            }
        ]
    },
    footer: {
        title: 'Мы в соцсетях:',
        items: [
            {
                icon: 'vk'
            },
            {
                icon: 'fb'
            },
            {
                icon: 'in'
            }
        ]
    }
}};
