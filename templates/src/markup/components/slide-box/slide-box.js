import Component from 'helpers-js/Component';
import 'slick-carousel';

export default Component.create('slide-box', class {
    constructor($block) {
        this.block = $block[0];

        this.init();
    }

    init(settings) {
        const items = this.block.querySelector('.slide-box__items');

        $(items).slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 751,
                    settings: {
                        fade: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        speed: 100,
                        useCSS: false
                    }
                }
            ]
        });

        const addClasses = () => {
            const current = $(items).slick('getSlick').currentSlide;
            const count = $(items).slick('getSlick').slideCount;

            const getPrev = () => {
                if (current === 0) {
                    return count - 1;
                } else {
                    return current - 1;
                }
            };

            const getNext = () => {
                if (current === count - 1) {
                    return 0;
                } else {
                    return current + 1;
                }
            };

            const prev = getPrev();
            const next = getNext();

            $(items).slick('getSlick').$slides.each((n, slide) => {
                if (n === prev) {
                    slide.classList.add('slide-box__prev');
                } else {
                    slide.classList.remove('slide-box__prev');
                }

                if (n === next) {
                    slide.classList.add('slide-box__next');
                } else {
                    slide.classList.remove('slide-box__next');
                }
            });
        };

        addClasses();

        $(items).on('afterChange', (e, slick, current) => {
            addClasses();
        });
    }
});

