import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';
import 'jquery-validation';

export default Component.create('edit-profile', class {
    constructor($block) {
        this.block = $block[0];
        this.form = document.getElementById('edit-profile');
        this.phone = this.block.querySelector('.edit-profile__input-phone');
        this.fileInput = this.block.querySelector('.edit-profile__file-input');
        this.photoControl = this.block.querySelector('.edit-profile__photo-control');
        this.removeBtn = this.block.querySelector('.edit-profile__delete-photo');
        this.message = this.block.querySelector('.edit-profile__message');

        this.init();
    }

    init() {
        this.mask(); // Маска на поле телефона
        this.fileInput.addEventListener('change', this.selectImageProfile.bind(this)); // Навешивание фотки профиля на label
        this.removeBtn.addEventListener('click', this.removeImageProfile.bind(this)); // Удаление фотки профиля
        this.requiredForm(); // Валидация формы и отправка ajax
    }

    mask() {
        $(this.phone).mask('+7 (999) 999-99-99');
    }

    requiredForm() {

        $(this.form).validate({
            rules: {
                'User[last_name]': {
                    required: true
                },
                'User[first_name]': {
                    required: true
                },
                'User[email]': {
                    required: true,
                    email: true
                }
            },
            messages: {
                'User[last_name]': 'Поле обязательно к заполнению',
                'User[first_name]': 'Поле обязательно к заполнению',
                'User[email]': {
                    required: 'Поле обязательно к заполнению',
                    email: 'Введите валидный e-mail адрес'
                }
            },
            submitHandler: (form) => {
                let formData = new FormData(form);
                const url = form.getAttribute('action');
                const type = form.getAttribute('method');

                $.ajax({
                    url: url,
                    contentType: false,
                    processData: false,
                    data: formData,
                    type: type,
                    success: (json) => {
                        json = JSON.parse(json);
                        if (json.success) {
                            this.message.classList.remove('edit-profile__message_error');
                            this.message.innerHTML = 'Ваш профайл обновлён успешно!';

                            setTimeout( () => {
                                location.href = '/cabinet/view-profile';
                                return;
                            }, 500);
                        }

                        if (json.errors.email) {
                            this.message.classList.add('edit-profile__message_error');
                            this.message.innerHTML += json.errors.email;
                        }
                    }
                });

                return false;
            }
        });
    }

    selectImageProfile(e) {
        const target = e.target;
        let files = target.files;
        const parent = target.closest('.edit-profile__img-select');

        let fr = new FileReader();
        fr.readAsDataURL(files[0]);

        fr.addEventListener('load', () => {
            this.photoControl.style.backgroundImage = `url(${fr.result})`;
            this.photoControl.style.backgroundSize = 'cover';
            parent.classList.add('edit-profile__img-select_load');
        }, false);
    }

    removeImageProfile(e) {
        const target = e.currentTarget;
        const parent = target.closest('.edit-profile__img-select');

        this.photoControl.removeAttribute('style');
        parent.classList.remove('edit-profile__img-select_load');
    }

});

