import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('title-subpage', class {
    constructor($block) {
        this.block = $block[0];
        this.$block = $block;

        this.init();
    }

    init() {
        const backLink = this.block.querySelector('.title-subpage__back-link');

        if (!backLink) {
            return;
        }

        backLink.addEventListener('click', () => {
            if (document.referrer !== '' & document.referrer !== location.href) {
                window.location = document.referrer;
            } else {
                window.location = '/';
            }
        });
    }

});

