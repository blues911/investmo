import 'magnific-popup';
import 'jquery-validation';
import 'jquery-mask-plugin';



$('.js-go-to-event').on('click', function (e) {
    e.preventDefault();

    $.magnificPopup.open({
        items: {
            src: '.js-trainings-popup'
        },
        type: 'inline',
        showCloseBtn: false,
        mainClass: 'trainings-popup-overlay'
    });
});

$('.js-trainings-popup-close').on('click', function (e) {
    e.preventDefault();

    $.magnificPopup.close();
});


if ($('.js-birth-year').length) {
    $('.js-birth-year').mask('9999');
}

if ($('.js-phone-input').length) {
    $('.js-phone-input').mask('+7 (999) 999-99-99');
}

if ($('#trainings-form').length) {

    $('#trainings-form').validate({

        ignore: 'input[type="text"]:hidden',

        rules: {
            fio: {
                required: true
            },
            'birth_year': {
                required: true,
                minlength: 4
            },
            address: {
                required: true
            },
            phone: {
                required: true
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            fio: {
                required: 'Поле обязательно к заполнению'
            },
            'birth_year': {
                required: 'Поле обязательно к заполнению'
            },
            address: {
                required: 'Поле обязательно к заполнению'
            },
            phone: {
                required: 'Поле обязательно к заполнению'
            },
            email: {
                required: 'Поле обязательно к заполнению',
                email: 'Укажите корректный e-mail'
            }
        }
    });
}

