import Component from 'helpers-js/Component';

export default Component.create('select-primary', class {
    constructor() {
        this.init();
    }

    init() {
        $('.select-primary').select2({
            minimumResultsForSearch: -1,
            // width: 'auto',
            width: '100%',
            placeholder: $(this).data('placeholder'),
            dropdownAutoWidth: true,
            theme: 'primary'
        });
    }

});
