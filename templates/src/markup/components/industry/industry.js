$(function () {

    $('.industry__items').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.industry__controls',
        responsive: [
            {
                breakpoint: 750,
                settings: {
                    adaptiveHeight: true
                }
            }
        ]
    });

    $('.industry__controls').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.industry__items',
        focusOnSelect: true,
        centerMode: true,
        centerPadding: '0',

        responsive: [
            {
                breakpoint: 1281,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 751,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });
});
