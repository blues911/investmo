import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';
import { indexOf } from 'helpers-js';

export default Component.create('tabs', class {
    constructor() {

        this.tabsLink = document.querySelectorAll('.tab__title');
        this.tabsContent = document.querySelectorAll('.tab__content');

        this.header = document.querySelector('header');

        this.init();
    }

    init() {

        let headerHeight = this.header.offsetHeight;

        var SimpleTabs = function () {
            var self = this,
                tabs = $('.tabs');

            tabs.on('click', '.tab__title', function (event) {
                let target = event.currentTarget;
                let tag = target.nodeName;
                if (tag === 'SPAN') {
                    self.open($(this), event);
                } else {
                    let hash = $(target).attr('href');
                    $('html, body')
                        .animate({
                            scrollTop: ($(hash).offset().top - (headerHeight * 2))
                        }, 100);
                    return false;
                }
            });

            self.open = function (elem, event) {

                let clientWidth = document.documentElement.clientWidth;
                let header = document.querySelector('header');

                if (!elem.hasClass('is-active')) {
                    event.preventDefault();
                    var parentTabs = elem.closest(tabs);
                    parentTabs.find('.is-open').removeClass('is-open');

                    var index = elem.index();
                    let $openTab = parentTabs.find('.tab__content').eq(index);
                    $openTab.toggleClass('is-open');
                    parentTabs.find('.is-active').removeClass('is-active');
                    elem.addClass('is-active');

                    if (clientWidth <= 750) {
                        let firstBlock = $openTab.find('div').first();
                        let height = header.offsetHeight;

                        if (firstBlock) {
                            $('html, body')
                                .animate({
                                    scrollTop: firstBlock.offset().top - height
                                }, 300);
                        }

                    }

                } else {
                    event.preventDefault();
                }
            };
        };

        var simpleTabs = new SimpleTabs();

        this.openBySearch(); // Если есть в урле номер таба, открывает этот таб
        window.addEventListener('load', this.hashScroll.bind(this)); // Добавляем отступ сверху, чтобы был виден заголовок при переходе к хэшу
    }

    hashScroll() {
        const hashName = window.location.hash;
        let headerHeight = this.header.offsetHeight;

        if (hashName) {

            $('html, body')
                .animate({
                    scrollTop: ($(hashName).offset().top - (headerHeight * 2))
                }, 100);
            return false;
        }
    }

    openBySearch() {

        const search = window.location.search;
        let result = search.match( /open-tab/gi);

        if (result !== null) {
            let number = +search.slice(-1);
            if (number) {

                forEach(this.tabsLink, (link) => {

                    link.classList.remove('is-active');

                    const idx = indexOf(link, this.tabsLink);

                    if (idx === number) {
                        link.classList.add('is-active');
                    }
                });

                forEach(this.tabsContent, (tab) => {
                    tab.classList.remove('is-open');

                    const idx = indexOf(tab, this.tabsContent);

                    if (idx === number) {
                        tab.classList.add('is-open');
                    }
                });
            }
        }
    }

});
