var data = {tabs: {
    finance: {
        1: {
            title: 'Финансовая и кредитная поддержка',
            active: true
        },
        2: {
            title: 'Имущественная поддержка',
            active: false
        },
        3: {
            title: 'Налоговые льготы',
            active: false
        },
        4: {
            title: 'Коворкинги',
            active: false
        }
    },
    property: {
        1: {
            title: 'Финансовая и кредитная поддержка',
            active: false
        },
        2: {
            title: 'Имущественная поддержка',
            active: true
        },
        3: {
            title: 'Налоговые льготы',
            active: false
        },
        4: {
            title: 'Коворкинги',
            active: false
        }
    },
    coWorking: {
        1: {
            title: 'Финансовая и кредитная поддержка',
            active: false
        },
        2: {
            title: 'Имущественная поддержка',
            active: false
        },
        3: {
            title: 'Налоговые льготы',
            active: false
        },
        4: {
            title: 'Коворкинги',
            active: true
        }
    },
    support31: {
        1: {
            title: 'Финансовая и кредитная поддержка',
            active: false
        },
        2: {
            title: 'Имущественная поддержка',
            active: false
        },
        3: {
            title: 'Налоговые льготы',
            active: true
        },
        4: {
            title: 'Коворкинги',
            active: false
        }
    },
    support3: {
        1: {
            title: 'Научные и социальные',
            active: false
        },
        2: {
            title: 'Бизнес',
            active: false
        },
        3: {
            title: 'Промышленные',
            active: false
        }
    },
    sBlock1: {
        1: {
            title: 'Офисы, технопарки,<br> гостиницы',
            active: true
        },
        2: {
            title: 'Научные и социальные,<br> индустриальные парки',
            active: false
        },
        3: {
            title: 'С/Х, Обрабатывающая<br> промышленность, транспорт',
            active: false
        },
        4: {
            title: 'Малые предприятия',
            active: false
        }
    }
}}
