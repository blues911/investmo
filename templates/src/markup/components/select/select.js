$('.select').each((ind, block) => {
    const $block = $(block);
    const $widget = $block.find('.select__widget');

    $widget.select2();
});
