var data = {projects: {
    default: {
        settings: {
            slidesToShow: 3,
            responsive: [
                {
                    breakpoint: 751,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false
                    }
                }
            ]
        },
        items: [
            {
                bg: 'project-bg1.jpg',
                title: '<strong>45</strong> Технопарков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg4.jpg',
                title: '<strong>75</strong> Складов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            }
        ]
    },

    investoram: {
        title: 'Расширенный фильтр поможет вам <br> <b>быстро подобрать нужный проект</b>',
        mobileTitle: 'Хотите <b>быстро подобрать интересующий вас проект ?</b> <br> Вам поможет расширенный фильтр',
        settings: {
            noSlider: true,
            noDefaultContent: true,
            slidesToShow: 4,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 751,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false
                    }
                }
            ]
        },
        advantages: {
            button: 'Подробнее об инвестиционной инфраструктуре',
            mButton: 'Посмотреть все объекты на карте',
            items: [
                {
                    number: '3',
                    text: 'особые экономические зоны'
                },
                {
                    number: '28',
                    text: 'индустриальных парков'
                },
                {
                    number: '3',
                    text: 'действующих технопарка'
                },
                {
                    number: '2',
                    text: 'промышленных кластеар'
                }
            ],
            mItems: [
                {
                    number: '32',
                    text: 'Заводских <br> цехов'
                },
                {
                    number: '56',
                    text: 'Технопарков <br> и браунфилдов'
                },
                {
                    number: '12',
                    text: '<br> Складов'
                },
                {
                    number: '31',
                    text: 'Земельных <br> участков'
                }
            ]
        },
        items: [
            {
                bg: 'project-bg1.jpg',
                title: '<strong>45</strong> Технопарков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg4.jpg',
                title: '<strong>75</strong> Складов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            }
        ]
    },

    business: {
        title: 'Расширенный фильтр поможет вам <br> <b>быстро подобрать нужный проект</b>',
        mobileTitle: 'Хотите <b>быстро подобрать интересующий вас проект ?</b> <br> Вам поможет расширенный фильтр',
        settings: {
            noSlider: true,
            noDefaultContent: true,
            slidesToShow: 4,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 751,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false
                    }
                }
            ]
        },
        advantages: {
            button: 'Посмотреть все объекты на карте',
            button2: 'Подробней об инвестиционной инфраструктуре',
            items: [
                {
                    number: '3',
                    text: 'особые экономические зоны'
                },
                {
                    number: '28',
                    text: 'индустриальных парков'
                },
                {
                    number: '3',
                    text: 'действующих технопарка'
                },
                {
                    number: '2',
                    text: 'промышленных кластеар'
                }
            ],
            mItems: [
                {
                    number: '3',
                    text: 'особые <br> экономические <br> зоны'
                },
                {
                    number: '28',
                    text: 'индустриальных <br> парков'
                },
                {
                    number: '3',
                    text: '<br> действующих технопарка'
                },
                {
                    number: '2',
                    text: 'промышленных <br> кластера'
                }
            ]

        },
        items: [
            {
                bg: 'project-bg1.jpg',
                title: '<strong>45</strong> Технопарков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg4.jpg',
                title: '<strong>75</strong> Складов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg2.jpg',
                title: '<strong>95</strong> Заводских цехов',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            },
            {
                bg: 'project-bg3.jpg',
                title: '<strong>120</strong> Участков',
                subtitle: 'Средняя стоимость <br> цехов составляет <br> 40 000–45 000 руб за 1 кв. м'
            }
        ]
    },
    property: {
        title: 'Аренда площадей',
        mobileTitle: 'Аренда площадей',
        settings: {
            noSlider: true,
            noDefaultContent: false
        },
        advantages: {
            items: [

            ],
            mItems: [

            ]
        }
    }
}}
