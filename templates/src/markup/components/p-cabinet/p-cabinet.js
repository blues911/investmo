import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('p-cabinet', class {
    constructor($block) {
        this.block = $block[0];
        this.objectlink = this.block.querySelectorAll('.object-list__close-link');
        this.popup = this.block.querySelector('.popup-success');

        if (this.popup) {
            this.titlePopup = this.popup.querySelector('.popup-success__title');
            this.subtitlePopup = this.popup.querySelector('.popup-success__sub-title');
            this.confirmationBtn = this.block.querySelector('.popup-success__button_confirmation');
            this.renouncementBtn = this.block.querySelector('.popup-success__button_renouncement');
        }
        this.body = document.body;
        this.init();
    }

    init() {

        forEach(this.objectlink, (link) => {
            link.addEventListener('click', this.getId.bind(this)); // Показываем попап предупреждения
        });

        if (this.popup) {
            this.renouncementBtn.addEventListener('click', this.closePopup.bind(this)); // Закрываем попап
            this.confirmationBtn.addEventListener('click', this.deleteObject.bind(this)); // Удаляем элемент
        }
    }

    getId(e) {
        const target = e.currentTarget;
        this.id = target.getAttribute('data-id');
        this.viewPoup();
    }

    deleteObject() {

        const text = {
            title: 'Объект успешно удалён!',
            subtitle: 'Страница будет перезагружена.'
        };

        $.ajax({
            type: 'GET',
            url: `/cabinet/delete-object/${this.id}`,
            success: (response) => {
                response = $.parseJSON(response);

                if (response.success) {
                    this.popup.classList.remove('popup-success_edit-object');
                    this.titlePopup.innerHTML = text.title;
                    this.subtitlePopup.innerHTML = text.subtitle;

                    setTimeout(() => {
                        window.location = '/cabinet';
                        return;
                    }, 2000);
                }
            }
        });
    }

    viewPoup(text) {
        this.popup.classList.add('popup-success_visible');
        this.popup.classList.add('popup-success_edit-object');
        this.body.classList.add('open-popup');
    }

    closePopup(e) {
        let target = e.currentTarget;
        const parent = target.closest('.popup-success');

        parent.classList.remove('popup-success_visible');
        this.body.classList.remove('open-popup');
    }
});

