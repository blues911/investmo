import Component from 'helpers-js/Component';
import 'slick-carousel';

export default Component.create('trainings', class {
    constructor($block) {
        this.block = $block[0];

        this.init();
    }

    init() {
        const select = this.block.querySelector('.js-custom-select');

        const tabs = [].slice.call(this.block.querySelectorAll('.trainings__box'));

        const activateTab = (id) => {
            tabs.forEach(tab => {
                if (tab.getAttribute('data-id') === id) {
                    tab.classList.add('active');
                } else {
                    tab.classList.remove('active');
                }
            });
        };

        activateTab(select.value);



        $('.js-custom-select').select2({
            theme: 'custom-select',
            minimumResultsForSearch: Infinity
        }).on('change', () => {
            activateTab(select.value);
        });
    }
});


