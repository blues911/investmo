import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';
import '../../static/js/libraries/waypoint/jquery.waypoints.min';

export default Component.create('footer', class {
    constructor($block) {
        this.block = $block[0];

        this.init();
    }

    init() {
        this.continuousElements = document.getElementsByClassName('hash-block');

        this.getHash();
    }

    getHash() {
        let subNav = document.querySelector('.info-box__subnav');

        if (subNav) {
            var subLink = subNav.querySelectorAll('.info-box__subnav-link');
        }

        for (let i = 0; i < this.continuousElements.length; i++) {
            this.point = new Waypoint({ // eslint-disable-line
                element: this.continuousElements[i],
                handler: function () {
                    let hash = this.element.id;
                    history.pushState('', document.title, `${location.pathname}#${hash}`);

                    // Выставляем активный пункт в подменю
                    forEach(subLink, (link) => {
                        let url = link.getAttribute('href');

                        link.classList.remove('info-box__subnav-link_active');

                        if (url === `#${hash}`) {
                            link.classList.add('info-box__subnav-link_active');
                        }
                    });
                }
            });
        }

    }
});
