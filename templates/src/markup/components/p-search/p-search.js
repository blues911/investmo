import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('p-search', class {
    constructor($block) {
        this.block = $block[0];

        this.textResult = document.querySelector('.searching__text-result');
        this.searchResult = document.querySelector('.search-result');
        this.pagination = document.querySelector('.p-search__pagination');

        this.resetBtn = this.block.querySelector('.search__btn_reset');

        this.init();
    }

    init() {
        $(document).on('click', '.pagination__marker a', this.result.bind(this));
        this.resetBtn.addEventListener('click', this.formReset);
    }

    result(e) {
        e.preventDefault();
        let target = e.target;
        let link = target.getAttribute('href');
        const searchForm = $('form.searching__form');

        if (!link) {
            return;
        }
        $.ajax({
            dataType: 'html',
            data: searchForm.serialize(),
            method: 'POST',
            url: link,
            success: (data) => {
                if ($(data).length < 5) {
                    return;
                }
                let textResult = $(data)[0].innerHTML;
                let searchResult = $(data)[2].innerHTML;
                let pagination = $(data)[4].innerHTML;


                this.textResult.innerHTML = textResult;
                this.searchResult.innerHTML = searchResult;
                this.pagination.innerHTML = pagination;
            }
        });
    }

    formReset(e) {
        let target = e.target;
        const form = target.closest('.searching__form');
        let input = form.querySelector('.search__input');
        input.setAttribute('value', '');
    }

});

