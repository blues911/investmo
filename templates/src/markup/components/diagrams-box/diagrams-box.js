import Component from 'helpers-js/Component';

export default Component.create('diagrams-box', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    init() {
        this.diagrams = [].slice.call(this.block.querySelectorAll('.diagrams-box__diagram'));

        this.setSize();
    }

    setSize() {
        this.diagrams.forEach(diagram => {
            if (document.body.offsetWidth > 750) {
                diagram.style.height = `${+diagram.getAttribute('data-height') / 16}rem`;
            } else {
                diagram.style.height = `${(+diagram.getAttribute('data-height') * 1.87) / 16}rem`;
            }
        });
    }

});


