import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js/for-each';

export default Component.create('popup-base', class {
    constructor($block) {
        this.block = $block[0];

        this.subtitles = [].slice.call(this.block.querySelectorAll('.popup-base__label'));
        this.minifyBtn = this.block.querySelectorAll('.popup-base__minify');
        this.closeBtn = this.block.querySelectorAll('.popup-base__close');
        this.init();
    }

    init() {
        this.subtitlesClick();
        this.hideBtnClick();
        this.hideFrom();
    }

    subtitlesClick() {

        forEach(this.subtitles, (subtitle) => {
            subtitle.addEventListener('click', function () {
                subtitle.closest('.popup-base__box').classList.toggle('popup-base__box_inner_visible');
            });
        });
    }

    hideBtnClick() {
        forEach( this.minifyBtn, (btn) => {
            btn.addEventListener('click', function () {
                btn.closest('.popup-base').classList.toggle('popup-base_content-hidden');
            });
        });
    }

    hideFrom() {

        forEach(this.closeBtn, (btn) => {
            btn.addEventListener('click', function () {
                btn.closest('.popup-base').classList.toggle('popup-base_visible');
            });
        });
    }
});

