import 'select2';

document.addEventListener('DOMNodeInserted', function (event) {
    const element = event.target;
    $(element).find('.support-form__select').select2({
        minimumResultsForSearch: -1,
        width: 'auto',
        dropdownAutoWidth: true,
        theme: 'support'
    });
});

$('.support-form__select').select2({
    minimumResultsForSearch: -1,
    width: 'auto',
    dropdownAutoWidth: true,
    theme: 'support'
});
