import 'select2';

if ($('.js-custom-select').length) {
    $('.js-custom-select').select2({
        theme: 'custom-select',
        minimumResultsForSearch: Infinity
    });
}
