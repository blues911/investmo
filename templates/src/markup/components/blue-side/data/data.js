var data = {blueSide: {
    docs: {
        title: 'Необходимые <b> документы</b>',
        items: [
            {
                icon: 'business-plan',
                text: 'Бизнес-план и финансовая модель'
            },
            {
                icon: 'document-founder',
                text: 'Учредительные документы <span class="span-inline">организации-инициатора</span> проекта'
            },
            {
                class: 'blue-side__item-end',
                icon: 'finance-grow',
                text: 'Документы, подтверждающие положительную финансово-экономическую деятельность инициатора'
            }
        ]
    }
}};

