import Component from 'helpers-js/Component';

export default Component.create('search-box', class {
    constructor($block) {
        this.block = $block[0];
        this.searchBtn = this.block.querySelector('.search-box__btn');
        this.init();
    }

    init() {

        this.searchBtn.addEventListener('click', this.redirectUrl);
    }

    redirectUrl() {
        window.location = '/search';
    }

});


