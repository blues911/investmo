import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('info-box', class {

    constructor($block) {
        this.block = $block[0];

        if (this.block) {
            this.init();
        }
    }

    init() {
        this.subNav = this.block.querySelector('.info-box__subnav');
        this.subLink = this.subNav.querySelectorAll('.info-box__subnav-link');
        this.header = document.querySelector('.header');

        this.fixedSubmenu();
        this.setInfoPadding();
        window.addEventListener('scroll', this.fixedSubmenu.bind(this));
        window.addEventListener('resize', this.setInfoPadding.bind(this));
        forEach(this.subLink, (link) => {
            link.addEventListener('click', this.scrollToHash.bind(this)); // при переходе по якоре делаем отступ, чтобы шапка не крыла заголовок
        });
    }

    scrollToHash(e) {
        let target = e.target;
        let hash = target.getAttribute('href');

        $('html, body')
            .stop()
            .animate({
                scrollTop: $(hash).offset().top - ( this.subNav.offsetHeight + 100 )
            }, 0);
        e.preventDefault();
    }

    setInfoPadding() {

        if (this.subNav) {
            let subNavHeight = this.subNav.offsetHeight;
            let boxStyles = getComputedStyle(this.block);
            let padding = boxStyles.paddingTop;
            let ind = padding.indexOf('px');
            let number = padding.substring(0, ind);

            if (subNavHeight > number) {
                this.block.style.paddingTop = subNavHeight + 'px';
            }
        }
    }

    fixedSubmenu() {

        let scrollTop = window.pageYOffset || document.documentElement.scrollTop;

        if (scrollTop > this.getBlockCoords()) {
            this.subNav.style.top = this.getHeaderHeight() + 'px';
            this.subNav.classList.add('info-box__subnav_scroll');

            setTimeout(() => {
                this.subNav.classList.add('info-box__subnav_position');
            }, 500);

        } else {
            this.subNav.style = '';
            this.subNav.classList.remove('info-box__subnav_scroll');
            setTimeout(() => {
                this.subNav.classList.remove('info-box__subnav_position');
            }, 500);

            forEach(this.subLink, (link) => {
                link.classList.remove('info-box__subnav-link_active');
            });

        }
    }

    getHeaderHeight() {
        return this.header.offsetHeight;
    }

    getBlockCoords() {
        let coords = this.block.getBoundingClientRect();

        return coords.top;
    }

});
