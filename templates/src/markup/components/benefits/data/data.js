var data = {benefits: {
    support: {
        items: [
            {
                badge: 'Стратегический',
                rate: '0<span class="benefits__percent">%</span><span class="benefits__dash">-</span>1,5<span class="benefits__percent">%</span>',
                years: 'на 8 лет',
                sum: 'от 500 млн руб. до 5 лет',
                low: 'Снижение ставки по налогу на прибыль на 4,5% на 7 лет'
            },
            {
                badge: 'Приоритетный',
                badgeClass: 'benefits__badge-dark',
                rate: '0<span class="benefits__percent">%</span><span class="benefits__dash">-</span>1,5<span class="benefits__percent">%</span>',
                years: 'на 5 лет',
                sum: '50 млн руб. – 500 млн руб. до 3 лет',
                low: 'Снижение ставки по налогу на прибыль на 4,5% на 5 лет'
            },
            {
                badge: 'Для малых предприятий',
                badgeClass: 'benefits__badge-dark',
                rate: '0<span class="benefits__percent">%</span><span class="benefits__dash">-</span>1,5<span class="benefits__percent">%</span>',
                years: 'на 5 лет',
                sum: 'от 50 млн руб. до 3 лет',
                low: 'Снижение ставки по налогу на прибыль на 4,5% на 7 лет'
            }
        ]
    }
}};
