var data = {object_list: {
    cabinet: [
        {
            image: '%=static=%img/content/cabinet/image-lc.png',
            name: 'Земельный участок',
            address: 'Центральный АО, Мещанский',
            location: 'ПС 220/10/6 кВ Центральная',
            lease: true,
            area: '4 га',
            road: '2 км'
        },
        {
            image: '%=static=%img/content/cabinet/image-lc2.png',
            name: 'Земельный участок',
            address: 'Западный АО, Можайский',
            location: 'ПС 110/10/6 кВ Сетунь',
            lease: true,
            area: '2 га',
            road: '2 км'
        },
        {
            image: '%=static=%img/content/cabinet/image-lc3.png',
            name: 'Земельный участок',
            address: 'Центральный АО, Мещанский',
            location: 'ПС 220/10/6 кВ Центральная',
            lease: true,
            area: '2 га',
            road: '2 км'
        }
    ]
}}
