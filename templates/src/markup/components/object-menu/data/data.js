var data = {object_menu: {
    cabinet: [
        {
            name: 'Ваши объекты',
            isButton: false,
            isLink: true
        },
        {
            name: 'Просмотры ваших объектов',
            isButton: false,
            isLink: true
        },
        {
            name: 'Добавить проект',
            isButton: true,
            buttonClass: '_add btn _theme_standart',
            isLink: true
        }
    ],
    addObject: [
        {
            name: 'Заявка на добавление объекта',
            isButton: false,
            isLink: false
        },
        {
            name: 'Отмена',
            isButton: true,
            buttonClass: '_delete btn _theme_light',
            isLink: true
        }
    ],
    prоfile: [
        {
            name: 'Профиль',
            isButton: false,
            isLink: false
        },
        {

        }
    ]
}}
