import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';
import 'jquery-validation';

export default Component.create('object-form', class {
    constructor($block) {
        this.block = $block[0];
        this.form = this.block.querySelector('.object-form__form');
        this.select = document.querySelectorAll('.object-form__select');
        this.files = document.getElementById('photos');
        this.prevBtn = document.querySelectorAll('.object-form__step-back');
        this.nextBtn = document.querySelectorAll('.object-form__continue');
        this.steps = document.querySelectorAll('.object-form__step-box');
        this.photoBlock = document.querySelector('.object-form__photo-preview');
        this.formData = [];
        this.output = [];

        this.ie = window.navigator.userAgent.indexOf('MSIE') > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
        this.edge = navigator.userAgent.indexOf('Edge') !== -1;

        this.phonesInput = document.querySelectorAll('.object-form__input_phone');
        this.selects = document.querySelectorAll('.object-form__select ');

        this.numberInput = document.querySelectorAll('.object-form__required-number');

        this.popup = document.querySelector('.popup-success');
        this.body = document.body;
        this.titlePopup = this.popup.querySelector('.popup-success__title');
        this.subtitlePopup = this.popup.querySelector('.popup-success__sub-title');

        this.menuLinks = document.querySelectorAll('.object-menu__link');
        this.popupSaveBtn = document.querySelector('.popup-success__button_save');

        this.deletePhoto = this.block.querySelectorAll('.object-form__delete-photo_edit');
        this.delete = [];

        this.init();
    }

    init() {
        this.customSelect();
        this.clickPrevBtn(); // предыдущий шаг
        this.clickNextBtn(); // следующий шаг
        this.initMasks();
        this.validateInputNumber(); // Валидация числовых полей
        this.files.addEventListener('change', this.handleFileSelect.bind(this), false); // Загрузка фото объекта
        $(document).on('click', 'a[id^="delete"]', this.removeFilePreview.bind(this)); // Удаление загруженного файла

        forEach(this.menuLinks, (link) => {
            link.addEventListener('click', this.closeEditForm.bind(this)); // Редактирование объекта
        });

        this.popupSaveBtn.addEventListener('click', this.popupSave.bind(this)); // Клик по кнопке сохранить в попапе

        if (this.deletePhoto) {

            forEach(this.deletePhoto, (photo) => {
                photo.addEventListener('click', this.editObjectDeletePhoto.bind(this)); // При редактировании обработчик удаления фото
            });
        }
    }

    editObjectDeletePhoto(e) {
        let target = e.target;
        const id = target.getAttribute('data-id');

        let inStock = this.delete.indexOf(id);

        if (inStock === -1) {
            this.delete.push(id);
        }

        let parent = target.closest('.object-form__wrap-photo');
        parent.classList.add('object-form__wrap-photo_hidden');
    }

    initMasks() {

        forEach(this.phonesInput, (input) => {
            $(input).mask('+7 (999) 999-99-99');
        });
    }

    customSelect() {

        forEach(this.select, (select) => {
            $(select).select2({
                placeholder: $(select).attr('placeholder'),
                minimumResultsForSearch: Infinity
            });
        });

    }

    clickPrevBtn() {

        forEach(this.prevBtn, (prev) => {
            prev.addEventListener('click', this.prevStep.bind(this));
        });
    }

    clickNextBtn() {
        forEach(this.nextBtn, (next) => {
            next.addEventListener('click', this.nextStep.bind(this));
        });
    }

    prevStep(e) {
        e.preventDefault();
        let target = e.currentTarget;
        let step = target.getAttribute('data-prev');

        forEach( this.steps, (item ) => {

            let number = item.getAttribute('data-number');

            item.classList.remove('object-form__step-box_visible');

            if (step === number) {
                item.classList.add('object-form__step-box_visible');
            }
        });
    }

    showError(container, newtext) {
        const errorText = document.createElement('span');
        errorText.className = 'object-form__error-text';
        errorText.innerHTML = 'Обязательное поле';

        if (newtext) {
            errorText.innerHTML = newtext;
        }
        container.appendChild(errorText);
    }

    hideError(container) {
        if (container.lastChild.className === 'object-form__error-text') {
            container.removeChild(container.lastChild);
        }
    }

    getchar(event) {

        if (event.which == null) {
            if (event.keyCode < 32) {
                return null;
            }
            return String.fromCharCode(event.keyCode); // IE
        }

        if (event.which !== 0 && event.charCode !== 0) {
            if (event.which < 32) {
                return null;
            }
            return String.fromCharCode(event.which); // остальные
        }

        return null; // специальная клавиша
    }

    validateInputNumber() {
        // обработка полей только с номером
        forEach(this.numberInput, (input) => {
            input.onpaste = (e) => {
                return false;
            };
            input.onkeypress = (e) => {
                e = e || event;

                if (e.ctrlKey || e.altKey || e.metaKey) {
                    return;
                }

                let chr = this.getchar(e);
                if (chr === null) {
                    return;
                }

                if (!(chr === '0' || chr === '1' || chr === '2' || chr === '3' || chr === '4' || chr === '5' || chr === '6' || chr === '7' || chr === '8' || chr === '9' || chr === '.' || chr === ',')) {
                    return false;
                } else {
                    // Проверка поля на дробность
                    input.onkeyup = (event) => {
                        input.value = input.value.replace(/[^\d.|,]*/g, '').replace(/([.|,])[.|,]+/g, '$1').replace(/^[^\d]*(\d+([.|,]\d{0,5})?).*$/g, '$1');
                    };
                }
            };
        });
    }

    validateStep(element) {

        // Обработка ошибок
        let parent = element.closest('.object-form__data-wrapper');

        const submitBtn = parent.querySelector('.object-form__continue');
        const inputs = parent.querySelectorAll('.object-form__required-input');
        const selects = parent.querySelectorAll('.object-form__required-select');

        if (selects.length > 0) {

            forEach(selects, (select) => {

                let prnt = select.closest('.object-form__col');

                if (select.value === '') {
                    prnt.classList.add('object-form__error');
                    this.hideError(prnt);
                    this.showError(prnt);
                } else {
                    prnt.classList.remove('object-form__error');
                    this.hideError(prnt);
                }
            });
        }

        if (inputs.length > 0) {

            forEach(inputs, (input) => {

                let prnt = input.closest('.object-form__col');

                if (!input.value.replace(/^\s+|\s+$/g, '')) {
                    prnt.classList.add('object-form__error');
                    this.hideError(prnt);
                    this.showError(prnt);
                } else {
                    prnt.classList.remove('object-form__error');
                    this.hideError(prnt);
                }
            });
        }

        let errors = parent.querySelectorAll('.object-form__error');

        return errors.length;
    }

    openStep(obj) {
        if (this.validateStep(obj.target) === 0) {

            forEach(this.steps, (item) => {

                let number = +item.getAttribute('data-number');

                item.classList.remove('object-form__step-box_visible');

                if (obj.step === number) {
                    item.classList.add('object-form__step-box_visible');
                }
            });
        }
    }

    nextStep(e) {
        e.preventDefault();
        let target = e.currentTarget;
        let tagname = target.tagName;

        if (tagname === 'A') {
            // Открытие следующего шага

            let step = +target.getAttribute('data-next');

            let obj = {
                target: target,
                step: step
            };

            switch (step) {
                case 3:
                    // Проверка поля кадастра на существование
                    this.checkInventory('/cabinet/validate-object/')
                        .then(JSON.parse)
                        .then((response) => {

                            const parent = target.closest('.object-form__data-wrapper');
                            const inventory = parent.querySelector('input[name="Object[cadastral_number]"]');
                            const prnt = inventory.closest('.object-form__col');

                            if (response === 1) {
                                prnt.classList.remove('object-form__error');
                                this.hideError(prnt);
                            } else {
                                prnt.classList.add('object-form__error');
                                this.hideError(prnt);
                                this.showError(prnt, response.cadastral_number);
                            }
                            return response;
                        })
                        .then((response) => {

                            if (response === 1) {
                                this.openStep(obj);
                            }
                        });
                    break;
                default:
                    this.openStep(obj);
                    break;
            }

        } else {
            // Обработка отправки формы
            if (this.validateStep(target) === 0) {
                this.submitForm();
            }
        }
    }

    checkInventory(url) {

        let params = url;

        if (this.objectId !== 0) {
            params = `${url}?id=${this.objectId}`;
        }

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'POST',
                url: params,
                data: $(this.form).serialize(),
                success: (response) => {
                    resolve(response);
                }
            });
        });
    }

    submitForm(text) {

        let activeStep = this.form.querySelector('.object-form__step-box_visible');
        let activeNumber = +activeStep.getAttribute('data-number');

        if (activeNumber === 3) {
            let form = new FormData(this.form);

            // Редактирование объекта, удалённые фото
            if (this.delete.length > 0) {

                for (let i = 0; i < this.delete.length; i++) {
                    form.append('deletePhotos[]', this.delete[i]);
                }
            }

            for (let i = 0; i < this.formData.length; i++) {
                form.append('photos[]', this.formData[i]);
            }

            const url = $(this.form).attr('action');
            let error = [];

            $.ajax({
                type: 'POST',
                url: url,
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: (data) => {
                    $('body').prepend('<div class="preloader preloader_spinner"><span class="preloader__text">Ваш запрос обрабатывается...</span></div>');
                },
                success: (response) => {

                    response = $.parseJSON(response);

                    if (response.status !== 'success') {
                        return;
                    }

                    $('body').find('.preloader').remove();

                    this.popup.classList.add('popup-success_visible');
                    this.popup.classList.remove('popup-success_edit-object');
                    this.body.classList.add('open-popup');

                    if (text) {
                        this.titlePopup.innerHTML = text.title;
                        this.subtitlePopup.innerHTML = text.subtitle;
                    }

                    setTimeout(() => {
                        window.location = '/cabinet';
                        return;
                    }, 2000);

                }
            });
        }

    }

    handleFileSelect(e) {

        let files = e.target.files;


        for (let i = 0; i < files.length; i++) {

            let f = files[i];

            if (!f.type.match('image.*')) {
                continue;
            }

            let reader = new FileReader();
            this.formData.push(f);
            reader.onload = ((theFile) => {

                return (event) => {
                    this.output.push(event.target.result);
                    this.addFilePreview(); // Первью фоток объекта
                };
            })(f);
            reader.readAsDataURL(f);
        }
    }

    addFilePreview() {
        this.photoBlock.innerHTML = '';
        for (let i = 0; i < this.output.length; i++) {
            $(this.photoBlock).append(`<div class="object-form__wrap-photo"><div class="object-form__photo-item"><img class="object-form__thumb" src="${this.output[i]}" alt=""> <a class="object-form__delete-photo" href="javascript:void(0)" id="delete-${i}"></a></div>`);
        }
    }

    removeFilePreview(e) {
        e.preventDefault();
        let target = e.currentTarget;

        let id = target.getAttribute('id');
        let temp = [];
        temp = id.split('-');
        this.output.splice(temp[1], 1); // При удалении файлов также очищаем их количество, оправляемое на сервер
        this.formData.splice(temp[1], 1);
        this.photoBlock.innerHTML = '';
        this.addFilePreview(); // После удаления обновляем список файлов
    }

    closeEditForm(e) {
        const target = e.currentTarget;
        const id = target.getAttribute('data-id');
        const save = target.getAttribute('data-save');

        if (id) {
            e.preventDefault();
            const url = target.getAttribute('href');
            const form = new FormData($(this.form).get(0));

            // Редактирование объекта, удалённые фото
            if (this.delete.length > 0) {

                for (let i = 0; i < this.delete.length; i++) {
                    form.append('deletePhotos[]', this.delete[i]);
                }
            }

            if (this.formData.length > 0) {
                for (let i = 0; i < this.formData.length; i++) {
                    form.append('photos[]', this.formData[i]);
                }
            }

            $.ajax({
                type: 'POST',
                url: url + id,
                data: form,
                cache: false,
                contentType: false,
                processData: false,
                success: (response) => {
                    response = $.parseJSON(response);

                    if (response.success) {
                        window.location = '/cabinet';
                        return;
                    }

                    this.popup.classList.add('popup-success_visible');
                    this.popup.classList.add('popup-success_edit-object');

                    this.titlePopup.innerHTML = 'Сохранить изменения?';
                    this.subtitlePopup.innerHTML = 'Некоторые изменения не были сохранены.';
                    this.body.classList.add('open-popup');
                }
            });
        }

        if (save) {

            let text = {
                title: 'Объект отредактирован',
                subtitle: 'Изменения в описании объекта сохранены'
            };

            this.submitForm(text);
        }
    }

    popupSave() {

        let text = {
            title: 'Объект отредактирован',
            subtitle: 'Изменения в описании объекта сохранены'
        };

        this.submitForm(text);
    }

});
