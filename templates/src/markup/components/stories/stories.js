import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('stories', class {
    constructor($block) {
        this.block = $block[0];

        this.arrows = this.block.querySelectorAll('.stories__button');
        this.storiesItem = this.block.querySelectorAll('.stories__item');
        this.storiesControls = this.block.querySelectorAll('.stories__control:not(.stories__control_theme_link)');
        this.active = [];

        this.init();
    }

    init() {
        const storiesCount = $('.stories__item').length;
        const storiesCurrent = $('.stories__control_active').index() + 1;

        function formattedNumber(myNumber) {
            return ('0' + myNumber).slice(-2);
        }

        $('.stories__counter-item_total').text(storiesCount);
        $('.stories__counter-item_current').text(formattedNumber(storiesCurrent));

        $('.stories__control').not('.stories__control_theme_link').on('click', function () {
            const $this = $(this); // eslint-disable-line no-invalid-this
            const index = $this.index();

            $('.stories__control').removeClass('stories__control_active');
            $this.addClass('stories__control_active');
            $('.stories__counter-item_current').text(formattedNumber(index + 1));

            $('.stories__item').removeClass('stories__item_state_active');
            $('.stories__item').eq(index).addClass('stories__item_state_active');
        });

        const initialActiveCount = 5;
        const storiesItemsAll = [].slice.call(document.querySelectorAll('.stories__control'));
        const storiesItems = [];
        let storiesMoreBtn;

        storiesItemsAll.forEach(storiesItem => {
            if (!storiesItem.classList.contains('stories__control_theme_link')) {
                storiesItems.push(storiesItem);
            } else {
                storiesMoreBtn = storiesItem;
            }
        });

        const storiesMoreBtnText = storiesMoreBtn.querySelector('.stories__text');

        const showItems = () => {
            storiesItems.forEach((storiesItem, n) => {
                if (n < (initialActiveCount - 1)) {
                    storiesItem.classList.add('visible');
                } else {
                    storiesItem.classList.remove('visible');
                }
            });
        };

        const showAllItems = () => {
            storiesItems.forEach((storiesItem, n) => {
                storiesItem.classList.add('visible');
            });
        };

        storiesMoreBtn.addEventListener('click', (e) => {
            e.preventDefault();

            if (!storiesMoreBtn.classList.contains('show-all')) {
                const currentText = storiesMoreBtnText.innerHTML;
                const openText = storiesMoreBtnText.getAttribute('data-open');

                storiesMoreBtn.classList.add('show-all');
                storiesMoreBtnText.setAttribute('data-close', currentText);
                storiesMoreBtnText.innerText = openText;
                showAllItems();

            } else {
                const closeText = storiesMoreBtnText.getAttribute('data-close');

                storiesMoreBtn.classList.remove('show-all');
                storiesMoreBtnText.innerText = closeText;
                showItems();
            }
        });

        if (storiesItems.length <= initialActiveCount) {
            storiesItems.forEach(storiesItem => {
                storiesItem.classList.add('visible');
            });
        } else {
            showItems();
            storiesMoreBtn.classList.add('visible');
        }

        forEach(this.arrows, (arrow) => {
            arrow.addEventListener('click', this.arrowsClick.bind(this)); // Клик по стрелочкам на слепой версии
        });

    }

    arrowsClick(e) {
        e.preventDefault();

        this.active = [];

        let direction = e.target.getAttribute('data-direction');
        let controlsCount = this.storiesControls.length;
        let activeControls = 0;
        // Добавляем в массив индекс текущего активного и удаляем активный класс у всех
        forEach(this.storiesControls, (control, ind) => {

            if (control.classList.contains('visible')) {
                activeControls++;
            }

            if (control.classList.contains('stories__control_active')) {
                this.active.push(ind);
            }
            control.classList.remove('stories__control_active');
        });

        switch (direction) {
            case 'prev':
                if (this.active[0] > 0) {

                    const stepControl = this.active[0] - 1;

                    this.addActiveControl(stepControl);
                    this.addActiveContentControl(stepControl);

                } else {

                    const stepStop = 0;

                    this.stopScrollingControl(stepStop);
                }
                break;
            case 'next':
                // Сравниваем общую длину и количество видимых, чтобы остановиться
                if (controlsCount > activeControls) {

                    if ( (this.active[0] + 2) <= activeControls) {

                        const stepControl = this.active[0] + 1;

                        this.addActiveControl(stepControl);
                        this.addActiveContentControl(stepControl);

                    } else {

                        const stepStop = activeControls;
                        this.stopScrollingControl(stepStop);

                    }
                } else {

                    if ( (this.active[0] + 2) <= controlsCount) {

                        const stepControl = this.active[0] + 1;

                        this.addActiveControl(stepControl);
                        this.addActiveContentControl(stepControl);

                    } else {

                        const stepStop = controlsCount - 1;
                        this.stopScrollingControl(stepStop);

                    }
                }

                break;
            default:
                break;
        }
    }

    addActiveControl(step) {

        // Добавляем активный класс контролу
        forEach(this.storiesControls, (control, ind) => {

            if (step === ind) {
                control.classList.add('stories__control_active');
            }
        });
    }

    addActiveContentControl(step) {

        // добавляем активный класс контенту контрола
        forEach(this.storiesItem, (story, ind) => {

            story.classList.remove('stories__item_state_active');

            if (step === ind) {
                story.classList.add('stories__item_state_active');
            }
        });
    }

    stopScrollingControl(step) {

        // если левее нет ничего активный класс у первого/последнего
        forEach(this.storiesControls, (control, ind) => {

            if (step === ind) {
                control.classList.add('stories__control_active');
            }
        });
    }

});
