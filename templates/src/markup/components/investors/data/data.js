var data = {investors: {
    default: [
        {
            flag: 'germany.png',
            target: 'germany',
            country: 'Германия'
        },
        {
            flag: 'netherlands.png',
            target: 'netherlands',
            country: 'Нидерланды'
        },
        {
            flag: 'cyprus.png',
            target: 'cyprus',
            country: 'Кипр'
        },
        {
            flag: 'usa.png',
            target: 'usa',
            country: 'США'
        },
        {
            flag: 'rok.png',
            target: 'rok',
            country: 'Республика Корея'
        },
        {
            flag: 'ireland.png',
            target: 'ireland',
            country: 'Ирландия'
        },
        {
            flag: 'uk.png',
            target: 'uk',
            country: 'Великобритания'
        }
    ],
    tooltips: [
        {
            flag: 'germany.png',
            country: 'germany',
            title: 'Германия',
            percent: '9%'
        },
        {
            flag: 'germany.png',
            country: 'netherlands',
            title: 'Нидерланды',
            percent: '9%'
        },
        {
            flag: 'rok-big.png',
            country: 'cyprus',
            title: 'Кипр',
            percent: '9%'
        },
        {
            flag: 'usa.png',
            country: 'usa',
            title: 'США',
            percent: '9%'
        },
        {
            flag: 'rok-big.png',
            country: 'rok',
            title: 'Республика корея',
            percent: '9%'
        },
        {
            flag: 'ireland.png',
            country: 'ireland',
            title: 'Ирландия',
            percent: '9%'
        },
        {
            flag: 'uk.png',
            country: 'uk',
            title: 'Великобритания',
            percent: '9%'
        }
    ]
}}
