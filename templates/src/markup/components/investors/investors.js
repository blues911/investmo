let $target;

$('.investors__link').hover(function () {
    $target = $(this).data('target');

    $(`[data-country="${$target}"]`).show();
}, function () {
    $(`[data-country="${$target}"]`).hide();
});
