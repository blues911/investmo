import Component from 'helpers-js/Component';

export default Component.create('popup-success', class {
    constructor($block) {
        this.block = $block[0];
        this.closeBtn = this.block.querySelector('.popup-success__close');
        this.body = document.body;

        this.init();
    }

    init() {
        this.closeBtn.addEventListener('click', this.closePopup.bind(this));
    }

    closePopup(e) {
        let target = e.currentTarget;
        const parent = target.closest('.popup-success');
        parent.classList.remove('popup-success_visible');
        this.body.classList.remove('open-popup');
    }
});

