var data = {descriptionInfo: {
    oez: {
        items: [
            {
                colL: {
                    mainClass: 'description-info__col-icon',
                    icon: 'house',
                    name: 'на имущество',
                    years: 'на 10 лет'
                },
                colR: {
                    mainClass: 'description-info__col-icon description-info__col-icon-sm',
                    icon: 'yard',
                    type: 'icon',
                    name: 'На землю',
                    years: 'на 5 лет'
                }

            },
            {
                colL: {
                    mainClass: 'description-info__col-icon',
                    iconPng: 'icon-upper',
                    name: 'на прибыль организаций',
                    years: 'на 8 лет'
                },
                colR: {
                    mainClass: 'description-info__col-text',
                    text: 'Начиная с первого числа квартала, следующего за датой признания их резидентами особой экономической зоны',
                    subcontent: {
                        rate1: '5',
                        years1: 'после 9 до 13 лет',
                        rate2: '13,5',
                        years2: 'после 14 лет'
                    }
                }
            },
            {
                colL: {
                    mainClass: 'description-info__col-icon',
                    iconPng: 'icon-bus',
                    name: 'Транспортный налог',
                    years: 'на 5 лет'
                },
                colR: {
                    mainClass: 'description-info__col-text',
                    text: 'Кроме автомобилей легковых, водных и воздушных транспортных средств.'
                }
            },
            {
                colL: {
                    mainClass: 'description-info__col-icon',
                    icon: 'customofficer',
                    name: 'Таможенные пошлины',
                    years: 'на 5 лет'
                },
                colR: {
                    mainClass: 'description-info__col-text',
                    text: 'При ввозе оборудования, сырья и компонентов'
                }
            }
        ]
    }

}};
