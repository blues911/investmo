var data = {pMap: {
    mapResults: {
        land: {
            count: '109',
            items: [
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">3 га</span> Кудиновское',
                    owner: 'Moesk',
                    address: 'Ногинский, п.Кудиновское',
                    phone: '+7 946 128 92 12'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">34 га</span> Тимохово',
                    owner: 'ООО «НацСталь»',
                    address: 'Ногинский, д.Тимохово',
                    phone: '+7 946 922 15 11'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">12 га</span> Колонтаево',
                    owner: 'ООО «НацСталь»',
                    address: 'Ногинский, д.Колонтаево',
                    phone: '+7 946 555 74 67'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">21 га</span> в п. Ивушка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Ивушка',
                    phone: '+7 946 091 00 33'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
                {
                    name: 'Участок&nbsp;<span class="map-results__size">49 га</span> Алексеевка',
                    owner: 'Moesk',
                    address: 'Щелковский, п.Алексеевка',
                    phone: '+7 949 433 44 97'
                },
            ]
        }
    },

    filters: {
        land: {
            name: 'land'
        }
    }
}};
