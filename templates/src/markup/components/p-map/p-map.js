import Component from 'helpers-js/Component';

export default Component.create('p-map', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    init() {
        this.header = document.querySelector('.header');
        this.headerContainer = this.header.querySelector('.header__container');

        this.filterOpenBtn = this.block.querySelector('.map-controls__btn_filter');
        this.filterCloseBtn = this.block.querySelector('.map-filter__btn_close');
        this.filterCoworking = this.block.querySelector('.map-nav__item[data-id="8"]');
        this.filterItem = document.getElementsByClassName('map-nav__item');
        this.searchBtn = this.block.querySelector('.map-controls__btn_search');
        this.showBtn = this.block.querySelector('.map-controls__btn_show-list');
        // this.filterShowBtn = this.block.querySelector('.map-filter__show-btn');

        this.resultsCloseBtn = this.block.querySelector('.map-results__close');

        const close = () => {
            this.block.classList.remove('p-map_filter_visible');
            this.block.classList.remove('p-map_search_visible');
        };

        this.filterOpenBtn.addEventListener('click', () => {
            this.block.classList.add('p-map_filter_visible');
        });

        this.filterCloseBtn.addEventListener('click', () => {
            close();
        });

        this.showBtn.addEventListener('click', () => {
            this.block.classList.add('p-map_results_visible');
        });

        this.resultsCloseBtn.addEventListener('click', () => {
            this.block.classList.remove('p-map_results_visible');
        });


        document.addEventListener('click', this.toggleSearch.bind(this)); // показываем/прячем поле поиска по имени

        // Выставляем отступ по высоте шапке
        this.setIndent();
        window.addEventListener('resize', this.setIndent.bind(this));
    }

    setIndent() {
        setTimeout( () => {
            let headerHeight = this.header.offsetHeight;
            this.block.style.paddingTop = headerHeight + 'px';
        }, 100);
    }

    toggleSearch(e) {
        let target = e.target;

        let hasClassSearch = target.classList.contains('map-controls__btn_search');
        let thisInputSearchValue = document.querySelector('.map-filter__search-input');
        let thisInputSearch = target.classList.contains('map-filter__search-input');

        if (hasClassSearch) {
            this.block.classList.toggle('p-map_search_visible');
            this.block.classList.add('p-map_filter_visible');
            thisInputSearchValue.focus();
            thisInputSearchValue.setAttribute('name', 'ObjectMapSearch[name]');
        } else if (thisInputSearch) {
            this.block.classList.add('p-map_search_visible');
        } else if (thisInputSearchValue.value === '' || thisInputSearchValue.value === 'undefined') {
            this.block.classList.remove('p-map_search_visible');
        }
    }

});


