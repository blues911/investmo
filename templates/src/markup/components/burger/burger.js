$('.burger').on('click', function () {
    const $this = $(this); // eslint-disable-line no-invalid-this
    $this.toggleClass('burger_open');
    $('.header').toggleClass('menu-open');
    $('body').toggleClass('menu-open');
});

window.onresize = e => {
    if (window.outerWidth > 750) {
        $('.burger').removeClass('burger_open');
        $('.header').removeClass('menu-open');
        $('body').removeClass('menu-open');
    }
};
