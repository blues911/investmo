import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('header', class {
    constructor($block) {
        this.block = $block[0];

        this.select = $('.header__select select');
        this.content = document.querySelector('.p-content');
        this.$mainBlock = $('html');
        this.paletteLink = this.block.querySelectorAll('.cecutient-header__palette__item');
        this.fontLink = this.block.querySelectorAll('.cecutient-header__font__item');
        this.init();
    }

    classChange() {
        const distanceY = $(window).scrollTop();
        const shrinkOn = 0;
        const header = $('.header');

        if (distanceY > shrinkOn) {
            if (header.hasClass('header_theme_simple')) {
                header.removeClass('header_theme_simple').addClass('header_theme_fixed');
                if (this.content) {
                    let $height = header.innerHeight();
                    this.content.style.marginTop = ( $height + 28 ) + 'px';
                }
            }
        } else {

            if (/header_theme_simple/i.test(header.attr('data-class'))) {
                header.addClass('header_theme_simple');
            }

            header.removeClass('header_theme_fixed');
            if (this.content) {
                this.content.style.marginTop = '';
            }
        }
    }

    customSelect() {
        this.select.select2({
            width: 'auto',
            minimumResultsForSearch: Infinity,
            theme: 'lang',
            //dropdownParent: $('.header__select') не с первого раза прогружалось при переключении на слабовидящую версию
        });
    }

    gotToVersion() {
        const url = $(this).attr('data-url');
        const currentLang = $(this).attr('data-current-lang');
        const newLang = $(this).val();
        if (currentLang === newLang) {
            return;
        }
        window.location = url + '/' + newLang;
    }

    clearPalette() {
        this.$mainBlock.removeClass('cecutient-mode_palette_light');
        this.$mainBlock.removeClass('cecutient-mode_palette_dark');
        this.$mainBlock.removeClass('cecutient-mode_palette_sky');
        this.$mainBlock.removeClass('cecutient-mode_palette_blue');
        this.$mainBlock.removeClass('cecutient-mode_palette_brown');
    }

    clearFont() {
        this.$mainBlock.removeClass('cecutient-mode_font_lg');
        this.$mainBlock.removeClass('cecutient-mode_font_xl');
        this.$mainBlock.removeClass('cecutient-mode_font_md');
    }

    cecutientMode() {
        this.$mainBlock.toggleClass('cecutient-mode');
        this.$mainBlock.addClass('cecutient-mode_palette_light');

        // Запоминаем слепую версию
        if (this.$mainBlock.hasClass('cecutient-mode')) {
            localStorage.setItem('cecutien-mode', true);
        } else {
            localStorage.removeItem('cecutien-mode');
            localStorage.removeItem('palette');
            localStorage.removeItem('font');
        }

    }

    cecutientFontSize(e) {
        $(e.currentTarget)
            .addClass('is-active')
            .siblings()
            .removeClass('is-active');
        this.clearFont();
        this.$mainBlock.addClass(`cecutient-mode_font_${e.currentTarget.getAttribute('data-size')}`);


        if (this.$mainBlock.hasClass(`cecutient-mode_font_${e.currentTarget.getAttribute('data-size')}`)) {
            localStorage.setItem('font', e.currentTarget.getAttribute('data-size'));
        } else {
            localStorage.removeItem('font ');
        }
    }

    cecutientSetPalette(e) {
        let target = e.currentTarget;
        $(target)
            .addClass('is-active')
            .siblings()
            .removeClass('is-active');
        this.clearPalette();
        this.$mainBlock.addClass(`cecutient-mode_palette_${e.currentTarget.getAttribute('data-palette')}`);

        if (this.$mainBlock.hasClass(`cecutient-mode_palette_${e.currentTarget.getAttribute('data-palette')}`)) {
            localStorage.setItem('palette', e.currentTarget.getAttribute('data-palette'));
        } else {
            localStorage.removeItem('palette');
        }
    }

    getLocalStoragePalette() {
        let palette = localStorage.getItem('palette');

        if (palette) {
            this.$mainBlock.addClass(`cecutient-mode_palette_${palette}`);
            forEach(this.paletteLink, (link) => {

                link.classList.remove('is-active');

                if (link.getAttribute('data-palette') === palette) {
                    link.classList.add('is-active');
                }
            });
        }
    }

    getLocalStorageFont() {
        let font = localStorage.getItem('font');

        if (font) {
            this.$mainBlock.addClass(`cecutient-mode_font_${font}`);
            forEach(this.fontLink, (link) => {

                link.classList.remove('is-active');

                if (link.getAttribute('data-size') === font) {
                    link.classList.add('is-active');
                }
            });
        }
    }

    getLocalStorageCecutientMode() {

        let cecutienOn = localStorage.getItem('cecutien-mode');
        let cecutientMode = localStorage.getItem('palette');

        if (cecutienOn) {
            this.$mainBlock.addClass('cecutient-mode');
        }

        if (cecutientMode) {
            this.$mainBlock.addClass(cecutientMode);
        }
    }

    init() {
        window.addEventListener('scroll', this.classChange.bind(this));
        this.classChange.bind(this);
        $(document).on('change', '.header__select select', this.gotToVersion);
        $('.header__control_theme_link').on('click', $.proxy(this.cecutientMode, this));
        $('.cecutient-header__font__item').on('click', $.proxy(this.cecutientFontSize, this));
        $('.cecutient-header__palette__item').on('click', $.proxy(this.cecutientSetPalette, this));

        $('.js-off-image').on('click', function () {
            $('html').toggleClass('img-off');
        });
        this.customSelect();
        this.getLocalStorageCecutientMode(); // если слепая версия включена, устанавливаем нужные классы
        this.getLocalStoragePalette(); // если в слепой версии выбран цвет, устанавливаем его
        this.getLocalStorageFont(); // если в слепой версии выбран размер шрифта, устанавливаем его
    }

});
