import Component from 'helpers-js/Component';

export default Component.create('map-nav', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    init() {
        this.buttons = [].slice.call(this.block.querySelectorAll('.map-nav__item'));
        this.buttonClick();

        const boxBtns = [].slice.call(document.querySelectorAll('.map-nav__box-btn'));

        boxBtns.forEach(btn => {
            btn.addEventListener('click', () => {
                btn.closest('.map-nav__box').classList.toggle('active');
            });
        });

        this.boxItemsActions();
    }

    boxItemsActions() {
        const allBoxItems = [].slice.call(document.querySelectorAll('.map-nav__box-item_contains_inner'));
        const arrows = [].slice.call(document.querySelectorAll('.map-nav__arrow'));

        const checkAll = (innerCheckboxes) => {
            let checked = false;

            innerCheckboxes.forEach(innerCheckbox => {
                if (innerCheckbox.checked) {
                    checked = true;
                }
            });

            return checked;
        };

        const iItems = [].slice.call(document.querySelectorAll('.map-nav__inner-items'));

        const hideInnerItems = () => {
            iItems.forEach(iItem => {
                iItem.classList.remove('active');
            });
        };

        allBoxItems.forEach(boxItem => {
            const mainCheckBox = boxItem.querySelector('.checkbox_main').querySelector('.checkbox__input');
            const innerCheckBoxContainers = [].slice.call(boxItem.querySelectorAll('.checkbox_inner'));
            const innerCheckBoxes = [];

            innerCheckBoxContainers.forEach(innerCheckBoxContainer => {
                innerCheckBoxes.push(innerCheckBoxContainer.querySelector('.checkbox__input'));
            });
/*
            mainCheckBox.addEventListener('change', () => {
                const currentInnerCheckBoxContainer = mainCheckBox.closest('.map-nav__box-item').querySelector('.map-nav__inner-items');
                if (!currentInnerCheckBoxContainer.classList.contains('active')) {
                    hideInnerItems();
                    currentInnerCheckBoxContainer.classList.add('active');
                }

                innerCheckBoxes.forEach(innerCheckBox => {
                    innerCheckBox.checked = mainCheckBox.checked;
                });
            });

            innerCheckBoxes.forEach(innerCheckBox => {
                innerCheckBox.addEventListener('change', () => {
                    if (innerCheckBox.checked) {
                        mainCheckBox.checked = true;
                    } else {
                        if (!checkAll(innerCheckBoxes)) {
                            mainCheckBox.checked = false;
                        }
                    }
                });
            });
*/

        });




        /* arrows.forEach(arrow => {
            arrow.addEventListener('click', () => {
                const innerItems = arrow.parentNode.querySelector('.map-nav__inner-items');

                if (innerItems) {
                    if (!innerItems.classList.contains('active')) {
                        hideInnerItems();
                    }
                    innerItems.classList.toggle('active');
                }
            });
        });*/
    }

    buttonClick() {
        const navBoxes = [].slice.call(this.block.querySelectorAll('.map-nav__box'));

        this.block.addEventListener('click', e => {
            const button = e.target.closest('.map-nav__item');

            if (button) {
                this.buttons.forEach(btn => {
                    if (btn === button) {
                        if (!btn.classList.contains('map-nav__item_active')) {
                            btn.classList.add('map-nav__item_active');
                        }

                        if (!button.classList.contains('map-nav__item-box')) {
                            // navBoxes.forEach(navBox => {
                            //     navBox.classList.remove('active');
                            // });
                        } else {
                            if (!e.target.closest('.map-nav__box-items')) {
                                button.querySelector('.map-nav__box').classList.toggle('active');
                            }
                        }

                    } else {
                        btn.classList.remove('map-nav__item_active');
                    }
                });
            }
        });

        document.querySelector('body').addEventListener('click', e => {
            const trueButton = e.target.closest('.map-nav__box');
            const trueButtonType2 = e.target.closest('.map-nav__item-box');

            navBoxes.forEach(navBox => {
                if ((navBox !== trueButton)) {
                    if ((navBox.parentNode !== trueButtonType2)) {
                        navBox.classList.remove('active');
                    }
                }
            });
        });
    }

});
