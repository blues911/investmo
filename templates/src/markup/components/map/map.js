import Component from 'helpers-js/Component';
import proschet from 'proschet';
import { forEach } from 'helpers-js';

export default Component.create('map', class {
    constructor($block) {
        this.block = $block[0];
        ymaps.ready(() => { // eslint-disable-line
            this.init();
        });
    }

    init() {
        this.cache = {};

        this.pMap = document.querySelector('.p-map');

        this.nav = document.querySelector('.map-nav');
        this.mapBox = document.querySelector('.map');
        this.results = document.querySelector('.map-results');

        this.section = this.nav.querySelector('.map-nav__item_active').getAttribute('data-id');
        this.resultsBox = this.results.querySelector('.map-results__box_results');
        this.resultTitle = this.results.querySelector('.map-results__title');
        this.resultMoreBtn = this.results.querySelector('.map-result__more');
        this.resultsItems = this.resultsBox.querySelector('.map-results__items');

        this.resultBox = this.results.querySelector('.map-results__box_result');

        this.keys = [];

        this.filterBox = this.mapBox.querySelector('.map-filter');
        this.filterForm = this.filterBox.querySelector('.map-filter__content');
        this.filterResetBtn = this.filterBox.querySelector('.map-filter__reset');
        this.filterCheckboxes = [].slice.call(this.filterBox.querySelectorAll('.checkbox__input'));
        this.filterInputs = [].slice.call(this.filterBox.querySelectorAll('.map-filter__input'));

        this.filterCount = this.filterBox.querySelector('.map-filter__count');
        this.cardDynamicContent = document.querySelector('.map-card__dynamic-content');

        this.popupBtn = document.querySelectorAll('.btn_open-popup');
        this.popup = this.block.querySelectorAll('.popup-base');

        const objectsText = ['объект', 'объекта', 'объектов'];
        this.getObjectsText = proschet(objectsText);

        this.cadastralInput = document.getElementsByName('ObjectMapSearch[cadastral_number]')[0];

        this.searchInput = document.querySelector('.map-filter__search-input');
        this.searchForm = document.querySelector('.map-filter__search-form');
        this.timer = '';
        this.timerInputsForms = '';

        this.addObjectMobileBtn = document.querySelector('.map-controls__btn_add');

        this.viewAllBtn = this.block.querySelector('.map-filter__view-all-link');

        this.checkboxWrapper = document.querySelector('.map-nav__box-items');
        this.checkBox = this.checkboxWrapper.querySelectorAll('.checkbox__input');

        this.createMap(); // создает карту
        this.sectionChange(); // клик по кнопке навигации
        this.filter(); // обработчик события изменения фильтра

        this.resultOpen(); // обработчик клика по списку результатов и по резидентам

        this.points(); // обработчик точек

        this.filterResetBtnClick();

        this.subSectionChange();

        forEach(this.popupBtn, (popup) => {
            popup.addEventListener('click', this.viewPopup.bind(this)); // Показ попапов на карте
        });

        this.addObjectMobileBtn.addEventListener('click', this.viewPopup.bind(this));

        this.searchInput.addEventListener('keyup', this.searchOnChange.bind(this)); // Обработка поиска на форме

        this.cadastreMask(this.cadastralInput); // Маска на поле кадастра

        this.openCardByTd(); // Если есть id в урле, на странице карты открываем карточку этого объекта

        document.addEventListener('click', this.resetClickMobile.bind(this)); // Очищаем фильтр на мобильной версии

        this.viewAllBtn.addEventListener('click', this.resetClickDesktop.bind(this));

        document.addEventListener('click', this.openBigPhoto); // Открываем фото в карточке объекта
    }

    openBigPhoto(e) {
        let target = e.target;

        if (target.classList.contains('map-card__img')) {
            e.preventDefault();
            const parent = target.closest('.map-card__fancybox');
            const parentUrl = parent.getAttribute('href');

            $.fancybox.open({
                src: parentUrl
            });
        }
    }

    resetClickDesktop() {
        this.filterForm.reset();
        this.filterOnChange();
    }

    resetClickMobile(e) {
        let target = e.target;

        if (target.classList.contains('map-filter__reset')) {
            this.filterForm.reset();
            this.filterOnChange();

            this.pMap.classList.remove('p-map_filter_visible');
        }
    }

    cadastreMask(elem) {

        let SPMaskBehavior = function (val) {
            let length = val.replace(/\D/g, '').length;

            if (length <= 12) {
                return '00:00:000000:009';
            } else if (length >= 13) {
                return '00:00:0000000:00';
            }
        };
        let spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
        $(elem).mask(SPMaskBehavior, spOptions);
    }

    viewPopup(e) {
        let target = e.currentTarget;
        let data = target.getAttribute('data-popup');

        if (data) {

            this.viewLoggedIs(data);

            forEach(this.popup, (popup) => {
                if (popup.classList.contains(`popup-base_${data}`)) {
                    popup.classList.add('popup-base_visible');
                }
            });

        }
    }

    viewLoggedIs(data) {
        if (data === 'add-object') {
            let loggedIs = document.documentElement.classList.contains('logged-in');

            if (loggedIs) {
                window.location = '/cabinet';
                return;
            }

            forEach(this.popup, (popup) => {
                if (popup.classList.contains('popup-base_authorization')) {
                    popup.classList.add('popup-base_visible');
                }
            });
        }
    }

    mapCardHideBtnClick() {
        const btn = document.querySelector('.map-card__hide-btn');

        if (btn) {
            btn.addEventListener('click', () => {
                document.querySelector('.map-card').classList.toggle('map-card_hide_on');
            });
        }
    }

    sectionChange() {
        this.nav.addEventListener('click', e => {
            let section = e.target.closest('.map-nav__item');
            let arr = [];
            let data = {};

            this.searchInput.value = '';
            this.keys = [];
            this.pMap.classList.remove('p-map_filter_visible');

            if (section) {
                if (section.classList.contains('map-nav__item-box')) {
                    section = section.getAttribute('data-id');
                    this.getFromCheckboxFilterParams();
                } else {
                    section = section.getAttribute('data-id');
                    arr.push(section);
                    data.types = arr;
                    this.setNewFilter(data);
                }

                if (section !== this.section) {
                    this.clearMapObjects();
                    this.section = section;
                    this.urlUpdate();
                    // this.filterOnChange();
                }
            }
        });
    }

    loadData(file, callbackName) {
        // Кешируем данные, если их нет
        if (this.cache[file]) {
            this[callbackName](this.cache[file]);
        } else {
            $.ajax({
                dataType: 'json',
                url: '/api/' + file,
                success: data => {
                    this.cache[file] = data;
                    this[callbackName](this.cache[file]);
                }
            });
        }
    }

    createMap() {

        this.map = new ymaps.Map('map', { // eslint-disable-line
            center: [55.76, 37.2],
            zoom: 9,
            controls: []
        }, {
            minZoom: 0,
            restrictMapArea: [[82, 24], [39, -168]]
        });

        // Кнопки зума
        /*eslint-disable */
        // Создадим пользовательский макет ползунка масштаба.
        var ZoomLayout = ymaps.templateLayoutFactory.createClass("<div>" +
            "<div id='zoom-in' class='btn'><i class='icon-plus'></i></div><br>" +
            "<div id='zoom-out' class='btn'><i class='icon-minus'></i></div>" +
            "</div>", {

            // Переопределяем методы макета, чтобы выполнять дополнительные действия
            // при построении и очистке макета.
            build: function () {
                // Вызываем родительский метод build.
                ZoomLayout.superclass.build.call(this);

                // Привязываем функции-обработчики к контексту и сохраняем ссылки
                // на них, чтобы потом отписаться от событий.
                this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);

                // Начинаем слушать клики на кнопках макета.
                $('.map__btn_increase').bind('click', this.zoomInCallback);
                $('.map__btn_decrease').bind('click', this.zoomOutCallback);
            },

            clear: function () {
                // Снимаем обработчики кликов.
                $('.map__btn_increase').unbind('click', this.zoomInCallback);
                $('.map__btn_decrease').unbind('click', this.zoomOutCallback);

                // Вызываем родительский метод clear.
                ZoomLayout.superclass.clear.call(this);
            },

            zoomIn: function () {
                var map = this.getData().control.getMap();
                map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
            },

            zoomOut: function () {
                var map = this.getData().control.getMap();
                map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
            }
        }),
        zoomControl = new ymaps.control.ZoomControl({options: {layout: ZoomLayout}});
        /*eslint-enable */

        this.map.controls.add(zoomControl);

        this.objectManager = new ymaps.ObjectManager({ // eslint-disable-line
            clusterize: true,
            clusterHasBalloon: false
        });

        this.pointsObjectManager = new ymaps.ObjectManager({ // eslint-disable-line
            clusterize: true
        });

        this.objectManager.objects.events.add('click', e => {
            this.loadMoreData(e.get('objectId'));
            if (document.body.offsetWidth <= 750) {
                document.querySelector('.p-map').classList.add('p-map_results_visible');
            }
        });

        // Задаём свой шаблон для кластеров
        this.objectManager.clusters.events.add('add', (e) => {
            let cluster = this.objectManager.clusters.getById(e.get('objectId'));

            let cls = '';
            let myRadius = 45;

            if (cluster.number > 10) {
                cls = 'layout-cluster_big';
                myRadius = 55;
            }

            this.objectManager.clusters.setClusterOptions(cluster.id, {
                iconLayout: ymaps.templateLayoutFactory.createClass(`<div class="layout-cluster ${cls}"> <div class="layout-cluster__wrapper"><div class="layout-cluster__count"><span class="layout-cluster__number">${cluster.number}</span></div></div></div></div>`), // eslint-disable-line
                iconShape: {
                    type: 'Circle',
                    coordinates: [0, 0],
                    radius: myRadius
                }
            });
        });

        // Есть объекты с одинаковыми координатами, они не разбиваются на точки.
        // Находим их и выводим справа
        this.objectManager.clusters.events.add('click', (e) => {
            let cluster = this.objectManager.clusters.getById(e.get('objectId'));
            let obj = cluster.properties.geoObjects;
            let cache;
            let count = 0;
            for (let i = 0; i < obj.length; i++ ) {

                cache = obj[i].geometry.coordinates;
                for (let j = 0; j < obj.length; j++ ) {

                    if (JSON.stringify(cache) === JSON.stringify(obj[j].geometry.coordinates)) {
                        if (i === j) {
                            continue;
                        }
                        count++;
                        break;
                    }
                }
                if (count === obj.length) {

                    this.cluster = this.objectManager.clusters.getById(e.get('objectId'));

                    // Показываем результаты
                    this.resultsTemplate(obj);
                }
            }
        });

        // Клик в любом месте, кроме не разбиваемого кластера возвращает к результатам фильтрации
        this.map.events.add('click', (e) => {
            let coords = e.get('coords');
            if (this.cluster) {
                if (JSON.stringify(this.cluster.geometry.coordinates) !== JSON.stringify(coords)) {
                    this.filterOnChange();
                }
            }
        });

        this.map.geoObjects.add(this.objectManager); // eslint-disable-line
        this.map.geoObjects.add(this.pointsObjectManager); // eslint-disable-line

        const ico = [
            {
                name: 'gas', // Газификация
                type: 'gas'
            },
            {
                name: 'electricity', // Электрификация
                type: 'point_1'
            },
            {
                name: 'coworking', // Коворкинг
                type: 'type_8'
            },
            {
                name: 'resident', // Резиденты
                type: 'type_5'
            },
            {
                name: 'freeland', // Свободные земли
                type: 'type_6'
            },
            {
                name: 'propertyobject', // Объект имущества
                type: 'type_10'
            },
            {
                name: 'landplot', // Земельный участок
                type: 'type_9'
            },
            {
                name: 'freespace', // Свободная площадь
                type: 'type_7'
            },
            {
                name: 'industrialpark', // Индустриальные парки
                type: 'type_a'
            }
        ];

        // Иконки
        for ( var i = 0; i < ico.length; i++) {
            ymaps.option.presetStorage.add(ico[i].type+'#icon', { // eslint-disable-line
                iconLayout: ymaps.templateLayoutFactory.createClass('<div class="layout-ico"> <div class="layout-ico__wrapper"><img class="layout-ico__img layout-ico__img_' + ico[i].name + '" src="/static/img/assets/map/' + ico[i].name + '.svg" alt=""></div></div>'), // eslint-disable-line
                iconShape: {
                    type: 'Circle',
                    coordinates: [0, 0],
                    radius: 30
                }
            });
        }
    }

    addMapObjects(data) {
        this.objectManager.add(data);
    }

    clearMapObjects() {
        // Сбрасываем фильтр
        this.filterReset();
        this.objectManager.removeAll();
    }

    subSectionChange() {
        const subSectionsCheckBoxes = [].slice.call(document.querySelector('.map-nav__items').querySelectorAll('.checkbox__input'));

        subSectionsCheckBoxes.forEach(subSectionsCheckBox => {
            subSectionsCheckBox.addEventListener('change', () => {
                if (this.nav.querySelector('.map-nav__item_active').classList.contains('map-nav__item-box')) {
                    this.filterOnChange();
                    this.getFromCheckboxFilterParams();
                }
            });
        });
    }

    // Для первого пункта получаем активные чекбоксы
    getFromCheckboxFilterParams() {
        let arr = [];
        let data = {};

        forEach(this.checkBox, (checkbox) => {
            if (checkbox.checked) {
                let id = checkbox.getAttribute('id');
                if (arr.indexOf(id.slice(-1)) === -1 ) {
                    arr.push(id.slice(-1));
                }
            }
        });

        data.types = arr;

        this.setNewFilter(data);
    }

    // Выставляем поля фильтру в зависимости от активного пункта вверху
    setNewFilter(data) {
        $.ajax({
            type: 'POST',
            data: data,
            url: '/map/render-filter',
            success: response => {
                let filterContent = this.block.querySelector('.map-filter__content');
                let container = filterContent.querySelector('.mCSB_container');
                $(container).html(response);
                const filterCheckBoxes = container.querySelectorAll('.checkbox__input');
                const filterInputs = container.querySelectorAll('.map-filter__input');

                // делаем новые поля фильтрабельными
                this.filter(filterCheckBoxes, filterInputs);
            }
        });
    }

    getSubSections() {
        if (!this.nav.querySelector('.map-nav__item_active').classList.contains('map-nav__item-box')) {
            return false;
        }
        return $('.map-nav__item_active').find('.map-nav__box-items').serialize();
    }

    searchOnChange(e) {
        let input = e.target;
        let value = input.value;
        let params = '';
        let subSections = this.getSubSections();
        this.keys.push(value);
        if (value.length > 0) {
            clearTimeout(this.timer);
            this.timer = setTimeout( () => {
                if (subSections) {
                    params = `&${subSections}`;
                }

                let result = $(this.searchForm).serialize() + params;
                let symbol = result.slice(-1);

                if (symbol !== '=') { // Если имя пустое, не отправляем запрос
                    $.ajax({
                        dataType: 'json',
                        data: result,
                        url: '/api/filter',
                        beforeSend: data => {
                            input.blur();
                            this.filterPreload();
                            this.preloaderView();
                        },
                        success: data => {
                            this.filterResults(data);
                            this.preloaderHide();
                            input.focus();
                        }
                    });
                }
            }, 1000);
            return;
        }
        if (!(this.keys[this.keys.length - 2] === value)) { // Не отправляем повторно запрос, если нажали несколько раз backspace
            this.filterOnChange();
        }
    }

    filterOnChange(input) {
        let count = 0;
        this.filterCheckboxes.forEach(checkbox => {
            if (checkbox.checked) {
                count += 1;
            }
        });

        this.filterInputs.forEach(filterInput => {
            if (filterInput.value > 0) {
                count += 1;
            }
        });

        this.filterCount.innerHTML = count;

        let subSections = this.getSubSections();
        let filter = $(this.filterForm).serialize() + `&section=${this.section}`;

        if (subSections) {
            filter += `&${subSections}`;
        }

        // Загрузка данных
        $.ajax({
            dataType: 'json',
            data: filter,
            url: '/api/filter',
            beforeSend: data => {

                if (input) {
                    input.blur();
                }
                this.filterPreload();
                this.preloaderView();
            },
            success: data => {
                this.filterResults(data);
                this.preloaderHide();
                if (input) {
                    input.focus();
                }
            }
        });
    }

    preloaderView() {
        const body = document.body;
        const preloader = body.querySelector('.preloader');

        if (preloader) {
            return;
        }

        $(body).prepend('<div class="preloader preloader_spinner"><span class="preloader__text"></span></div>');
    }

    preloaderHide() {
        const body = document.body;
        $(body).find('.preloader').remove();
    }

    filterPreload() {
        this.resultTitle.innerHTML = 'Идет поиск объектов';
        this.resultsItems.innerHTML = '';
    }

    filter(checkBoxes = this.filterCheckboxes, inputs = this.filterInputs) {
        checkBoxes.forEach(checkbox => {
            checkbox.addEventListener('change', () => {
                this.filterOnChange();
            });
        });

        inputs.forEach(input => {
            input.addEventListener('input', (e) => {
                let el = e.target;
                let value = el.value;

                if (value.length > 0) {
                    clearTimeout(this.timerInputsForms);
                    this.timerInputsForms = setTimeout( () => {
                        this.filterOnChange(el);
                    }, 1000);
                }
            });
        });

        if (location.search.length > 1) {
            const filter = location.search.substr(1);

            const dataArray = filter.split('&');
            const dataObj = {};

            dataArray.forEach(item => {
                const key = item.split('=')[0];
                const value = item.split('=')[1];
                dataObj[key] = value;
            });

            this.section = dataObj.section;

            const navItems = [].slice.call(this.nav.querySelectorAll('.map-nav__item'));

            navItems.forEach(navItem => {
                if (navItem.getAttribute('data-id') === this.section) {
                    navItem.classList.add('map-nav__item_active');
                } else {
                    navItem.classList.remove('map-nav__item_active');
                }
            });

            $(this.filterForm).deserialize(filter); // заполняет форму c фильтром
            if (this.nav.querySelector('.map-nav__item_active').classList.contains('map-nav__item-box')) {
                $('.map-nav__item_active').find('.map-nav__box-items').deserialize(filter); // заполняет sub-sections
            }

            this.filterOnChange();
            return;
        }

        this.filterOnChange();
    }

    urlUpdate() {
        history.pushState(false, '', '?' + $(this.filterForm).serialize() + `&section=${this.section}`);
    }

    filterResetBtnClick() {
        this.filterResetBtn.addEventListener('click', () => {
            this.filterOnChange();
        });
    }

    filterReset() {
        this.filterForm.reset();
        this.objectManager.setFilter((obj) => {
            return obj;
        });
    }

    filterResults(data) {
        this.renderData(data);
    }

    renderData(data) {
        this.objectManager.removeAll();
        // Размещаем объекты на карте
        this.addMapObjects(data);

        // Отображаем результаты
        this.renderResults(data);

        this.resultsBox.classList.add('map-results__box_open');
    }

    renderResultsTop(count) {
        this.resultTitle.innerHTML = `Найдено: ${count + ' ' + this.getObjectsText(count)}`;
    }

    renderResults(data) {
        // Отображаем количество найденных объектов
        this.resultsTemplate(data.features); // Шаблон вывода
    }

    breakArray(arr, chunk) {
        let i, j, tmp = [];
        for (i = 0, j = arr.length; i < j; i += chunk) {
            tmp.push(arr.slice(i, i + chunk));
        }
        return tmp;
    }

    resultsTemplate(data) {
        if (data) {
            this.renderResultsTop(data.length);
        } else {
            this.renderResultsTop(0);
        }

        this.resultsItems.innerHTML = '';

        if (!data) {
            return;
        }

        let arr = this.breakArray(data, 50); // Делим вывод на количество

        // Заполняем список результатами
        arr.forEach( (item, i) => {
            let tpl = `<div class="map-results__break ${(i === 0) ? 'map-results__break_visible' : ''}">`;
            item.forEach(el => {

                let template = (
                    `<div class="map-results__item" data-id="${el.id}">
                        <div class="map-results__pic">`
                );

                if (el.properties.preview) {
                    template += (
                        `<img class="map-results__img" src="${el.properties.preview}" alt="">`
                    );
                }

                template += (`
                        </div>
                            <div class="map-results__content">
                                <a class="map-results__name" href="javascript:void(0)">${el.properties.name}</a>
                                <div class="map-results__address">${el.properties.address}</div>
                            </div>
                        </div>`
                );
                tpl += template;
            });
            tpl += '</div>';

            const result = document.createElement('div');

            result.innerHTML = tpl;

            this.resultsItems.appendChild(result.firstChild);
        });

    }



    resultOpen() {
        var self = this;
        // prevent fancybox on cecuent mode
        $(document).on('click', '.map-card__close-btn', function () {
            $(this).closest('.map-card').fadeOut();
        });

        $(document).on('click', '.map-results__item, .map-card__link', function () {
            $.fancybox.close();
            self.loadMoreData($(this).data('id'));
        });
    }

    openCardByTd() {
        const filter = location.search.substr(1);

        const dataArray = filter.split('&');
        const dataObj = {};
        dataArray.forEach(item => {
            const key = item.split('=')[0];
            const value = item.split('=')[1];
            dataObj[key] = value;
        });

        const id = dataObj.id;

        if (id) {
            $.fancybox.close();
            this.loadMoreData(id);
        }
    }

    points() {
        const points = [].slice.call(this.nav.querySelector('.map-nav__points').querySelectorAll('.checkbox__input'));

        points.forEach(point => {
            const pointName = point.name.replace('-points', '');

            if (point.checked) {
                this.loadData(pointName, 'addPoints');
            }

            point.addEventListener('change', () => {
                if (point.checked) {
                    this.loadData(pointName, 'addPoints');
                } else {
                    this.loadData(pointName, 'delPoints');
                }
            });
        });
    }

    addPoints(data) {
        this.pointsObjectManager.add(data);
    }

    delPoints(data) {
        this.pointsObjectManager.remove(data);
    }

    loadMoreData(id) {
        $.ajax({
            dataType: 'json',
            url: '/api/object?id=' + id,
            context: document.body
        }).done(data => {
            this.moreRender(data);
        });
    }

    moreRender(data) {
        // Определяем тип объекта
        const objectType = data.object_type_id;

        // Если есть слайдер
        if ($('.map-card__slider').length > 0) {
            // Если слайдер инициализирован - выключаем
            if ($('.map-card__slider')[0].slick) {
                $('.map-card__slider').slick('unslick');
            }
        }

        let template = '';

        const customText = (element) => {
            return (
                `<div class="map-card__box">
                    <div class="map-card__sub-title">${element.title}</div>
                    <div class="map-card__text">${element.text}</div>
              </div>`
            );
        };

        if (data.title) {
            template += `<h3 class="map-card__title">${data.title}</h3>`;
        }

        template += '<div class="map-card__content custom-scroll custom-scroll custom-scroll_theme_standart">';
        if (data.slider) {
            template += `<div class="map-card__slider-box">
                            <div class="map-card__slider">`;
            data.slider.forEach(slide => {
                template += `<div class="map-card__slide"><a class="map-card__fancybox" href="${slide.img}"><img class="map-card__img" src=${slide.img} alt="" role="presentation"/></a></div>`;
            });
            template += `    </div>
                        </div>`;
        }

        if (data.description) {
            template += customText(data.description);
        }

        if (data.address) {
            template += customText(data.address);
        }

        if (data.phone) {
            template += customText(data.phone);
        }

        // Общие сведения
        if (data.details) {
            // Сортируем массив, чтобы указанные значения шли друг за другом
            let cardClass = '';
            template += '<div class="map-card__details map-card__block">';
            let coords = [];

            data.details.forEach(detail => {
                // Делаем адрес кликабельным, чтобы перейти к конкретной точке на карте
                if (detail.title === 'Координаты') {
                    coords[0] = detail.latitude;
                    coords[1] = detail.longitude;
                }

                if (detail.title === 'Координаты') {
                    return;
                }

                if (detail.title === 'Адрес') {
                    detail.text = `<a class="map-card__go-point" href="http://maps.yandex.ru/?text=${detail.text}&sll=${coords}" target="_blank">${detail.text}</a>`;
                }

                if ((typeof detail.text) === 'string') {
                    // Ищем мэйл и заменяем его на кликабельный
                    const sendMail = /[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}/g;
                    let result = detail.text.match(sendMail);
                    let substMail = '';

                    if (result) {
                        forEach(result, (mail) => {
                            substMail = `<a href="mailto:${mail}">${mail}</a>`;
                            detail.text = detail.text.replace(mail, substMail);
                        });
                    }

                    // Ищем ссылки и заменяем их на кликабельные
                    const link = /([^\"=]{2}|^)((https?|ftp):\/\/\S+[^\s.,> )\];'\"!?])/;
                    const subst = '$1<a href="$2" target="_blank">$2</a>';

                    detail.text = detail.text.replace(link, subst);
                }
                if (detail.title === 'Общие сведения' || detail.title === 'Спецпредложения для бизнеса') {
                    cardClass = 'map-card__detail_big';
                } else {
                    cardClass = 'map-card__detail';
                }

                // Убираем не нужные поля на коворкингах
                if (objectType === 8) {

                    if (detail.title === 'Наличие строений' || detail.title === 'Наличие доступа к автодороге' || detail.title === 'Наличие обременения' || detail.title === 'Возможность присоединения к грузовой ж/д станции') {
                        return;
                    }
                    template += `<div class="${cardClass}">
                        <b class="map-card__text">${detail.title}</b>
                        <div class="map-card__text">${detail.text}</div>
                    </div>`;

                } else {
                    template += `<div class="${cardClass}">
                            <b class="map-card__text">${detail.title}</b>
                            <div class="map-card__text">${detail.text}</div>
                        </div>`;
                }

            });
            template += '</div>';
        }

        // Резиденты
        const residentText = (resident) => {
            if (resident.link) {
                return `<a href="${resident.link}" class="map-card__text">${resident.text}</a>`;
            }

            return `<div class="map-card__text">${resident.text}</div>`;
        };

        if (data.residents.length > 0) {
            template += `<div class="map-card__residents">
                            <div class="map-card__sub-title">Резиденты</div>
                            <div class="map-card__res-items">`;
            data.residents.forEach(resident => {
                template += `<div class="map-card__res-item" data-id="${resident.id}">
                                <div class="map-card__res-pic">
                                    <img class="map-card__img" src="${resident.img}" alt="" role="presentation"/>
                                </div>                     
                               <a class="map-card__link" data-id="${resident.id}"  href="javascript:void(0)">${residentText(resident)}</a>              
                            </div>`;
            });
            template += `    </div>
                        </div>`;
        }

        // Документы
        if (data.documents.length > 0) {
            template += `<div class="map-card__documents">
                            <div class="map-card__sub-title">Документы</div>
                            <div class="map-card__res-items">`;
            data.documents.forEach(document => {
                template += `<div class="map-card__res-item">
                                <div class="map-card__document">
                                     <svg class="icon map-card__icon map-card__icon_doc"
                                        width="34px" height="37px">
                                        <use xlink:href="/svg-symbols.svg#icon_attached_doc"></use>
                                    </svg>
                                    <a href="${document.path}" class="map-card__document-name">${document.title}</a>
                                    <p class="map-card__dop-info">
                                        <span class="map-card__document-format">.${document.format}</span>
                                        <span class="map-card__document-size">${document.size}</span>
                                    </p>
                                </div>
                            </div>`;
            });
            template += `    </div>
                        </div>`;
        }

        // Другое
        if (data.other) {
            data.other.forEach(item => {
                template += '<div class="map-card__box">';
                template += `<div class="map-card__sub-title">${item.title}</div>`;
                template += '<div class="map-card__box-items">';
                item.items.forEach(subItem => {
                    template += `<div class="map-card__box-item">
                                    <div class="map-card__text">Участок 59 га</div>
                                 </div>`;
                });
                template += '</div>';
                template += '</div>';
            });
        }

        template += '</div>';

        this.cardDynamicContent.innerHTML = template;

        if (!$('html').hasClass('cecutient-mode')) {
            $.fancybox.open({
                src: '#map-card',
                type: 'inline',
            }, {
                baseClass: 'map-card__popup',
            });
        } else {
            $(document).find('.map-card').stop().fadeIn();
        }

        // Получаем попап, если коворкинг, прячем кнопку отправить запрос
        const popup = document.querySelector('.map-card__popup');
        let button = popup.querySelector('.map-card__send');

        if (objectType === 8) {
            button.classList.add('map-card__send_hide');
        } else {
            button.classList.remove('map-card__send_hide');
        }

        if ($('.fancybox-container').length) {
            document.querySelector('.fancybox-container').addEventListener('click', e => {
                if (!e.target.closest('.map-card')) {
                    $.fancybox.close();
                }
            });
        }

        $('.map-card__content').mCustomScrollbar();


        $('.map-card__slider').slick({
            slidesToShow: 5,
        });
    }

});
