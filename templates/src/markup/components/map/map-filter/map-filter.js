import Component from 'helpers-js/Component';

export default Component.create('map-filter', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    init() {
        this.content = this.block.querySelector('.map-filter__content');
        this.subtitles = [].slice.call(this.block.querySelectorAll('.map-filter__sub-title'));
        this.hideBtn = this.block.querySelector('.map-filter__btn_hide');
        this.closeBtn = [].slice.call(this.block.querySelectorAll('.map-filter__sub-title'));

        $(this.content).mCustomScrollbar();

        this.subtitlesClick();
        this.hideBtnClick();
    }

    subtitlesClick() {
        $('.map-filter__box').last().addClass('map-filter__box_last');
        document.addEventListener('click', this.openFilterParams);
    }

    openFilterParams(e) {
        let target = e.target;
        let flag = target.classList.contains('map-filter__sub-title');

        if (flag) {
            let scroll = target.closest('.map-filter__box').offsetTop;
            let parent = target.closest('.map-filter__box');
            parent.classList.toggle('map-filter__box_inner_visible');

            if (parent.classList.contains('map-filter__box_last')) {
                $('.map-filter__content').mCustomScrollbar('scrollTo', scroll, {
                    scrollInertia: 100
                }); // прокручиваем к открытому полю
            }
        }
    }

    hideBtnClick() {
        this.hideBtn.addEventListener('click', () => {
            this.block.classList.toggle('map-filter_content-hidden');
        });
    }

});

