import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('map-results', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    init() {
        this.backLink = this.block.querySelector('.map-result__back-link');
        this.result = this.block.querySelector('.map-results__box_result');
        this.resultClose();
        this.customScroll(); // Кастомный скролл на результатах карты
    }

    customScroll() {
        function getScrollTop(el) {
            return el.mcs.top;
        }

        function elemCoords(elem) {
            let coords = elem.getBoundingClientRect();
            return coords.top;
        }

        function scrolling() {
            const mapResults = document.querySelector('.map-results');
            let breakBlock = mapResults.querySelectorAll('.map-results__break');
            let scroll = getScrollTop(this);

            if (breakBlock.length > 0) {
                breakBlock.forEach( (block) => {
                    if (scroll <= -elemCoords(block)) {
                        block.classList.add('map-results__break_visible');
                    }
                });
            }
        }

        $('.map-results__box').mCustomScrollbar({
            callbacks: {
                whileScrolling: scrolling
            }
        });
    }

    resultClose() {
        this.backLink.addEventListener('click', () => {
            this.result.classList.remove('map-results__box_open');
        });
    }

});

