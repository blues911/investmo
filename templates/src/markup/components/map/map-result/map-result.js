import Component from 'helpers-js/Component';

export default Component.create('map-result', class {
    constructor($block) {
        this.block = $block[0];
        this.init();
    }

    popup() {
        $('.js-open-popup').fancybox({

        });
    }

    setObject() {
        $('.map-result__submit').on('click', function () {
            const $self = $(this);
            const $parent = $self.closest('.map-result');
            const object = $parent.find('.map-result__address').text();
            $('#form-info-object').find('.form__input_object').val(object);
        });

        $('.map-card__send').on('click', function () {
            const $self = $(this);
            const $parent = $self.closest('.map-card');
            const object = $parent.find('.map-card__title').text();
            $('.form-info-object').find('.form__input_object').val(object);
        });
    }

    init() {
        this.popup();
        this.setObject();
    }

});

