import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('p-news', class {
    constructor($block) {
        this.block = $block[0];

        this.init();
    }

    init() {

        if (this.block) {
            $(document).on('click', '.pagination__marker a', this.result.bind(this));
        }
    }

    result(e) {
        e.preventDefault();
        let target = e.target;
        let link = target.getAttribute('href');

        if (!link) {
            return;
        }
        $.ajax({
            dataType: 'html',
            method: 'GET',
            url: link,
            success: (data) => {
                this.block.innerHTML = data;
            }
        });
    }

});

