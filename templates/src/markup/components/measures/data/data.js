var data = {measures: {
    default: {
        items: [
            {
                active: true,
                title: 'Инвесторам',
                thumb: 'thumb1.jpg',
                subtitle: '10 &nbsp;возможностей'
            },
            {
                title: 'Девелоперам',
                thumb: 'thumb2.jpg',
                subtitle: '6 возможностей'
            },
            {
                title: 'Владельцам',
                thumb: 'thumb5.jpg',
                subtitle: '6 возможностей'
            },
            {
                title: 'Малому бизнесу',
                thumb: 'thumb4.jpg',
                subtitle: '6 возможностей'
            }
        ]
    }
}}
