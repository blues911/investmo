import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';


export default Component.create('measures', class {
    constructor($block) {
        this.block = $block[0];

        this.links = document.querySelectorAll('.measures__link');
        this.contentItem = document.querySelectorAll('.measures__content-item');

        this.measureLink = document.querySelectorAll('.measures-list__link');

        this.init();
    }

    init() {

        // Открываем активную вкладку
        forEach(this.links, (link) => {
            link.addEventListener('click', this.openActiveItem.bind(this));
        });

        // При переходе на страницу, если необходимо открываем нужную вкладку
        forEach(this.measureLink, (link) => {
            link.addEventListener('click', this.hashredirect.bind(this));
        });
    }

    hashredirect() {

    }

    openActiveItem(e) {
        let target = e.currentTarget;

        const index = $(target).parent().index();

        if ($(target).parent().hasClass('measures__control_active')) {

            switch (index) {
                case 0:
                    window.location = '/page/investors';
                    break;
                case 1:
                    window.location = '/page/developers';
                    break;
                case 2:
                    window.location = '/page/business';
                    break;
                default:
                    break;

            }
        }

        $(this.links).parent().removeClass('measures__control_active');
        $(target).parent().addClass('measures__control_active');

        $(this.contentItem).removeClass('measures__content-item_state_active');
        $(this.contentItem).eq(index).addClass('measures__content-item_state_active');
    }

});


/*
$('.measures__link').on('click', function () {
    const $this = $(this); // eslint-disable-line no-invalid-this
    const index = $this.parent().index();

    $('.measures__link').parent().removeClass('measures__control_active');
    $this.parent().addClass('measures__control_active');

    $('.measures__content-item').removeClass('measures__content-item_state_active');
    $('.measures__content-item').eq(index).addClass('measures__content-item_state_active');
});
*/
