$('.collaboration__projects-slider').slick({
    slidesToShow: 3,
    responsive: [
        {
            breakpoint: 751,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});
