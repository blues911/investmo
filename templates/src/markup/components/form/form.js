import Component from 'helpers-js/Component';
import { forEach } from 'helpers-js';

export default Component.create('form', class {
    constructor($block) {
        this.block = $block[0];
        this.$block = $block;
        this.version = $('html').attr('lang');
        this.inputs = document.querySelectorAll('.form__input');
        this.form = document.getElementById('feedback-form');
        this.header = document.querySelector('.header');

        this.fileLoadBlock = '';
        this.formData = [];
        this.files = document.querySelectorAll('.form__docs');
        this.output = [];

        this.ie = window.navigator.userAgent.indexOf('MSIE') > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
        this.init();

        this.onloadCallback = onloadCallback; // eslint-disable-line
    }

    init() {
        const upd = (element) => {
            $(element).find('.form__input_phone').mask('+7 (999) 999-99-99');
            $(element).find('.form__select').select2({
                minimumResultsForSearch: -1,
                width: 'auto',
                dropdownAutoWidth: true,
                theme: 'support'
            });
            $(element).find('.form__input-select').select2({
                width: '100%',
                placeholder: $(this).data('placeholder'),
                dropdownCssClass: 'select2-dropdown--multi',
                containerCssClass: 'select2-container--multi'
            });

            element.addEventListener('submit', e => {
                e.preventDefault();
                this.sendForm(e);
            });

            const inputs = [].slice.call(element.querySelectorAll('.form__input'));
            const textAreas = [].slice.call(element.querySelectorAll('.form__textarea'));
            const selects = [].slice.call(element.querySelectorAll('.select2'));
            const radioInputs = [].slice.call(element.querySelectorAll('.radio__input'));

            const clearTextOnClick = () => {
                if (element.querySelector('.success-text')) {
                    element.querySelector('.success-text').innerText = '';
                }
            };

            const actions = (item) => {
                item.addEventListener('click', () => {
                    clearTextOnClick();
                });

                item.addEventListener('input', () => {
                    item.parentNode.classList.remove('error-parent');
                    const errorText = item.parentNode.querySelector('.error-badge');

                    if (errorText) {
                        errorText.remove();
                    }
                });
            };

            inputs.forEach(input => {
                actions(input);
            });

            textAreas.forEach(textArea => {
                actions(textArea);
            });

            selects.forEach(select => {
                select.addEventListener('click', () => {
                    clearTextOnClick();
                });
            });

            radioInputs.forEach(radioInput => {
                radioInput.addEventListener('click', () => {
                    clearTextOnClick();
                });
            });
        };

        document.addEventListener('DOMNodeInserted', (event) => {
            if (event.target.classList && event.target.classList.contains('form') && !event.relatedNode.classList.contains('fancybox-slide') && !event.relatedNode.classList.contains('p-map__map')) {

                this.output = []; // При переключении между формами очищаю файлы
                const element = event.target;
                upd(element);
                this.setPlaceholder();
                this.onloadCallback();
            }
        });

        upd(this.block);

        window.addEventListener('resize', this.getWindowSize);
        window.addEventListener('resize', this.setPlaceholder.bind(this));

        this.getWindowSize();
        this.setPlaceholder();

        if (this.files.length > 0) {
            $(document).on('change', '.form__docs', this.handleFileSelect.bind(this));
            // this.files.addEventListener('change', this.handleFileSelect.bind(this)); // Загрузка файла
            $(document).on('click', 'a[id^="drop"]', this.removeFile.bind(this)); // Удаление загруженного файла
        }
    }

    errorHandler(evt) {
        switch (evt.target.error.code) {
            case evt.target.error.NOT_FOUND_ERR:
                console.log('File Not Found!');
                break;
            case evt.target.error.NOT_READABLE_ERR:
                console.log('File is not readable');
                break;
            case evt.target.error.ABORT_ERR:
                break; // noop
            default:
                console.log('An error occurred reading this file.');
        }
    }

    updateProgress(event) {
        if (event.lengthComputable) {

            let percentLoaded = Math.round((event.loaded / event.total) * 100);
            // Increase the progress bar length.
            this.progress = document.getElementsByClassName('form__docs-progress');
            if (percentLoaded < 100) {
                this.progress.style = 'width:' + percentLoaded + '%';
                this.progress.textContent = percentLoaded + '%';
            }
        }
    }

    countError(params) {

        /**
         * Считаем количество загруженных файлов. Т.к. данные добавляются на лету при загрузке надо смотреть количество
         * в массиве и количество на форме. При удалении можно смотреть только количество в массиве
         */

        let fileBlocks = params.form.querySelectorAll('.form__load-wrapper');
        let count = fileBlocks.length + params.files.length;

        switch (params.flag) {
            case 'add':
                if (count === 10) {
                    params.target.setAttribute('disabled', 'disabled'); // Отключаем возможность добавления файлов
                    params.message.innerHTML = '';
                } else if (count < 10) {
                    params.target.removeAttribute('disabled');
                    params.message.innerHTML = '';
                } else {
                    params.message.innerHTML = 'Файлов не должно быть больше 10.';
                }
                break;
            case 'del':
                if (params.files.length === 10) {
                    params.target.setAttribute('disabled', 'disabled');
                    params.message.innerHTML = '';
                } else if (params.files.length < 10) {
                    params.target.removeAttribute('disabled');
                    params.message.innerHTML = '';
                }
                break;
            default:
                params.target.removeAttribute('disabled');
                params.message.innerHTML = '';
                break;
        }

        return params.message.innerHTML;
    }


    typeError(params) {
        const types = ['jpg', 'png', 'jpeg', 'gif', 'doc', 'docx', 'pdf'];

        for (let i = 0; i < params.files.length; i++) {

            let f = params.files[i];

            let ext = f.name.split('.');

            if (types.indexOf(String(ext[ext.length - 1])) === -1) {
                let message = '';

                switch (this.version) {
                    case 'en':
                        message = 'Incorrect file type.';
                        break;
                    case 'ru':
                    default:
                        message = 'Неверный тип файла.';
                }

                params.message.innerHTML += message;
                return;
            }
        }
        params.message.innerHTML = '';
        this.progressResult(params.files); // Прогресс загрузки файла
    }

    progressResult(files) {

        this.progress.style.width = '0%';
        this.progress.textContent = '0%';

        for (let i = 0; i < files.length; i++) {

            let f = files[i];

            // Статус загрузки файла
            let reader = new FileReader();

            reader.onerror = this.errorHandler;
            reader.onprogress = this.updateProgress;

            reader.onabort = (env) => {
                console.log('File read cancelled');
            };

            reader.onloadstart = (env) => {
                this.progressBar.classList.add('form__progress-bar_loading');
            };

            reader.onload = ((file) => {
                this.progress.style.width = '100%';
                this.progress.textContent = '100%';
                setTimeout( () => {
                    this.progressBar.classList.remove('form__progress-bar_loading');
                }, 1000);
            })(f);

            // ie fix
            if (this.ie) {
                reader.readAsText(f);
            } else {
                reader.readAsBinaryString(f);
            }

            this.formData.push(f);
            this.output.push(f.name);
        }
        this.addFile(); // Вывод добавленных файлов в форму
    }

    handleFileSelect(e) {
        let target = e.target;
        const form = e.target.closest('.form');
        const message = form.querySelector('.form__docs-message');

        this.progressBar = form.querySelector('.form__progress-bar');
        this.progress = form.querySelector('.form__docs-progress');
        this.fileWrap = form.querySelector('.form__docs__wrap-input');
        this.fileLoadBlock = form.querySelector('.form__load-docs');

        let files = e.target.files;

        // Собираем нужные данные в объект
        let params = {
            files: files,
            message: message,
            target: target,
            form: form,
            flag: 'add'
        };

        this.sizeError(params); // Проверка размера
    }

    sizeError(params) {

        let success = [];
        let message = '';

        switch (this.version) {
            case 'en':
                message = 'Maximum size of each file is 10 mb.';
                break;
            case 'ru':
            default:
                message = 'Размер одного файла не должен превышать 10 Мб.';
        }

        for (let i = 0; i < params.files.length; i++) {

            if (params.files[i].size < 10485760) {
                success.push(params.files[i]);
            } else {
                this.$block.append(`<div class="form__text form__text_msg">${message}</div>`);
            }
        }


        params.files = success;

        if (this.countError(params) === '') {
            this.typeError(params); // Проверка типа
        }
    }

    addFile() {
        this.fileLoadBlock.innerHTML = ''; // Очищаем вывод, если пользователь еще захочет добавить документы
        for (let i = 0; i < this.output.length; i++) {
            $(this.fileLoadBlock).append(`<div class="form__load-wrapper"><span class="form__load-item"> ${this.output[i]} <a class="form__del-docs" href="javascript:void(0)" id="drop-${i}"></a></span></div>`);
        }

    }

    removeFile(e) {
        e.preventDefault();
        let target = e.currentTarget;
        let form = target.closest('.form');
        let message = form.querySelector('.form__docs-message');
        let input = form.querySelector('.form__docs');
        let params = {files: this.output, message: message, target: input, form: form, flag: 'del'};

        let id = target.getAttribute('id');
        let temp = [];
        temp = id.split('-');
        this.output.splice(temp[1], 1);
        this.formData.splice(temp[1], 1); // При удалении файлов также очищаем их количество, оправляемое на сервер
        this.fileLoadBlock.innerHTML = '';
        this.addFile(); // После удаления обновляем список файлов
        this.countError(params);
    }

    setPlaceholder() {
        let form = document.getElementById('feedback-form');

        if (form) {
            let input = form.getElementsByClassName('form__input');

            if (this.getWindowSize() > 750) {

                forEach(input, (item ) => {
                    item.setAttribute('placeholder', '');
                });
            } else {

                forEach(input, (item ) => {
                    let data = item.closest('.form__label').textContent;
                    item.setAttribute('placeholder', data);
                });
            }
        }
    }

    getWindowSize() {
        let width = document.documentElement.clientWidth;
        return width;
    }


    sendForm(event) {
        if (event.target.classList.contains('no-default-ajax')) {
            return;
        }
        const form = event.target;
        const $block = $(event.target);
        const url = $block.attr('action');
        const data = new FormData(form);
        const $this = this;

        for (let i = 0; i < this.formData.length; i++) {
            data.append('files[' + i + ']', this.formData[i]);
        }

        $.ajax({
            url: url,
            data: data,
            method: 'post',
            processData: false,
            contentType: false,
            beforeSend: () => {
                $block.append('<div class="form__text loader-text" style="display: none;"></div>');
                setTimeout(function () {
                    $block.find('.loader-text').html('Сообщение отправляется <img src="static/img/assets/form/dot_loader.gif">').attr('style', 'display:block;');
                }, 500);
            },
            success: (json) => {
                this.formData = [];
                $block.find('.loader-text').remove();
                if (json.success && json.success === true) {
                    $this.success(event.target);
                    grecaptcha.reset(widget); // eslint-disable-line
                    let oldMsg = $block.find('.success-text');

                    if (oldMsg) {
                        oldMsg.remove();
                    }

                    $block.append('<div class="form__text success-text"> ' + this.successMessage(this.version) + '</div>');

                    // очищаем поле файлов после отправки
                    if (this.fileLoadBlock) {
                        this.fileLoadBlock.innerHTML = '';
                    }
                    $($block)[0].reset();
                    this.output = [];
                    grecaptcha.reset(this.widget);  // eslint-disable-line

                } else if (json.success === false) {
                    this.showErrors(json, event.target);

                    this.scrollToBadge(form); // Прокрутка к полю с ошибкой
                }
            },
        });
    }

    scrollToBadge(form) {
        let badges = form.querySelectorAll('.error-badge');
        if (badges) {
            let coords = this.getCoords(badges[0]);
            let headerHeight = this.header.offsetHeight;
            $('body, html').animate({
                scrollTop: ( coords.top - (headerHeight + 20) )
            }, 300);
        }
    }

    getCoords(elem) {
        const box = elem.getBoundingClientRect();

        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        };
    }

    successMessage(version) {
        let message = '';

        switch (version) {
            case 'en':
                message = 'Message has been sent';
                break;
            case 'ru':
            default:
                message = 'Сообщение отправлено';
        }
        return message;
    }

    success(block) {
        this.clearErrors(block);
        block.reset();
        $(block).find('.form__input-select').select2('val', 'All');
        console.log('ok');
    }

    clearErrors(block) {

        [].slice.call(block.querySelectorAll('.error-badge')).forEach(errorBadge => {
            errorBadge.remove();
        });

        [].slice.call(block.querySelectorAll('.error-parent')).forEach(errorItem => {
            errorItem.classList.remove('error-parent');
        });
    }

    showErrors(json, block) {
        console.log(block);
        this.clearErrors(block);

        Object.keys(json.errors).forEach(item => {
            [].slice.call(block.elements).forEach(element => {
                if (element.name === item) {
                    // добавляет класс с ошибкой к родителю елемента
                    element.parentNode.classList.add('error-parent');

                    // Текст ошибки
                    let errorText = '';

                    json.errors[item].forEach(error => {
                        errorText += error + ' ';
                    });

                    const errorBadge = document.createElement('div');

                    errorBadge.classList.add('error-badge');

                    errorBadge.innerHTML = errorText;

                    element.parentNode.appendChild(errorBadge);
                }
            });
        });
    }

});
