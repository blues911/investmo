import Component from 'helpers-js/Component';

export default Component.create('searching', class {
    constructor() {
        this.init();
    }

    init() {

        const searchForm = $('form.searching__form');
        const searchBtn = $('button.search__btn_submit');
        const searchArea = $('select.select__widget[name="area"]');
        const searchSort = $('select.select__widget[name="order"]');

        const params = {
            query: '',
            area: 1,
            order: 'date'
        };

        searchBtn.on('click', (e) => {
            e.preventDefault();
            params.query = $('input.search__input').val();

            console.log('====================================');
            console.log(params);
            console.log('====================================');
            this.initAjax(params, searchForm);

            return false;

        });

        searchArea.on('change', (e) => {
            params.area = e.target.value;
            this.initAjax(params, searchForm);
        });

        searchSort.on('change', (e) => {
            params.order = e.target.value;
            this.initAjax(params, searchForm);
        });

        window.addEventListener('popstate', (e) => {
            let search = this.getParameterByName('search');
            if (search) {
                params.query = search;
                $('input.search__input[name="search"]').val(search);
                this.initAjax(params, searchForm);
            }
        }, false);

    }

    initAjax(params, searchForm) {
        $.ajax({
            url: searchForm.attr('action'),
            data: searchForm.serialize(),
            method: 'POST',
            crossDomain: true,
            beforeSend: (request) => {
                $('.search-result').fadeOut('fast');
                // поставим сюда спиннер, если нужен
            },
        }).then((res) => {
            this.renderData(res);
            const link = window.location.origin + window.location.pathname + '?search=' + params.query;
            history.pushState({ title: 'Поиск', href: link }, null, link);
        });
    }

    renderData(data) {
        const ddata = data;
        const tempContainer = $('<div style="display:none;" id="temp"></div>');
        $(tempContainer).append(ddata);
        $('.search-result').html($(tempContainer).find('.search-result').html());
        $('.pagination').html($(tempContainer).find('.pagination').html());
        $('.searching__text-result').html($(tempContainer).find('.searching__text-result').html());
        $(tempContainer).remove();
        $('.search-result').fadeIn('fast');
    }

    getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, '\\$&');
        let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) {
            return null;
        }
        if (!results[2]) {
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

});
