import Component from 'helpers-js/Component';
import _ from 'lodash';
import 'slick-carousel';

const Slider = Component.create('slider', class {
    static init($block, settings) {
        if (!$block.length) {
            return;
        }

        new Slider($block).init(settings);
    }

    constructor($block) {
        this.$block = $block;

        this.init();

        $block.on('Slider:needUpdate', this.update.bind(this));
        $block.on('click', this._onClick.bind(this));
    }

    _onClick(e) {
        const $target = $(e.target);
        const $navBtn = $target.closest('.slider__nav-btn');

        if ($navBtn.length) {
            this._onNavBtnClick(e, $navBtn);
        }
    }

    init(settings) {
        let dataSettings = this.$block.attr('data-settings');

        if (typeof dataSettings === 'string') {
            dataSettings = JSON.parse(dataSettings);
        }

        if (dataSettings) {
            settings = _.assign({}, settings, dataSettings);
        }

        if (!settings) {
            return;
        }

        this.$block.slick(settings);

        $('.header__control').on('click', function () {
            if ($('.slider ').length) {
                $('.slider ').slick('refresh');
            }
        });
    }

    update() {
        this.$block.slick('setPosition');
    }
});

export default Slider;
