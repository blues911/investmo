import Component from 'helpers-js/Component';

export default Component.create('hero', class {
    constructor($block) {
        this.block = $block[0];
        this.link = this.block.querySelector('.js-hero__link');
        this.init();
    }


    init() {
        this.link.addEventListener('click', this._scrollSection.bind(this));
    }

    _scrollSection(e) {
        let target = e.currentTarget;
        let href = target.getAttribute('href');

        $('html, body').animate({
            scrollTop: $(href).offset().top
        }, 500);
        e.preventDefault();
        return false;
    }

});

