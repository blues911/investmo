'use strict';

window.$ = window.jQuery = jQuery;

import Component from 'helpers-js/Component';
import './libraries/device/device.min.js';
import polyfills from './libraries/polyfills';

import 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min';
import 'jquery-deserialize';
import 'jquery-validation';
import 'jquery-mask-plugin';
import 'select2/dist/js/select2.full.js';
import 'slick-carousel';
import './plugins/jquery.fancybox';
// import '@fancyapps/fancybox';

import 'components/select-primary/select-primary';
import 'components/map/map-results/map-results';
import 'components/map/map-result/map-result';
import 'components/map/map-filter/map-filter';
import 'components/p-map/p-map';
import 'components/map/map-nav/map-nav';
import 'components/map/map';
import 'components/slider/slider';
import 'components/slider-content/slider-content';
import 'components/select/select';
import 'components/header/header';
import 'components/slide-box/slide-box';
import 'components/stories/stories';
import 'components/measures/measures';
import 'components/measures-list/measures-list';
import 'components/burger/burger';
import 'components/form/form';
import 'components/tabs/tabs';
import 'components/diagrams-box/diagrams-box';
import 'components/subscribe/subscribe';
import 'components/industry/industry';
import 'components/collaboration/collaboration';
import 'components/investors/investors';
import 'components/hero/hero';
import 'components/custom-select/custom-select';
import 'components/trainings-popup/trainings-popup';
import 'components/title-subpage/title-subpage';
import 'components/object-form/object-form';
import 'components/trainings/trainings';
import 'components/popup-base/popup-base';
import 'components/searching/searching';
import 'components/popup-success/popup-success';
import 'components/p-search/p-search';
import 'components/p-news/p-news';
import 'components/search-box/search-box';
import 'components/edit-profile/edit-profile';
import 'components/p-cabinet/p-cabinet';
import 'components/footer/footer';
import 'components/top-box/top-box';
import 'components/info-box/info-box';

$(() => {
    polyfills.init();
    // ================ Здесь инициализируем модули =====================
    Component.initAll();
});
