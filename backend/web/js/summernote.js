// Init Summernote
var $summernote = $('#summernote');

$summernote.summernote({
	height: 200,
  minHeight: null,
  toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['misc', ['codeview']],
    ['insert', ['link','picture']],
  ],
  callbacks: {
    onImageUpload: function(file) {
      var form = new FormData();
      form.append("file", file[0]);
      $.ajax({
        data: form,
        type: 'POST',
        url: '/admin/summernote/upload',
        cache: false,
        contentType: false,
        processData: false,
        success: function(result) {
            if (result.code == 200) {
                $summernote.summernote('insertImage', result.message);
            } else {
                console.log(result.message);
            }
        },
        error: function(result){
          console.log(result);
        }
      });
    }
  }
});