<?php

use common\models\SuccessStory;
use common\models\InvestorType;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\widgets\CKEditor\MyCKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\SuccessStory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="success-story-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'investor_type_ids')->listBox(ArrayHelper::map(InvestorType::find()->all(), 'id', 'text'), ['multiple' => true])->label('Тип инвестирования') ?>

    <?php  if ($model->logo):?>
        <img src="<?= $model->getLogoImg() ?>" style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>

    <?= $form->field($model, 'logo')->fileInput(['accept' => 'image/*']) ?>

    <?php  if ($model->main_img):?>
        <img src="<?= $model->getMainImg() ?> " style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>
    
    <?= $form->field($model, 'main_img')->fileInput(['accept' => 'image/*']) ?>

    <?php  if ($model->background_img):?>
        <img src="<?= $model->getBackgroundImg() ?>" style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>

    <?= $form->field($model, 'background_img')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'investments')->textInput() ?>

    <?= $form->field($model, 'currency')->radioList($model::CURRENCY) ?>

    <?= $form->field($model, 'date_of_start')->textInput() ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'investment_goal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'html')->widget(MyCKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
        'customStyles' => [
            'page' => 'js/ckeditor_style.js'
        ],
        'clientOptions' => [
            'allowedContent' => true,
            'removeFormatAttributes' => '',
            //'extraPlugins' => 'youtube',
            'filebrowserUploadUrl' => Url::to(['/ckeditor/upload']),
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
