<?php

use common\models\SuccessStory;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SuccessStory */

$this->title = Yii::t('app', 'Просмотр').': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Истории успеха'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="success-story-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'is_active',
            'address',
            'phone',
            'email:email',
            'logo',
            'background_img',
            'investments',
            [
                'label' => 'Валюта',
                'value' => function($model) {
                    return $model->getCurrency();
                },
            ],
            'date_of_start',
            'country_of_origin',
            'investment_goal',
            'short_desc:ntext',
            'full_desc:ntext',
            'html:ntext',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
