<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ObjectType */

$this->title = Yii::t('app', 'Создание типа объекта');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Типы объектов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
