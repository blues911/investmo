<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$disabledFields = [
    'name',
    'object_type_id',
    'parent_id',
    'status',
    'description',
    'latitude',
    'longitude',
    'address_address',
    'system_id',
    'request_status',
    'photos',
    'documents',
];
$fieldsList = json_decode($model->object_fields, true);
?>

<div class="object-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_container')->checkbox() ?>

    <?= $form->field($model, 'is_child')->checkbox() ?>

    <h4>Поля объекта</h4>
    <div class="form-group">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                <div class="row">
                    <?php if ($model->isNewRecord) : ?>
                        <!-- new record -->
                        <?php foreach ($model::getObjectFields() as $slug => $title) : ?>
                            <?php if (in_array($slug, $disabledFields)) : ?>
                                <div class="col-md-4">
                                <div class="checkbox disabled">
                                    <label><input type="checkbox" name="objectFields[<?=$slug?>]" checked disabled> <?=$title?></label>
                                    <input type="hidden" name="objectFields[<?=$slug?>]" value="on">
                                </div>
                                </div>
                            <?php else : ?>
                                <div class="col-md-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="objectFields[<?=$slug?>]" checked> <?=$title?></label>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <!-- not new record -->
                        <?php foreach ($model::getObjectFields() as $slug => $title) : ?>
                            <?php if (in_array($slug, $disabledFields)) : ?>
                                <div class="col-md-4">
                                <div class="checkbox disabled">
                                    <label><input type="checkbox" name="objectFields[<?=$slug?>]" checked disabled> <?=$title?></label>
                                    <input type="hidden" name="objectFields[<?=$slug?>]" value="on">
                                </div>
                                </div>
                            <?php else : ?>
                                <div class="col-md-4">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="objectFields[<?=$slug?>]" <?=(isset($fieldsList[$slug])) ? "checked" : ''?>> <?=$title?></label>
                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
