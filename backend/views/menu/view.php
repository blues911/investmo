<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = Yii::t('app', 'Просмотр') . ': ' .$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Меню'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
        
        <?
            $langs = ['ru' => 'Ru', 'en' => 'En', 'de' => 'De'];
            unset($langs[$model->lang]);
        ?>
        
        <div class="form-group">
            <?php $form = ActiveForm::begin([
                'action' => Url::toRoute(['menu/copy', 'id' => $model->id])
            ]); ?>
                <?= Html::dropDownList('lang', false, $langs) ?>
                
                <?= Html::submitButton(Yii::t('app', 'Копировать'), ['class' => 'btn btn-primary']) ?>
            <?php ActiveForm::end(); ?>
        </div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'is_active',
            'url:url',
            'sort',
            'lang',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
