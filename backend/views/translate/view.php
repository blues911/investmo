<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


$this->title = Yii::t('app', 'Просмотр').': '.  $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Переводы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-view">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="btn-group">
                        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    $arr = [
                        'id',
                        'category',
                        'message:ntext',
                    ];
                    foreach ($model->languages as $key=>$one){
                        $arr[] = ['label'=>$key, 'value' => $one];
                    }
                    ?>
                    <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => $arr,
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
