<?php

use yii\helpers\Html;
use yii\grid\GridView;


$this->title = Yii::t('app', 'Переводы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-index row">

    <div class="pad no-print">
        <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> <?=Yii::t('app', 'Правила'); ?>:</h4>
            <?= Yii::t('app', 'Если перевод отсутствует, отображается текст из «ключа». Соответственно, если «ключ» и перевод идентичны, перевод добавлять не нужно. «Ключ» - это идентификатор. Он находится в коде сайта и служит ссылкой на этот модуль. Чтобы добавить новый перевод, необходимо не только создать его здесь, но и вставить соответствующий ключ в код сайта.');?>
        </div>
    </div>
    <div class="col-xs-12">
        <?= Html::a(Yii::t('app', 'Создать перевод'), ['create'], ['class' => 'btn btn-success']) ?>
        <div class="box box-default">
            <div class="box-header with-border">
                <span class="label label-default">записей <?= $dataProvider->getCount()?> из <?= $dataProvider->getTotalCount()?></span>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'summary' => '',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => \backend\models\Message::getGridColumns(),
                ]); ?>
            </div>
        </div>
    </div>
</div>
