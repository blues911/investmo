<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
            <div class="row">
                <div class="col-lg-4">
                    <h2>Объекты</h2>
                    <a href="<?=Url::toRoute(['/object-type']); ?>"><?=Yii::t('app', 'Типы объектов')?></a><br/>
                    <a href="<?=Url::toRoute(['/object']); ?>"><?=Yii::t('app', 'Объекты')?></a><br/>
                    <a href="<?=Url::toRoute(['/point']); ?>"><?=Yii::t('app', 'Точки подключения')?></a><br/>
                    <a href="<?=Url::toRoute(['/cluster']); ?>"><?=Yii::t('app', 'Кластеры')?></a><br/>
                    <a href="<?=Url::toRoute(['/object-request']); ?>"><?=Yii::t('app', 'Заявки на размещение')?></a><br/>
                </div>
                <div class="col-lg-4">
                    <h2>Контент</h2>
                    <a href="<?=Url::toRoute(['/menu']); ?>"><?=Yii::t('app', 'Меню')?></a><br/>
                    <a href="<?=Url::toRoute(['/page']); ?>"><?=Yii::t('app', 'Страницы')?></a><br/>
                    <a href="<?=Url::toRoute(['/content-block']); ?>"><?=Yii::t('app', 'Контентные блоки')?></a><br/>
                    <a href="<?=Url::toRoute(['/news']); ?>"><?=Yii::t('app', 'Новости')?></a><br/>
                    <a href="<?=Url::toRoute(['/story']); ?>"><?=Yii::t('app', 'Истории успеха')?></a><br/>
                </div>
                <div class="col-lg-4">
                    <h2>Обратная связь</h2>
                    <a href="<?=Url::toRoute(['/feed-back']); ?>"><?=Yii::t('app', 'Обращения пользователей')?></a><br/>
                    <a href="<?=Url::toRoute(['/news-sender']); ?>"><?=Yii::t('app', 'Рассылка новостей')?></a><br/>
                    <a href="<?=Url::toRoute(['/object-sender']); ?>"><?=Yii::t('app', 'Рассылка объектов')?></a><br/>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <h2>Переводы</h2>
                    <a href="<?=Url::toRoute(['/translate']); ?>">Переводы</a><br/>
                </div>
                <div class="col-lg-4">
                    <h2>Пользователи</h2>
                    <a href="<?=Url::toRoute(['/user']); ?>">Список пользователей</a><br/><br/>
                </div>
                <div class="col-lg-4">
                    <h2>Группы пользователей</h2>
                    <a href="<?=Url::toRoute(['/user-group']); ?>">Список групп</a><br/><br/>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <h2>Конструктор</h2>
                    <a href="<?=Url::toRoute(['/investor-type']); ?>">Типы инвесторов</a><br/>
                    <a href="<?=Url::toRoute(['/page-composer/story']); ?>">Конструктор историй успеха</a><br/>
                </div>
            </div>
    </div>
</div>