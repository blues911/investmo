<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = Yii::t('app', 'Просмотр').': '. $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Страницы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]);

            $langs = ['ru' => 'Ru', 'en' => 'En', 'de' => 'De'];
            unset($langs[$model->lang]);
        ?>
    <div class="form-group">
        <?php $form = ActiveForm::begin([
            'action' => Url::toRoute(['page/copy', 'id' => $model->id])
        ]); ?>
        <?= Html::dropDownList('lang', false, $langs) ?>

        <?= Html::submitButton(Yii::t('app', 'Копировать'), ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'is_active',
            'title',
            'slug',
            'lang',
            'header',
            'content:ntext',
            'is_constructor',
            'created_at:datetime',
            'updated_at:datetime',
            'is_deleted',
        ],
    ]) ?>

</div>
