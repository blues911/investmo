<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 06.12.2017
 * Time: 1:38
 */
use \common\modules\PageComposer\widgets\BlockButtonWidget;
?>
<ul id="sortable" class="list-group">
    <?php if (isset($blocks)): ?>
        <?php foreach ($blocks as $block) : ?>
            <li class="form-block bg-success list-group-item" id="<?= $block->id ?>">
                <button type="button" class="close" id="block-delete" data-dismiss="modal" aria-hidden="true">×</button>
                <?= $block->block->getDescription(); ?><br>
                <? foreach ($block->getBlock()->fields as $key => $field) {
                    echo $field->getFieldHtml('Page[blocks][' . $block->id . ']');
                } ?>
                <input type="hidden" class="form-control" name="Page[blocks][<?= $block->id ?>][type]>" value="<?= $block->type ?>">
                <input type="hidden" class="form-control" id="form-sort" name="Page[blocks][<?= $block->id ?>][sort]" value="<?= $block->sort ?>">
            </li>
        <? endforeach; ?>
    <?php endif; ?>
</ul>
<?= BlockButtonWidget::widget() ?>
