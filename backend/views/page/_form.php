<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\widgets\CKEditor\MyCKEditor;
use \common\modules\PageComposer\widgets\BlockButtonWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
    $(document).on('change','.page_mode_select',function(){
        if($(this).val() == 1){
            $('#editor_mode').addClass('hidden')
            $('#constructor_mode').removeClass('hidden')
        }else {
            $('#editor_mode').removeClass('hidden')
            $('#constructor_mode').addClass('hidden')
        }
    });
JS;
$this->registerJs($js);
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data'
    ]]); ?>

    <?= $form->field($model, 'is_active')->dropDownList([ '0' => 'No', '1' => 'Yes']) ?>
    
    <?= $form->field($model, 'has_background')->checkbox() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lang')->dropDownList(['ru' => 'Ru', 'en' => 'En', 'de' => 'De']) ?>

    <?= $form->field($model, 'header')->textInput(['maxlength' => true]) ?>

    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th colspan="2">Выберите вид создаваемой страницы</th>
        </tr>
        <td class="lite-font">
            Html-редактору и конструктору блоков
            соответствует разное содержимое страницы.
            Вы можете создать страницу либо при помощи html-редактора,
            либо в конструкторе.
        </td>
        </thead>
        <td>
            <div class="half-width">
                <input id="use_constructor" type="radio" name="Page[is_constructor]" class="page_mode_select" data-page-id="<?=$model->id?>" value="1" <?=$model->is_constructor ? 'checked': null?>>
                <label for="use_constructor">Использовать конструктор</label>
            </div>
            <div class="half-width">
                <input id="not_use_constructor" type="radio" name="Page[is_constructor]" class="page_mode_select" data-page-id="<?=$model->id?>" value="0" <?=!$model->is_constructor ? 'checked': null?>>
                <label for="not_use_constructor">Использовать html-редактор</label>
            </div>
        </td>
    </table>
    <div id="page_mode_loader"></div>
<div id="page_mode_box">
    <div id="editor_mode" class="<?=$model->is_constructor?'hidden':''?>">
        <?= $form->field($model, 'content')->widget(MyCKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'basic',
            'customStyles' => [
                'page' => 'js/ckeditor_style.js'
            ],
            'clientOptions' => [
                'allowedContent' => true,
                'removeFormatAttributes' => '',
                //'extraPlugins' => 'youtube',
                'filebrowserUploadUrl' => Url::to(['/ckeditor/upload']),
            ],
        ]); ?>
    </div>

    <div id="constructor_mode" class="<?=!$model->is_constructor?'hidden':''?>">
        <ul id="sortable" class="list-group">
            <?php if (isset($blocks)): ?>
                <?php foreach ($blocks as $block) : ?>
                    <li class="form-block bg-success list-group-item" id="<?= $block->id ?>">
                        <button type="button" class="close" id="block-delete" data-dismiss="modal" aria-hidden="true">×</button>
                        <?= $block->block->getDescription(); ?><br>
                        <? foreach ($block->getBlock()->fields as $key => $field) {
                            echo $field->getFieldHtml('Page[blocks][' . $block->id . ']');
                        } ?>
                        <input type="hidden" class="form-control" name="Page[blocks][<?= $block->id ?>][type]>" value="<?= $block->type ?>">
                        <input type="hidden" class="form-control" id="form-sort" name="Page[blocks][<?= $block->id ?>][sort]" value="<?= $block->sort ?>">
                    </li>
                <? endforeach; ?>
            <?php endif; ?>
        </ul>
        <?= BlockButtonWidget::widget() ?>
    </div>

</div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>