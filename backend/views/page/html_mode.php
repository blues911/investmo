<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 06.12.2017
 * Time: 1:38
 */


echo $form->field($model, 'content')->widget(backend\widgets\CKEditor\MyCKEditor::className(), [
    'options' => ['rows' => 6],
    'preset' => 'basic',
    'customStyles' => [
        'page' => 'js/ckeditor_style.js'
    ],
    'clientOptions' => [
        'allowedContent' => true,
        'removeFormatAttributes' => '',
        //'extraPlugins' => 'youtube',
        'filebrowserUploadUrl' => \yii\helpers\Url::to(['/ckeditor/upload']),
    ],
]);