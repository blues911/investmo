<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 17:35
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
if (Yii::$app->user->identity->status == 40){
    $roles = [
        '10' => 'Пользователь',
        '30' => 'Администратор',
        '40' => 'Суперадмин',
    ];
    $js = <<<JS
    $(document).on('click','.form-field-add',function(e){
         e.preventDefault();
         var select = $('#useringroup-group_id');
         if(select.length !== 0){
             $('.field-useringroup-group_id').replaceWith('<div class="form-group input-group">'+
                '<li class="form-control input-group" disabled>'+select[0].children[0].label+'</li><input type="hidden" name="UserInGroup[group_id][]" value="'+select.val()+'">'+
                '<span class="input-group-btn">'+
                               '<button class="btn btn-default form-field-delete" style=" height: 34px;">'+
                                    '<span class="glyphicon glyphicon-minus"></span>'+
                                '</button>'+
                            '</span>'+
                        '</div>'); 
         }
         $.ajax({
         url:'/admin/user/form-field-add',
         type: 'GET',
            success: function(res){
                $('.field-user-group').append(res);
            }
         })
    });
    $(document).on('click','.form-field-delete',function(e){
         e.preventDefault();
        $(this).closest('.form-group').remove();
    });
    $(document).on('change','#user-status',function() {
      if($(this).context.selectedOptions[0].value == 40)
          $('.field-user-group').hide();
      else 
          $('.field-user-group').show();
    });
     $(document).on('change','#useringroup-group_id',function(e){
        $(this).closest('.form-group').replaceWith('<div class="form-group input-group">'+
                '<li class="form-control input-group" disabled>'+$(this).context.selectedOptions[0].innerText+'</li><input type="hidden" name="UserInGroup[group_id][]" value="'+$(this).val()+'">'+
                '<span class="input-group-btn">'+
                               '<button class="btn btn-default form-field-delete" style=" height: 34px;">'+
                                    '<span class="glyphicon glyphicon-minus"></span>'+
                                '</button>'+
                            '</span>'+
                        '</div>');
    });
JS;
    $this->registerJs($js);
foreach ($groups as $group){
    $group_list[$group['id']]= $group['name'];
}

    foreach ($user_groups as $group){
        $user_group_list[]= $group['group_id'];
    }
}else{
    $roles = [
        '10' => 'Пользователь',
        '30' => 'Администратор',
    ];
}
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'third_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'readonly' => !$model->isNewRecord]) ?>

    <?php if(Yii::$app->user->identity->status == 40 && $model->status != 40) {
        echo '<div class="field-user-group">
                <label class="control-label" for="user-group">Группы пользователей</label>
                <div class="form-group input-group">
                    <button type="submit" class="btn btn-success form-field-add">Добавить пользователя в группу</button>
                </div>';
        if(!empty($user_group_list[0])) {
            foreach ($user_group_list as $key => $group) {
                foreach ($group_list as $list_key => $list_value) {
                    if ($list_key == $group){
                        echo '<div class="form-group input-group">
                                <li class="form-control input-group" disabled>'.$list_value.'</li>
                                <input type="hidden" name="UserInGroup[group_id][]" value="'.$list_key.'">
                                <span class="input-group-btn">
                                <button class="btn btn-default form-field-delete" style=" height: 34px;">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                        </div>';
                    }
                }
            }
        }
        echo '</div>';
    } ?>
    <?php if($model->status != 40 || Yii::$app->user->identity->status == 40)
            echo $form->field($model, 'status')->dropDownList($roles) ?>
    <?php if ($render == 'create'){ ?>

        <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true])->label('Password') ?>

    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>