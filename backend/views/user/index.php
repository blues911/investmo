<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 17:24
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Список пользователей');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
    $(document).on('click', '.get-user-list', function(e){
        e.preventDefault();
        $(this).next('div').find('.abs-block').slideToggle('normal');
    });
JS;
$this->registerJs($js);
?>
<div class="user-index">
<?php if($su_notify_email){
$js = <<<JS
    $(document).on('submit', '#su-notify-update', function(){
        
       var data = $(this).serialize();
       var email = $(this).context.elements.su_notify_email.value;
       var emails = email.replace(/\s/g,'').split(',');
       for(var i = 0; i < emails.length;i++){
           if(!emails[i].match(/^([a-z0-9_\.-])+@[a-z0-9_\.-]+\.([a-z0-9_\.-]+\.)?[a-z]{2,4}$/)){
                alert('Неверный формат почты №'+ i++);
               return false;
           }
       }
       console.log(emails)
       var url = $(this).attr('action');
       $.ajax({
               url: url,
               type: 'POST',
               data: data,
       success: function(res){
               alert(res);
       },
       error: function(){
               alert('Что то пошло не так, попробуйде изменить данные');
               }
       });
       return false;
   });
JS;
$this->registerJs($js);
    ?>
    <form action="/admin/user/su-notify-update" method="post" id="su-notify-update">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <td colspan="2"><b>Email для уведомлений </b><span class="text-muted">*cписок адресов через запятую</span></td>
            </tr>
            </thead>
            <td>
                <input type="text" class="form-control" name="su_notify_email" value="<?=$su_notify_email?>"/>
            </td>
            <td>
                <button type="submit" class="btn btn-primary" style="width: 100%;">Обновить</button>
            </td>
        </table>

    </form>
    <?php } ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Html::a(Yii::t('app', 'Создать пользователя'), ['create'], ['class' => 'btn btn-success']) ?>
    <?php
        $gridColumns = [
            'id' ,
            'email',
            'username',
            'first_name',
            'last_name',
            'third_name',
            'status',
            'phone',
            'company_name',
        ];
        // Renders a export dropdown menu
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns
        ]);
    ?>
    <br/><br/>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'first_name',
            'last_name',
            'third_name',
            'company_name',
            'phone',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            ['label' => 'Статус',
                'format' => 'raw',
                'value' => function($data){
                   if($data->status == 10)
                       return 'Пользователь';
                   if ($data->status == 20)
                        return 'Модератор';
                   if ($data->status == 30)
                       return 'Администратор';
                   if ($data->status == 40)
                       return 'Суперадмин';
                }],
//             'created_at',
//             'updated_at',
            ['label' => 'Группы',
                'format' => 'row',
                'content' => function($data){
                    $groups = $data->getUserGroups();
                    $li = null;
                    if($groups){
                        if (count($groups) == 1){
                            return $groups[0]->name;
                        }else{
                            foreach ($groups as $group){
                                $li .= '<li>'.$group->name.'</li>';
                            }
                            $list = '<button class="btn btn-default get-user-list full-width">Посмотреть</button>
                                <div style="position: relative">
                                    <div class="abs-block" style="display: none">
                                        <ul>
                                        '.$li.'
                                        </ul>
                                    </div>
                                </div>';
                        }
                        return $list;
                        }

                }],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                    },
                    'update' => function ($url,$model) {
                        return \common\models\User::checkSuperAdmin($model, $url, 'glyphicon-pencil');
                    },
                    'delete' => function ($url,$model) {
                        return \common\models\User::checkSuperAdmin($model, $url,'glyphicon-trash');
                    },
                ],
                'template' => '{view} {update} {delete}{link}'
            ],
        ],
    ]); ?>
</div>