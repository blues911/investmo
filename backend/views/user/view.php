<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 17:34
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Просмотр').': '.$model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список пользователей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'first_name',
            'last_name',
            'third_name',
            'company_name',
            'phone',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'email:email',
            ['label' => 'Status',
                'format' => 'raw',
                'value' => function($data){
                    if($data->status == 10)
                        return 'Пользователь';
                    if ($data->status == 30)
                        return 'Администратор';
                }],
//             'created_at',
//             'updated_at',
        ],
    ]) ?>
    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Группы пользователя</th>
        </tr>
        <?php
        $groups = $model->getUserGroups();
        $li = null;
        if($groups){
            foreach ($groups as $group){
                echo ' <tr>
            <td>'.$group->name.'</td>
        </tr>';
            }
        } ?>
        </tbody>
    </table>
</div>