<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 17:35
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = Yii::t('app', 'Редактирование').': ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Список пользователей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'group_model' => $group_model,
        'groups' => $groups,
        'user_groups' => $user_groups,
        'render' => $render
    ]) ?>

    <?php if(Yii::$app->user->identity->status == 40 || $model->id == Yii::$app->user->identity->id){
        $model->scenario = 'password';
        echo $this->render('_password_form', [
            'model' => $model,
        ]);
    } ?>


</div>