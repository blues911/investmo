<div class="form-group input-group field-useringroup-group_id">
    <select id="useringroup-group_id" class="form-control" name="UserInGroup[group_id][]" aria-required="true" aria-invalid="false">
    <?php foreach ($groups as $group){
        echo '<option value="'.$group['id'].'" data-name="'.$group['name'].'">'.$group['name'].'</option>';
    } ?>
    </select>
        <span class="input-group-btn">
            <button class="btn btn-default form-field-delete" style=" height: 34px;">
                <span class="glyphicon glyphicon-minus"></span>
            </button>
        </span>
</div>