<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubscribeEmail */

$this->title = Yii::t('app', 'Редактирование') . ': ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рассылка новостей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="subscribe-email-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>