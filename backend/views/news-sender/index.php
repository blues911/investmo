<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use backend\widgets\NewsMailSender;
use backend\widgets\NewsPeriodSender;
use common\models\NewsFeedbackSenderPeriod;
$this->title = Yii::t('app', 'Рассылка новостей');
$this->params['breadcrumbs'][] = $this->title;
/* @var $this yii\web\View */
/* @var $model common\models\Subscribe */
/* @var $form ActiveForm */
$script = <<<JS
    $(document).on('submit', '.vip-news-sender', function(){
        var data = $(this).serialize();
        $.ajax({
            url: '/admin/news-sender/send-news',
            type: 'POST',
            data: data,
            success: function(res){
                alert(res);
            },
            error: function(){
                alert('Что то пошло не так, попробуйде изменить данные');
            }
        });
        return false;
    });
JS;

$this->registerJs($script);
?>
<div class="subscribe-index">
    <?php Pjax::begin(['id' => 'pjax', 'enablePushState' => false]) ?>
    <?php $form = ActiveForm::begin(['id' => 'form', 'action' => ['/news-sender/save'], 'method' => 'POST', 'options' => ['data-pjax' => true]]); ?>
    <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Запросить консультацию'), ['class' => 'btn btn-default'])?>
    </div>
    <?php ActiveForm::end(); ?>
    <?if(isset($result)) echo $result;?>
    <?php Pjax::end() ?>
</div>
<div class="subscribe-email-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div id="news_sender">
        <?= NewsPeriodSender::widget(['period_model' => $period_model, 'type' => 'news-sender']); ?>
        <?= NewsMailSender::widget(['periods' => $periods, 'type' => 'news-sender']); ?>
    </div>
    <p>
        <?= Html::a(Yii::t('app', 'Создать рассылку новостей'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'email:email',
            ['label' => Yii::t('app', 'Неотправленные'),
                'format' => 'row',
                'content' => function($data){
                    $vip_periods =  NewsFeedbackSenderPeriod::getPeriods($data->id, 0);
                    if($vip_periods){
                        $select = '<form class="vip-news-sender" action="/admin/news-sender/send-news" method="post">
                                <input type="hidden" name="NewsMailSenderForm[id]" value="'. $data->id .'">
                                <select class="form-control line-list" name="NewsMailSenderForm[time]">
                                    <option  value="0">'.Yii::t('app', 'Все неотправленные').'</option>';
                        foreach ($vip_periods as $vip_period){
                            $select .= '<option value="' . $vip_period->id . '">' . date('M-d H:i',strtotime($vip_period->start_date)) . ' - ' . date('M-d H:i',strtotime($vip_period->end_date)) . '</option>';
                        }
                        $select .= '</select>';
                        $select .= '<button type="submit" class="send_news btn btn-success line-button">'.Yii::t('app', 'Отправить').'</button>
                            </form>';
                        return $select;
                    }
                }],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
