<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubscribeEmail */

$this->title = Yii::t('app', 'Создание рассылки новостей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рассылка новостей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscribe-email-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>