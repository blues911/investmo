<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Заявки на размещение');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'name',
                [
                    'attribute' => 'request_status',
                    'format'=>'raw',
                    'value'=> function ($model) {
                        $status = null;
                        switch($model->request_status) {
                            case $model::STATUS_APPROVED:
                                $status = 'Размещено';
                                break;
                            case $model::STATUS_REJECTED:
                                $status = 'Отклонено';
                                break;
                            default:
                                $status = 'На рассмотрении';
                                break;
                        }
                        return $status;
                    },
                    'filter' => array(1 => 'На рассмотрении', 2 => 'Размещено', 3 => 'Отклонено'),
                ],
                [
                    'attribute' => 'object_type_id',
                    'format'=>'raw',
                    'value'=> function ($model) {
                        return $model->getObjectTypeName();
                    },
                    'filter' => \common\models\ObjectType::getTypeNameList(),
                ],
                [
                    'attribute' => 'updated_at',
                    'format'=>'raw',
                    'value'=> function ($model) {
                        return Yii::$app->formatter->asDate($model->updated_at,'long');
                    },
                ],
                //'created_at',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'Действия',
                    'headerOptions' => ['width' => '80'],
                    'buttons' => [
                        'view' => function ($url) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                        },
                        'update' => function ($url,$model) {
                            return \common\models\Object::checkObjectGroups($model->user_id, $url, 'glyphicon-pencil');
                        },
                        'delete' => function ($url,$model) {
                            return \common\models\Object::checkObjectGroups($model->user_id, $url, 'glyphicon-trash');
                        },
                    ],
                    'template' => '{view} {update} {delete}{link}'
                ],
            ],
        ]); ?>
    <?php Pjax::end(); ?></div>

</div>