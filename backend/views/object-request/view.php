<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ObjectRequest;

/* @var $this yii\web\View */
/* @var $model common\models\Object */

$this->title = Yii::t('app', 'Просмотр').': '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Заявки на размещение'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="object-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h3>Информация о заявке</h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user.username',
                'label' => 'Пользователь',
                'value' => function($model) {
                    if($model->user){
                        if ($model->user->first_name && $model->user->last_name) {
                            return $model->user->first_name ." ". $model->user->last_name;
                        } else {
                            return NULL;
                        }
                    }
                    return NULL;
                }
            ],
            [
                'attribute' => 'user.email',
                'label' => 'Email',
            ],
            [
                'attribute' => 'user.phone',
                'label' => 'Телефон',
            ],
            [
                'attribute' => 'request_status',
                'format'=>'raw',
                'value'=> function ($model) {
                    $status = null;
                    switch($model->request_status) {
                        case ObjectRequest::STATUS_APPROVED:
                            $status = 'Размещено';
                            break;
                        case ObjectRequest::STATUS_REJECTED:
                            $status = 'Отклонено';
                            break;
                        default:
                            $status = 'На рассмотрении';
                            break;
                    }
                    return $status;
                },
            ],
            'request_comment:ntext',
            'created_at:datetime',
        ],
    ]) ?>

    <h3>Данные объекта</h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:ntext',
            'latitude',
            'longitude',
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getStatus($model->status?$model->status:'no');
                }
            ],
            'address_address',
            [
                'attribute' => 'owner',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getOwner($model->owner?$model->owner:'no');
                },
            ],
            'owner_name',
            [
                'attribute' => 'bargain_type',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getBargain($model->bargain_type?$model->bargain_type:'no');
                },
            ],
            'bargain_price',
            [
                'attribute' => 'object_type_id',
                'format'=>'raw',
                'value'=> function ($model) {
                    return $model->getObjectTypeName();
                }
            ],
            'square',
            'square_sqm',
            'cadastral_number',
            'distance_to_moscow',
            'square_target',
            [
                'attribute' => 'has_electricity_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_electricity_supply?$model->has_electricity_supply:'no');
                }
            ],
            [
                'attribute' => 'has_gas_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_gas_supply?$model->has_gas_supply:'no');
                }
            ],
            [
                'attribute' => 'has_water_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_water_supply?$model->has_water_supply:'no');
                }
            ],
            [
                'attribute' => 'has_water_removal_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_water_removal_supply?$model->has_water_removal_supply:'no');
                }
            ],
            [
                'attribute' => 'has_heat_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_heat_supply?$model->has_heat_supply:'no');
                }
            ],
            [
                'attribute' => 'can_use_water_drain',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->can_use_water_drain?$model->can_use_water_drain:'no');
                }
            ],
            'electricity_supply_capacity',
            'gas_supply_capacity',
            'water_supply_capacity',
            'water_removal_supply_capacity',
            'heat_supply_capacity',
            [
                'attribute' => 'land_category',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getLandCategoryData($model->land_category?$model->land_category:'no');
                }
            ],
            'vri',
            'area_config',
            'location_feature',
            [
                'attribute' => 'has_buildings',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_buildings?$model->has_buildings:'no');
                }
            ],
            'buildings_count',
            [
                'attribute' => 'has_road_availability',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_road_availability?$model->has_road_availability:'no');
                }
            ],
            'distance_to_nearest_road',
            'distance_to_major_transport_routes',
            [
                'attribute' => 'has_railway_availability',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_railway_availability?$model->has_railway_availability:'no');
                }
            ],
            'distance_to_rw_station',
            'pipe_burial',
            'hotel_available',
            'workforse_available',
            'available_communication_system',
            [
                'attribute' => 'has_charge',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_charge?$model->has_charge:'no');
                }
            ],
            'charge_type',
            [
                'attribute' => 'is_special_ecological_zone',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->is_special_ecological_zone?$model->is_special_ecological_zone:'no');
                }
            ],
            [
                'attribute' => 'danger_class',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getDangerClass($model->danger_class ? $model->danger_class:'no');
                }
            ],
            'phone',
            'owner_contacts',
            'management_company',
            'municipality',
            'municipal_education_contacts',
            'object_contact_face',
            'email',
            'website',
            'worktime',
            'special_business_offer',
            [
                'attribute' => 'property_status',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getPropertyStatus($model->property_status?$model->property_status:'no');
                }
            ],
            'rent_end',
            'renter'
        ],
    ]) ?>
    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Фотографии объекта</th>
        </tr>
        <?php
        $photos = $model->getObjectPhotos();
        $li = null;
        if($photos){
            foreach ($photos as $photo){
        echo ' <tr>
            <td><img src="/uploads/objects/'.$model->id.'/'.$photo->img.'"></td>
        </tr>';
            }
        } ?>
        </tbody>
    </table>
</div>
