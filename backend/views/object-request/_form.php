<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Object;
use common\models\ObjectDocument;
use common\models\ObjectType;
use common\models\Systems;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Object */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="object-form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'request_status')->dropDownList([
            1 => 'На рассмотрении',
            2 => 'Размещено',
            3 => 'Отклонено'
        ]) ?>

        <?= $form->field($model, 'request_comment')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Редактировать'), ['class' => 'btn btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
