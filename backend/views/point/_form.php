<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ConnectionPoint;

/* @var $this yii\web\View */
/* @var $model common\models\ConnectionPoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="connection-point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'latitude')->textInput() ?>

    <?= $form->field($model, 'longitude')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(ConnectionPoint::getStatus()) ?>

    <?= $form->field($model, 'type')->dropDownList(ConnectionPoint::getType()) ?>

    <?= $form->field($model, 'capacity')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
