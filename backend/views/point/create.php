<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ConnectionPoint */

$this->title = Yii::t('app', 'Создание точки подключение');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Точки подключения'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="connection-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
