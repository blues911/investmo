<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ObjectSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="object-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'latitude') ?>

    <?= $form->field($model, 'longitude') ?>

    <?= $form->field($model, 'username') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'address_area') ?>

    <?php // echo $form->field($model, 'address_address') ?>

    <?php // echo $form->field($model, 'address_nearest_place') ?>

    <?php // echo $form->field($model, 'owner') ?>

    <?php // echo $form->field($model, 'bargain_type') ?>

    <?php // echo $form->field($model, 'bargain_price') ?>

    <?php // echo $form->field($model, 'square') ?>

    <?php // echo $form->field($model, 'cadastral_number') ?>

    <?php // echo $form->field($model, 'distance_to_moscow') ?>

    <?php // echo $form->field($model, 'square_target') ?>

    <?php // echo $form->field($model, 'has_electricity_supply') ?>

    <?php // echo $form->field($model, 'has_gas_supply') ?>

    <?php // echo $form->field($model, 'has_water_supply') ?>

    <?php // echo $form->field($model, 'has_water_removal_supply') ?>

    <?php // echo $form->field($model, 'has_heat_supply') ?>

    <?php // echo $form->field($model, 'electricity_supply_capacity') ?>

    <?php // echo $form->field($model, 'gas_supply_capacity') ?>

    <?php // echo $form->field($model, 'water_supply_capacity') ?>

    <?php // echo $form->field($model, 'water_removal_supply_capacity') ?>

    <?php // echo $form->field($model, 'heat_supply_capacity') ?>

    <?php // echo $form->field($model, 'proprietorship_type') ?>

    <?php // echo $form->field($model, 'land_category') ?>

    <?php // echo $form->field($model, 'vri') ?>

    <?php // echo $form->field($model, 'has_buildings') ?>

    <?php // echo $form->field($model, 'buildings_count') ?>

    <?php // echo $form->field($model, 'has_road_availability') ?>

    <?php // echo $form->field($model, 'distance_to_nearest_road') ?>

    <?php // echo $form->field($model, 'has_railway_availability') ?>

    <?php // echo $form->field($model, 'has_charge') ?>

    <?php // echo $form->field($model, 'charge_type') ?>

    <?php // echo $form->field($model, 'is_special_ecological_zone') ?>

    <?php // echo $form->field($model, 'danger_class') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
