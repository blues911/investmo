<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Object;
use common\models\ObjectDocument;
use common\models\ObjectType;
use common\models\Systems;
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

$tTitle = 'Данный параметр позволяет использовать поля объекта указанные в настройках типа. Когда checkbox активнен, то при смене типа объекта будут автоматически подгружаться доступные в нем поля.';
?>

<div class="object-form">

    <?php $params = ['prompt' => '-не выбрано-']; ?>

    <?php $form = ActiveForm::begin([
        'id' => 'object-form',
        'enableAjaxValidation' => true,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>
    <?= $form->field($model, 'status', ['options' => ['id' => 'status']])->dropDownList(Object::getStatus()) ?>
    <?= $form->field($model, 'system_id', ['options' => ['id' => 'system_id']])->dropDownList(ArrayHelper::map(Systems::find()->all(), 'id', 'name')) ?>
    <?= $form->field($model, 'name', ['options' => ['id' => 'name']])->textInput(['maxlength' => true]) ?>
    <? if ($model->type && $model->type->is_child) : ?>
        <?= $form->field($model, 'parent_id', ['options' => ['id' => 'parent_id']])->dropDownList([null => 'Отдельный объект'] + ArrayHelper::map(Object::find()->active()->isContainer()->all(), 'id', 'name'), $params) ?>
    <? endif; ?>
    <?= $form->field($model, 'description', ['options' => ['id' => 'description']])->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'latitude', ['options' => ['id' => 'latitude']])->textInput() ?>
    <?= $form->field($model, 'longitude', ['options' => ['id' => 'longitude']])->textInput() ?>

    <?= $form->field($model, 'address_address', ['options' => ['id' => 'address_address']])->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'object_type_id', ['options' => ['id' => 'object_type_id']])->dropDownList(ArrayHelper::map(ObjectType::find()->all(), 'id', 'name')) ?>
    <?= $form->field($model, 'only_type_fields')->checkbox([
        'id' => 'only_type_fields',
        'data-toggle' => 'tooltip',
        'data-placement' => 'top',
        'title' => $tTitle,
    ]) ?>

    <div id="mutable-fields">
        <?= $form->field($model, 'owner_name', ['options' => ['id' => 'owner_name']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'owner', ['options' => ['id' => 'owner']])->dropDownList(Object::getOwner(),$params) ?>
        <?= $form->field($model, 'bargain_type', ['options' => ['id' => 'bargain_type']])->dropDownList(Object::getBargain(),$params) ?>
        <?= $form->field($model, 'okved', ['options' => ['id' => 'okved']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'bargain_price', ['options' => ['id' => 'bargain_price']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'square', ['options' => ['id' => 'square']])->textInput() ?>
        <?= $form->field($model, 'square_sqm', ['options' => ['id' => 'square_sqm']])->textInput() ?>
        <?= $form->field($model, 'cadastral_number', ['options' => ['id' => 'cadastral_number']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'distance_to_moscow', ['options' => ['id' => 'distance_to_moscow']])->textInput() ?>
        <?= $form->field($model, 'square_target', ['options' => ['id' => 'square_target']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'has_electricity_supply', ['options' => ['id' => 'has_electricity_supply']])->checkbox() ?>
        <?= $form->field($model, 'has_gas_supply', ['options' => ['id' => 'has_gas_supply']])->checkbox() ?>
        <?= $form->field($model, 'has_water_supply', ['options' => ['id' => 'has_water_supply']])->checkbox() ?>
        <?= $form->field($model, 'has_water_removal_supply', ['options' => ['id' => 'has_water_removal_supply']])->checkbox() ?>
        <?= $form->field($model, 'has_heat_supply', ['options' => ['id' => 'has_heat_supply']])->checkbox() ?>
        <?= $form->field($model, 'can_use_water_drain', ['options' => ['id' => 'can_use_water_drain']])->checkbox() ?>
        <?= $form->field($model, 'electricity_supply_capacity', ['options' => ['id' => 'electricity_supply_capacity']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'gas_supply_capacity', ['options' => ['id' => 'gas_supply_capacity']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'water_supply_capacity', ['options' => ['id' => 'water_supply_capacity']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'water_removal_supply_capacity', ['options' => ['id' => 'water_removal_supply_capacity']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'heat_supply_capacity', ['options' => ['id' => 'heat_supply_capacity']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'power_water_drain', ['options' => ['id' => 'power_water_drain']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'land_category', ['options' => ['id' => 'land_category']])->dropDownList(Object::getLandCategoryData(),$params) ?>
        <?= $form->field($model, 'land_status', ['options' => ['id' => 'land_status']])->dropDownList(Object::getLandStatusData(),$params) ?>
        <?= $form->field($model, 'vri', ['options' => ['id' => 'vri']])->textInput() ?>
        <?= $form->field($model, 'area_config', ['options' => ['id' => 'area_config']])->textInput() ?>
        <?= $form->field($model, 'location_feature', ['options' => ['id' => 'location_feature']])->textInput() ?>
        <?= $form->field($model, 'has_buildings', ['options' => ['id' => 'has_buildings']])->checkbox() ?>
        <?= $form->field($model, 'buildings_count', ['options' => ['id' => 'buildings_count']])->textInput() ?>
        <?= $form->field($model, 'has_road_availability', ['options' => ['id' => 'has_road_availability']])->checkbox() ?>
        <?= $form->field($model, 'distance_to_nearest_road', ['options' => ['id' => 'distance_to_nearest_road']])->textInput() ?>
        <?= $form->field($model, 'distance_to_major_transport_routes', ['options' => ['id' => 'distance_to_major_transport_routes']])->textInput() ?>
        <?= $form->field($model, 'has_railway_availability', ['options' => ['id' => 'has_railway_availability']])->checkbox() ?>
        <?= $form->field($model, 'distance_to_rw_station', ['options' => ['id' => 'distance_to_rw_station']])->textInput()  ?>
        <?= $form->field($model, 'pipe_burial', ['options' => ['id' => 'pipe_burial']])->textInput()  ?>
        <?= $form->field($model, 'hotel_available', ['options' => ['id' => 'hotel_available']])->textInput()  ?>
        <?= $form->field($model, 'workforse_available', ['options' => ['id' => 'workforse_available']])->textInput()  ?>
        <?= $form->field($model, 'available_communication_system', ['options' => ['id' => 'available_communication_system']])->textInput()  ?>
        <?= $form->field($model, 'has_charge', ['options' => ['id' => 'has_charge']])->checkbox() ?>
        <?= $form->field($model, 'charge_type', ['options' => ['id' => 'charge_type']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'is_special_ecological_zone', ['options' => ['id' => 'is_special_ecological_zone']])->dropDownList([0 => 'Нет', 1 => 'Да']) ?>
        <?= $form->field($model, 'danger_class', ['options' => ['id' => 'danger_class']])->dropDownList(Object::getDangerClass(),$params) ?>
        <?= $form->field($model, 'flat', ['options' => ['id' => 'flat']])->textInput() ?>
        <?= $form->field($model, 'flat_height', ['options' => ['id' => 'flat_height']])->textInput() ?>
        <?= $form->field($model, 'industry_type', ['options' => ['id' => 'industry_type']])->dropDownList(Object::getIndustryTypes(),$params) ?>
        <?= $form->field($model, 'phone', ['options' => ['id' => 'phone']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'owner_contacts', ['options' => ['id' => 'owner_contacts']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'management_company', ['options' => ['id' => 'management_company']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'municipality', ['options' => ['id' => 'municipality']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'municipal_education_contacts', ['options' => ['id' => 'municipal_education_contacts']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'object_contact_face', ['options' => ['id' => 'object_contact_face']])->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'email', [
            'options' => ['id' => 'email'],
            'template' => "{label}\n<span class='text-muted'>*cписок адресов через запятую</span>\n{input}\n{hint}\n{error}"
        ])->textInput() ?>
        <?= $form->field($model, 'website', [
            'options' => ['id' => 'website'],
            'template' => "{label}\n<span class='text-muted'>*пример: http://example.ru или https://www.example.ru/admin/object/update?id=55</span>\n{input}\n{hint}\n{error}"
        ])->textInput() ?>
        <?= $form->field($model, 'worktime', ['options' => ['id' => 'worktime']])->textInput() ?>

        <?= $form->field($model, 'special_business_offer', ['options' => ['id' => 'special_business_offer']])->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'property_status', ['options' => ['id' => 'property_status']])->dropDownList(Object::getPropertyStatus(),$params) ?>
        <?php if($model->rent_end) $model->rent_end = date('d.m.Y', strtotime($model->rent_end)); ?>
        <?= $form->field($model, 'rent_end', ['options' => ['id' => 'rent_end']])->widget(DatePicker::class, [
            'name' => 'rent_end',
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy',
                'autoclose' => true,
                'todayHighlight' => true
            ]
        ]) ?>
        <?= $form->field($model, 'renter', ['options' => ['id' => 'renter']])->textInput() ?>
        <?= Html::activeHiddenInput($model, 'tempId') ?>
        <!-- upload photos -->
        <?php
        $initialPreviews = [];
        $initialPreviewsConfig = [];
        foreach (\common\models\ObjectPhoto::find()->where(['object_id' => $model->id])->all() as $file) {
            if ($file->exists()) {
                $initialPreviews[] = $file->getUrl();
                $initialPreviewsConfig[] = [
                    'caption' => $file->getUrl(),
                    'size' => 124,
                    'url' => Url::to(['download/delete', 'id' => $file->id]),
                ];
            }
        }
        ?>
        <?= $form->field($model, 'photos[]', ['options' => ['id' => 'photos']])->widget(FileInput::class, [
            'name' => 'photos[]',
            'options' => [
                'multiple' => true
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'showDelete' => false,
                'uploadUrl' => Url::toRoute(['/download/upload']),
                'uploadExtraData' => [
                    'class' => $model->className(),
                    'field' => 'photos',
                    'id' => $model->id ? $model->id : false,
                    'tempId' => $model->id ? false : $model->getTempId(),
                ],
                'uploadAsync' => true,
                'initialPreview' => $initialPreviews,
                'initialPreviewConfig' => $initialPreviewsConfig,
                'initialPreviewAsData' => true,
                'initialCaption' => "Выберите фото",
                'overwriteInitial' => false,
                'maxFileSize' => 2800,
                'maxFileCount' => false,
                'autoReplace' => false,
            ]
        ]); ?>
        <!-- end upload photos -->
        <!-- upload documents -->
        <?php
        $docInitPreviews = [];
        $docInitPreviewsConfig = [];
        foreach (ObjectDocument::find()->where(['object_id' => $model->id])->all() as $file) {
            if ($file->exists()) {
                $docInitPreviews[] = '<div class="file-preview-other"><span class="file-other-icon"><i class="glyphicon glyphicon-file"></i></span></div>';
                $docInitPreviewsConfig[] = [
                    'caption' => $file->title.'.'.$file->format,
                    'size' => $file->size,
                    'url' => Url::to(['object/delete-document', 'id' => $file->id]),
                ];
            }
        }
        ?>
        <?= $form->field($model, 'documents[]', ['options' => ['id' => 'documents']])->widget(FileInput::class, [
            'name' => 'documents[]',
            'options' => [
                'multiple' => true
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'showDelete' => false,
                'uploadUrl' => Url::toRoute(['/object/upload-document']),
                'uploadExtraData' => [
                    'class' => $model->className(),
                    'object_id' => ($model->id) ?: false,
                ],
                'uploadAsync' => true,
                'allowedFileExtensions' => ['doc', 'docx', 'pdf', 'ppt', 'pptx', 'rar', 'zip'],
                'initialCaption' => "Выберите документ",
                'initialPreview' => $docInitPreviews,
                'initialPreviewConfig' => $docInitPreviewsConfig,
                'overwriteInitial' => false,
                'maxFileSize' => 50000,
                'maxFileCount' => false,
                'autoReplace' => false,
            ]
        ])->label('Документы (.doc, .docx, .pdf, .ppt, .pptx, .rar, .zip)'); ?>
        <!-- end upload documents -->
    </div>

    <div id="preloader" class="text-center" style="display:none;">
        <img src="/backend/web/ajax-loader.gif" alt="">
    </div>

    <hr/>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php
    $script1 = <<< JS
    $('#object-photos').on('fileloaded', function(event, file, previewId, index, reader) {
        $('#object-photos').fileinput('upload');
        $('#object-photos').fileinput('disable');
    });
    $('#object-photos').on('fileuploaded', function(event, file, previewId, index, reader) {
        $('#object-photos').fileinput('enable');
    });
JS;
    $script2 = <<< JS
    $('#object-documents').on('fileloaded', function(event, file, previewId, index, reader) {
        $('#object-documents').fileinput('upload');
        $('#object-documents').fileinput('disable');
    });
    $('#object-documents').on('fileuploaded', function(event, file, previewId, index, reader) {
        $('#object-documents').fileinput('enable');
    });
JS;
    $this->registerJs($script1, yii\web\View::POS_READY);
    $this->registerJs($script2, yii\web\View::POS_READY);
    ?>

    <?php ActiveForm::end(); ?>
</div>

<?php
$js = <<<JS
$(function() {
    var objectForm = $(this).find('#object-form');

    // default checking
    if (objectForm.find('#only_type_fields').attr('checked')) {
        var typeId = objectForm.find('#object-object_type_id').val();
        initFieldsList(typeId, false);
    }

    objectForm.on('click', '#only_type_fields', function() {
        var typeId = objectForm.find('#object-object_type_id').val();
        if (this.checked) {
            initFieldsList(typeId, false);
        } else {
            initFieldsList(typeId, true);
        }
    });

    objectForm.on('change', '#object-object_type_id', function() {
        var typeId = $(this).val();
        var typeFields = objectForm.find('#only_type_fields').val();
        if (typeFields) {
            initFieldsList(typeId, false);
        }else {
            initFieldsList(typeId, true);
        }
    });

    function initFieldsList(id, showAll) {
        var mutableForm = $('#mutable-fields');
        mutableForm.children('div').hide();
        $('#preloader').show();
        setTimeout(function() {
            $.ajax({
                url: '/admin/object-type/get-fields-list/',
                data: {'type_id': id},
                type: 'POST',
                dataType: 'json',
                success: function (result) {
                    if (result.success == true) {
                        if (showAll) {
                            mutableForm.children('div').show();
                        } else {
                            Object.entries(result.text).forEach(function([key, value]){
                                if (value == 1) {
                                    mutableForm.find('#'+key).show();
                                }
                            });
                        }
                    } else {
                        console.log(result);
                    }
                }
            });
            $('#preloader').hide();
        }, 2000);
    }
});
JS;
$this->registerJs($js);
?>