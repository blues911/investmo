<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Object */

$this->title = Yii::t('app', 'Просмотр').': '. $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Объекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h3>Описание объекта</h3>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'system_id',
                'format'=>'raw',
                'value'=> function ($model) {
                    $system_name = $model->getObjectSystemName();
                    return $system_name;
                },
                'filter' => \common\models\Systems::getSystemsList(),
            ],
            'external_id',
            [
                'attribute' => 'username',
                'format'=>'raw',
                'value'=> function ($model) {
                    $user = $model->getObjectUser();
                    return $user ? $user->username: 'admin';
                },
            ],
            [
                'attribute' => 'useremail',
                'format'=>'raw',
                'value'=> function ($model) {
                    $user = $model->getObjectUser();
                    return $user ? $user->email: 'admin@mail.ru';
                },
            ],
            'name',
            'description:ntext',
            'latitude',
            'longitude',
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getStatus($model->status?$model->status:'no');
                }
            ],
            'address_address',
            [
                'attribute' => 'owner',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getOwner($model->owner?$model->owner:'no');
                },
            ],
            'owner_name',
            [
                'attribute' => 'bargain_type',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getBargain($model->bargain_type?$model->bargain_type:'no');
                },
            ],
            'bargain_price',
            [
                'attribute' => 'object_type_id',
                'format'=>'raw',
                'value'=> function ($model) {
                    return $model->getObjectTypeName();
                }
            ],
            'square',
            'square_sqm',
            'cadastral_number',
            'distance_to_moscow',
            'square_target',
            [
                'attribute' => 'has_electricity_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_electricity_supply);
                }
            ],
            [
                'attribute' => 'has_gas_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_gas_supply);
                }
            ],
            [
                'attribute' => 'has_water_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_water_supply);
                }
            ],
            [
                'attribute' => 'has_water_removal_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_water_removal_supply);
                }
            ],
            [
                'attribute' => 'has_heat_supply',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_heat_supply);
                }
            ],
            [
                'attribute' => 'can_use_water_drain',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->can_use_water_drain);
                }
            ],
            'electricity_supply_capacity',
            'gas_supply_capacity',
            'water_supply_capacity',
            'water_removal_supply_capacity',
            'heat_supply_capacity',
            [
                'attribute' => 'land_category',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getLandCategoryData($model->land_category?$model->land_category:'no');
                }
            ],
            [
                'attribute' => 'land_status',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getLandStatusData($model->land_status?$model->land_status:'no');
                }
            ],
            'vri',
            'area_config',
            'location_feature',
            [
                'attribute' => 'has_buildings',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_buildings);
                }
            ],
            'buildings_count',
            [
                'attribute' => 'has_road_availability',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_road_availability);
                }
            ],
            'distance_to_nearest_road',
            'distance_to_major_transport_routes',
            [
                'attribute' => 'has_railway_availability',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_railway_availability);
                }
            ],
            'distance_to_rw_station',
            'pipe_burial',
            'hotel_available',
            'workforse_available',
            'available_communication_system',
            [
                'attribute' => 'has_charge',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->has_charge);
                }
            ],
            'charge_type',
            [
                'attribute' => 'is_special_ecological_zone',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getYesNoAnswer($model->is_special_ecological_zone);
                }
            ],
            [
                'attribute' => 'danger_class',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getDangerClass($model->danger_class ? $model->danger_class:'no');
                }
            ],
            'phone',
            'owner_contacts',
            'management_company',
            'municipality',
            'municipal_education_contacts',
            'object_contact_face',
            'email',
            'website',
            'worktime',
            'special_business_offer',
            [
                'attribute' => 'property_status',
                'format'=>'raw',
                'value'=> function ($model) {
                    return \common\models\Object::getPropertyStatus($model->property_status?$model->property_status:'no');
                }
            ],
            'rent_end',
            'renter'
        ],
    ]) ?>

</div>
