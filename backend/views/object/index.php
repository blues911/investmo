<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use \backend\widgets\ObjectExport;
use \backend\widgets\ObjectImport;
use \backend\widgets\ObjectDuplicat;
use \backend\widgets\ObjectModerator;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ObjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Объекты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-index">
    <?= ObjectDuplicat::widget() ?>
    <?= ObjectImport::widget() ?>
    <?= ObjectExport::widget([
            'object_types' => $object_types,
                'groups' => $groups
    ]) ?>
    <?php if(Yii::$app->user->identity->status == 40){
        echo ObjectModerator::widget([
            'object_types' => $object_types,
            'systems' => $systems
        ]);
    }?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Создать объект'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'object_type_id',
                'format'=>'raw',
                'value'=> function ($model) {
                    return $model->getTypeName();
                },
                'filter' => \common\models\ObjectType::getTypeNameList()
            ],
            'latitude',
            'longitude',
           [
                'attribute' => 'status',
                'format'=>'raw',
                'value'=> function ($model) {
                    $status = null;
                    switch($model->status) {
                        case $model::STATUS_DRAFT:
                            $status = 'Черновик';
                            break;
                        case $model::STATUS_PUBLISHED:
                            $status = 'Опубликован';
                            break;
                        default:
                            $status = 'На обработке';
                            break;
                    }
                    return $status;
                },
                'filter' => array(0 => 'На обработке', 1 => 'Черновик', 2 => 'Опубликован'),
            ],
            [
                'attribute' => 'username',
                'label' => 'Кто создал',
                'format'=>'raw',
                'value'=> function ($model) {
                    $user = $model->getObjectUser();
                    return $user ? $user->username: 'admin';
                },
            ],
            'external_id',
            [
                'attribute' => 'system_id',
                'format'=>'raw',
                'value'=> function ($model) {
                    $system_name = $model->getObjectSystemName();
                    return $system_name;
                },
                'filter' => \common\models\Systems::getSystemsList(),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                    },
                    'update' => function ($url,$model) {
                       return \common\models\Object::checkObjectGroups($model->user_id, $url, 'glyphicon-pencil');
                    },
                    'delete' => function ($url,$model) {
                        return \common\models\Object::checkObjectGroups($model->user_id, $url,'glyphicon-trash');
                    },
                ],
                'template' => '{view} {update} {delete}{link}'
            ],

        ],
    ]); ?>
<?php Pjax::end(); ?></div>
