<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use backend\widgets\CKEditor\MyCKEditor;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => Yii::t('app', 'Дата публикации')],
        'removeButton' => false,
//        'convertFormat' => true,
        'pluginOptions' => [
            'autoclose' => true,
//            'format' => 'dd-M-yyyy H:i:s'
        ]
    ]); ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_desc')->widget(MyCKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'standard',
        'customStyles' => [
            'page' => 'js/ckeditor_style.js'
        ],
        'clientOptions' => [
            'allowedContent' => true,
            //'removeFormatAttributes' => '',
            //'extraPlugins' => 'youtube',
            'filebrowserUploadUrl' => Url::to(['/ckeditor/upload']),
        ],
    ]) ?>

    <?php  if ($model->img):?>
        <img src="<?= $model->getFullImagePath() ?>"/>
    <?php endif; ?>


    <?= $form->field($model, 'img')->fileInput(['accept' => 'image/*']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
