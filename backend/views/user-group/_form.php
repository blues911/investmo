<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\UserGroup */
/* @var $form yii\widgets\ActiveForm */
$js = <<<JS
    $(document).on('click','.form-field-add',function(e){
         e.preventDefault();
         $('.field-user-group').append('<div class="abs-container" style="position: relative;"></div><div class="form-group input-group form-search">'+
                '<input type="text" class="form-control input-group input-search" autocomplete="off">'+
                '<span class="input-group-btn">'+
                                '<button class="btn btn-default form-field-delete" style=" height: 34px;">'+
                                    '<span class="glyphicon glyphicon-minus"></span>'+
                                '</button>'+
                            '</span>'+
                       '</div>');
         
    });
    $(document).on('click','.form-field-delete',function(e){
         e.preventDefault();
        $(this).closest('.form-group').remove();
    });
    $(document).on('keyup','.input-search',function() {
        if($(this).val() == ''){
             $('.abs-container').html('');
        }else{
           $.ajax({
         url:'/admin/user-group/form-field-add?user='+$(this).val(),
         type: 'GET',
            success: function(res){
                $('.abs-container').html(res);
            }
         }) 
        }
         
    });
    $(document).on('click','.user-li',function(e){
         $('.form-search').replaceWith('<div class="form-group input-group">'+
                '<li class="form-control input-group" disabled><b>'+$(this).attr('data-name')+'</b> ('+$(this).attr('data-email')+')</li><input type="hidden" name="UserInGroup[user_id][]" value="'+$(this).val()+'">'+
                 '<span class="input-group-btn">'+
                               '<button class="btn btn-default form-field-delete" style=" height: 34px;">'+
                                    '<span class="glyphicon glyphicon-minus"></span>'+
                                '</button>'+
                            '</span>'+
                        '</div>');
        $('.abs-container').remove();
    });
JS;
$this->registerJs($js);
?>

<div class="user-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
        echo '<div class="field-user-group">
                    <label class="control-label" for="user-group">Пользователи группы</label>
                    <div class="form-group input-group field-useringroup-user_id">
                        <button type="submit" class="btn btn-success form-field-add">Добавить пользователя в группу</button>
                    </div>';
        if(!empty($user_groups[0])) {
            foreach ($user_groups as $user) {
                echo '<div class="form-group input-group">
                <li class="form-control input-group" disabled><b>'.$user->getFullUserName().'</b> ('.$user->email.')</li>
                <input type="hidden" name="UserInGroup[user_id][]" value="'.$user->id.'">
                <span class="input-group-btn">
                                <button class="btn btn-default form-field-delete" style=" height: 34px;">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                        </div>';
            }
        }
        echo '</div>';
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>