<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\UserGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Groups';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
    $(document).on('click', '.get-user-list', function(e){
        e.preventDefault();
        $(this).next('div').find('.abs-block').slideToggle('normal');
    });
JS;
$this->registerJs($js);
?>
<div class="user-group-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php if(Yii::$app->user->identity->status == 40){ ?>
        <p>
            <?= Html::a('Create User Group', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
  <?php  } ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            ['label' => 'Пользователи',
                'format' => 'row',
                'content' => function($data){
                    $users = $data->getGroupUsers();
                    $li = null;
                    if($users){
                        if(count($users) == 1){
                            return '<b>'.$users[0]->getFullUserName().'</b> ('.$users[0]->email.')';
                        }else{
                            foreach ($users as $user){
                                $li .= '<li><b>'.$user->getFullUserName().'</b> ('.$user->email.')</li>';
                            }
                            $list = '<button class="btn btn-default get-user-list full-width">Посмотреть</button>
                                <div style="position: relative">
                                    <div class="abs-block" style="display: none">
                                        <ul>
                                        '.$li.'
                                        </ul>
                                    </div>
                                </div>';
                        }
                        return $list;
                        }

                }],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => Yii::$app->user->identity->status == 40 ?'{view} {update} {delete}{link}':'{view} {link}'
            ],
        ],
    ]); ?>
</div>