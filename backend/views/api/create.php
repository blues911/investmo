<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ApiMethods */

$this->title = 'Create Api Methods';
$this->params['breadcrumbs'][] = ['label' => 'Api Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-methods-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
