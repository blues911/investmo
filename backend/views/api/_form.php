<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\ApiMethods;

/* @var $this yii\web\View */
/* @var $model common\models\ApiMethods */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="api-methods-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'request_type')->dropDownList(ApiMethods::getRequestType()) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'request')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'response')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
