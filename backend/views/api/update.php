<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ApiMethods */

$this->title = 'Update Api Methods: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Api Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="api-methods-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
