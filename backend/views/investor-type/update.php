<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.12.2017
 * Time: 0:26
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\InvestorType */

$this->title = 'Update Investor Type: ' . $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Investor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->text, 'url' => ['view', 'id' => $model->text]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="investor-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'measures' => $measures
    ]) ?>

</div>