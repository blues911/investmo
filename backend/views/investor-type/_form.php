<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.12.2017
 * Time: 0:24
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\InvestorType */
/* @var $form yii\widgets\ActiveForm */

$js = <<<JS
    $(document).on('click','.form-field-add',function(e){
         e.preventDefault();
         var count = $(this).val();
         $('.field-measures').append('<div class="repeater jumbotron"><span class="remove-repeated-field pull-right glyphicon glyphicon-remove"></span>' +
                '<label class="control-label pull-left">Содержание</label>' +
                '<input type="text" class="form-control" name="MeasuresSupport['+count+'][name]" value="">' +
                '<label class="control-label pull-left">Ссылка на страницу</label>' +
                '<input type="text" class="form-control" name="MeasuresSupport['+count+'][page_link]" value="">' +
            '</div>');
         $(this).val(parseInt(count) + 1);
    });
    $(document).on('click','.remove-repeated-field',function(e){
         e.preventDefault();
        $(this).parent().remove();
    });
JS;
$this->registerJs($js);
?>

<div class="investor-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textInput() ?>

    <?= $form->field($model, 'page_link')->textInput() ?>
    <?php  if ($model->img):?>
        <img src="<?= $model->getFullImagePath() ?>"/>
    <?php endif; ?>
    <?= $form->field($model, 'img')->fileInput(['accept' => 'image/*']) ?>


    <div class="field-measures">
        <label class="control-label">Меры поддержки инвесторов</label>
        <div class="form-group input-group">
            <button type="submit" class="btn btn-success form-field-add" value="<?=isset($measures) ? count($measures) + 1 : 1?>">Добавить поддержку</button>
        </div>
       <?php if(!empty($measures[0])) {
        foreach ($measures as $key => $measure) {
            echo '<div class="repeater jumbotron"><span class="remove-repeated-field pull-right glyphicon glyphicon-remove"></span>
                <label class="control-label pull-left">Содержание</label>
                <input type="text" class="form-control" name="MeasuresSupport['.$key.'][name]" value="'.$measure->name.'">
                <label class="control-label pull-left">Ссылка на страницу</label>
                <input type="text" class="form-control" name="MeasuresSupport['.$key.'][page_link]" value="'.$measure->page_link.'">
            </div>';
            }
        }
        echo '</div>'; ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>