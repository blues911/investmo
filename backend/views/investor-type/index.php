<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.12.2017
 * Time: 0:25
 */
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\InvestorTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Investor Types';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-type-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Investor Type', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'img',
            'text:ntext',
            'page_link',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>