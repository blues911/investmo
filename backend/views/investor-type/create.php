<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.12.2017
 * Time: 0:25
 */

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\InvestorType */

$this->title = 'Create Investor Type';
$this->params['breadcrumbs'][] = ['label' => 'Investor Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="investor-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>