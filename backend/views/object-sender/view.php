<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MapFeedBack */

$this->title = Yii::t('app', 'Просмотр').': '.$model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рассылка объектов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-feed-back-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'email:email',
            'min_square',
            'max_square',
            'min_bargain_price',
            'max_bargain_price',
            'min_distance_to_moscow',
            'max_distance_to_moscow',
            'created_at',
            'updated_at',
            'deleted_at',
            'is_deleted',
        ],
    ]) ?>

</div>