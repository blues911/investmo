<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.07.2017
 * Time: 12:08
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MapFeedBack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="map-feed-back-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bargain_type')->checkboxList([
        '1' => 'Продажа',
        '2' => 'Аренда',
    ])->label('Форма реализации') ?>

    <?= $form->field($model, 'min_square')->textInput()->label('Мин. площадь, кв.м') ?>

    <?= $form->field($model, 'max_square')->textInput()->label('Макс. площадь, кв.м') ?>

    <?= $form->field($model, 'type')->checkboxList([
        '1' => 'Индустриальные парки',
        '2' => 'Технопарки',
        '3' => 'ОЭЗ',
        '4' => 'Браунфилды',
        '9' => 'Земельные участки'
    ])->label('Тип объекта') ?>

    <?= $form->field($model, 'min_bargain_price')->textInput(['maxlength' => true])->label('Мин. стоимость, руб.') ?>

    <?= $form->field($model, 'max_bargain_price')->textInput(['maxlength' => true])->label('Мин. стоимость, руб.') ?>

    <?= $form->field($model, 'min_distance_to_moscow')->textInput()->label('Мин.Дистанция до Москвы, км') ?>

    <?= $form->field($model, 'max_distance_to_moscow')->textInput()->label('Макс.Дистанция до Москвы, км') ?>

    <div class="form-group">
        <?= Html::submitButton($model->id =='' ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->id =='' ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>