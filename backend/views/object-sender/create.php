<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MapFeedBack */

$this->title = Yii::t('app', 'Создание новой подписки на объекты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Подписки на объекты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="map-feed-back-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>