<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\widgets\NewsPeriodSender;
use backend\widgets\NewsMailSender;
use common\models\NewsSenderPeriod;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\MapFeedBackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Рассылка объектов');
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
    $(document).on('submit', '.vip-news-sender', function(){
        var data = $(this).serialize();
        $.ajax({
            url: '/admin/object-sender/send-news',
            type: 'POST',
            data: data,
            success: function(res){
                alert(res);
                
            },
            error: function(){
                alert('Что то пошло не так, попробуйде изменить данные');
            }
        });
        return false;
            });
JS;
$this->registerJs($js);
?>
<div class="map-feed-back-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div id="news_sender">
        <?= NewsPeriodSender::widget(['period_model' => $period_model, 'type' => 'object-sender']); ?>
        <?= NewsMailSender::widget(['periods' => $periods, 'type' => 'object-sender']); ?>
    </div>
    <p>
        <?= Html::a(Yii::t('app', 'Создать рассылку объектов'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'email:email',
            'min_square',
            'max_square',
            'min_bargain_price',
             'max_bargain_price',
             'min_distance_to_moscow',
             'max_distance_to_moscow',
//             'created_at',
            ['label' => 'Неотправленные',
              'format' => 'row',
                'content' => function($data){
                $vip_periods =  NewsSenderPeriod::getPeriods($data->id, 0);
                if($vip_periods){
                    $select = '<form class="vip-news-sender" action="/admin/object-sender/send-news" method="post">
                                <input type="hidden" name="NewsMailSenderForm[id]" value="'. $data->id .'">
                                <select class="form-control line-list" name="NewsMailSenderForm[time]">
                                    <option  value="0">'.Yii::t('app', 'Все неотправленные').'</option>';
                    foreach ($vip_periods as $vip_period){
                        $select .= '<option value="' . $vip_period->id . '">' . date('M-d H:i',strtotime($vip_period->start_date)) . ' - ' . date('M-d H:i',strtotime($vip_period->end_date)) . '</option>';
                    }
                    $select .= '</select>';
                    $select .= '<button type="submit" class="send_news btn btn-success line-button">'.Yii::t('app', 'Отправить').'</button>
                            </form>';
                    return $select;
                }
                }],
            // 'updated_at',
            // 'deleted_at',
            // 'is_deleted',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>