<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\MapFeedBackSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="map-feed-back-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'min_square') ?>

    <?= $form->field($model, 'max_square') ?>

    <?= $form->field($model, 'min_bargain_price') ?>

    <?php // echo $form->field($model, 'max_bargain_price') ?>

    <?php // echo $form->field($model, 'min_distance_to_moscow') ?>

    <?php // echo $form->field($model, 'max_distance_to_moscow') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <?php // echo $form->field($model, 'is_deleted') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Найти'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Сбросить'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>