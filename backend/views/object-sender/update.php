<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MapFeedBack */

$this->title = Yii::t('app', 'Редактирование').': ' . $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Рассылка объектов'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->email, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="map-feed-back-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>