<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\FeedBack */

$this->title = Yii::t('app', 'Создание обращения пользователя');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Обращения пользователей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feed-back-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
