<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FeedBack */

$this->title = Yii::t('app', 'Просмотр'). ': ' .$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Обращения пользователей'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="feed-back-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
<!--        --><?//= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>
    <?php
    $attribute = [];
    if($model->data !== null && $model->data != 'null' && $model->data != '[]'){
        foreach (json_decode($model->data) as $key => $value) {
            if (is_array($value)) {
                $value = implode('<br/>', $value);
            }
            $attribute[] = [
                'label' => $model->getDataTranslation($key),
                'value' => $value,
                'format' => 'raw',
            ];
        }
    }
    $attr = [
        'id',
        'lastname',
        'name',
        'middlename',
        'email:email',
        'phone',
    ];
    $attr2 = [
        [
            'label' => Yii::t('app', 'Загруженные файлы'),
            'format'=> 'raw',
            'value' => function($model){
                function formatBytes($size, $precision = 2)
                {
                    $base = log($size, 1024);
                    $suffixes = array('', 'Kb', 'Mb', 'Gb', 'Tb');   
                    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
                }
                if ($model->getFiles()->all()) {
                    $items = [];
                    foreach ($model->getFiles()->all() as $file) {
                        if (!$file->exists()) {
                            continue;
                        }
                        $items[] = Html::a($file->title.'.'.$file->format, ['/uploads/feedback_files/'.$file->feedback_id ."/".$file->file], ['target' => 'blank']).' '.formatBytes($file->size);
                    }
                    return Html::ul($items, ['encode' => false]);
                }
            },
        ],
        'created_at',
        'updated_at',
    ];
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => array_merge($attr, $attribute, $attr2),
    ]) ?>

</div>
