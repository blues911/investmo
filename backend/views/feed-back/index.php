<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\FeedBackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Обращения пользователей');
$this->params['breadcrumbs'][] = $this->title;

$script = <<<JS
    $(document).on('ready pjax:success', function() {
        $('.ajaxDelete').on('click', function(e) {
            e.preventDefault();
            var deleteUrl = $(this).attr('delete-url');
            var pjaxContainer = $(this).attr('pjax-container');
            var result = confirm('Вы точно хотите удалить?');
            if(result) {
                $.ajax({
                    url: deleteUrl,
                    type: 'post',
                    error: function(xhr, status, error) {
                      alert('Ошибка в выполнении запроса.' + xhr.responseText);
                    }
                }).done(function(data) {
                    $.pjax.reload({container: '#' + $.trim(pjaxContainer)});
                });
            }
        });
    });
JS;

$this->registerJs($script);

?>
<div class="feed-back-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php //echo Html::a('Create Feed Back', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(['id' => 'pjax-list']); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'textType',
            'lastname',
            'name',
            'middlename',
            //'data:ntext',
            'email:email',
            'phone',
            'created_at',
             //'updated_at',
            // 'deleted_at',
            // 'is_deleted',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => 'Посмотреть']);
                    },
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', 'javascript:void(0)', ['class' => 'ajaxDelete', 'delete-url' => $url, 'pjax-container' => 'pjax-list', 'title' => 'Удалить']);
                    }
                ],
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
