<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Cluster */

$this->title = Yii::t('app', 'Создание кластера');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кластеры'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cluster-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
