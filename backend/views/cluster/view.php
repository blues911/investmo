<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cluster */

$this->title = Yii::t('app', 'Просмотр') .': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Кластеры'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
<div class="cluster-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Редактировать'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Удалить'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Вы уверены, что хотите удалить запись?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'map_x',
            'map_y',
            'latitude',
            'longitude',
            'quantity',
            'created_at:datetime',
            'updated_at:datetime',
            'is_deleted',
        ],
    ]) ?>

</div>
