<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.10.2017
 * Time: 16:50
 */

namespace backend\widgets;


use yii\base\Widget;

class ObjectExport extends Widget
{
    public $object_types;
    public $groups;

    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('_export_form',[
            'object_types' => $this->object_types,
            'groups' => $this->groups
        ]);
    }
}