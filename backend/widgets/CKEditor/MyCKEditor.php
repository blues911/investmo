<?php
/**
 * @copyright Copyright (c) 2013-2015 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace backend\widgets\CKEditor;

use backend\assets\MyCKEditorWidgetAsset;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class MyCKEditor extends CKEditor
{

    public $customStyles = [];

    protected function initOptions()
    {
        $options = [];
        switch ($this->preset) {
            case 'custom':
                $preset = null;
                break;
            case 'basic':
            case 'full':
            case 'standard':
                $preset = __DIR__ . '/presets/' . $this->preset . '.php';
                break;
            default:
                $preset = __DIR__ . '/presets/standard.php';
        }
        if ($preset !== null) {
            $options = require($preset);
        }
        $this->clientOptions = ArrayHelper::merge($options, $this->clientOptions);
    }

    public function init()
    {
        parent::init();
    }

    /**
     * Registers CKEditor plugin
     * @codeCoverageIgnore
     */
    protected function registerPlugin()
    {
        $js = [];

        $view = $this->getView();

        $bundle = MyCKEditorWidgetAsset::register($view);

        $id = $this->options['id'];

        $options = $this->clientOptions !== false && !empty($this->clientOptions)
            ? Json::encode($this->clientOptions)
            : '{}';

        $js[] = "CKEDITOR.replace('$id', $options);";
        $js[] = "dosamigos.ckEditorWidget.registerOnChangeHandler('$id');";
        //$js[] = "CKEDITOR.plugins.addExternal('youtube', '$bundle->baseUrl/js/youtube/');";
        //$js[] = "CKEDITOR.config.extraPlugins = 'youtube';";

        if (isset($this->clientOptions['filebrowserUploadUrl'])) {
            $js[] = "dosamigos.ckEditorWidget.registerCsrfImageUploadHandler();";
        }

        if(!empty($this->customStyles)){
            foreach($this->customStyles as $name => $file){
                $file = $bundle->baseUrl . '/' . $file;
                $js[] = "CKEDITOR.config.stylesSet = '" . $name . ":" . $file . "'";
            }
        }

        $view->registerJs(implode("\n", $js), \yii\web\View::POS_END);
    }
}