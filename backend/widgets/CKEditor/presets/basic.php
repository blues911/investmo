<?php

/**
 *
 * basic preset returns the basic toolbar configuration set for CKEditor.
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 */
return [
    'height' => 200,
    'toolbarGroups' => [
//        ['name' => 'undo'],
//        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
//        ['name' => 'colors'],
//        ['name' => 'styles'],
//        ['name' => 'links', 'groups' => ['links', 'insert']],
//        ['name' => 'others', 'groups' => ['others', 'about']],

        ['name' => 'clipboard', 'groups' => [ 'clipboard', 'undo' ] ],
		['name' => 'editing', 'groups' => [ 'find', 'selection', 'spellchecker', 'editing' ] ],
		['name' => 'links', 'groups' => [ 'links' ] ],
		['name' => 'insert', 'groups' => [ 'insert' ] ],
		['name' => 'forms', 'groups' => [ 'forms' ] ],
		['name' => 'tools', 'groups' => [ 'tools' ] ],
		['name' => 'document', 'groups' => [ 'mode', 'document', 'doctools' ] ],
		['name' => 'others', 'groups' => [ 'others' ] ],
		//'/',
		['name' => 'basicstyles', 'groups' => [ 'basicstyles', 'cleanup' ] ],
		['name' => 'paragraph', 'groups' => [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] ],
		['name' => 'styles', 'groups' => [ 'styles' ] ],
		['name' => 'colors', 'groups' => [ 'colors' ] ],
		['name' => 'about', 'groups' => [ 'about' ] ]

    ],
//    'toolbar' => [
//        [ 'name' => 'undo', 'items' => ['*']],
//        [ 'name' => 'styles', 'items' => ['Image', 'Youtube']],
//    ],
    'removeButtons' => 'Subscript,Superscript,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe',
    'removePlugins' => 'elementspath',
    'resize_enabled' => false
];
