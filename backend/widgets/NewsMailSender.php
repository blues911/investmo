<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.07.2017
 * Time: 21:26
 */

namespace backend\widgets;


use yii\base\Widget;

class NewsMailSender extends Widget
{
    public $periods;
    public $type;

    public function init()
    {
        parent::init();
    }

    public function run(){
        $select = '<select class="form-control" name="NewsMailSenderForm[time]">
                        <option  value="0">Все неотправленные</option>';
        foreach ($this->periods as $period){
            $select .= '<option  value="' . $period->id . '">' . date('M-d H:i',strtotime($period->start_date)) . ' - ' . date('M-d H:i',strtotime($period->end_date)) . '</option>';
        }
        $select .= '</select>';
        return $this->render('_mail_form',[
            'select' => $select,
            'type' => $this->type
        ]);
    }
}