<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 24.11.2017
 * Time: 1:20
 */

namespace backend\widgets;


use yii\base\Widget;

class ObjectModerator extends Widget
{
    public $systems;
    public $object_types;
    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('_object_moderator',[
            'object_types' => $this->object_types,
            'systems' => $this->systems
        ]);
    }
}