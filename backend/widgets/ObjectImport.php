<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.10.2017
 * Time: 13:53
 */

namespace backend\widgets;


use yii\base\Widget;

class ObjectImport extends Widget
{
    public $file;

    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('_import_form');
    }
}