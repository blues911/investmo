<?php

/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.07.2017
 * Time: 20:39
 */
namespace backend\widgets;

use yii\base\Widget;

class NewsPeriodSender extends Widget
{
    public $period_model;
    public $type;

    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('_period_form',[
            'model' => $this->period_model,
            'type' => $this->type
        ]);
    }
}