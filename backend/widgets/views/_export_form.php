<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.10.2017
 * Time: 16:56
 */
$js = <<<JS
    $(document).on('change','.export_select',function(){
        if($(this).val() == 'my' || $(this).val() == 'group'){
            $('#export_group').attr('disabled',true);
        }else{
             $('#export_group').attr('disabled',false);
        }
    });
JS;
$this->registerJs($js);
?>
<form action="/admin/object/export-objects" method="post" id="export-objects">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken()?>">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Выборка объектов</th>
            <th>Тип объекта</th>
            <th>Группа пользователей</th>
            <th>Название объекта</th>
            <th><a id="empty-export" href="/admin/object/get-empty-export" class="btn btn-primary" style=" width: 100%">Шаблон</a></th>
        </tr>
        </thead>
        <td>
            <div>
                <input id="all" class="export_select" type="radio" name="ObjectExport[select]" value="all" title="Все объекты" <?= Yii::$app->user->identity->status == 40? 'checked':'disabled'?>>
                <label for="all"> Все объекты</label>
            </div>
            <div>
                <input id="my" class="export_select" type="radio" name="ObjectExport[select]" value="my" title="Мои объекты" <?= Yii::$app->user->identity->status != 40? 'checked':''?>>
                <label for="my"> Мои объекты</label>
            </div>
            <div>
                <input id="group" class="export_select" type="radio" name="ObjectExport[select]" value="group" title="Мои группы">
                <label for="group"> Мои группы</label>
            </div>
        </td>
        <td>
            <select id="export_type" name="ObjectExport[type][]" class="form-control" multiple>
                <?php foreach($object_types as $type){ ?>
                    <option value="<?=$type->id?>"><?=$type->name?></option>
                <?php } ?>

            </select>
        </td>
        <td>
            <select id="export_group" name="ObjectExport[group][]" class="form-control" multiple>
                <?php foreach($groups as $group){ ?>
                    <option value="<?=$group->id?>"><?=$group->name?></option>
                <?php } ?>

            </select>
        </td>
        <td>
            <textarea type="text" class="form-control" name="ObjectExport[name]" rows="3" style="height: 82px;    resize: none;"></textarea>
        </td>
        <td>
            <button type="submit" class="btn btn-primary" style="height: 82px; width: 100%">Экспорт</button>
        </td>
    </table>

</form>