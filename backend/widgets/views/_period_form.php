<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.07.2017
 * Time: 20:41
 */
$js = <<<JS
    $(document).on('submit', '#news_sender_period', function(){
        var data = $(this).serialize();
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res){
                alert(res);
            },
            error: function(){
                alert('Что то пошло не так, попробуйде изменить данные');
            }
        });
        return false;
            });
JS;
$this->registerJs($js);
?>
<form action="/admin/<?= $type ?>/settings-update" method="post" id="news_sender_period">
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>Автоматическая рассылка</th>
                <th colspan="2">Интервал Рассылки в часах</th>
            </tr>
        </thead>
        <td>
            <input type="checkbox" class="checkbox_big" name="NewsPeriodSenderForm[check]" value="1" <?=$model->check == 1 ? 'checked': null ?>/>
        </td>
        <td>
            <input type="number" class="form-control" name="NewsPeriodSenderForm[time]" value="<?=$model->time?>" min="1"/>
        </td>
        <td>
            <button type="submit" class="btn btn-primary">Обновить</button>
        </td>
    </table>

</form>