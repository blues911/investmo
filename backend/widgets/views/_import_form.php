<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.10.2017
 * Time: 13:55
 */
$js = <<<JS
       $("#import-objects").submit(function (event) {
        event.preventDefault();
        if ($(this).context.elements.file.files[0].size !== 0){
          var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: $(this).attr('action')+'?type='+document.pressed,
            type: 'POST',
            data: formData,
            success: function (response) {
                alert(response);
                $('#importer').html('').hide();
                window.location = '';
            },
            beforeSend: function() {
             $('#importer').html('<div><strong>Дождитесь окончания импорта объектов</strong> <img src="/backend/web/ajax-loader.gif"></div>').show()
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return true;  
        }else{
            alert('Файл имеет неверный размер')
        }
        
    });
JS;
$this->registerJs($js);
?>
<div id="importer" style="display: none; padding: 20px;"></div>
<form action="/admin/object/import-objects" method="post" id="import-objects" enctype = "multipart/form-data">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken()?>">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th colspan="3">Выберите CSV файл с объектами для импорта</th>
        </tr>
        </thead>
        <td>
            <input type="file" name="file" accept="text/csv" required>
        </td>
        <td>
            <button type="submit" class="btn btn-primary full-width" onClick="document.pressed = this.value" value="delete">Удалить по Id</button>
        </td>
        <td>
            <button type="submit" class="btn btn-primary full-width" onClick="document.pressed = this.value" value="import">Импорт</button>
        </td>

    </table>

</form>