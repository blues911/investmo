<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.10.2017
 * Time: 13:55
 */
$js = <<<JS
    $(document).on('change','.duplicate_select',function(){
        if($(this).val() == 'all'){
            $('.duplicate-field').prop('checked',true);
            $('.duplicate-block').attr('style','opacity: 0.4');
            $('.duplicate-field').attr('onClick','return false');
        }else{
            $('.duplicate-field').removeAttr('onClick');
            $('.duplicate-block').removeAttr('style');
        }  
    });
       $("#duplicate-objects").submit(function (event) {
        event.preventDefault();
        var check_arr = [];
        $(this).find('.duplicate-field').each(function(i) {
          check_arr.push($(this).prop("checked"))
        });
        if(check_arr.indexOf(true) != -1){
           var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: $(this).attr('action')+'?type='+document.pressed,
            type: 'POST',
            data: formData,
            success: function (response) {
                var data = JSON.parse(response);
                alert(data.res);
                if(data.xls)
                    window.location = document.location.protocol+'//'+ document.location.host+'/admin/object/get-dp-file?file='+data.file;
                $('#duplicater').html('').hide();
            },
            beforeSend: function() {
             $('#duplicater').html('<div><strong>Дождитесь окончания поиска дубликатов</strong> <img src="/backend/web/ajax-loader.gif"></div>').show()
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return true;   
        }else{
            alert('Выберите хотя бы один параметр поиска дубликатов')
        }
    });
JS;
$this->registerJs($js);
?>
<div id="duplicater" style="display: none; padding: 20px;"></div>
<form action="/admin/object/get-duplicate-objects" method="post" id="duplicate-objects" enctype = "multipart/form-data">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken()?>">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Критерий выборки</th>
            <th colspan="3">Выберите параметы поика дубликатов</th>
        </tr>
        </thead>
        <td>
            <div>
                <input id="all" class="duplicate_select" type="radio" name="ObjectDuplicate[select]" value="all" title="Все поля" checked>
                <label for="all">Все поля</label>
            </div>
            <div>
                <input id="select" class="duplicate_select" type="radio" name="ObjectDuplicate[select]" value="select" title="Выбранные поля">
                <label for="select">Выбранные поля</label>
            </div>
        </td>
        <td>
            <div>
                <div class="half-width duplicate-block" style="opacity:0.4">
                    <input type="checkbox" name="ObjectDuplicate[field][]" class="duplicate-field" onclick="return false" value="name" checked>
                    <label for="name">Название объекта</label>
                </div>
                <div class="half-width duplicate-block" style="opacity:0.4">
                    <input type="checkbox" name="ObjectDuplicate[field][]" class="duplicate-field" onclick="return false" value="object_type_id" checked>
                    <label for="type">Тип объекта</label>
                </div>

            </div>
            <div>
                <div class="half-width duplicate-block" style="opacity:0.4">
                    <input type="checkbox" name="ObjectDuplicate[field][]" class="duplicate-field" onclick="return false" value="coord" checked>
                    <label for="coord">Координаты объекта</label>
                </div>
                <div class="half-width duplicate-block" style="opacity:0.4">
                     <input type="checkbox" name="ObjectDuplicate[field][]" class="duplicate-field" onclick="return false" value="user_id" checked>
                    <label for="user">Автор объекта</label>
                </div>
            </div>
        </td>
        <td>
            <button type="submit" class="btn btn-primary full-width" style="height: 54px" onClick="document.pressed = this.value" value="duplicate">Только дубликаты</button>
        </td>
        <td>
            <button type="submit" class="btn btn-primary full-width" style="height: 54px" onClick="document.pressed = this.value" value="all">Все объекты</button>
        </td>

    </table>

</form>