<?php
$js = <<<JS
         $(document).on('change','.moderate_select',function(){
            if($(this).val() == 'moder_all'){
                $('.moderate_status').prop('checked',true);
                $('.moderate_status').attr('style','opacity: 0.4');
                $('.moderate_status, .moderate_period, #moderate_object_type').prop('disabled',true);
                $('#moderate_object_type').val('');
            }else{
                $('.moderate_status, .moderate_period, #moderate_object_type').prop('disabled',false);
                $('.moderate_status').removeAttr('style');
            }  
        });
       $("#moderate-objects").submit(function (event) {
        event.preventDefault();
           var formData = new FormData($(this)[0]);
        $.ajax({
            type: "POST",
            url: $(this).attr('action')+'?type='+document.pressed,
            type: 'POST',
            data: formData,
            success: function (response) {
                console.log(response)
                alert(response);
                $('#moderator').html('').hide();
            },
            beforeSend: function() {
             $('#moderator').html('<div><strong>Дождитесь окончания модерации</strong> <img src="/backend/web/ajax-loader.gif"></div>').show()
            },
            cache: false,
            contentType: false,
            processData: false
        });
        return true;   
    });
JS;
$this->registerJs($js);
?>
<div id="moderator"></div>
<form action="/admin/object/moderate-objects" id="moderate-objects" method="post">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken()?>">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>Критерий модерации</th>
            <th>Система</th>
            <th>Тип объекта</th>
            <th colspan="2">Статус</th>
        </tr>
        </thead>
        <td style="width: 30rem;">
            <div>
                <input id="moder_all" class="moderate_select" type="radio" name="ObjectModerator[select]" value="moder_all" title="Все поля" checked>
                <label for="moder_all">Все объекты системы</label>
                <input id="moder_param" class="moderate_select" type="radio" name="ObjectModerator[select]" value="moder_param" title="Параметры">
                <label for="moder_param">Параметры</label>
            </div>
            <div class="d-inline-block">
                <label class="date-label">От</label>
                <div class="date-pick">
                    <?= \kartik\date\DatePicker::widget( [
                        'name' => 'ObjectModerator[start_period]',
                        'options' => ['disabled' => 'true', 'class' => 'moderate_period'],
                        'pluginOptions' => [
                            'format' => 'dd.mm.yyyy',
                            'autoclose' => true,
                            'todayHighlight' => true
                        ]
                    ]) ?>
                </div>

            </div>
            <div class="d-inline-block">
                <label class="date-label">До</label>
                <div class="date-pick">
                    <?= \kartik\date\DatePicker::widget( [
                        'name' => 'ObjectModerator[end_period]',
                        'options' => ['disabled' => 'true', 'class' => 'moderate_period'],
                        'pluginOptions' => [
                            'format' => 'dd.mm.yyyy',
                            'autoclose' => true,
                            'todayHighlight' => true
                        ]
                    ]) ?>
                </div>
            </div>
        </td>
        <td style="width: 30rem">
            <select id="moderate_system" name="ObjectModerator[system][]" class="form-control" style="height: 10rem" multiple>
                <?php foreach($systems as $system){ ?>
                    <option value="<?=$system->id?>"><?=$system->name?></option>
                <?php } ?>

            </select>
        </td>
        <td>
            <select id="moderate_object_type" name="ObjectModerator[type][]" class="form-control" style="height: 10rem" multiple disabled>
                <?php foreach($object_types as $type){ ?>
                    <option value="<?=$type->id?>"><?=$type->name?></option>
                <?php } ?>

            </select>
        </td>
        <td>
            <div>
                <input id="publish" class="moderate_status" type="checkbox" name="ObjectModerator[status][]" style="opacity:0.4" value="2" title="Все поля" checked disabled>
                <label for="publish">Опубликован</label>
            </div>
            <div>
                <input id="moderate" class="moderate_status" type="checkbox" name="ObjectModerator[status][]" style="opacity:0.4" value="0" title="На обработке" checked disabled>
                <label for="moderate">На обработке</label>
            </div>
            <div>
                <input id="draft" class="moderate_status" type="checkbox" name="ObjectModerator[status][]" style="opacity:0.4" value="1" title="Черновик" checked disabled>
                <label for="draft">Черновик</label>
            </div>
        </td>
        <td>
            <div class="btn-group-vertical full-width">
                <button type="submit" class="btn btn-primary full-width" onClick="document.pressed = this.value" value="2">Опубликовать</button>
                <button type="submit" class="btn btn-primary full-width" onClick="document.pressed = this.value" value="0">На обработку</button>
                <button type="submit" class="btn btn-primary full-width" onClick="document.pressed = this.value" value="1">Черновик</button>
            </div>

        </td>

    </table>
</form>