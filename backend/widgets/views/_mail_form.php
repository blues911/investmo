<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.07.2017
 * Time: 21:27
 */
$js = <<<JS
    $(document).on('submit', '#news_sender_hand', function(){
        var data = $(this).serialize();
        var url = $(this).attr('action');
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res){
                alert(res);
            },
            error: function(){
                alert('Что то пошло не так, попробуйде изменить данные');
            }
        });
        return false;
            });
JS;
$this->registerJs($js);
?>
<form action="/admin/<?= $type ?>/send-news" method="post" id="news_sender_hand">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th colspan="2">Временной период</th>
        </tr>
        </thead>
        <td>
            <?=$select?>
        </td>
        <td>
            <button type="submit" class="btn btn-primary">разослать</button>
        </td>
    </table>

</form>
