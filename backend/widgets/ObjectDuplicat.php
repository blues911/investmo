<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 26.10.2017
 * Time: 18:55
 */

namespace backend\widgets;


use yii\base\Widget;

class ObjectDuplicat extends Widget
{
    public $select;
    public $field;

    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('_duplicate_form');
    }
}