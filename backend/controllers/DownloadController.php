<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.05.2017
 * Time: 17:39
 */

namespace backend\controllers;

use common\models\ObjectPhoto;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\UploadedFile;

class DownloadController extends Controller
{
    public function actionIndex($id)
    {
        if(!Yii::$app->user->isGuest){
            $model = $this->findModel($id);

            if (!file_exists($model->path)) {
                throw new NotFoundHttpException('The requested file does not exist.');
            }

            return Yii::$app->response->sendFile($model->path, $model->fileName);
        }
    }

    public function actionUpload()
    {
        if(!Yii::$app->user->isGuest){
            $class = Yii::$app->request->post('class');
            $id = Yii::$app->request->post('id');
            $tempId = Yii::$app->request->post('tempId');
            $field = Yii::$app->request->post('field');

            $model = Yii::createObject([
                'class' => $class
            ]);


            if (isset($id) && $id) {
                $newModel = $model::findOne($id);
                $model = $newModel ? $newModel : $model;
            }


            $files = UploadedFile::getInstances($model, $field);
            $result = [];

            if (isset($tempId) && $tempId) {
                $model->tempId = $tempId;
            }

            $result[] = ObjectPhoto::saveFile($files, $model, 'files');

            \Yii::$app->response->format = 'json';

            return $result;
        }
    }

    public function actionDelete($id)
    {
        if(!Yii::$app->user->isGuest){
            \Yii::$app->response->format = 'json';

            try {
                $model = $this->findModel($id);
                $model->removeFile();
                $model->delete();
            } catch (Exception $e) {
                return ['result' => false];
            }

            return ['result' => true];
        }
    }


    protected function findModel($id)
    {
        if (($model = ObjectPhoto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}