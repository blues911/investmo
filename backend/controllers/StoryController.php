<?php

namespace backend\controllers;

use Yii;
use common\models\SuccessStory;
use common\models\search\SuccessStorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * StoryController implements the CRUD actions for SuccessStory model.
 */
class StoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuccessStory models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $searchModel = new SuccessStorySearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single SuccessStory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new SuccessStory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = new SuccessStory();
            $logoImage = UploadedFile::getInstance($model, 'logo');
            $backgroundImage = UploadedFile::getInstance($model, 'background_img');
            $mainImage = UploadedFile::getInstance($model, 'main_img');

            if (!empty($logoImage)) {
                $oldLogoImage = $model->logo;
                $model->logoImage = UploadedFile::getInstance($model, 'logo');;
            }

            if (!empty($backgroundImage)) {
                $oldBackgroundImage = $model->background_img;
                $model->backgroundImage = UploadedFile::getInstance($model, 'background_img');;
            }

            if (!empty($mainImage)) {
                $oldMainImage = $model->main_img;
                $model->mainImage = UploadedFile::getInstance($model, 'main_img');;
            }

            if ($model->load(Yii::$app->request->post())) {
                if (empty($logoImage)) {
                    unset($model->logo);
                } else {
                    $model->upload($oldLogoImage);
                }

                if (empty($backgroundImage)) {
                    unset($model->background_img);
                } else {
                    $model->upload($oldBackgroundImage);
                }

                if (empty($mainImage)) {
                    unset($model->main_img);
                } else {
                    $model->upload($oldMainImage);
                }

                $model->save();

                if (isset($model->logoImage) || isset($model->backgroundImage)) {
                    $model->upload();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing SuccessStory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = $this->findModel($id);

            $oldAttributes = $model->attributes;

            $logoImage = UploadedFile::getInstance($model, 'logo');
            $backgroundImage = UploadedFile::getInstance($model, 'background_img');
            $mainImage = UploadedFile::getInstance($model, 'main_img');

            if (!empty($logoImage)) {
                $model->logoImage = UploadedFile::getInstance($model, 'logo');;
            }

            if (!empty($backgroundImage)) {
                $model->backgroundImage = UploadedFile::getInstance($model, 'background_img');;
            }

            if (!empty($mainImage)) {
                $model->mainImage = UploadedFile::getInstance($model, 'main_img');;
            }

            if ($model->load(Yii::$app->request->post())) {
                $model->logo = empty($model->logo) ? $oldAttributes['logo'] : $model->logo;
                $model->background_img = empty($model->background_img) ? $oldAttributes['background_img'] : $model->background_img;
                $model->main_img = empty($model->main_img) ? $oldAttributes['main_img'] : $model->main_img;

                $model->save();
                if ($model->logoImage || $model->backgroundImage || $model->mainImage) {
                    $model->upload($oldAttributes);
                    $model->save();
                }

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing SuccessStory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = $this->findModel($id);
            $model->removeImage();
            $model->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the SuccessStory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuccessStory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuccessStory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
