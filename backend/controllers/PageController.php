<?php

namespace backend\controllers;

use common\modules\PageComposer\models\PageBlock;
use Yii;
use common\models\Page;
use common\models\search\SearchPage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $searchModel = new SearchPage();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionCopy($id)
    {
        if(!Yii::$app->user->isGuest){
            $lang = Yii::$app->request->post('lang', false);

            $model = $this->findModel($id);
            $attributes = $model->attributes;
            unset($attributes['id']);

            $newModel = new Page();

            $newModel->setAttributes($attributes, false);
            $newModel->lang = $lang;
            if ($newModel->save()) {
                return $this->redirect(['page/view', 'id' => $newModel->id]);
            }
            return $this->redirect(['page/view', 'id' => $id]);
        }
    }
    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = new Page();
            $model->is_constructor = true;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $blocksRequest = isset(Yii::$app->request->post('Page')['blocks']) ? Yii::$app->request->post('Page')['blocks'] : null;
                if ($blocksRequest != null) {
                    foreach ($blocksRequest as $blockId => $data) {
                        Yii::$app->loadedModules['PageComposer'] = new \common\modules\PageComposer\PageComposer('PageComposer');
                        $block = Yii::$app->loadedModules['PageComposer']->getBlock($blockId);
                        $block->setFields($data);
                        $block->setBlockValue($data);
                        $block->page_id = $model->id;
                        $block->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = $this->findModel($id);
            $blocks = PageBlock::find()->where(['page_id' => $id])->andWhere(['class' => Page::className()])->orderBy('sort')->all();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $blocksRequest = isset(Yii::$app->request->post('Page')['blocks']) ? Yii::$app->request->post('Page')['blocks'] : null;
                if ($blocksRequest != null) {
                    foreach ($blocksRequest as $blockId => $data) {
                        Yii::$app->loadedModules['PageComposer'] = new \common\modules\PageComposer\PageComposer('PageComposer');
                        $block = Yii::$app->loadedModules['PageComposer']->getBlock($blockId);
                        $block->setFields($data);
                        $block->setBlockValue($data);
                        $block->page_id = $model->id;
                        $block->save();
                    }
                }

                PageBlock::setHash($model->id, Page::className());

                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'blocks' => $blocks
                ]);
            }
        }
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $this->findModel($id)->delete();
            PageBlock::deleteAll(['page_id' => $id]);

            return $this->redirect(['index']);
        }
    }
    public function actionChangePageMode(){
        $page_mode = Yii::$app->request->post('page_mode');
        $id = Yii::$app->request->post('page_id');
        if($page_mode){
            if($id != '')
                $blocks = PageBlock::find()->where(['page_id' => $id])->andWhere(['class' => Page::className()])->orderBy('sort')->all();
            else
                $blocks = null;
            return $this->renderAjax('constructor_mode', [
                'blocks' => $blocks
            ]);
        }else{
            if($id != '')
                $model = $this->findModel($id);
            else
                $model = new Page();
            $form = new ActiveForm();
            return $this->renderAjax('html_mode', [
                'model' => $model,
                'form' => $form
            ]);
        }
    }
    public function actionDeleteImage()
    {
        $blockId = Yii::$app->request->post('blockId');
        $imageName = Yii::$app->request->post('imageName');
        $block = PageBlock::findOne($blockId);
        $data = json_decode($block->data, true);
        foreach ($data as &$repeater) {
            if (is_array($repeater)) {
                foreach ($repeater as &$repeatBlock) {
                    if ($repeatBlock['img'] == $imageName) {
                        $repeatBlock['img'] = null;
                    }
                }
            }
        }
        $block->deleteImage($imageName);
        $block->data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $block->save();
        return PageBlock::setHash($block->page_id, Page::className());

    }


    public function actionRenderForm()
    {
        $class = Yii::$app->request->post('class');
        $pageBlock = new PageBlock($class);
        $pageBlock->class = Page::className();
        $pageBlock->save();
        return $this->renderAjax('renderform', [
            'block' => $pageBlock,
        ]);
    }

    public function actionDeleteBlock()
    {
        $blockId = Yii::$app->request->post('id');
        $block = PageBlock::findOne($blockId);
        $pageId = $block->page_id;
        $block->delete();
        return PageBlock::setHash($pageId, Page::className());
    }
    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
