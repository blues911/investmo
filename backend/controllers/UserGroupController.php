<?php

namespace backend\controllers;

use common\models\User;
use common\models\UserInGroup;
use Yii;
use common\models\UserGroup;
use common\models\search\UserGroupSearch;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserGroupController implements the CRUD actions for UserGroup model.
 */
class UserGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            $searchModel = new UserGroupSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single UserGroup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new UserGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            if(Yii::$app->user->identity->status == 40){
                $model = new UserGroup();
                if ($model->load(Yii::$app->request->post())) {

                    if($model->save()) {
                        $group_id = Yii::$app->db->lastInsertID;
                        $group_model = new UserInGroup();
                        $user_ids = Yii::$app->request->post('UserInGroup')['user_id'] ? Yii::$app->request->post('UserInGroup')['user_id'] : [];
                        $super_admins = User::find()->where(['status' => 40])->asArray()->all();
                        foreach ($super_admins as $admin){
                            $su_ids[] = $admin['id'];
                        }
                        $user_ids = array_diff($user_ids, $su_ids);
                        $group_model->user_id = $user_ids;
                        $group_model->group_id = $group_id;
                        if ($group_model->saveGroupUsers()) {
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }else {
                return $this->render('create', [
                    'model' => $model,
                ]);
                }
            }else{
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Updates an existing UserGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            if (Yii::$app->user->identity->status == 40) {
                $model = $this->findModel($id);
                $user_groups = $model->getGroupUsers();
                if ($model->load(Yii::$app->request->post())) {
                    if($model->save()){
                        $group_model = new UserInGroup();
                        $user_ids = Yii::$app->request->post('UserInGroup')['user_id'] ? Yii::$app->request->post('UserInGroup')['user_id'] : [];
                        $super_admins = User::find()->where(['status' => 40])->asArray()->all();
                        foreach ($super_admins as $admin){
                            $su_ids[] = $admin['id'];
                        }
                        $user_ids = array_diff($user_ids, $su_ids);
                        $group_model->user_id = $user_ids;
                        $group_model->group_id = $id;
                        if($group_model->saveGroupUsers()){
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }else{
                        $errors = $model->getErrors();
                        $er = null;
                        foreach ($errors as $key => $value) {
                            $er .= $key.': '.$value[0].'<br>';
                        }
                        return $er;
                    }
                } else {
                    return $this->render('update', [
                        'model' => $model,
                        'user_groups' => $user_groups
                    ]);
                }
            }else{
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Deletes an existing UserGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            if (Yii::$app->user->identity->status == 40){
                $this->findModel($id)->delete();
                UserInGroup::deleteAll(['group_id' => $id]);
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Finds the UserGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserGroup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionFormFieldAdd($user){
        $users = User::find()->where(['LIKE','first_name',$user])
            ->orWhere(['LIKE','last_name',$user])
            ->orWhere(['LIKE','third_name',$user])
            ->orWhere(['LIKE','email',$user])
            ->orWhere(['LIKE','username',$user])
            ->andWhere(['<>','status', 40])->all();
        return $this->renderPartial('abs-add',[
            'users' => $users
        ]);
    }
}