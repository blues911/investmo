<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\Url;

class CkeditorController extends Controller
{
    public function actionUpload()
    {
        if(!Yii::$app->user->isGuest){
            $file = UploadedFile::getInstanceByName('upload');
            $fileName = uniqid() . '.' . $file->extension;
            $filePath = Yii::getAlias('@backend') . '/web/uploads/ckeditor/' . $fileName;
            $fileUrl = Url::to('/uploads/ckeditor/'. $fileName);

            if ($file==null) {
                $message = "No file uploaded.";
            } else if ($file->size == 0) {
                $message = "The file is of zero length.";
            } else if (!in_array($file->type, ["image/jpeg", "image/jpg", "image/png", "image/bmp"])) {
                $message = "The image must be in either not format. Please upload normal format.";
            } else if ($file->tempName == null) {
                $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
            } else {
                $message = "";
                $move = $file->saveAs($filePath);
                if(!$move) {
                    $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
                }
            }

            $funcNum = Yii::$app->request->get('CKEditorFuncNum');
            echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$fileUrl', '$message');</script>";
        }
    }
}