<?php

namespace backend\controllers;

use backend\widgets\ObjectImport;
use common\models\ExportToExcel;
use common\models\InvestHelpers;
use common\models\ObjectPhoto;
use common\models\ObjectRequest;
use common\models\ObjectType;
use common\models\Systems;
use common\models\UploadModel;
use common\models\UserGroup;
use common\models\UserInGroup;
use Yii;
use common\models\Object;
use common\models\search\ObjectSearch;
use common\models\ObjectDocument;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ObjectUploadForm;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\User;
use yii\widgets\ActiveForm;

/**
 * ObjectController implements the CRUD actions for Object model.
 */
class ObjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Object models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $searchModel = new ObjectSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $object_types = ObjectType::find()->all();
            $systems = Systems::find()->all();
            $groups = UserGroup::find()->all();
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'object_types' => $object_types,
                'systems' => $systems,
                'groups' => $groups
            ]);
        }
    }

    /**
     * Displays a single Object model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Object model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = new Object();

            if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $date = date('Y-m-d H:i:s', time());
                $model->user_id = Yii::$app->user->id;
                $model->is_back = 1;
                $model->created_at = $date;
                $model->updated_at = $date;
                $model->actuality_date = $date;
                $model->request_status = ObjectRequest::STATUS_APPROVED;
                if($model->save()){
                    $id = $model->id;
                    $model->updateTempPhotos($id);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Object model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        } else {

            $model = $this->findModel($id);
            if(Object::checkObjectGroups($model->user_id)){
                if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return ActiveForm::validate($model);
                }
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    $date = date('Y-m-d H:i:s', time());
                    $model->updated_at = $date;
                    $model->actuality_date = $date;
                    if($model->save()){
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
            }else{
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Deletes an existing Object model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = $this->findModel($id);
            if(Object::checkObjectGroups($model->user_id)){
                $model->delete();
                $object_photo = ObjectPhoto::findAll(['object_id' => $id]);
                foreach ($object_photo as $photo){
                    $photo->removeFile();
                    $photo->delete();
                }
                InvestHelpers::RDir(Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id);
                $object_document = ObjectDocument::findAll(['object_id' => $id]);
                foreach ($object_document as $document){
                    $document->remove($document->id);
                    $document->delete();
                }
                InvestHelpers::RDir(Yii::getAlias('@frontend') . '/web/uploads/objects_documents/' . $id);
            }
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Object model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\models\Object
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Object::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    /**
     * Deletes objects document.
     * @param integer $id document id
     * @return bool
     */
    public function actionDeleteDocument($id)
    {
        if(!Yii::$app->user->isGuest){
            Yii::$app->response->format = 'json';

            try {
                $model = new ObjectDocument;
                $model->remove($id);
            } catch (Exception $e) {
                return ['result' => false];
            }

            return ['result' => true];
        }
    }


    public function actionUploadDocument()
    {
        if(!Yii::$app->user->isGuest){
            Yii::$app->response->format = 'json';

            $class = Yii::$app->request->post('class');
            $objectId = Yii::$app->request->post('object_id');

            $model = Yii::createObject([
                'class' => $class
            ]);

            $files = UploadedFile::getInstances($model, 'documents');

            $objectDocument = new ObjectDocument;
            $result = $objectDocument->upload($files, $objectId);

            return $result;
        }
    }
    public function actionModerateObjects($type = 2){
        if(Yii::$app->user->identity->status == 40){
            ini_set('memory_limit', '8192M');
            $data = Yii::$app->request->post('ObjectModerator');
            $date = date('Y-m-d');
            $updated = 0;
            $where = ['and'];
            if(isset($data['system']))
                $where[] = ['IN','system_id',$data['system']];
            if(isset($data['status']))
                $where[] = ['IN','status',$data['status']];
            if(isset($data['type']))
                $where[] = ['IN', 'object_type_id', $data['type']];
            if(isset($data['start_period']) && $data['start_period'] != null)
                $where[] = ['>', 'updated_at', date('Y-m-d',strtotime($data['start_period']))];
            if(isset($data['end_period']) && $data['end_period'] != null)
                $where[] = ['<', 'updated_at', date('Y-m-d',strtotime($data['end_period']))];
            if(Object::updateAll(['updated_at' => $date,
            'actuality_date' => $date,
            'status' => $type],$where)){
                $updated = Object::find()->where($where)->count();
            }
            $res = 'Успешно обновлено объектов '.$updated."\n";
            return $res;
        }
    }
    private function getExportFields(){
        return [
            'id' => 'ID',
            'name' => 'Название объекта',
            'description' => 'Общие сведения',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'address_address' => 'Адрес',
            'owner' => 'Форма собственности',
            'bargain_type' => 'Форма реализации',
            'bargain_price' => 'Стоимость, руб.',
            'square' => 'Площадь, га',
            'cadastral_number' => 'Кадастровый номер',
            'distance_to_moscow' => 'Расстояние до Москвы, км',
            'square_target' => 'Назначение площади',
            'vri' => 'ВРИ',
            'has_electricity_supply' => 'Наличие электроснабжения',
            'has_gas_supply' => 'Наличие газоснабжения',
            'has_water_supply' => 'Наличие водоснабжения',
            'has_water_removal_supply' => 'Наличие водоотведения',
            'has_heat_supply' => 'Наличие теплоснабжения',
            'electricity_supply_capacity' => 'Мощность электроснабжения, МВт',
            'gas_supply_capacity' => 'Мощность газоснабжения, м3/час',
            'water_supply_capacity' => 'Мощность водоснабжения, м3/день',
            'water_removal_supply_capacity' => 'Мощность водоотведения, м3/день',
            'heat_supply_capacity' => 'Мощность теплоснабжения, ГКал/час',
            'land_category' => 'Категория земель',
            'has_buildings' => 'Наличие строений',
            'buildings_count' => 'Число строений',
            'has_road_availability' => 'Наличие доступа к автодороге',
            'distance_to_nearest_road' => 'Расстояние до автодороги, км',
            'has_railway_availability' => 'Возможность присоединения к грузовой ж/д станции',
            'has_charge' => 'Наличие обременения',
            'charge_type' => 'Вид обременения',
            'is_special_ecological_zone' => 'Расположение в ОЭЗ',
            'danger_class' => 'Класс опасности производства',
            'phone' => 'Контактный телефон',
            'parent_id' => 'Родительский объект',
            'object_type_id' => 'Тип объекта',
            'system_id' => 'ID внешней системы',
            'okved' => 'ОКВЭД',
            'owner_name' => 'Собственник',
            'flat' => 'Этажность',
            'flat_height' => 'Высота потолка, м',
            'industry_type' => 'Сектор промышленности',
            'municipality' => 'Муниципальное образование',
            'land_status' => 'Статус участка',
            'square_sqm' => 'Площадь, кв.м',
            'property_status' => 'Статус объекта имущества',
            'rent_end' => 'Дата окончания действия договора аренды',
            'renter' => 'Наименование арендополучателя',
            'email' => 'E-mail',
            'website' => 'Сайт',
            'worktime' => 'График работы',
            'area_config' => 'Конфигурация участка',
            'location_feature' => 'Характеристика местности',
            'can_use_water_drain' => 'Возможность подключения к ливневой канализации',
            'power_water_drain' => 'Мощность ливневой канализации, м3/час',
            'distance_to_rw_station' => 'Расстояние до ж/д станции, км',
            'pipe_burial' => 'Места захоронений, нефтяной трубопровод, проектируемые линии ж/д',
            'hotel_available' => 'Наличие гостиниц',
            'workforse_available' => 'Наличие рабочей силы',
            'available_communication_system' => 'Наличие системы связи',
            'distance_to_major_transport_routes' => 'Расстояние до крупных транспортных путей, км',
            'special_business_offer' => 'Спецпредложения для бизнеса',
            'owner_contacts' => 'Контакты собственника',
            'municipal_education_contacts' => 'Контакты муниципального образования',
            'object_contact_face' => 'Контактное лицо по объекту',
            'management_company' => 'Управляющая компания'
        ];
    }
    public function actionExportObjects(){
        if(Yii::$app->user->isGuest)
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');

        $data = Yii::$app->request->post('ObjectExport');
        if ($data){
            $objects = Object::find();

            if(isset($data['select']) && $data['select'] == 'all' && Yii::$app->user->identity->status != 40) $data['select'] = 'my';

            if(isset($data['type'])){
                $objects = $objects->where(['IN','object_type_id', $data['type']]);
            }
            if(isset($data['group'])){
                $user_groups = UserInGroup::find()->select(['user_id'])
                    ->where(['IN', 'group_id',$data['group']])->groupBy('user_id')->asArray()->all();
                $user_arr = [];
                foreach ($user_groups as $group)
                    $user_arr[] = $group['user_id'];
                $objects = $objects->andWhere(['IN','user_id', $user_arr]);
            }
            if(isset($data['select']) && $data['select'] == 'group'){
                $my_groups = UserInGroup::find()->select(['group_id'])
                    ->where(['user_id' => Yii::$app->user->identity->id])->asArray()->all();
                $my_group_arr = [];
                foreach ($my_groups as $group)
                    $my_group_arr[] = $group['group_id'];
                $users = UserInGroup::find()->select(['user_id'])
                    ->where(['IN', 'group_id',$my_group_arr])->groupBy('user_id')->asArray()->all();
                $my_user_arr = [];
                foreach ($users as $user)
                    $my_user_arr[] = $user['user_id'];
                $objects = $objects->andWhere(['IN','user_id', $my_user_arr]);
            }
            if(isset($data['select']) && $data['select'] == 'my'){
                $objects = $objects->andWhere(['user_id' => Yii::$app->user->identity->id]);
            }
            $names = explode(',',$data['name']);
            if($names[0]){
                $objects = $objects->andWhere([ 'LIKE','name', $names[0]]);
                for($i=1;$i < count($names);$i++){
                    $objects = $objects->orWhere([ 'LIKE','name', $names[$i]]);
                }
            }
            $objects = $objects->all();
            $fields = $this->getExportFields();
            $export = null;
            foreach ($fields as $key => $name) {
                $key == 'management_company' ? $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$name).'"' : $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$name).'";';
            }
            $export .= "\n";
            foreach ($fields as $key => $name) {
                $key == 'management_company' ? $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$key).'"' : $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$key).'";';
            }
            $export .= "\n";
            foreach ($objects as $object){
                foreach ($fields as $key => $name) {
                    $key == 68 ? $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$object->$key).'"': $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$object->$key).'";';
                }
                $export .= "\n";
            }
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="export_'.time().'.csv"');
            return $export;
        }

    }
    public function actionImportObjects($type){
        if(Yii::$app->user->isGuest)
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        $success = 0;
        $error = 0;
        $created = 0;
        $updated = 0;
        $date = date('Y-m-d H:i:s', time());
        $error_line = [];
        $csv = new UploadModel();
        $csv->file = UploadedFile::getInstancesByName('file');
        if($csv->file){
            $csv_parse = fopen($csv->file[0]->tempName,'r');
            while ( ($data = fgetcsv($csv_parse,0, ';','"') ) !== FALSE ) {
                $csv_data[] = preg_replace('/\t/',"\n",$data);
            }
            for($i=2;$i < count($csv_data);$i++){
                if($csv_data[$i][0]){
                    $object = Object::findOne($csv_data[$i][0]);
                    if(!$object){
                        $object = false;
                        if($type == 'import')
                            $error_line[$i + 1][] = 'id: Попытка обновить несуществующий объект';
                        else
                            $error_line[$i + 1][] = 'id: Попытка удалить несуществующий объект';
                    }else{
                        if(Object::checkObjectGroups($object->user_id) || $object->user_id == Yii::$app->user->identity->id){
                            $object->updated_at = $date;
                            $object->actuality_date = $date;
                        }else{
                            if($type == 'import')
                                $error_line[$i + 1][] = 'error: Попытка обновить объект на который нет прав';
                            else
                                $error_line[$i + 1][] = 'error: Попытка удалить объект на который нет прав';
                        }
                    }
                }else{
                    $object = false;
                    if ($type == 'import'){
                        $object = new Object();
                        $object->user_id = Yii::$app->user->identity->id;
                        $object->created_at = $date;
                        $object->updated_at = $date;
                        $object->actuality_date = $date;
                    }else{
                        $error_line[$i + 1][] = 'id: Не указан идентификатор объекта';
                    }
                }

                if(array_key_exists($csv_data[1][0],$this->getExportFields())){
                    if($type == 'import' && $object){
                        for($n=1;$n < count($csv_data[1]);$n++){
                            if($csv_data[1][$n]){
                                $object->$csv_data[1][$n] = $csv_data[$i][$n] != NULL ? $csv_data[$i][$n]: '';
                            }
                        }
                        $object->status = 2;
                        $object->request_status = 2;
                        $object->is_back = 1;
                    }
                }else{
                    return 'Файл не является корректным CSV файлом импорта объектов';
                }
                if($type == 'import'){
                    if($object){
                        if((Object::checkObjectGroups($object->user_id) || $object->user_id == Yii::$app->user->identity->id) && ($csv_data[$i][0] == $object->id || !$csv_data[$i][0]) && $object->validate() && $object->save()){
                            $success++;
                            if($csv_data[$i][0] == $object->id)
                                $updated++;
                            else
                                $created++;
                        }else{
                            $error++;
                            if($csv_data[$i][0] == $object->id || !$csv_data[$i][0]){
                                foreach ($object->getErrors() as $key => $value) {
                                    $error_line[$i + 1][] = $key.': '.$value[0];
                                }
                            }
                        }
                    }else{
                        $error++;
                    }
                }else{
                    if($object){
                        if((Object::checkObjectGroups($object->user_id) || $object->user_id == Yii::$app->user->identity->id) && ($csv_data[$i][0] == $object->id) && $object->delete()){
                            $success++;
                        }else{
                            $error++;
                            if($csv_data[$i][0] == $object->id){
                                foreach ($object->getErrors() as $key => $value) {
                                    $error_line[$i + 1][] = $key.': '.$value[0];
                                }
                            }
                        }
                    }else{
                        $error++;
                    }
                }
            }
            if ($type == 'import'){
                $res = 'Создано новых объектов '.$created."\n".'Обновлено объектов '.$updated."\n".'Успешно импортировано '.$success.' объектов'."\n";
                if($error > 0){
                    $res .= 'Не удалось импортировать '. $error.' объектов'."\n";
                    foreach ($error_line as $key => $value){
                        $res .= 'Ошибки на строке '.$key."\n";
                        foreach ($value as $item)
                            $res .= $item."\n";
                    }
                }
            }else{
                $res = 'Успешно удалено '.$success.' объектов'."\n";
                if($error > 0){
                    $res .= 'Не удалось удалить '. $error.' объектов'."\n";
                    foreach ($error_line as $key => $value){
                        $res .= 'Ошибки на строке '.$key."\n";
                        foreach ($value as $item)
                            $res .= $item."\n";
                    }
                }
            }

            return $res;
        }
    }
    public function actionGetEmptyExport(){
        if(Yii::$app->user->isGuest)
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');

        $fields = $this->getExportFields();
        $export = null;
        foreach ($fields as $key => $name) {
            $key == 'management_company' ? $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$name).'"' : $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$name).'";';
        }
        $export .= "\n";
        foreach ($fields as $key => $name) {
            $key == 'management_company' ? $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$key).'"' : $export .= '"'.preg_replace(['/"/','/\n/'],['""',"\t"],$key).'";';
        }
        $export .= "\n";
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="export_'.time().'.csv"');
        return $export;
    }
    public function actionGetDuplicateObjects($type){
        if(Yii::$app->user->isGuest)
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        ini_set('memory_limit', '8192M');
        $object_count = 0;
        $data = Yii::$app->request->post('ObjectDuplicate');
        if($data){
            $select[] = 'id';
            foreach ($data['field'] as $field){
                if($field == 'coord'){
                    $select[] = 'longitude';
                    $select[] = 'latitude';
                    $group_by[] = 'LEFT(longitude,6)';
                    $group_by[] = 'LEFT(latitude,6)';
                    $where[] = 'a.longitude RLIKE LEFT(b.longitude,6)';
                    $where[] = 'a.latitude RLIKE LEFT(b.latitude,6)';
                    $dp_where[] = 'latitude IS NOT NULL';
                    $dp_where[] = 'longitude IS NOT NULL';
                }else{
                    $select[] = $field;
                    $group_by[] = $field;
                    $where[] = 'a.'.$field.' = b.'.$field;
                    $dp_where[] = $field.' IS NOT NULL';
                }
            }
            $str_select = implode(', ', $select);
            $str_group_by = implode(', ', $group_by);
            $str_where = implode(' AND ', $where);
            $str_dp_where = implode(' AND ', $dp_where);
            $dp_object_count = (new Query())->select(['count(id)'])->from('object')
                ->where($str_dp_where)
                ->groupBy($group_by)
                ->having(['>','count(id)',1])
                ->orderBy('created_at')->count();
            $objects = Object::find()->from('object a')
                ->join('JOIN','(
                SELECT '.$str_select.', count(id)  FROM object
                WHERE '.$str_dp_where.'
                GROUP BY '.$str_group_by.'
                HAVING count(id) > 1
                ORDER BY created_at) b')->where($str_where);
            if($type == 'duplicate')
                $objects = $objects->andWhere('a.id NOT IN(b.id)')->asArray()->all();
            else
                $objects = $objects->asArray()->all();
            $fields = $this->getExportFields();
            foreach($fields as $key => $value){
                $dataArray[0][] = $value;
                $dataArray[1][] = $key;
                if($key =='land_status'){
                    $dataArray[0][] = 'Автор объекта';
                    $dataArray[1][] = 'user_id';
                }
            }
            if($objects){
                $i = 2;
                foreach ($objects as $object){
                    foreach (array_keys($fields) as $key){
                        $dataArray[$i][] = $object[$key];
                        if($key =='land_status'){
                            $dataArray[$i][] = $object['user_id'];
                        }
                    }
                    $object_count++;
                    $i++;
                }

            }
            $doc = new \PHPExcel();
            $doc->setActiveSheetIndex(0);
            if($dp_object_count == 0){
                $res = 'Найдено: 0 Дубликатов.';
                $xls = false;
            }else{
                $xls = true;
                if($type == 'all')
                    $res = 'Найдено: '.$object_count.' Объектов, '.intval($object_count - $dp_object_count).' Дубликатов.';
                else
                    $res = 'Найдено: '.$object_count.' Дубликатов.';
            }
            $doc->getActiveSheet()->fromArray($dataArray);
            $objWriter = \PHPExcel_IOFactory::createWriter($doc, 'Excel2007');
            $time = time();
            $objWriter->save(Yii::getAlias('@frontend').'/web/uploads/duplicate_objects_'.$time.'.xls');
            return json_encode(['res' => $res,'file' => 'duplicate_objects_'.$time.'.xls', 'xls' => $xls]);
        }
    }
    public function actionGetDpFile($file){
        if(Yii::$app->user->isGuest)
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        $res = file_get_contents('uploads/'.$file);
        unlink(Yii::getAlias('@frontend') . '/web/uploads/'.$file);
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=duplicate_".time().".xls" );
        return $res;
    }
}

