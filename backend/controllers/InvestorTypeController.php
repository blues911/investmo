<?php

namespace backend\controllers;

use common\models\MeasuresSupport;
use Yii;
use common\models\InvestorType;
use common\models\search\InvestorTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InvestorTypeController implements the CRUD actions for InvestorType model.
 */
class InvestorTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestorType models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            $searchModel = new InvestorTypeSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single InvestorType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new InvestorType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            $model = new InvestorType();
            $model->image = UploadedFile::getInstance($model, 'img');
            $measures = Yii::$app->request->post('MeasuresSupport');
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $investor_id = Yii::$app->db->lastInsertID;
                if (isset($model->image)) {
                    $model->upload();
                }
                if($measures){
                    foreach ($measures as $measure){
                        $measure_model = new MeasuresSupport();
                        $measure_model->name = $measure['name'];
                        $measure_model->page_link = $measure['page_link'];
                        $measure_model->investor_type_id = $investor_id;
                        $measure_model->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing InvestorType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            $model = $this->findModel($id);
            $measures = MeasuresSupport::find()->where(['investor_type_id' => $id])->all();
            $uploadedImg = UploadedFile::getInstance($model, 'img');
            if (!empty($uploadedImg)) {
                $oldImage = $model->img;
                $model->image = UploadedFile::getInstance($model, 'img');
            }
            if ($model->load(Yii::$app->request->post())) {
                if (empty($uploadedImg)) {
                    unset($model->img);
                } else {
                    $model->upload($oldImage);
                }
                $model->save();
                MeasuresSupport::deleteAll(['investor_type_id' => $id]);
                $measures_input = Yii::$app->request->post('MeasuresSupport');
                if($measures_input){
                    foreach ($measures_input as $measure){
                        $measure_model = new MeasuresSupport();
                        $measure_model->name = $measure['name'];
                        $measure_model->page_link = $measure['page_link'];
                        $measure_model->investor_type_id = $id;
                        $measure_model->save();
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'measures' => $measures
                ]);
            }
        }
    }

    /**
     * Deletes an existing InvestorType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else {
            $model = $this->findModel($id);
            $model->removeImage();
            $model->delete();
            MeasuresSupport::deleteAll(['investor_type_id' => $id]);
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the InvestorType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvestorType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvestorType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}