<?php

namespace backend\controllers;

use common\models\Settings;
use common\models\UserGroup;
use common\models\UserInGroup;
use Yii;
use common\models\User;
use common\models\search\UserSearch;
use yii\base\Model;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $searchModel = new UserSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            Yii::$app->user->identity->status == 40 ?
                $SU_notify_email = Settings::getSettings('su_notify_email')
                : $SU_notify_email = false;
            $emails = null;
            if($SU_notify_email){
                foreach ($SU_notify_email as $email){
                    $emails .= trim($email->value).', ';
                }
            }
            $emails = trim(substr($emails,0,-2));
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'su_notify_email' => $emails
            ]);
        }
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $groups = UserGroup::find()->orderBy('name ASC')->asArray()->all();
            $user_groups = [];
            $model = new User(['scenario' => 'create']);
            $group_model = new UserInGroup();
            if(isset($_POST['User'])){
                if($_POST['User']['status'] != 40 || Yii::$app->user->identity->status == 40){
                    if ($model->load(Yii::$app->request->post())){
                        $model->setPassword($_POST['User']['password_hash']);
                        $model->generateAuthKey();
                        if ($model->save()){
                            if(Yii::$app->user->identity->status == 40 && $model->status != 40){
                                    $group_model->user_id = Yii::$app->db->lastInsertID;
                                    $group_model->load(Yii::$app->request->post());
                                    if(!$group_model->saveUserGroups())
                                        return 'Ошибка';
                                return $this->redirect(['view', 'id' => $model->id]);
                            }else{
                                return $this->redirect(['view', 'id' => $model->id]);
                            }
                        }else{
                            $errors = $model->getErrors();
                            $er = null;
                            foreach ($errors as $key => $value) {
                                $er .= $key.': '.$value[0].'<br>';
                            }
                            return $er;
                        }
                    }
                }else{
                    return $this->redirect(['index']);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'group_model' => $group_model,
                    'render' => 'create',
                    'groups' => $groups,
                    'user_groups' => $user_groups,
                ]);
            }
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $groups = UserGroup::find()->orderBy('name ASC')->asArray()->all();
            $user_groups = UserInGroup::find()->where(['user_id' => $id])->asArray()->all();
            $model = $this->findModel($id);
            $group_model = new UserInGroup();
            if($model->status != 40 || Yii::$app->user->identity->status == 40){
                if(isset($_POST['User'])){
                    if ($_POST['User']['password_hash']) {
                        if (Yii::$app->user->identity->status == 40 || $model->id == Yii::$app->user->identity->id) {
                            $model->scenario = 'password';
                        } else {
                            return $this->redirect(['index']);
                        }
                    }
                    if(Yii::$app->request->post('User')['status'] != 40 || Yii::$app->user->identity->status == 40) {
                        $group_model->load(Yii::$app->request->post());
                        $group_model->user_id = $id;
                        if ($model->load(Yii::$app->request->post())) {
                            if ($_POST['User']['password_hash']) {
                                if(Yii::$app->user->identity->status == 40 || $model->id == Yii::$app->user->identity->id) {
                                    $model->setPassword($_POST['User']['password_hash']);
                                }
                            }
                            $transaction = Yii::$app->db->beginTransaction();
                            if ($model->save()){
                                if(Yii::$app->user->identity->status == 40 && $model->status != 40){
                                    if($group_model->saveUserGroups() || $model->scenario == 'password'){
                                        $transaction->commit();
                                        return $this->redirect(['view', 'id' => $model->id]);
                                    }else{
                                        $transaction->rollBack();
                                        return 'Ошибка';
                                    }
                                }else{
                                    $transaction->commit();
                                    return $this->redirect(['view', 'id' => $model->id]);
                                }
                            }else{
                                $transaction->rollBack();
                                $errors = $model->getErrors();
                                $er = null;
                                foreach ($errors as $key => $value) {
                                    $er .= $key.': '.$value[0].'<br>';
                                }
                                return $er;
                            }
                        }
                    }else{
                        return $this->redirect(['index']);
                    }
                }else{
                    return $this->render('update', [
                        'model' => $model,
                        'group_model' => $group_model,
                        'groups' => $groups,
                        'user_groups' => $user_groups,
                        'render' => 'update'
                    ]);
                }
            }else{
                return $this->redirect(['index']);
            }
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model =$this->findModel($id);
            if($model->status != 40 || Yii::$app->user->identity->status == 40) {
                $model->delete();
                UserInGroup::deleteAll(['user_id' => $id]);
            }
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExportFile()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $searchModel = new GeoCitySearch();
            $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

            return $this->render('export-file', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }
    public function actionFormFieldAdd(){
        $groups = UserGroup::find()->orderBy('name ASC')->asArray()->all();
        return $this->renderPartial('form-add',[
            'groups' => $groups
        ]);
    }
    public function actionSuNotifyUpdate(){
        $email = Yii::$app->request->post('su_notify_email');
        $email = preg_replace('/\s/','',$email);
        $emails = explode(',', $email);
        if(Settings::saveMultiSettings('su_notify_email', $emails))
            return 'Почта успешно обновленна';
        else
            return 'Не удалось обновить почту';
    }
}