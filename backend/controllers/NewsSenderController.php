<?php

namespace backend\controllers;

use Yii;
use common\models\News;
use common\models\SubscribeEmail;
use common\models\search\SubscribeEmailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\NewsPeriodSenderForm;
use backend\models\NewsMailSenderForm;
use common\models\Settings;
use common\models\NewsFeedbackSenderReg;
use common\models\NewsFeedbackSenderPeriod;
/**
 * SubscribeController implements the CRUD actions for SubscribeEmail model.
 */
class NewsSenderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubscribeEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = new SubscribeEmail();
            $searchModel = new SubscribeEmailSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $period_model = new NewsPeriodSenderForm();
            $period_model->check = Settings::getSettingValue('is_auto_news_feedback_sender');
            $period_model->time = Settings::getSettingValue('news_feedback_sender_period');
            $periods = NewsFeedbackSenderPeriod::getPeriods();
            return $this->render('index', [
                'model' => $model,
                'period_model' => $period_model,
                'periods' => $periods,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single SubscribeEmail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new SubscribeEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = new SubscribeEmail();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing SubscribeEmail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing SubscribeEmail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->isGuest){
            return $this->redirect(Yii::$app->urlManager->baseUrl.'/site/login');
        }else{
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the SubscribeEmail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubscribeEmail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubscribeEmail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionSave()
    {
        if(!Yii::$app->user->isGuest){
            $model = new SubscribeEmail();
            $data = Yii::$app->request->post();
            if($model->load($data) && $model->save())
            {
                return $this->renderAjax('index', ['model' => $model, 'result' => 'success']);
            }
            return $this->renderAjax('index', ['model' => $model, 'result' => 'failed']);
        }
    }
    public function actionSend()
    {
        if(!Yii::$app->user->isGuest){
            Yii::$app->mailer->compose('emailSubscribe')->setFrom('ko22009@yandex.ru')->setTo('ko22009@yandex.ru')->setSubject('Message subject')->send();
        }
    }
    public function actionSettingsUpdate(){
        if(!Yii::$app->user->isGuest){
            if (isset($_POST['NewsPeriodSenderForm'])){
                $model = new NewsPeriodSenderForm();
                $model->attributes = Yii::$app->request->post('NewsPeriodSenderForm');
                if($model->validate() && $model->save('news-sender'))
                    return 'Данные обновлены';
                else
                    return 'Произошла ошибка во время обновления данных.';
            }
        }
    }
    public function VipSendNews($feedback, $time){
        if(!Yii::$app->user->isGuest){
            if($time == null){
                $count_not_sent_period = NewsFeedbackSenderReg::find()->where(['feedback_id' => $feedback->id, 'is_sent' => 0])->count();
                if($count_not_sent_period != 0){
                    $n = 0;
                    while ($n < $count_not_sent_period){
                        $not_sent_periods = NewsFeedbackSenderReg::getAllNotSentPeriods($feedback->id,$n);
                        foreach ($not_sent_periods as $not_sent_period){
                            $news = News::getPeriodNews($not_sent_period->news_feedback_sender_period,$feedback->id, 0);
                            if($news){
                                NewsFeedbackSenderReg::updateAll(['is_sent' => 1],['feedback_id' => $feedback->id, 'news_feedback_sender_period' => $not_sent_period->news_feedback_sender_period]);
                                SubscribeEmail::sendNewsEmail($news, $feedback, Yii::$app->request->hostName);
                            }else{
                                NewsFeedbackSenderReg::deleteAll(['feedback_id' => $feedback->id, 'news_feedback_sender_period' => $not_sent_period->news_feedback_sender_period]);
                            }
                        }
                        $n +=50;
                    }
                }
            }else{
                $news = News::getPeriodNews($time,$feedback->id,0);
                if($news){
                    NewsFeedbackSenderReg::updateAll(['is_sent' => 1],['feedback_id' => $feedback->id, 'news_feedback_sender_period' => $time]);
                    SubscribeEmail::sendNewsEmail($news, $feedback, Yii::$app->request->hostName);
                }else{
                    NewsFeedbackSenderReg::deleteAll(['feedback_id' => $feedback->id, 'news_feedback_sender_period' => $time]);
                }
            }
        }
    }
    public function actionSendNews(){
        if(!Yii::$app->user->isGuest){
            if(isset($_POST['NewsMailSenderForm']['id'])){
                $mail = new NewsMailSenderForm(['scenario' => 'vip']);
                if ($mail->load(Yii::$app->request->post()) && $mail->validate()) {
                    $mail->time == 0 ? $mail->time = null : $mail->time;
                    $feedback = SubscribeEmail::findOne($mail->id);
                    $this->VipSendNews($feedback, $mail->time);
                    return 'Подборка новостей за выбранный период отправлена.';
                }
            }else{
                $mail = new NewsMailSenderForm();
                if ($mail->load(Yii::$app->request->post()) && $mail->validate()){
                    $mail->time == 0 ? $mail->time = null : $mail->time;
                    $count_feedback = SubscribeEmail::find()->count();
                    if($count_feedback != 0){
                        $i = 0;
                        while ($i < $count_feedback){
                            $feedbacks = SubscribeEmail::find()->offset($i)->limit(50)->all();
                            foreach ($feedbacks as $feedback){
                                $this->VipSendNews($feedback, $mail->time);
                            }
                            $i += 50;
                        }
                        return 'Подборка новостей за выбранный период отправлена.';
                    }
                }
            }
        }
    }
}
