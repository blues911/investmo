<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 17.07.2017
 * Time: 22:53
 */


namespace backend\models;

use Yii;
use yii\base\Model;

use common\models\MapFeedBack;
use common\models\MapFeedBackTerm;

class MapFeedBackForm extends Model
{
    public $id;
    public $email;
    public $min_square;
    public $max_square;
    public $bargain_type;
    public $type;
    public $min_bargain_price;
    public $max_bargain_price;
    public $min_distance_to_moscow;
    public $max_distance_to_moscow;
    public $created_at;
    public $updated_at;
    public $is_deleted;
    public $type_object_id;

    public function rules(){
        return [
            ['email', 'email'],
            ['email', 'required'],
            [['min_bargain_price', 'max_bargain_price'], 'number'],
            [['min_square','max_square'], 'double'],
            [['min_distance_to_moscow', 'max_distance_to_moscow'],'integer'],
            [['type','bargain_type'], 'checkInteger']
        ];
    }
    public function checkInteger()
    {
        if (!$this->type) $this->type = [];
        if (!$this->bargain_type) $this->bargain_type = [];
        $arr = array_merge($this->type, $this->bargain_type);
        if($arr)
        {
            foreach ($arr as $num)
            {
                if(!is_numeric($num))
                    return false;
            }
        }
        return true;
    }
    public function checkTerm($term_type)
    {
        if($term_type == 'type_object_id') $term_type = 'type';
        $input_terms = $this->$term_type;
        if($term_type == 'type') $term_type = 'type_object_id';
        if(empty($input_terms[0])) $input_terms = [];
        $terms = MapFeedBackTerm::findAll(['map_feed_back_id' => $this->id, 'term_type' => $term_type]);
        if($terms){
            $base_terms = [];
            foreach ($terms as $term)
                $base_terms[] .= $term->term_id;
        }else
            {
                $base_terms = [];
            }
        foreach (array_diff($base_terms,$input_terms) as $del){
            if(!MapFeedBackTerm::deleteAll(['map_feed_back_id' => $this->id, 'term_id' => $del, 'term_type' => $term_type]))
                return false;
        }
            foreach (array_diff($input_terms,$base_terms) as $trm)
            {
                $map_feed_back_term = new MapFeedBackTerm();
                $map_feed_back_term->map_feed_back_id = $this->id;
                $map_feed_back_term->term_id = $trm;
                $map_feed_back_term->term_type = $term_type;
                if(!$map_feed_back_term->save())
                    return false;
            }
            return true;
    }
    public function save_map_feed_back(){
        if (!$this->validate()) {
            return null;
        }
        $update_date = Date('Y-m-d H:m:s',time());
        if(!$this->created_at)
            $create_date = $update_date;
        else
            $create_date = $this->created_at;
        if($this->id)
            $map_feedback = MapFeedBack::findOne(['id' => $this->id]);
        else
            $map_feedback = new MapFeedBack();
        $map_feedback->email = $this->email;
        $map_feedback->min_square = $this->min_square;
        $map_feedback->max_square = $this->max_square;
        $map_feedback->min_bargain_price = $this->min_bargain_price;
        $map_feedback->max_bargain_price = $this->max_bargain_price;
        $map_feedback->min_distance_to_moscow = $this->min_distance_to_moscow;
        $map_feedback->max_distance_to_moscow = $this->max_distance_to_moscow;
        $map_feedback->created_at = $create_date;
        $map_feedback->updated_at = $update_date;
        $transaction = Yii::$app->db->beginTransaction();
        if($map_feedback->save()){
            if(!$this->id)
                $this->id = Yii::$app->db->lastInsertID;
            if($this->checkTerm('bargain_type') && $this->checkTerm('type_object_id')){
                $transaction->commit();
                return true;
            }else{
                $transaction->rollBack();
                return true;
            }

        }
    }
}