<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 21.07.2017
 * Time: 15:40
 */

namespace backend\models;


use yii\base\Model;

class NewsMailSenderForm extends Model
{
    public $id;
    public $time;

    public function scenarios()
    {
        return [
          'default' => ['time'],
            'vip' => ['id','time']
        ];
    }

    public function rules()
    {
        return [
            ['time', 'required'],
            [['id', 'time'],'integer'],
            ['id', 'required', 'on' => 'vip']
        ];
    }
}