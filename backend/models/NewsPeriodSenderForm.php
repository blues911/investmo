<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.07.2017
 * Time: 13:54
 */

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\Settings;

class NewsPeriodSenderForm extends Model
{
    public $check;
    public $time;

    public function rules()
    {
        return [
                    ['time', 'required'],
                    ['check','boolean'],
                    ['time','integer']
                ];
    }
    public function save($type){
        if ($type == 'news-sender')
            $setting = ['check' => 'is_auto_news_feedback_sender', 'time' => 'news_feedback_sender_period'];
        elseif ($type == 'object-sender')
            $setting = ['check' => 'is_auto_news_sender', 'time' => 'news_sender_period'];
        $check = Settings::getSetting($setting['check']);
        if(!$this->check)
            $this->check = '0';
        if($check){
            $check->value = $this->check;
        }else{
            $check = new Settings();
            $check->name = $setting['check'];
            $check->value = $this->check;
        }
        $time = Settings::getSetting($setting['time']);
        if($time){
            $time->value = $this->time;
        }else{
            $time = new Settings();
            $time->name = $setting['time'];
            $time->value = $this->time;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if($check->save() && $time->save()){
            $transaction->commit();
            return true;
        }else{
            $transaction->rollBack();
            return false;
        }

    }
}