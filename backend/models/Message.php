<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'language'], 'required'],
            [['id'], 'integer'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => SourceMessage::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Язык',
            'translation' => 'Перевод',
        ];
    }
    public static function getGridColumns(){
        $lang = Yii::$app->params['languages'];
        $columns = [
            ['class' => 'yii\grid\SerialColumn'],
            'category',
            'message:ntext'
        ];

        foreach ($lang as $one){
            $columns[] = [
                'label' => $one,
                'value' => 'languages.'.$one,
                'filter' => '<input type="text" class="form-control" name="SourceMessageSearch[languages]['.$one.']">',
            ];
        }

        $columns[] = ['class' => 'yii\grid\ActionColumn'];

        return $columns;
    }
}
