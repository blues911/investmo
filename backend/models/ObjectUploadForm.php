<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\ObjectPhoto;
use common\models\Object;

/**
 * ObjectUploadForm
 */
class ObjectUploadForm extends Model
{
    public $name;
    public $description;
    public $latitude;
    public $longitude;
    public $status;
    public $type;
    public $address_address;
    public $owner;
    public $bargain_type;
    public $bargain_price;
    public $square;
    public $cadastral_number;
    public $distance_to_moscow;
    public $square_target;
    public $has_electricity_supply;
    public $has_gas_supply;
    public $has_water_supply;
    public $has_water_removal_supply;
    public $has_heat_supply;
    public $electricity_supply_capacity;
    public $gas_supply_capacity;
    public $water_supply_capacity;
    public $water_removal_supply_capacity;
    public $heat_supply_capacity;
    public $land_category;
    public $vri;
    public $has_buildings;
    public $buildings_count;
    public $has_road_availability;
    public $distance_to_nearest_road;
    public $has_railway_availability;
    public $has_charge;
    public $charge_type;
    public $is_special_ecological_zone;
    public $danger_class;
    public $phone;
    private $tempId;
    public $photos = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type',], 'required'],
            [['photos'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxFiles' => 2],
        ];
    }


    public function uploadPhoto($objectId)
    {
        if ($this->validate()) {
            foreach ($this->photos as $file) {
                $objectPhoto = new ObjectPhoto();
                $objectPhoto->object_id = $objectId;
                $objectPhoto->img = $file->baseName . '.' . $file->extension;
                $objectPhoto->save();
                $file->saveAs(Yii::getAlias('@frontend') . '/web/uploads/objects/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function createObject($attributes)
    {
        $object = new Object();
        $object->attributes = $attributes;
        $object->save();
        return $object;
    }

    public function getTempId()
    {
        if (!$this->tempId) {
            $this->tempId = md5(time());
        }

        return $this->tempId;
    }

    public function setTempId($value)
    {
        $this->tempId = $value;
    }


}
