<?php
return [
    'adminEmail' => 'admin@example.com',
    'emailFrom' => 'no-replay@digitalwand.ru',
    'languages' => ['en','ru','de']
];