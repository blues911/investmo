CKEDITOR.stylesSet.add( 'page', [

    { name: 'Заголовок h1',		    element: 'h1', attributes: {'class': 'head-title__title head-title__title_big'}},
    { name: 'Заголовок h3',		    element: 'h3'},
    { name: 'Заголовок h4',		    element: 'h4'},


    { name: 'Абзац p',		        element: 'p'},
    { name: 'Абзац серого цвета p',	element: 'p', attributes: {'class': 'color-grey'}},

] );

CKEDITOR.stylesSet.add( 'page_style', [

    { name: 'Заголовок h1',		    element: 'h1', attributes: {'style': 'font-size: 3.625rem;letter-spacing: -.01875rem;font-family: vw_headline,Arial,sans-serif;font-weight: 400;margin: 0 0 0 1.125rem;display: inline-block;vertical-align: middle;margin-left: 0;'}},
    { name: 'Заголовок h3',		    element: 'h3', attributes: {'style': 'font-weight: 400;font-size: 1.5rem;color: #000;margin: 3.75rem 1.25rem 1.25rem;font-family: vw_headline,Arial,sans-serif;letter-spacing: -.00625rem;'}},
    { name: 'Заголовок h4',		    element: 'h4', attributes: {'style': 'font-size: 1.5625rem;line-height: 2.3125rem;color: #535353;margin: 4.375rem 1.25rem;font-weight: 100;'}},


    { name: 'Абзац p',		        element: 'p', attributes: {'style': 'font-size: .9375rem;line-height: 1.5rem;color: #1f1f1f;font-weight: 100;margin: .9375rem 1.25rem;'}},
    { name: 'Абзац серого цвета p',	element: 'p', attributes: {'style': 'font-size: .9375rem;line-height: 1.5rem;color: #1f1f1f;font-weight: 100;margin: .9375rem 1.25rem;opacity: .5;'}},

] );

CKEDITOR.config.entities = false;
CKEDITOR.config.basicEntities = false;
CKEDITOR.config.allowedContent = true;
// CKEDITOR.config.startupMode = 'wysiwyg';
CKEDITOR.config.startupMode = 'source';