var addImageInput;
(function ($) {
    addImageInput = function (event) {
        var $currentInput = $(this).parents('.fileinput');
        var $field = $currentInput.parent();

        var maxImages = $(this).data('max-images');
        var currentImagesCount = $field.find('.fileinput').length;

        if ($(this).val() == "" || $currentInput.find('.id').val() != "" || maxImages == 1 || maxImages >= currentImagesCount) {

        } else if ($currentInput.find('span.fileinput-new').is(':visible')) {
            var $insertAfter = $field.find('.fileinput').last();
            var $clone = $currentInput.clone();

            $clone.find('input[type=file]').on('change.bs.fileinput', addImageInput);
            var thumbnail =  $clone.find('img').attr('data-thumbnail');
            $clone.find('img').attr('src', thumbnail);
            $clone.find('input').val('');
            $clone.find('input').attr('checked', false);
            $clone.insertAfter($insertAfter);

        }
    };

})(jQuery);
