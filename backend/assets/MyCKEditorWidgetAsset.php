<?php
namespace backend\assets;


use yii\web\AssetBundle;

class MyCKEditorWidgetAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/widgets/';

    public $depends = [
        'dosamigos\ckeditor\CKEditorWidgetAsset'
    ];

    public $js = [
        'js/ckeditor_style.js',
        'js/ckeditor_plugin.js',
    ];

}