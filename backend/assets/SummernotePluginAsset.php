<?php

namespace backend\assets;

use yii\web\AssetBundle;

class SummernotePluginAsset extends AssetBundle
{
    public $sourcePath = '@bower/summernote/dist/';
    public $css = [
        'summernote.css'
    ];
    public $js = [
        'summernote.min.js'
    ];
}