<?php

use yii\db\Migration;

class m170726_062122_add_key_to_subscribe_email_table extends Migration
{
//    public function safeUp()
//    {
//
//    }
//
//    public function safeDown()
//    {
//        echo "m170726_062122_add_key_to_subscribe_email_table cannot be reverted.\n";
//
//        return false;
//    }

    public function up()
    {
        $this->addColumn('subscribe_email', 'key', $this->string(60));
    }

    public function down()
    {
        $this->dropColumn('subscribe_email', 'key');
    }
}
