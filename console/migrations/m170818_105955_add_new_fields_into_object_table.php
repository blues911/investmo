<?php

use yii\db\Migration;

class m170818_105955_add_new_fields_into_object_table extends Migration
{
    // public function safeUp()
    // {

    // }

    // public function safeDown()
    // {
    //     echo "m170818_105955_add_new_fiealds_into_object_table cannot be reverted.\n";

    //     return false;
    // }


    public function up()
    {
        $this->addColumn('object', 'property_status', $this->integer(2));
        $this->addColumn('object', 'rent_end', $this->dateTime());
        $this->addColumn('object', 'renter', $this->string());
        $this->addColumn('object', 'email', $this->string());
        $this->addColumn('object', 'website', $this->string());
        $this->addColumn('object', 'worktime', $this->string());
    }

    public function down()
    {
        $this->dropColumn('object', 'property_status');
        $this->dropColumn('object', 'rent_end');
        $this->dropColumn('object', 'renter');
        $this->dropColumn('object', 'email');
        $this->dropColumn('object', 'website');
        $this->dropColumn('object', 'worktime');
    }
}
