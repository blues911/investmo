<?php

use yii\db\Migration;

class m171023_134628_add_actuality_date_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'actuality_date',$this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'actuality_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171023_134628_add_actuality_date_to_object_table cannot be reverted.\n";

        return false;
    }
    */
}
