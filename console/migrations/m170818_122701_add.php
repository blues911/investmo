<?php

use yii\db\Migration;

class m170818_122701_add extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'file_id', $this->integer()->null()->unique());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'file_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170818_122701_add cannot be reverted.\n";

        return false;
    }
    */
}
