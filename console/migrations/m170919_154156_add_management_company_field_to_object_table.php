<?php

use yii\db\Migration;

class m170919_154156_add_management_company_field_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'management_company', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'management_company');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_154156_add_management_company_field_to_object_table cannot be reverted.\n";

        return false;
    }
    */
}
