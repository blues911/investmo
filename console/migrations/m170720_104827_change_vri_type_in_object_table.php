<?php

use yii\db\Migration;

class m170720_104827_change_vri_type_in_object_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('object', 'vri', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('object', 'vri', $this->integer());

        return true;
    }
}
