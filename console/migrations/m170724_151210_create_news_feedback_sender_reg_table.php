<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_feedback_sender_reg`.
 */
class m170724_151210_create_news_feedback_sender_reg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_feedback_sender_reg', [
            'id' => $this->primaryKey(),
            'feedback_id' => $this->integer(11),
            'news_feedback_sender_period' => $this->integer(11),
            'is_sent' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_feedback_sender_reg');
    }
}
