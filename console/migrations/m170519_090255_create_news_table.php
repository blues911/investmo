<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m170519_090255_create_news_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'is_active' => $this->integer()->defaultValue(1),
            'short_desc' => $this->text(),
            'full_desc' => $this->text(),
            'img' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
