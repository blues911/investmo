<?php

use yii\db\Migration;

class m170529_152055_add_currency_to_success_story extends Migration
{
    public function up()
    {
        $this->addColumn('success_story', 'currency', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('success_story', 'currency');
    }
}
