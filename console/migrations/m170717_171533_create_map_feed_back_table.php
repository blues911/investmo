<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map_feed_back`.
 */
class m170717_171533_create_map_feed_back_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('map_feed_back', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255),
            'min_square' => $this->double(),
            'max_square' => $this->double(),
            'min_bargain_price' => $this->decimal(11.2),
            'max_bargain_price' => $this->decimal(11.2),
            'min_distance_to_moscow' => $this->integer(11),
            'max_distance_to_moscow' => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('map_feed_back');
    }
}
