<?php

use yii\db\Migration;

class m170529_110623_add_story_small_image extends Migration
{
    public function up()
    {
        $this->addColumn('success_story', 'main_img', $this->string());
    }

    public function down()
    {
        $this->dropColumn('success_story', 'main_img');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
