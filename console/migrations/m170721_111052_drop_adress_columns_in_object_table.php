<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `adress_columns_in_object`.
 */
class m170721_111052_drop_adress_columns_in_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropColumn('object','address_area');
        $this->dropColumn('object','address_nearest_place');
        $this->dropColumn('object','proprietorship_type');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('object', 'address_area', $this->string());
        $this->addColumn('object', 'address_nearest_place', $this->string());
        $this->addColumn('object', 'proprietorship_type', $this->integer());
    }
}
