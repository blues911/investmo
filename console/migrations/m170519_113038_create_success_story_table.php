<?php

use yii\db\Migration;

/**
 * Handles the creation of table `success_story`.
 */
class m170519_113038_create_success_story_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('success_story', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'is_active' => $this->integer()->defaultValue(1),
            'address' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'logo' => $this->string(),
            'background_img' => $this->string(),
            'investments' => $this->integer(),
            'date_of_start' => $this->dateTime(),
            'country_of_origin' => $this->string(),
            'investment_goal' => $this->string(),
            'short_desc' => $this->text(),
            'full_desc' => $this->text(),
            'html' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('success_story');
    }
}
