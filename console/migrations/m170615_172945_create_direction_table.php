<?php

use yii\db\Migration;

/**
 * Handles the creation of table `direction`.
 */
class m170615_172945_create_direction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('direction', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'address' => $this->string(),
            'img' => $this->string(),
            'slider_img' => $this->string(),
            'description' => $this->string(),
            'set_of_points' => $this->text(),
            'investor_type_id' => $this->integer(),
        ]);

        $this->insert('direction',
            [
                'name' => 'Химическое производство',
                'address' => 'o Щелковский, Люберецкий<br>и Раменский районы',
                'img' => 'https://lh3.googleusercontent.com/bnxQGHhH3cwbe-UeQu2eYYzdmI9-0GdpFhbpPqiS-ObR7z1WrL71wiaYDD7yV5od-d-hgFRpAEJot_6KfRJxtRocdJtVgyNqAqFV1N3mY1efc2ZvC-Sm_q-zwYDAPiSKisgGr_ky',
                'slider_img' => 'svg-symbols.svg#chemical-production',
                'description' => 'В Московской области производится около 5% от всей выпускаемой химической продукции Российской Федерации.<br> В Регионе в основном присутствуют предприятия вторичной переработки и производства конечных изделий, например, производство лаков и красок на базе импортного сырья. Практически вся продукция тонкой химии импортируется из-за рубежа.',
                'set_of_points' => 'Производство высокотехнологичной продукции химической отрасли;Производство агрохимии;Производство косметики;Производство красок;',
                'investor_type_id' => null
            ]);

        $this->insert('direction',
            [
                'name' => 'Стройматериалы и пластики',
                'address' => 'Солнечногорский район, Воскресенский, Коломенский районы и городской округ Егорьевск<br>Солнечногорский, Мытищинский, Пушкинский районы, город Химки',
                'img' => 'https://lh4.googleusercontent.com/YDxPVBkrT2sWPRZLES9_scqulPrldf5Xwofbm1S5-hg6Z88LcyqBfIRHrb_fFqXWDPrS28cQ7j_YczX1n6bJNeU3U4j53NBN5urs1nrTYJ3Q-BbH_A_OJ71m8tT-xF9MBfpeptYJ',
                'slider_img' => 'svg-symbols.svg#building-materials',
                'description' => 'На Московскую область приходится около 15 процентов от всех выпускаемых строительных и отделочных материалов в России. Ключевым драйвером развития отрасли стройматериалов является рост спроса за счет строительства в Москве и Московской области.<br> Предприятия по производству пластиков специализируются на вторичной переработке и производстве готовых изделий. Хорошо развитыми являются сегменты производства пластмассовых изделий для строительства, а также сегмент производства шин.',
                'set_of_points' => 'Производство стекла и изделий из стекла, сыпучих материалов;Производство пластиковых изделий для предприятий;Развитие НИОКР в направлении разработки композиционных материалов с высоким уровнем технических, эстетических и специальных характеристик.',
                'investor_type_id' => null
            ]);


        $this->insert('direction',
            [
                'name' => 'Фармацевтика',
                'address' => 'Серпуховской, Красногорский, Химкинском районы,<br>Щелковский и Ногинский район',
                'img' => 'https://lh3.googleusercontent.com/2-t6E4_vyuqH3CUczQoK1IWPOJNmy48ZOB5-KswviG7WVC63M57rzNL-QAfQip3v71PQ8ht5vSFpmKwqO5EJUsiYeoHwCmCmprCDwr80a_c53lCJDgQAXv1gp0sbJTmo6CDaF_Ff',
                'slider_img' => 'svg-symbols.svg#engineering',
                'description' => 'Фармацевтика и биофармацевтика является отраслью специализации Московской области: на Регион приходится больше трети объема производства всей фармацевтической продукции ЦФО.<br> Ключевой фактор привлекательности Московской области - наличие высококвалифицированных кадров и специализированных НИИ в Пущино, а также большой рынок Москвы и Московской области с растущим спросом на более дорогие лекарственные средства.',
                'set_of_points' => 'Производство инновационных препаратов;Производство БАДов (биологически активных добавок) полного цикла;Создание организаций, специализирующихся на проведении клинических исследований.',
                'investor_type_id' => null
            ]);

        $this->insert('direction',
            [
                'name' => 'Пищевая промышленность',
                'address' => 'Раменский район<br>и вблизи Калужского автокластера',
                'img' => 'https://lh3.googleusercontent.com/SZ3_FJG3Tm7bHDcKLaksYFTj-slasBirkaNDQY86DH3QVWit51gA4rfZyGEkdX0ik8Nia3UCvGOTD7HbYzEHOUa6pvsDTKt-oxjMuEeZlAJ-kckvHGCgTqij3jRWlVKaAj7-XPzu',
                'slider_img' => 'svg-symbols.svg#food',
                'description' => 'Пищевая промышленность является второй по объему выручки отраслью в Московской области и одной из крупнейших по занятости. Большие объемы выручки отрасли объясняются в первую очередь емким рынком Москвы и Московской области с постоянно растущим спросом.',
                'set_of_points' => 'Производство молочной продукции;производство мясной продукции и колбасных изделий;производство экологически чистых продуктов питания;^Производство батончиков и снэков.',
                'investor_type_id' => null
            ]);

        $this->insert('direction',
            [
                'name' => 'Промышленное оборудование',
                'address' => 'o Истринский и Рузский районы<br>и вблизи Калужского автокластера',
                'img' => 'https://lh6.googleusercontent.com/ClL5oz7x7Pqoly_DJyHEW8ad7Lcq72n7UZ0438KVDJkt73_nUWDrrghgWh6glkZFrPebMf2dSaciHBL_wuLaO3SasRX3K4BEbCCLl8EHoDEBAVOwxmf7XYz16_GKK9u0TUR0kwaT',
                'slider_img' => 'svg-symbols.svg#metallurgy',
                'description' => 'Наиболее развитыми в рамках отрасли являются производства промышленных холодильников и вентиляционного оборудования, насосов и компрессоров. Инвестиции в предприятия Московской области составили около трети от инвестиций в ЦФО в 2013 году',
                'set_of_points' => 'Производство приводов и подъемного оборудования;Производство станков;Производство оборудования для пищевой промышленности;Производство компонентов для промышленных машин и оборудования.',
                'investor_type_id' => null
            ]);

        $this->insert('direction',
            [
                'name' => 'Аэрокосмическая промышленность',
                'address' => 'Солнечногорский район<br>и вблизи Калужского автокластера',
                'img' => 'https://lh5.googleusercontent.com/_jsjzqfNPViGYjrUWh5jw79Ha2qBUuyux7V10SZVuNyhWLRGOcVY0BuyH2cha-lyIlRCr92ewG5LU1mu9E3WY4mFzaY6xM6jj1BWZBgFPDhLYOAAyb__ipV-u6Z_o_Q6K3k5WqGi',
                'slider_img' => 'svg-symbols.svg#food',
                'description' => 'Аэрокосмическая промышленность хорошо развита в Московской области. В Регионе, преимущественно в наукограде Королев, сконцентрировано большое количество как крупных производственных предприятий аэрокосмической промышленности, так и конструкторских бюро и исследовательских центров.',
                'set_of_points' => 'Производство компонентов для космических аппаратов и двигателей;Производство вертолетов;Создание испытательных центров;Создание НИОКР-центров для развития разработок в области аэрокосмоса.',
                'investor_type_id' => null
            ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('direction');
    }
}
