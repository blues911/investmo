<?php

use yii\db\Migration;

class m170727_071137_add_image_to_user_table extends Migration
{
//    public function safeUp()
//    {
//
//    }
//
//    public function safeDown()
//    {
//        echo "m170727_071137_add_image_to_user_table cannot be reverted.\n";
//
//        return false;
//    }


    public function up()
    {
        $this->addColumn('user', 'image', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('user', 'image');
    }
}
