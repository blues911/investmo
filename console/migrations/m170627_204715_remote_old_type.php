<?php

use yii\db\Migration;

class m170627_204715_remote_old_type extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('object', 'type');
    }

    public function safeDown()
    {
        echo "m170627_204715_remote_old_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170627_204715_remote_old_type cannot be reverted.\n";

        return false;
    }
    */
}
