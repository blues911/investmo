<?php

use yii\db\Migration;

class m170727_061955_add_user_id_to_object_table extends Migration
{
//    public function safeUp()
//    {
//
//    }
//
//    public function safeDown()
//    {
//        echo "m170727_061955_add_user_id_to_object_table cannot be reverted.\n";
//
//        return false;
//    }


    public function up()
    {
        $this->addColumn('object', 'user_id', $this->integer(11));
    }

    public function down()
    {
        $this->dropColumn('object', 'user_id');
    }
}
