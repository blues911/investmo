<?php

use yii\db\Migration;

class m171204_101002_add_is_constructor_field_to_page_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('page', 'is_constructor',$this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('page', 'is_constructor');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171204_101002_add_is_constructor_field_to_page_table cannot be reverted.\n";

        return false;
    }
    */
}
