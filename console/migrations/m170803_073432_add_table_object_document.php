<?php

use yii\db\Migration;

class m170803_073432_add_table_object_document extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object_document', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'format' => $this->string()->notNull(),
            'size' => $this->string()->notNull(),
            'mime' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('object_document');
    }
}
