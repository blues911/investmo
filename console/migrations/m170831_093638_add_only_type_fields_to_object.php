<?php

use yii\db\Migration;

class m170831_093638_add_only_type_fields_to_object extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'only_type_fields', $this->integer(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'only_type_fields');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170831_093638_add_only_type_fields_to_object cannot be reverted.\n";

        return false;
    }
    */
}
