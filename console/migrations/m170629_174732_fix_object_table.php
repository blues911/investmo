<?php

use yii\db\Migration;

class m170629_174732_fix_object_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('object', 'vri', $this->text()->null());
        $this->alterColumn('object', 'land_category', $this->string(255)->null());
    }

    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170629_174732_fix_object_table cannot be reverted.\n";

        return false;
    }
    */
}
