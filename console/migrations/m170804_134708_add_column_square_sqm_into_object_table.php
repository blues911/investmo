<?php

use yii\db\Migration;

class m170804_134708_add_column_square_sqm_into_object_table extends Migration
{
    public function up()
    {
        $this->addColumn('object', 'square_sqm', $this->double());
    }

    public function down()
    {
        $this->dropColumn('object', 'square_sqm');
    }
}
