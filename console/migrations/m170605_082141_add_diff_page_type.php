<?php

use yii\db\Migration;

class m170605_082141_add_diff_page_type extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'has_background', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('page', 'has_background');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
