<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_constants`.
 */
class m170714_120850_create_api_constants_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('api_constants', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'values' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('api_constants');
    }
}
