<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map_feed_back_reg`.
 */
class m170719_043427_create_map_feed_back_reg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('map_feed_back_reg', [
            'id' => $this->primaryKey(),
            'map_feed_back_id' => $this->integer(11),
            'news_sender_period_id' => $this->integer(11),
            'is_sent' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('map_feed_back_reg');
    }
}
