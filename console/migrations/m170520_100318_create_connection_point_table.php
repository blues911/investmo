<?php

use yii\db\Migration;

/**
 * Handles the creation of table `connection_point`.
 */
class m170520_100318_create_connection_point_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('connection_point', [
            'id' => $this->primaryKey(),
            'name' => $this->string(511),
            'description' => $this->text(),
            'latitude' => $this->double(),
            'longitude' => $this->double(),
            'status' => $this->integer(2),
            'type' => $this->integer(2),
            'capacity' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('connection_point');
    }
}
