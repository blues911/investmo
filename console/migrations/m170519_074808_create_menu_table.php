<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu`.
 */
class m170519_074808_create_menu_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('menu', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'is_active' => $this->boolean()->defaultValue(1),
            'url' => $this->string(),
            'sort' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('menu');
    }
}
