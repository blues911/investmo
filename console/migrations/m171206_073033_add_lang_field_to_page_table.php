<?php

use yii\db\Migration;

class m171206_073033_add_lang_field_to_page_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('page', 'lang',$this->string()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('page', 'lang');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171206_073033_add_lang_field_to_page_table cannot be reverted.\n";

        return false;
    }
    */
}
