<?php

use yii\db\Migration;

class m170705_161116_add_story_investor_type extends Migration
{
    public function safeUp()
    {
        $this->createTable('story_investor_type', [
            'id' => $this->primaryKey(),
            'story_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
        ]);
        
        $this->addForeignKey('story_investor_type_story', 'story_investor_type', 'story_id', 'success_story', 'id', 'RESTRICT');
        $this->addForeignKey('story_investor_type_type', 'story_investor_type', 'type_id', 'investor_type', 'id', 'RESTRICT');
        
    }

    public function safeDown()
    {
        $this->dropForeignKey('story_investor_type', 'story_investor_type_story');
        $this->dropForeignKey('story_investor_type', 'story_investor_type_type');
        
        $this->dropTable('story_investor_type');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170705_161116_add_story_investor_type cannot be reverted.\n";

        return false;
    }
    */
}
