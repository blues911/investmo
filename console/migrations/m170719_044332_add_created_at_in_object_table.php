<?php

use yii\db\Migration;

class m170719_044332_add_created_at_in_object_table extends Migration
{
//    public function safeUp()
//    {
//
//    }
//
//    public function safeDown()
//    {
//        echo "m170719_044332_add_created_at_in_object_table cannot be reverted.\n";
//
//        return false;
//    }

    public function up()
    {
        $this->addColumn('object', 'created_at', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('object','created_at');
    }

}
