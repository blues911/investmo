<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cluster`.
 */
class m170523_145726_create_cluster_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cluster', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'map_x' => $this->double()->notNull(),
            'map_y' => $this->double()->notNull(),
            'latitude' => $this->double()->notNull(),
            'longitude' => $this->double()->notNull(),
            'quantity' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'is_deleted' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cluster');
    }
}
