<?php

use yii\db\Migration;

class m170608_065615_add_lang_version extends Migration
{
    public function up()
    {
        $this->addColumn('content_block', 'lang', $this->string(2));
        $this->addColumn('menu', 'lang', $this->string(2));

    }

    public function down()
    {
        $this->dropColumn('content_block');
        $this->dropColumn('menu', 'lang');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
