<?php

use yii\db\Migration;

class m170603_092009_add_feedback_fields extends Migration
{
    public function up()
    {
        $this->addColumn('feedback', 'lastname', $this->string(255));
        $this->addColumn('feedback', 'middlename', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('feedback', 'lastname');
        $this->dropColumn('feedback', 'middlename');
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
