<?php

use yii\db\Migration;

class m170919_070000_add_key_field_to_map_feed_back_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('map_feed_back', 'key', $this->string(60));
    }

    public function safeDown()
    {
        $this->dropColumn('map_feed_back', 'key');
    }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170919_070000_add_key_field_to_map_feed_back_table cannot be reverted.\n";

        return false;
    }
    */
}
