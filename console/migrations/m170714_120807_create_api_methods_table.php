<?php

use yii\db\Migration;

/**
 * Handles the creation of table `api_methods`.
 */
class m170714_120807_create_api_methods_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('api_methods', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'request_type' => $this->integer(),
            'url' => $this->string(),
            'request' => $this->text(),
            'response' => $this->text()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('api_methods');
    }
}
