<?php

use yii\db\Migration;

/**
 * Handles the creation of table `actuality_object`.
 */
class m171024_063134_create_actuality_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('actuality_object', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'date' => $this->dateTime(),
            'status' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('actuality_object');
    }
}
