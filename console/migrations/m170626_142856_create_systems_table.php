<?php

use yii\db\Migration;

/**
 * Handles the creation of table `system`.
 */
class m170626_142856_create_systems_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('systems', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'auth_token' => $this->string(),
            'work_token' => $this->string(),
            'token_created_at' => $this->dateTime(),
            'admin' => $this->string(),
            'last_enter' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('systems');
    }
}
