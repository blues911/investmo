<?php

use yii\db\Migration;

class m170627_101447_fix_objects extends Migration
{
    public function safeUp()
    {
        $this->createTable('object_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'code' => $this->string(255),
            'description' => $this->string(512),
            'is_container' => $this->boolean(),
            'is_child' => $this->boolean(),
        ]);
        
        $this->addColumn('object', 'parent_id', $this->integer());
        $this->addColumn('object', 'object_type_id', $this->integer());
        $this->addColumn('object', 'system_id', $this->integer());
        $this->addColumn('object', 'okved', $this->string(255));
        $this->addColumn('object', 'owner_name', $this->string(255));
        $this->addColumn('object', 'flat', $this->integer());
        $this->addColumn('object', 'flat_height', $this->double());
        $this->addColumn('object', 'industry_type', $this->integer());
        
        $this->addForeignKey('object_object_type', 'object', 'object_type_id', 'object_type', 'id', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropForeignKey('object', 'object_object_type');
        
        $this->dropTable('object_type');
                
        $this->dropColumn('object', 'parent_id');
        $this->dropColumn('object', 'object_type_id');
        $this->dropColumn('object', 'system_id');
        $this->dropColumn('object', 'okved');
        $this->dropColumn('object', 'owner_name');
        $this->dropColumn('object', 'flat');
        $this->dropColumn('object', 'flat_height');
        $this->dropColumn('object', 'industry_type');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170627_101447_fix_objects cannot be reverted.\n";

        return false;
    }
    */
}
