<?php

use yii\db\Migration;

class m170726_141803_fix_address extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE object CHANGE address_address address_address VARCHAR(511) COLLATE utf8_unicode_ci NULL
        ");
    }

    public function safeDown()
    {
        true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_141803_fix_address cannot be reverted.\n";

        return false;
    }
    */
}
