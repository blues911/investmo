<?php

use yii\db\Migration;

class m171204_130829_add_title_to_object_photo_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object_photo', 'title',$this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('object_photo', 'title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171204_130829_add_title_to_object_photo_table cannot be reverted.\n";

        return false;
    }
    */
}
