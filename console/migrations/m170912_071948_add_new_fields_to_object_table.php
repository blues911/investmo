<?php

use yii\db\Migration;

class m170912_071948_add_new_fields_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'area_config', $this->string(255));
        $this->addColumn('object', 'location_feature', $this->string(255));
        $this->addColumn('object', 'can_use_water_drain', $this->boolean());
        $this->addColumn('object', 'power_water_drain', $this->decimal(11.2));
        $this->addColumn('object', 'distance_to_rw_station', $this->double());
        $this->addColumn('object', 'pipe_burial', $this->string(255));
        $this->addColumn('object', 'hotel_available', $this->string(255));
        $this->addColumn('object', 'workforse_available', $this->string(255));
        $this->addColumn('object', 'available_communication_system', $this->string(255));
        $this->addColumn('object', 'special_business_offer', $this->text());
        $this->addColumn('object', 'owner_contacts', $this->string(255));
        $this->addColumn('object', 'municipal_education_contacts', $this->string(255));
        $this->addColumn('object', 'object_contact_face', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'area_config');
        $this->dropColumn('object', 'location_feature');
        $this->dropColumn('object', 'can_use_water_drain');
        $this->dropColumn('object', 'power_water_drain');
        $this->dropColumn('object', 'distance_to_rw_station');
        $this->dropColumn('object', 'pipe_burial');
        $this->dropColumn('object', 'hotel_available');
        $this->dropColumn('object', 'workforse_available');
        $this->dropColumn('object', 'available_communication_system');
        $this->dropColumn('object', 'special_business_offer');
        $this->dropColumn('object', 'owner_contacts');
        $this->dropColumn('object', 'municipal_education_contacts');
        $this->dropColumn('object', 'object_contact_face');

        /*
        // Use up()/down() to run migration code without a transaction.
        public function up()
        {

        }

        public function down()
        {
            echo "m170912_071948_add_new_fields_to_object_table cannot be reverted.\n";

            return false;
        }
        */
    }
}
