<?php

use yii\db\Migration;

class m170608_120607_fix_lang extends Migration
{
    public function safeUp()
    {
        $this->execute("
            update message m set translation = (SELECT message FROM source_message s WHERE s.id = m.id) WHERE m.id in (SELECT id FROM source_message m1 WHERE category = 'app') and m.translation is null and m.language = 'ru';
            update menu set lang = 'ru';
            update content_block set lang = 'ru';
        ");
    }

    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170608_120607_fix_lang cannot be reverted.\n";

        return false;
    }
    */
}
