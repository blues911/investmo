<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_photo`.
 */
class m170727_113624_create_user_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_photo', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'img' => $this->string(255)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_photo');
    }
}
