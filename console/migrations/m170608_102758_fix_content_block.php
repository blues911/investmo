<?php

use yii\db\Migration;

class m170608_102758_fix_content_block extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('title', 'content_block');
        $this->dropIndex('slug', 'content_block');
        $this->alterColumn('content_block', 'title', $this->string()->notNull());
        $this->alterColumn('content_block', 'slug', $this->string()->notNull());
    }

    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170608_102758_fix_content_block cannot be reverted.\n";

        return false;
    }
    */
}
