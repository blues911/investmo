<?php

use yii\db\Migration;

class m170802_155340_add_status_comment_into_object extends Migration
{
    public function up()
    {
        $this->addColumn('object', 'status_comment', $this->text());
    }

    public function down()
    {
        $this->dropColumn('object', 'status_comment');
    }
}
