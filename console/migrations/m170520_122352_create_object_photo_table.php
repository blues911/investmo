<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object_photo`.
 */
class m170520_122352_create_object_photo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object_photo', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'img' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('object_photo');
    }
}
