<?php

use yii\db\Migration;

class m171226_141420_add_page_link_to_measures_support_table extends Migration
{
    public function up()
    {
        $this->addColumn('measures_support', 'page_link',$this->string());
    }

    public function down()
    {
        $this->dropColumn('measures_support', 'page_link');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
