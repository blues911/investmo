<?php

use yii\db\Migration;

class m171226_142253_add_page_link_to_investor_type_table extends Migration
{
    public function up()
    {
        $this->addColumn('investor_type', 'page_link',$this->string());
    }

    public function down()
    {
        $this->dropColumn('investor_type', 'page_link');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
