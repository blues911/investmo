<?php

use yii\db\Migration;

class m170801_125914_add_esia_auth extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user', 'esia_id', $this->integer(11)->null()->unique());
    }

    public function safeDown()
    {
        $this->dropColumn('user', 'esia_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170801_125914_add_esia_auth cannot be reverted.\n";

        return false;
    }
    */
}
