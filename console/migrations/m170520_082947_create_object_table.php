<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object`.
 */
class m170520_082947_create_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object', [
            'id' => $this->primaryKey(),
            'name' => $this->string(511),
            'description' => $this->text(),
            'latitude' => $this->double(),
            'longitude' => $this->double(),
            'status' => $this->integer(2),
            'type' => $this->integer(2),
            'address_area' => $this->string(255),
            'address_address' => $this->string(255),
            'address_nearest_place' => $this->string(255),
            'owner' => $this->integer(2),
            'bargain_type' => $this->integer(2),
            'bargain_price' => $this->decimal(11.2),
            'square' => $this->double(),
            'cadastral_number' => $this->string(255),
            'distance_to_moscow' => $this->integer(11),
            'square_target' => $this->string(511),
            'has_electricity_supply' => $this->boolean(),
            'has_gas_supply' => $this->boolean(),
            'has_water_supply' => $this->boolean(),
            'has_water_removal_supply' => $this->boolean(),
            'has_heat_supply' => $this->boolean(),
            'electricity_supply_capacity' => $this->string(255),
            'gas_supply_capacity' => $this->string(255),
            'water_supply_capacity' => $this->string(255),
            'water_removal_supply_capacity' => $this->string(255),
            'heat_supply_capacity' => $this->string(255),
            'proprietorship_type' => $this->integer(3),
            'land_category' => $this->integer(3),
            'vri' => $this->integer(4),
            'has_buildings' => $this->boolean(),
            'buildings_count' => $this->integer(11),
            'has_road_availability' => $this->boolean(),
            'distance_to_nearest_road' => $this->integer(11),
            'has_railway_availability' => $this->boolean(),
            'has_charge' => $this->boolean(),
            'charge_type' => $this->string(511),
            'is_special_ecological_zone' => $this->boolean(),
            'danger_class' => $this->integer(),
            'phone' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('object');
    }
}
