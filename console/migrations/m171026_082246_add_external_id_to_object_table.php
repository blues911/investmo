<?php

use yii\db\Migration;

class m171026_082246_add_external_id_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'external_id',$this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'external_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171026_082246_add_external_id_to_object_table cannot be reverted.\n";

        return false;
    }
    */
}
