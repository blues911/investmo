<?php

use yii\db\Migration;

class m170821_090954_add_request_fields_into_object_table extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m170821_090954_add_request_fields_into_object_table cannot be reverted.\n";

        return false;
    }*/

    public function up()
    {
        $this->renameColumn('object', 'status_comment', 'request_comment');
    }

    public function down()
    {
        $this->renameColumn('object', 'request_comment', 'status_comment');
    }
}
