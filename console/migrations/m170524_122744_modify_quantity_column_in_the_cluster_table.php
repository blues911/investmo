<?php

use yii\db\Migration;

class m170524_122744_modify_quantity_column_in_the_cluster_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('cluster', 'quantity', $this->integer()->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('cluster', 'quantity', $this->integer()->defaultValue(0));
    }
}
