<?php

use yii\db\Migration;

class m170809_115938_add_request_status_to_object_table extends Migration
{

    public function up()
    {
        $this->addColumn('object', 'request_status', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('object', 'request_status');
    }

}
