<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page_block`.
 */
class m170605_101305_create_page_block_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('page_block', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'type' => $this->string(),
            'sort' => $this->integer(),
            'data' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('page_block');
    }
}
