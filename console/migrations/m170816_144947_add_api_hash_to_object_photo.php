<?php

use yii\db\Migration;

class m170816_144947_add_api_hash_to_object_photo extends Migration
{
    public function up()
    {
        $this->addColumn('object_photo', 'api_hash', $this->string());
    }

    public function down()
    {
        $this->dropColumn('object_photo', 'api_hash');
    }
}
