<?php

use yii\db\Migration;

class m170718_054111_add_map_feed_back_fields extends Migration
{

    public function up()
    {
        $this->addColumn('map_feed_back', 'created_at', $this->dateTime());
        $this->addColumn('map_feed_back', 'updated_at', $this->dateTime());
        $this->addColumn('map_feed_back', 'deleted_at', $this->dateTime());
        $this->addColumn('map_feed_back', 'is_deleted', $this->boolean());
    }

    public function down()
    {
        $this->dropColumn('map_feed_back', 'created_at');
        $this->dropColumn('map_feed_back', 'updated_at');
        $this->dropColumn('map_feed_back', 'deleted_at');
        $this->dropColumn('map_feed_back', 'is_deleted');
        return true;
    }

}
