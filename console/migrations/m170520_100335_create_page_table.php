<?php

use yii\db\Migration;

/**
 * Handles the creation of table `page`.
 */
class m170520_100335_create_page_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('page', [
            'id' => $this->primaryKey(),
            'is_active' => "ENUM('0', '1') NOT NULL DEFAULT '0'",
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull(),
            'header' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'is_deleted' => $this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('page');
    }
}
