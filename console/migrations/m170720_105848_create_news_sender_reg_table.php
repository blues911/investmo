<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_sender_reg`.
 */
class m170720_105848_create_news_sender_reg_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_sender_reg', [
            'id' => $this->primaryKey(),
            'map_feed_back_id' => $this->integer(11),
            'news_sender_period' => $this->integer(11),
            'is_sent' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_sender_reg');
    }
}
