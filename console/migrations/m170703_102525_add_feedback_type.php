<?php

use yii\db\Migration;

class m170703_102525_add_feedback_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('feedback', 'type', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('feedback', 'type');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170703_102525_add_feedback_type cannot be reverted.\n";

        return false;
    }
    */
}
