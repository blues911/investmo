<?php

use yii\db\Migration;

class m170627_161731_add_object_types extends Migration
{
    public function safeUp()
    {
        $this->insert('object_type', [
            'name' => 'Индустриальные парки',
            'code' => 'TYPE_INDUSTRIAL_PARK',
            'description' => '',
            'is_container' => true,
            'is_child' => false,
        ]);

        $this->insert('object_type', [
            'name' => 'Технопарки',
            'code' => 'TYPE_TECHNOLOGICAL_PARK',
            'description' => '',
            'is_container' => true,
            'is_child' => false,
        ]);
        
        $this->insert('object_type', [
            'name' => 'ОЭЗ',
            'code' => 'TYPE_SEZ',
            'description' => '',
            'is_container' => true,
            'is_child' => false,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Браунфилды',
            'code' => 'TYPE_BROWNFIELDS',
            'description' => '',
            'is_container' => true,
            'is_child' => false,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Резиденты',
            'code' => 'TYPE_RESIDENT',
            'description' => '',
            'is_container' => false,
            'is_child' => true,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Свободные земли',
            'code' => 'TYPE_FREE_LAND',
            'description' => '',
            'is_container' => false,
            'is_child' => true,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Свободные площади',
            'code' => 'TYPE_FREE_SPACE',
            'description' => '',
            'is_container' => false,
            'is_child' => true,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Коворкинг',
            'code' => 'TYPE_COWORKING',
            'description' => '',
            'is_container' => false,
            'is_child' => false,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Земельные участки',
            'code' => 'TYPE_LAND',
            'description' => '',
            'is_container' => false,
            'is_child' => false,
        ]);
        
        $this->insert('object_type', [
            'name' => 'Объекты имущества',
            'code' => 'TYPE_PROPERTY',
            'description' => '',
            'is_container' => false,
            'is_child' => false,
        ]);
    }

    public function safeDown()
    {
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170627_161731_add_object_types cannot be reverted.\n";

        return false;
    }
    */
}
