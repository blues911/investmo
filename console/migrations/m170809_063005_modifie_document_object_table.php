<?php

use yii\db\Migration;

class m170809_063005_modifie_document_object_table extends Migration
{
    public function up()
    {
        $this->alterColumn('object_document', 'object_id', $this->integer());
        $this->addColumn('object_document', 'tmp_id', $this->string());
    }

    public function down()
    {
        $this->dropColumn('object_document', 'tmp_id');
        $this->alterColumn('object_document', 'object_id', $this->integer()->notNull());
    }
}
