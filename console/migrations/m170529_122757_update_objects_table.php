<?php

use yii\db\Migration;

class m170529_122757_update_objects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('object', 'cadastral_number', $this->text(1000));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->alterColumn('object', 'cadastral_number', $this->string(255));
    }
}
