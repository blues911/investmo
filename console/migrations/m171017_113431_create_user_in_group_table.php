<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_in_group`.
 */
class m171017_113431_create_user_in_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_in_group', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_in_group');
    }
}
