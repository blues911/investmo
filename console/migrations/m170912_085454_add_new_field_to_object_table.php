<?php

use yii\db\Migration;

class m170912_085454_add_new_field_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'distance_to_major_transport_routes', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'distance_to_major_transport_routes');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170912_085454_add_new_field_to_object_table cannot be reverted.\n";

        return false;
    }
    */
}
