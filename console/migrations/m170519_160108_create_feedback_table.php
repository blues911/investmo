<?php

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m170519_160108_create_feedback_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'data' => $this->text(),
            'email' => $this->string(255),
            'phone' => $this->string(20),
			'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'is_deleted' => $this->boolean()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('feedback');
    }
}
