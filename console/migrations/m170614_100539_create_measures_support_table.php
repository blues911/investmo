<?php

use yii\db\Migration;

/**
 * Handles the creation of table `measures_support`.
 */
class m170614_100539_create_measures_support_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('measures_support', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'investor_type_id' => $this->integer(),
        ]);

        $this->insert('measures_support', ['name' => '<strong>Сопровождение</strong> инвестиционных проектов по принципу «Одного окна»',
            'investor_type_id' => 1]);
        $this->insert('measures_support', ['name' => '<strong>Индивидуальный подбор</strong> площадки для реализации проекта',
            'investor_type_id' => 1]);
        $this->insert('measures_support', ['name' => '<strong>Налоговые льготы:</strong> снижение ставки налога на прибыль на 4,5%',
            'investor_type_id' => 1]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление</strong>  <br> отраслевой аналитики',
            'investor_type_id' => 1]);
        $this->insert('measures_support', ['name' => '<strong>Государственная гарантийная поддержка</strong> инвестиционных проектов',
            'investor_type_id' => 1]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление льготных займов</strong> до 300млн рублей',
            'investor_type_id' => 1]);


        $this->insert('measures_support', ['name' => '<strong>Предоставление</strong>  <br> отраслевой аналитики',
            'investor_type_id' => 2]);
        $this->insert('measures_support', ['name' => '<strong>Государственная гарантийная поддержка</strong> инвестиционных проектов',
            'investor_type_id' => 2]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление льготных займов</strong> до 300млн рублей',
            'investor_type_id' => 2]);
        $this->insert('measures_support', ['name' => '<strong>Сопровождение</strong> инвестиционных проектов по принципу «Одного окна»',
            'investor_type_id' => 2]);
        $this->insert('measures_support', ['name' => '<strong>Индивидуальный подбор</strong> площадки для реализации проекта',
            'investor_type_id' => 2]);
        $this->insert('measures_support', ['name' => '<strong>Налоговые льготы:</strong> снижение ставки налога на прибыль на 4,5%',
            'investor_type_id' => 2]);


        $this->insert('measures_support', ['name' => '<strong>Налоговые льготы:</strong> снижение ставки налога на прибыль на 4,5%',
            'investor_type_id' => 3]);
        $this->insert('measures_support', ['name' => '<strong>Индивидуальный подбор</strong> площадки для реализации проекта',
            'investor_type_id' => 3]);
        $this->insert('measures_support', ['name' => '<strong>Сопровождение</strong> инвестиционных проектов по принципу «Одного окна»',
            'investor_type_id' => 3]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление</strong>  <br> отраслевой аналитики',
            'investor_type_id' => 3]);
        $this->insert('measures_support', ['name' => '<strong>Государственная гарантийная поддержка</strong> инвестиционных проектов',
            'investor_type_id' => 3]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление льготных займов</strong> до 300млн рублей',
            'investor_type_id' => 3]);

        $this->insert('measures_support', ['name' => '<strong>Индивидуальный подбор</strong> площадки для реализации проекта',
            'investor_type_id' => 4]);
        $this->insert('measures_support', ['name' => '<strong>Сопровождение</strong> инвестиционных проектов по принципу «Одного окна»',
            'investor_type_id' => 4]);
        $this->insert('measures_support', ['name' => '<strong>Налоговые льготы:</strong> снижение ставки налога на прибыль на 4,5%',
            'investor_type_id' => 4]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление</strong>  <br> отраслевой аналитики',
            'investor_type_id' => 4]);
        $this->insert('measures_support', ['name' => '<strong>Государственная гарантийная поддержка</strong> инвестиционных проектов',
            'investor_type_id' => 4]);
        $this->insert('measures_support', ['name' => '<strong>Предоставление льготных займов</strong> до 300млн рублей',
            'investor_type_id' => 4]);




    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('measures_support');
    }
}
