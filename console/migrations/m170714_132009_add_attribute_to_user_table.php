<?php

use yii\db\Migration;

class m170714_132009_add_attribute_to_user_table extends Migration
{
    private $table = '{{%user}}';
    public function up()
    {
        $this->addColumn($this->table, 'first_name', $this->string(24));
        $this->addColumn($this->table, 'last_name', $this->string(24));
        $this->addColumn($this->table, 'third_name', $this->string(24));
        $this->addColumn($this->table, 'company_name', $this->string(255));
        $this->addColumn($this->table, 'phone', $this->string(15));
    }

    public function down()
    {
        $this->dropColumn($this->table, 'first_name');
        $this->dropColumn($this->table, 'last_name');
        $this->dropColumn($this->table, 'third_name');
        $this->dropColumn($this->table, 'company_name');
        $this->dropColumn($this->table, 'phone');
    }
}
