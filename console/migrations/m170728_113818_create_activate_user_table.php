<?php

use yii\db\Migration;

/**
 * Handles the creation of table `activate_user`.
 */
class m170728_113818_create_activate_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('activate_user', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(11),
            'key' => $this->string(255)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('activate_user');
    }
}
