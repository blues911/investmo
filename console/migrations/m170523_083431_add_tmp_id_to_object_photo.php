<?php

use yii\db\Migration;

class m170523_083431_add_tmp_id_to_object_photo extends Migration
{
    public function up()
    {
        $this->addColumn('object_photo', 'tmp_id', $this->string());
    }

    public function down()
    {
        $this->dropColumn('object_photo', 'tmp_id');
    }

}
