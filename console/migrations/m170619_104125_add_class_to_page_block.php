<?php

use yii\db\Migration;

class m170619_104125_add_class_to_page_block extends Migration
{
    public function up()
    {
        $this->addColumn('page_block', 'class', $this->string());
    }

    public function down()
    {
        $this->dropColumn('page_block', 'class');
    }
}
