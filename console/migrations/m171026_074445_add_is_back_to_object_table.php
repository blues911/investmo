<?php

use yii\db\Migration;

class m171026_074445_add_is_back_to_object_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'is_back',$this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'is_back');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171026_074445_add_is_back_to_object_table cannot be reverted.\n";

        return false;
    }
    */
}
