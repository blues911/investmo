<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subscribe_email`.
 */
class m170714_144430_create_subscribe_email_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subscribe_email', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subscribe_email');
    }
}
