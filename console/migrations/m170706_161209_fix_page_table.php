<?php

use yii\db\Migration;

class m170706_161209_fix_page_table extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE page CHANGE content content MEDIUMTEXT COLLATE utf8_unicode_ci NULL
        ");
    }

    public function safeDown()
    {
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170706_161209_fix_page_table cannot be reverted.\n";

        return false;
    }
    */
}
