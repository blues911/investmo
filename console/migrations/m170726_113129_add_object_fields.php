<?php

use yii\db\Migration;

class m170726_113129_add_object_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object', 'municipality', $this->string(511));
        $this->addColumn('object', 'land_status', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('object', 'municipality');
        $this->dropColumn('object', 'land_status');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_113129_add_object_fields cannot be reverted.\n";

        return false;
    }
    */
}
