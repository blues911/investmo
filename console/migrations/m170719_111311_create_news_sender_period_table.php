<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_sender_period`.
 */
class m170719_111311_create_news_sender_period_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_sender_period', [
            'id' => $this->primaryKey(),
            'start_date' => $this->timestamp(),
            'end_date' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_sender_period');
    }
}
