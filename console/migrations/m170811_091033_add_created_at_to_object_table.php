<?php

use yii\db\Migration;

class m170811_091033_add_created_at_to_object_table extends Migration
{
//    public function safeUp()
//    {
//
//    }
//
//    public function safeDown()
//    {
//        echo "m170811_091033_add_created_at_to_object_table cannot be reverted.\n";
//
//        return false;
//    }


    public function up()
    {
        $this->addColumn('object', 'updated_at', $this->dateTime());
    }

    public function down()
    {
        $this->dropColumn('object', 'updated_at');
    }

}
