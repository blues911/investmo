<?php

use yii\db\Migration;

class m170830_090947_add_object_fields_to_object_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('object_type', 'object_fields', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('object_type', 'object_fields');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170830_090947_add_fields_to_object_type cannot be reverted.\n";

        return false;
    }
    */
}
