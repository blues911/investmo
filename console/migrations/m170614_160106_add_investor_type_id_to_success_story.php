<?php

use yii\db\Migration;

class m170614_160106_add_investor_type_id_to_success_story extends Migration
{
    public function up()
    {
        $this->addColumn('success_story', 'investor_type_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('success_story', 'investor_type_id');
    }
}
