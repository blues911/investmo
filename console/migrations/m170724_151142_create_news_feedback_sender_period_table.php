<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news_feedback_sender_period`.
 */
class m170724_151142_create_news_feedback_sender_period_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_feedback_sender_period', [
            'id' => $this->primaryKey(),
            'start_date' => $this->timestamp(),
            'end_date' => $this->timestamp()->defaultValue(null),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_feedback_sender_period');
    }
}
