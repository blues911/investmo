<?php

use yii\db\Migration;

/**
 * Handles the creation of table `map_feed_back_term`.
 */
class m170717_175146_create_map_feed_back_term_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('map_feed_back_term', [
            'id' => $this->primaryKey(),
            'map_feed_back_id' => $this->integer(11),
            'term_id' => $this->integer(11),
            'term_type' => $this->string(255)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('map_feed_back_term');
    }
}
