<?php

use yii\db\Migration;

/**
 * Handles the creation of table `investor_type`.
 */
class m170614_095804_create_investor_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('investor_type', [
            'id' => $this->primaryKey(),
            'img' => $this->string(),
            'text' => $this->text(),
        ]);

        $this->insert('investor_type',
            ['img' => 'thumb1.jpg',
                'text' => 'Инвесторам']);

        $this->insert('investor_type',
            ['img' => 'thumb2.jpg',
                'text' => 'Девелоперам']);

        $this->insert('investor_type',
            ['img' => 'thumb3.jpg',
                'text' => 'Владельцам']);

        $this->insert('investor_type',
            ['img' => 'thumb4.jpg',
                'text' => 'Малому бизнесу']);
        
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('investor_type');
    }
}
