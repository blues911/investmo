<?php

use yii\db\Migration;

/**
 * Handles the creation of table `foreign_projects`.
 */
class m170615_180229_create_foreign_projects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('foreign_projects', [
            'id' => $this->primaryKey(),
            'country' => $this->string(),
            'company' => $this->string(),
            'country_img' => $this->string(),
            'img' => $this->string(),
        ]);

        $this->insert('foreign_projects', [
            'country' => 'Япония',
            'company' => 'Тойота Мотор Корпорэйшн',
            'country_img' => 'static/img/assets/collaboration/japan.png',
            'img' => 'static/img/assets/collaboration/project1.jpg'
        ]);

        $this->insert('foreign_projects', [
            'country' => 'Франция',
            'company' => 'Michelin',
            'country_img' => 'static/img/assets/collaboration/france.png',
            'img' => 'static/img/assets/collaboration/project2.jpg'
        ]);

        $this->insert('foreign_projects', [
            'country' => 'США',
            'company' => 'Марс',
            'country_img' => 'static/img/assets/collaboration/usa.png',
            'img' => 'static/img/assets/collaboration/project3.jpg'
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('foreign_projects');
    }
}
