<?php

use yii\db\Migration;

class m170808_063836_add_feedback_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('feedback_file', [
            'id' => $this->primaryKey(),
            'feedback_id' => $this->integer()->notNull(),
            'file' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
            'format' => $this->string()->notNull(),
            'size' => $this->string()->notNull(),
            'mime' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('feedback_file');
    }
}
