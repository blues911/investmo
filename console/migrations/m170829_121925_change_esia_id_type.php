<?php

use yii\db\Migration;

class m170829_121925_change_esia_id_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user', 'email', $this->string()->null()->unique());
        $this->alterColumn('user', 'esia_id', $this->string()->null()->unique());
    }

    public function safeDown()
    {
        $this->alterColumn('user', 'email', $this->string()->notNull()->unique());
        $this->alterColumn('user', 'esia_id', $this->integer(11)->null()->unique());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170829_121925_change_esia_id_type cannot be reverted.\n";

        return false;
    }
    */
}
