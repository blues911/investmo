<?php

namespace console\controllers;

use yii\console\Controller;
use Yii;
use \moonland\phpexcel\Excel;
use yii\db\Exception;
use yii\helpers\FileHelper;
use common\models\Object;
use common\models\ObjectType;

class ExcelController extends Controller
{

	public function actionImport()
	{
		$dir = dirname(dirname(__DIR__)).'/import';

		$files = FileHelper::findFiles($dir);
	
			
		foreach ($files as $file) {
			echo PHP_EOL.'Processing >>> '.$file;
			
			$this->import($file);
		}
	}

    public function import($file)
    {
        $data = $this->getImportData($file);
		
        $errors = 0;
        foreach ($data as $line => $object) {
            foreach ($object as &$prop) {
                $prop = trim($prop);
            }
            
            if (!$this->objectExist($object)) {
                $newObject = new Object();
                if (!empty($object['name'])) {
                    $newObject->name = $object['name'];
                }
               
                if (!empty($object['description'])) {
                    $newObject->description = $object['description'];
                }
                if (!empty($object['latitude'])) {
                    $newObject->latitude = doubleval(trim($object['latitude']));
                }
                if (!empty($object['longitude'])) {
                    $newObject->longitude = doubleval(trim($object['longitude']));
                }
                /*if (!empty($object['status'])) {
                    $newObject->status = constant(Object::class . "::" . $object['status']);
                }*/
                $newObject->status = Object::STATUS_PUBLISHED;
               						
                if (!empty($object['object_type'])) {
                    $newObject->object_type_id = $this->findType($object['object_type']);
                }

                if (!empty($object['address_address'])) {
                    $newObject->address_address = $object['address_address'];
                }
				
				if (!empty($object['mo'])) {
                    $newObject->municipality = $object['mo'];
                }

                if (!empty($object['owner'])) {
                    $newObject->owner = constant(Object::class . "::" . $object['owner']);
                }
				
				if (!empty($object['land_status'])) {
                    $newObject->owner = constant(Object::class . "::" . $object['land_status']);
                }
                                
                if (!empty($object['owner_name'])) {
                    $newObject->owner_name = $object['owner_name'];
                }                
                
                if (!empty($object['bargain_type'])) {
                    $newObject->bargain_type = constant(Object::class . "::" . $object['bargain_type']);
                }
                if (!empty($object['bargain_price'])) {
                    $newObject->bargain_price = $object['bargain_price'];
                }
                if (!empty($object['square'])) {
                    $newObject->square = (double)$object['square'];
                }
				if (!empty($object['square_sqm'])) {
                    $newObject->square_sqm = (double)$object['square_sqm'];
                }	
                if (!empty($object['cadastral_number'])) {
                    $newObject->cadastral_number = $object['cadastral_number'];
                }
                if (!empty($object['distance_to_moscow'])) {
                    $newObject->distance_to_moscow = $object['distance_to_moscow'];
                }
                if (!empty($object['square_target'])) {
                    $newObject->square_target = $object['square_target'];
                }
                if (!empty($object['has_electricity_supply'])) {
                    $newObject->has_electricity_supply = $object['has_electricity_supply'];
                }
                if (!empty($object['has_gas_supply'])) {
                    $newObject->has_gas_supply = $object['has_gas_supply'];
                }
                if (!empty($object['has_water_supply'])) {
                    $newObject->has_water_supply = $object['has_water_supply'];
                }
                if (!empty($object['has_water_removal_supply'])) {
                    $newObject->has_water_removal_supply = $object['has_water_removal_supply'];
                }
                if (!empty($object['has_heat_supply'])) {
                    $newObject->has_heat_supply = $object['has_heat_supply'];
                }
                if (!empty($object['electricity_supply_capacity'])) {
                    $newObject->electricity_supply_capacity = $object['electricity_supply_capacity'];
                }						
                if (!empty($object['gas_supply_capacity'])) {
                    $newObject->gas_supply_capacity = $object['gas_supply_capacity'];
                }
                if (!empty($object['water_supply_capacity'])) {
                    $newObject->water_supply_capacity = $object['water_supply_capacity'];
                }
                if (!empty($object['water_removal_supply_capacity'])) {
                    $newObject->water_removal_supply_capacity = $object['water_removal_supply_capacity'];
                }
                if (!empty($object['heat_supply_capacity'])) {
                    $newObject->heat_supply_capacity = $object['heat_supply_capacity'];
                }
             
                if (!empty($object['land_category'])) {
                    $parts = split(',', $object['land_category']);
                    $values = [];
                    foreach ($parts as $value) {
                        $values[] = constant(Object::class."::".trim($value));
                    }
                    $newObject->setLandCategory($values);
                }
                if (!empty($object['vri'])) {
                    $newObject->vri = $object['vri'];
                }
                if (!empty($object['has_buildings'])) {
                    $newObject->has_buildings = $object['has_buildings'];
                }
                if (!empty($object['buildings_count'])) {
                    $newObject->buildings_count = $object['buildings_count'];
                }
																 
                if (!empty($object['has_road_availability'])) {
                    $newObject->has_road_availability = $object['has_road_availability'];
                }

                if (!empty($object['distance_to_nearest_road'])) {
                    $newObject->distance_to_nearest_road = $object['distance_to_nearest_road'];
                }

                if (!empty($object['has_railway_availability'])) {
                    $newObject->has_railway_availability = $object['has_railway_availability'];
                }

                if (!empty($object['has_charge'])) {
                    $newObject->has_charge = $object['has_charge'];
                }

                if (!empty($object['charge_type'])) {
                    $newObject->charge_type = $object['charge_type'];
                }

                if (!empty($object['is_special_ecological_zone'])) {
                    $newObject->is_special_ecological_zone = $object['is_special_ecological_zone'];
                }

                if (!empty($object['danger_class'])) {
                    $newObject->danger_class = constant(Object::class . "::" . $object['danger_class']);
                }

                if (!empty($object['phone'])) {
                    $newObject->phone = $object['phone'];
                }
                
                if (!empty($object['parent_id'])) {
                    $newObject->parent_id = $object['parent_id'];
                }
                
                if (!empty($object['okved'])) {
                    $newObject->okved = $object['okved'];
                }
                
                if (!empty($object['flat'])) {
                    $newObject->flat = $object['flat'];
                }
                
                if (!empty($object['flat_height'])) {
                    $newObject->flat_height = $object['flat_height'];
                }

                if (!empty($object['industry_type'])) {
                    $newObject->industry_type = constant(Object::class . "::" . $object['industry_type']);
                }


                if (!empty($object['latitude']) && !empty($object['longitude'])) {
                    $newObject->status = Object::STATUS_PUBLISHED;
                } else {
                    $newObject->status = Object::STATUS_PROCESSION;
                }


                if (!$newObject->save()) {
                    ++$errors;
                    echo PHP_EOL.'Errors at line: '.($line + 1).PHP_EOL;
					$cleanObject = $object;
					foreach ($cleanObject as $key => $value) {
						if (empty($value) || !$value) {
							unset($cleanObject[$key]);
						}
					}
					print_r($cleanObject);
                    print_r($newObject->errors);
                }
            }
        }

        echo "ИМПОРТ ЗАВЕРШЁН, ВО ВРЕМЯ ИМПОРТА БЫЛО СОВЕРШЕНО $errors ОШИБОК";
    }

    /**
     * @param string $fileName
     * @return array
     */
    public function getImportData($fileName)
    {

        $data = Excel::import($fileName, [
            'setFirstRecordAsKeys' => true,
            'setIndexSheetByName' => true,
            'getOnlySheet' => 'Sheet1',
        ]);

        return $data;
    }

    public function objectExist($object)
    {
        return false;
    }

    private function findType($type)
    {
        $typeObj = ObjectType::find()->code($type)->one();
        if ($typeObj) {
            return $typeObj->id;
        } else {
            echo PHP_EOL.'Bad coded type: '.$type;
        }
        
        return null;
    }

}