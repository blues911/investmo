<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Cluster;
use common\models\Object;

class MapController extends Controller
{
    /**
     * Default distance for cluster objects
     * @var double in km
     */
    private $radius = 20.0000;
    public function actionIndex()
    {
        $clusters = Cluster::find()->all();

        $objects = Object::find()
            ->select(['id', 'latitude', 'longitude'])
            ->asArray()
            ->all();

        $calculateDistances = function($lat, $lon, $data)
        {
            $result = [];
            foreach ($data as $k => $v) {
                $result[$v['id']] = $this->distance($lat, -$lon, $v['latitude'], -$v['longitude'], 'K');
            }
            return $result;
        };

        foreach ($clusters as $key => $cluster) {
            $cluster->quantity = 0; // reset
            $distances = [];
            $distances[$key] = $calculateDistances($cluster->latitude, $cluster->longitude, $objects);
            foreach ($distances[$key] as $id => $minDistance) {
                if ($minDistance < $this->radius) {
                    $cluster->quantity += 1; 
                }
            }
            $cluster->save();
        }

        echo 'Map clusters updated.' . PHP_EOL;
    }

    /**
     * Calculates the distance between two points.
     * http://www.geodatasource.com/developers/php
     * @param decimal $lat1
     * @param decimal $lon1
     * @param decimal $lat2
     * @param decimal $lon2
     * @param string $unit default NULL
     * the unit values:
     *      'M' is statute miles (default)
     *      'K' is kilometers
     *      'N' is nautical miles
     * Params example: (55.7558, -37.6173, 56.910906, -38.854142, 'K')
     * @return decimal $distance
     */
    private function distance($lat1, $lon1, $lat2, $lon2, $unit = NULL)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        switch ($unit) {
            case 'K':
                return ($miles * 1.609344);
            case 'N':
                return ($miles * 0.8684);
            case 'M':
                return $miles;
            default:
                return $miles;
        }
    }
}
