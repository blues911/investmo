<?php

namespace console\controllers;

use yii\console\Controller;
use Yii;
use \moonland\phpexcel\Excel;
use yii\db\Exception;
use yii\helpers\FileHelper;
use common\models\Object;
use common\models\ObjectType;

class ImportController extends Controller
{

	public function actionSpecialImport()
	{
        $file =  Yii::getAlias('@common').'/../special.xlsx';
		$this->import($file);
	}

    public function import($file)
    {
        $data = $this->getImportData($file);
		
        $errors = 0;
        foreach ($data as $line => $row) {
            foreach ($row as &$prop) {
                $prop = trim($prop);
            }
            
            $object = $this->getObject($row['file_id']);
            if (!$object) {
                echo PHP_EOL.'Bad file id : '.$row['file_id'];
                continue;
            }
            
            foreach ($row as $key => $value) {
                if ($key == 'file_id' || empty($key)) {
                    continue;
                }
                
                if (empty($value)) {
                    $value = null;
                } else if (in_array($key, [
                    'latitude', 'longitude', 'bargain_price', 'square', 'square_sqm', 'distance_to_moscow',
                    'electricity_supply_capacity', 'gas_supply_capacity', 'water_supply_capacity', 'water_removal_supply_capacity',
                    'heat_supply_capacity', 'distance_to_nearest_road', 'flat_height'
                ])) {
                    $value = (double)$value;
                } else if (in_array($key, [
                    'has_electricity_supply', 'has_gas_supply', 'has_water_supply', 'has_water_removal_supply', 'has_heat_supply',
                    'has_buildings', 'buildings_count', 'parent_id', 'flat', 'is_special_ecological_zone', 'has_road_availability',
                    'has_railway_availability', 'has_charge', 
                ])) {
                    $value = (int)$value;
                } else if (in_array($key, [
                    'owner', 'land_status', 'bargain_type', 'land_category', 'industry_type', 'danger_class', 'property_status',
                    '', '', '', '',
                ])) {
                    $object->$key = constant(Object::class."::".$value);
                    continue;
                } else if ($key == 'object_type') {
                    $object->object_type_id = $this->findType($value);
                    continue;
                } else if (in_array($key, [
                    'rent_end',
                ])) {
                    $time = strtotime($value);
                    $value = date('Y-m-d H:i:s', $time);
                }
                
                $object->$key = $value;
            }
           
            if (!empty($object->latitude) && !empty($object->longitude)) {
                $object->status = Object::STATUS_PUBLISHED;
            } else {
                $object->status = Object::STATUS_PROCESSION;
            }
            
            if (!$object->save()) {
                var_dump($object->attributes);
                var_dump($object->errors);
                ++$errors;
                echo PHP_EOL.'Errors at line: '.($line + 1).PHP_EOL;
            }
        }

        echo "ИМПОРТ ЗАВЕРШЁН, ВО ВРЕМЯ ИМПОРТА БЫЛО СОВЕРШЕНО $errors ОШИБОК";
    }

    /**
     * @param string $fileName
     * @return array
     */
    public function getImportData($fileName)
    {

        $data = Excel::import($fileName, [
            'setFirstRecordAsKeys' => true,
            'setIndexSheetByName' => true,
            'getOnlySheet' => 'Sheet1',
        ]);

        return $data;
    }

    public function getObject($fileId)
    {
        $object = Object::find()->fileId($fileId)->one();
        if ($object) {
            return $object;
        }
        
        return false;
    }

    private function findType($type)
    {
        $typeObj = ObjectType::find()->code($type)->one();
        if ($typeObj) {
            return $typeObj->id;
        } else {
            echo PHP_EOL.'Bad coded type: '.$type;
        }
        
        return null;
    }

}