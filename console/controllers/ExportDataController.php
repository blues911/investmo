<?php

namespace console\controllers;

use yii\console\Controller;
use Yii;
use \moonland\phpexcel\Excel;
use yii\db\Exception;
use yii\helpers\FileHelper;
use common\models\Object;
use common\models\ObjectType;

class ExportDataController extends Controller
{
    
    public function actionInit()
    {
        $objects = Object::find()->all();
        
        $updated = 0;
        $fileId = Object::getCurrentFileId();
        
        foreach ($objects as $key => $object) {
            if ($object->file_id > 0) {
                continue;
            }
            $object->file_id = ++$fileId;
            if ($object->save()) {
                ++$updated;
            } else {
                var_dump($object->errors);
            }
        }
        
        echo PHP_EOL.'Updated: '.$updated;
    }
    
    public function actionExport()
    {
        $type = ObjectType::find()->code('TYPE_PROPERTY')->one();
        $objects = Object::find()->type($type)->all();
        
        $filename = Yii::getAlias('@frontend').'/web/data.csv';
        $fh = fopen($filename, 'wt');
        
        $fields = [
            'file_id' => 'Идентификатор',
            'name' => 'Название',
            'cadastral_number' => 'Кадастровый номер',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'address_address' => 'Адрес',
        ];
        
        fputcsv($fh, array_keys($fields), ';');
        
        foreach ($objects as $object) {
            $item = [];
            foreach ($fields as $key => $name) {
                $item[] = $object->$key;
            }
            
            fputcsv($fh, $item, ';');
        }
        
        fclose($fh);
    }
    public function actionExportEmptyCoordinates(){
        $objects = Object::find()->where(['latitude' => null])
            ->andWhere(['longitude' => null])->all();

        $filename = Yii::getAlias('@frontend').'/web/data_empty_coordinates.csv';
        $fh = fopen($filename, 'wt');

        $fields = [
            'file_id' => 'Идентификатор',
            'name' => 'Название',
            'cadastral_number' => 'Кадастровый номер',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'address_address' => 'Адрес',
        ];

        fputcsv($fh, array_keys($fields), ';');

        foreach ($objects as $object) {
            $item = [];
            foreach ($fields as $key => $name) {
                $item[] = $object->$key;
            }

            fputcsv($fh, $item, ';');
        }

        fclose($fh);
    }
}