<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 23.11.2017
 * Time: 20:37
 */

namespace console\controllers;


use common\models\InvestHelpers;
use common\models\Object;
use common\models\ObjectDocument;
use common\models\ObjectPhoto;
use common\models\ObjectRequest;
use common\models\Systems;
use common\models\User;
use yii\console\Controller;
use common\models\SoapArip;

class SoapAripController extends Controller
{

    public function actionIndex(){
        ini_set('memory_limit', '8192M');
        $soap = new SoapArip(['url' => "http://185.120.188.170/Arip/Services/CommonService.svc?wsdl"]);
        $date = date('Y-m-d H:i:s');
        foreach ($soap->getSoapObjects_id() as $id) {
            $object = Object::find()->where(['external_id' => $id])->andWhere(['system_id' => 2])->one();
            $soap_object = $soap->getSoapObject($id);

            if($soap_object && $soap_object->HaveToDelete && $object){
                $object->delete();
                $obj_docs = ObjectDocument::find()->where(['object_id' => $object->id])->all();
                if($obj_docs){
                    foreach ($obj_docs as $doc)
                        $doc->remove($doc->id);
                }
                print_r('Успешно удалён '.$id."\n");
            }else{
                if(!$soap_object){
                    if($object) {
                        $object->delete();
                        $obj_docs = ObjectDocument::find()->where(['object_id' => $object->id])->all();
                        if ($obj_docs) {
                            foreach ($obj_docs as $doc)
                                $doc->remove($doc->id);
                            InvestHelpers::RDir(\Yii::getAlias('@frontend') . '/web/uploads/objects_documents/' . $object->id);
                        }
                        $obj_pics = ObjectPhoto::find()->where(['object_id' => $object->id])->all();
                        if ($obj_pics) {
                            foreach ($obj_pics as $pic){
                                $pic->removeFile();
                                $pic->delete();
                            }
                            InvestHelpers::RDir(\Yii::getAlias('@frontend') . '/web/uploads/objects/' . $object->id);
                        }
                        print_r('Нет ответа ' . $id . ' - Успешно удалён ' . "\n");
                    }else{
                        print_r('Нет ответа '.$id."\n");
                    }
                }else{
                    if(!$soap_object->HaveToDelete){
                        $type = 'обновлён';
                        if(!$object){
                            $type = 'добавлен';
                            $object = new Object();
                            $object->created_at = $date;
                        }
                        $object->attributes = $soap_object->attributes;
                        $object->updated_at = $date;
                        $object->actuality_date = $date;
                        $object->user_id = Systems::find()->where(['id' => 2])->one()->admin;
                        $object->request_status = ObjectRequest::STATUS_APPROVED;
                        if($object->validate() && $object->save()){
                            $obj_id = \Yii::$app->db->lastInsertID != 0 ? \Yii::$app->db->lastInsertID : $object->id;
                            $object_documents = ObjectDocument::find()->where(['object_id' => $obj_id])->all();
                            $object_photos = ObjectPhoto::find()->where(['object_id' => $obj_id])->all();

                            $object_document_arr = [];
                            $object_document_input_arr = [];
                            $object_photo_arr = [];
                            $object_photo_input_arr = [];
                            if($object_documents){
                                foreach ($object_documents as $object_document){
                                    $object_document_arr[preg_replace('/\s/','_',$object_document->title)] = $object_document->id;
                                }
                            }
                            if($soap_object->Document){
                                if(is_array($soap_object->Document)){
                                    foreach ($soap_object->Document as $Document){
                                        $object_document_input_arr[preg_replace('/\s/','_',$Document->FileName)] = $Document;
                                    }
                                }else{
                                    $object_document_input_arr[preg_replace('/\s/','_',$soap_object->Document->FileName)] = $soap_object->Document;
                                }
                            }
                            if($object_photos){
                                foreach ($object_photos as $object_photo){
                                    $object_photo_arr[preg_replace('/\s/','_',$object_photo->title)] = $object_photo->id;
                                }
                            }
                            if($soap_object->Presentation){
                                if(is_array($soap_object->Presentation)){
                                    foreach ($soap_object->Presentation as $Presentation){
                                        if(isset($Presentation->Kind->Id) && $Presentation->Kind->Id != 1000118)
                                            $object_document_input_arr[preg_replace('/\s/','_',$Presentation->FileName)] = $Presentation;
                                        else
                                            $object_photo_input_arr[preg_replace('/\s/','_',$Presentation->FileName)] = $Presentation;
                                    }
                                }else{
                                    if(isset($soap_object->Presentation->Kind->Id) && $soap_object->Presentation->Kind->Id != 1000118)
                                        $object_document_input_arr[preg_replace('/\s/','_',$soap_object->Presentation->FileName)] = $soap_object->Presentation;
                                    else
                                        $object_photo_input_arr[preg_replace('/\s/','_',$soap_object->Presentation->FileName)] = $soap_object->Presentation;
                                }
                            }

                            foreach (array_diff(array_keys($object_document_arr),array_keys($object_document_input_arr)) as $del){
                                $model = new ObjectDocument;
                                $model->remove($object_document_arr[$del]);
                            }

                            foreach (array_diff(array_keys($object_document_input_arr),array_keys($object_document_arr)) as $add)
                            {
                                ObjectDocument::uploadFromUrl($object_document_input_arr[$add]->Url,$obj_id, $object_document_input_arr[$add]->FileName);
                            }

                            foreach (array_diff(array_keys($object_photo_arr),array_keys($object_photo_input_arr)) as $del){
                                $model = ObjectPhoto::findOne($object_photo_arr[$del]);
                                $model->removeFile();
                                $model->delete();
                            }

                            foreach (array_diff(array_keys($object_photo_input_arr),array_keys($object_photo_arr)) as $add)
                            {
                                ObjectPhoto::uploadFromUrl($object_photo_input_arr[$add]->Url, $obj_id, $object_photo_input_arr[$add]->FileName);
                            }

                            print_r('Успешно '.$type.' '.$id."\n");
                        }else{
                            foreach ($object->getErrors() as $key => $value)
                                print_r('Ошибка '.$id.':'.$key.' - '.$value[0]."\n");
                        }
                    }else{
                        print_r($id."\n");
                    }
                }
            }
        };
    }
}