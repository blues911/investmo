<html>
<head>
    <meta charset="utf-8">
    <title>МойСклад JSON API 1.1</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <style>@import url('https://fonts.googleapis.com/css?family=Roboto:400,700|Inconsolata|Raleway:200');

        .hljs-comment, .hljs-title {
            color: #8e908c
        }

        .hljs-variable, .hljs-attribute, .hljs-tag, .hljs-regexp, .ruby .hljs-constant, .xml .hljs-tag .hljs-title, .xml .hljs-pi, .xml .hljs-doctype, .html .hljs-doctype, .css .hljs-id, .css .hljs-class, .css .hljs-pseudo {
            color: #c82829
        }

        .hljs-number, .hljs-preprocessor, .hljs-pragma, .hljs-built_in, .hljs-literal, .hljs-params, .hljs-constant {
            color: #f5871f
        }

        .ruby .hljs-class .hljs-title, .css .hljs-rules .hljs-attribute {
            color: #eab700
        }

        .hljs-string, .hljs-value, .hljs-inheritance, .hljs-header, .ruby .hljs-symbol, .xml .hljs-cdata {
            color: #718c00
        }

        .css .hljs-hexcolor {
            color: #3e999f
        }

        .hljs-function, .python .hljs-decorator, .python .hljs-title, .ruby .hljs-function .hljs-title, .ruby .hljs-title .hljs-keyword, .perl .hljs-sub, .javascript .hljs-title, .coffeescript .hljs-title {
            color: #4271ae
        }

        .hljs-keyword, .javascript .hljs-function {
            color: #8959a8
        }

        .hljs {
            display: block;
            background: white;
            color: #4d4d4c;
            padding: .5em
        }

        .coffeescript .javascript, .javascript .xml, .tex .hljs-formula, .xml .javascript, .xml .vbscript, .xml .css, .xml .hljs-cdata {
            opacity: .5
        }

        .right .hljs-comment {
            color: #969896
        }

        .right .css .hljs-class, .right .css .hljs-id, .right .css .hljs-pseudo, .right .hljs-attribute, .right .hljs-regexp, .right .hljs-tag, .right .hljs-variable, .right .html .hljs-doctype, .right .ruby .hljs-constant, .right .xml .hljs-doctype, .right .xml .hljs-pi, .right .xml .hljs-tag .hljs-title {
            color: #c66
        }

        .right .hljs-built_in, .right .hljs-constant, .right .hljs-literal, .right .hljs-number, .right .hljs-params, .right .hljs-pragma, .right .hljs-preprocessor {
            color: #de935f
        }

        .right .css .hljs-rule .hljs-attribute, .right .ruby .hljs-class .hljs-title {
            color: #f0c674
        }

        .right .hljs-header, .right .hljs-inheritance, .right .hljs-name, .right .hljs-string, .right .hljs-value, .right .ruby .hljs-symbol, .right .xml .hljs-cdata {
            color: #b5bd68
        }

        .right .css .hljs-hexcolor, .right .hljs-title {
            color: #8abeb7
        }

        .right .coffeescript .hljs-title, .right .hljs-function, .right .javascript .hljs-title, .right .perl .hljs-sub, .right .python .hljs-decorator, .right .python .hljs-title, .right .ruby .hljs-function .hljs-title, .right .ruby .hljs-title .hljs-keyword {
            color: #81a2be
        }

        .right .hljs-keyword, .right .javascript .hljs-function {
            color: #b294bb
        }

        .right .hljs {
            display: block;
            overflow-x: auto;
            background: #1d1f21;
            color: #c5c8c6;
            padding: .5em;
            -webkit-text-size-adjust: none
        }

        .right .coffeescript .javascript, .right .javascript .xml, .right .tex .hljs-formula, .right .xml .css, .right .xml .hljs-cdata, .right .xml .javascript, .right .xml .vbscript {
            opacity: .5
        }

        pre {
            background-color: #f5f5f5;
            padding: 12px;
            border: 1px solid #cfcfcf;
            border-radius: 6px;
            overflow: auto
        }

        pre code {
            color: black;
            background-color: transparent;
            padding: 0;
            border: none
        }

        pre.code {
            background-color: #f5f5f5;
            padding: 0;
            border: 0 solid #cfcfcf;
            border-radius: 6px;
            overflow: auto;
            transition: max-height .3s ease-in-out;
            max-height: 0
        }

        pre.code code {
            color: black;
            background-color: transparent;
            padding: 0;
            border: none
        }

        .right .example-names {
            background-color: #eee;
            padding: 12px;
            border-radius: 6px;
            line-height: 36px
        }

        .right .example-names .tab-button {
            cursor: pointer;
            color: black;
            border: 1px solid #ddd;
            padding: 6px;
            margin-left: 12px
        }

        .right .example-names .tab-button.active {
            background-color: #d5d5d5
        }

        .collapse-code-button {
            float: right
        }

        .collapse-code-button .open {
            color: #18bc9c;
            cursor: pointer
        }

        .collapse-code-button .close {
            display: none;
            color: #18bc9c;
            cursor: pointer
        }

        .collapse-code-button.show .open {
            display: none
        }

        .collapse-code-button.show .close {
            display: inline
        }

        .scrlable {
            max-height: 450px;
            display: block;
            overflow: auto
        }

        .collapse-content {
            max-height: 0;
            overflow: hidden;
            transition: max-height .7s ease-in-out
        }

        .right {
            box-sizing: border-box;
            float: right;
            width: 48.5%;
            padding-left: 12px
        }

        .right a {
            color: #18bc9c
        }

        .right h1, .right h2, .right h3, .right h4, .right h5, .right p, .right div {
            color: white
        }

        .right pre {
            background-color: #1d1f21;
            border: 1px solid #1d1f21
        }

        .right pre code {
            color: #c5c8c6
        }

        .right codepre {
            background-color: #1d1f21;
            border: 1px solid #1d1f21
        }

        .right codepre code {
            color: #c5c8c6
        }

        .right .description {
            margin-top: 12px
        }

        body {
            color: black;
            background: white;
            font: 400 14px / 1.42 "Helvetica Neue", Arial, sans-serif
        }

        header {
            border-bottom: 1px solid #ededed;
            margin-bottom: 12px
        }

        h1, h2, h3, h4, h5 {
            color: black;
            margin: 12px 0
        }

        h1 .permalink, h2 .permalink, h3 .permalink, h4 .permalink, h5 .permalink {
            margin-left: 0;
            opacity: 0;
            transition: opacity .25s ease
        }

        h1:hover .permalink, h2:hover .permalink, h3:hover .permalink, h4:hover .permalink, h5:hover .permalink {
            opacity: 1
        }

        .triple h1 .permalink, .triple h2 .permalink, .triple h3 .permalink, .triple h4 .permalink, .triple h5 .permalink {
            opacity: .15
        }

        .triple h1:hover .permalink, .triple h2:hover .permalink, .triple h3:hover .permalink, .triple h4:hover .permalink, .triple h5:hover .permalink {
            opacity: .15
        }

        h1 {
            font: 200 36px "Helvetica Neue", Arial, sans-serif;
            font-size: 36px
        }

        h2 {
            font: 200 36px "Helvetica Neue", Arial, sans-serif;
            font-size: 30px
        }

        h3 {
            font-size: 100%;
            text-transform: uppercase
        }

        h5 {
            font-size: 100%;
            font-weight: normal
        }

        p {
            margin: 0 0 10px
        }

        p.choices {
            line-height: 1.6
        }

        a {
            color: #18bc9c;
            text-decoration: none
        }

        li p {
            margin: 0
        }

        hr.split {
            border: 0;
            height: 1px;
            width: 100%;
            padding-left: 6px;
            margin: 12px auto;
            background-image: linear-gradient(to right, rgba(0, 0, 0, 0) 20%, rgba(0, 0, 0, 0.2) 51.4%, rgba(255, 255, 255, 0.2) 51.4%, rgba(255, 255, 255, 0) 80%)
        }

        dl dt {
            float: left;
            width: 130px;
            clear: left;
            text-align: right;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            font-weight: 700
        }

        dl dd {
            margin-left: 150px
        }

        blockquote {
            color: rgba(0, 0, 0, 0.5);
            font-size: 15.5px;
            padding: 10px 20px;
            margin: 12px 0;
            border-left: 5px solid #e8e8e8
        }

        blockquote p:last-child {
            margin-bottom: 0
        }

        pre {
            background-color: #f5f5f5;
            padding: 12px;
            border: 1px solid #cfcfcf;
            border-radius: 6px;
            overflow: auto
        }

        pre code {
            color: black;
            background-color: transparent;
            padding: 0;
            border: none
        }

        code {
            color: #444;
            background-color: #f5f5f5;
            font: 'Inconsolata', monospace;
            padding: 1px 4px;
            border: 1px solid #cfcfcf;
            border-radius: 3px
        }

        ul, ol {
            padding-left: 2em
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 12px
        }

        table tr:nth-child(2n) {
            background-color: #fafafa
        }

        table th, table td {
            padding: 6px 12px;
            border: 1px solid #e6e6e6
        }

        .text-muted {
            opacity: .5
        }

        .note, .warning {
            padding: .3em 1em;
            margin: 1em 0;
            border-radius: 2px;
            font-size: 90%
        }

        .note h1, .warning h1, .note h2, .warning h2, .note h3, .warning h3, .note h4, .warning h4, .note h5, .warning h5, .note h6, .warning h6 {
            font-family: 200 36px "Helvetica Neue", Arial, sans-serif;
            font-size: 135%;
            font-weight: 500
        }

        .note p, .warning p {
            margin: .5em 0
        }

        .note {
            color: black;
            background-color: #eff7fc;
            border-left: 4px solid #3498db
        }

        .note h1, .note h2, .note h3, .note h4, .note h5, .note h6 {
            color: #3498db
        }

        .warning {
            color: black;
            background-color: #fcf0ef;
            border-left: 4px solid #d62c1a
        }

        .warning h1, .warning h2, .warning h3, .warning h4, .warning h5, .warning h6 {
            color: #d62c1a
        }

        header {
            margin-top: 24px
        }

        nav {
            position: fixed;
            top: 24px;
            bottom: 0;
            overflow-y: auto
        }

        nav .resource-group {
            padding: 0
        }

        nav .resource-group .heading {
            position: relative
        }

        nav .resource-group .heading .chevron {
            position: absolute;
            top: 12px;
            right: 12px;
            opacity: .5
        }

        nav .resource-group .heading a {
            display: block;
            color: black;
            opacity: .7;
            border-left: 2px solid transparent;
            margin: 0
        }

        nav .resource-group .heading a:hover {
            text-decoration: none;
            background-color: bad-color;
            border-left: 2px solid black
        }

        nav ul {
            list-style-type: none;
            padding-left: 0
        }

        nav ul a {
            display: block;
            font-size: 13px;
            color: rgba(0, 0, 0, 0.7);
            padding: 8px 12px;
            border-top: 1px solid #ededed;
            border-left: 2px solid transparent;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap
        }

        nav ul a:hover {
            text-decoration: none;
            background-color: bad-color;
            border-left: 2px solid black
        }

        nav ul > li {
            margin: 0
        }

        nav ul > li:first-child {
            margin-top: -12px
        }

        nav ul > li:last-child {
            margin-bottom: -12px
        }

        nav ul ul a {
            padding-left: 24px
        }

        nav ul ul li {
            margin: 0
        }

        nav ul ul li:first-child {
            margin-top: 0
        }

        nav ul ul li:last-child {
            margin-bottom: 0
        }

        nav > div > div > ul > li:first-child > a {
            border-top: none
        }

        .preload * {
            transition: none !important
        }

        .pull-left {
            float: left
        }

        .pull-right {
            float: right
        }

        .badge {
            display: inline-block;
            float: right;
            min-width: 10px;
            min-height: 14px;
            padding: 3px 7px;
            font-size: 12px;
            color: #000;
            background-color: #ededed;
            border-radius: 10px;
            margin: -2px 0
        }

        .badge.get {
            color: white;
            background-color: #3498db
        }

        .badge.head {
            color: white;
            background-color: #3498db
        }

        .badge.options {
            color: white;
            background-color: #3498db
        }

        .badge.put {
            color: white;
            background-color: #f39c12
        }

        .badge.patch {
            color: white;
            background-color: #f39c12
        }

        .badge.post {
            color: white;
            background-color: #18bc9c
        }

        .badge.delete {
            color: white;
            background-color: #e74c3c
        }

        .collapse-button {
            float: right
        }

        .collapse-button .close {
            display: none;
            color: #18bc9c;
            cursor: pointer
        }

        .collapse-button .open {
            color: #18bc9c;
            cursor: pointer
        }

        .collapse-button.show .close {
            display: inline
        }

        .collapse-button.show .open {
            display: none
        }

        .collapse-content {
            max-height: 0;
            overflow: hidden;
            transition: max-height .3s ease-in-out
        }

        nav {
            width: 220px
        }

        .container {
            max-width: 940px;
            margin-left: auto;
            margin-right: auto
        }

        .container .row .content {
            margin-left: 244px;
            width: 696px
        }

        .container .row:after {
            content: '';
            display: block;
            clear: both
        }

        .container-fluid nav {
            width: 22%
        }

        .container-fluid .row .content {
            margin-left: 24%
        }

        .container-fluid.triple nav {
            width: 16.5%;
            padding-right: 1px
        }

        .container-fluid.triple .row .content {
            position: relative;
            margin-left: 16.5%;
            padding-left: 24px
        }

        .middle:before, .middle:after {
            content: '';
            display: table
        }

        .middle:after {
            clear: both
        }

        .middle {
            box-sizing: border-box;
            width: 51.5%;
            padding-right: 12px
        }

        .right {
            box-sizing: border-box;
            float: right;
            width: 48.5%;
            padding-left: 12px
        }

        .right a {
            color: #18bc9c
        }

        .right h1, .right h2, .right h3, .right h4, .right h5, .right p, .right div {
            color: white
        }

        .right pre {
            background-color: #1d1f21;
            border: 1px solid #1d1f21
        }

        .right pre code {
            color: #c5c8c6
        }

        .right .description {
            margin-top: 12px
        }

        .triple .resource-heading {
            font-size: 125%
        }

        .definition {
            margin-top: 12px;
            margin-bottom: 12px
        }

        .definition .method {
            font-weight: bold
        }

        .definition .method.get {
            color: #2e80b8
        }

        .definition .method.head {
            color: #2e80b8
        }

        .definition .method.options {
            color: #2e80b8
        }

        .definition .method.post {
            color: #2eb89d
        }

        .definition .method.put {
            color: #b8822e
        }

        .definition .method.patch {
            color: #b8822e
        }

        .definition .method.delete {
            color: #b83b2e
        }

        .definition .uri {
            word-break: break-all;
            word-wrap: break-word
        }

        .definition .hostname {
            opacity: .5
        }

        .example-names {
            background-color: #eee;
            padding: 12px;
            border-radius: 6px
        }

        .example-names .tab-button {
            cursor: pointer;
            color: black;
            border: 1px solid #ddd;
            padding: 6px;
            margin-left: 12px
        }

        .example-names .tab-button.active {
            background-color: #d5d5d5
        }

        .right .example-names {
            background-color: #444
        }

        .right .example-names .tab-button {
            color: white;
            border: 1px solid #666;
            border-radius: 6px
        }

        .right .example-names .tab-button.active {
            background-color: #5e5e5e
        }

        #nav-background {
            position: fixed;
            left: 0;
            top: 0;
            bottom: 0;
            width: 16.5%;
            padding-right: 14.4px;
            background-color: #fbfbfb;
            border-right: 1px solid #f0f0f0;
            z-index: -1
        }

        #right-panel-background {
            position: absolute;
            right: -12px;
            top: -12px;
            bottom: -12px;
            width: 48.6%;
            background-color: #333;
            z-index: -1
        }

        @media (max-width: 1200px) {
            nav {
                width: 198px
            }

            .container {
                max-width: 840px
            }

            .container .row .content {
                margin-left: 224px;
                width: 606px
            }
        }

        @media (max-width: 992px) {
            nav {
                width: 169.4px
            }

            .container {
                max-width: 720px
            }

            .container .row .content {
                margin-left: 194px;
                width: 526px
            }
        }

        @media (max-width: 768px) {
            nav {
                display: none
            }

            .container {
                width: 95%;
                max-width: none
            }

            .container .row .content, .container-fluid .row .content, .container-fluid.triple .row .content {
                margin-left: auto;
                margin-right: auto;
                width: 95%
            }

            #nav-background {
                display: none
            }

            #right-panel-background {
                width: 48.6%
            }
        }

        .back-to-top {
            position: fixed;
            z-index: 1;
            bottom: 0;
            right: 24px;
            padding: 4px 8px;
            color: rgba(0, 0, 0, 0.5);
            background-color: #ededed;
            text-decoration: none !important;
            border-top: 1px solid #ededed;
            border-left: 1px solid #ededed;
            border-right: 1px solid #ededed;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px
        }

        .resource-group {
            padding: 12px;
            margin-bottom: 12px;
            background-color: white;
            border: 1px solid #ededed;
            border-radius: 6px
        }

        .resource-group h2.group-heading, .resource-group .heading a {
            padding: 12px;
            margin: -12px -12px 12px -12px;
            background-color: #ededed;
            border-bottom: 1px solid #ededed;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            white-space: nowrap;
            text-overflow: ellipsis;
            overflow: hidden
        }

        .triple .content .resource-group {
            padding: 0;
            border: none
        }

        .triple .content .resource-group h2.group-heading, .triple .content .resource-group .heading a {
            margin: 0 0 12px 0;
            border: 1px solid #ededed
        }

        nav .resource-group .heading a {
            padding: 12px;
            margin-bottom: 0
        }

        nav .resource-group .collapse-content {
            padding: 0
        }

        .action {
            margin-bottom: 12px;
            padding: 12px 12px 0 12px;
            overflow: hidden;
            border: 1px solid transparent;
            border-radius: 6px
        }

        .action h4.action-heading {
            padding: 6px 12px;
            margin: -12px -12px 12px -12px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 6px;
            border-top-right-radius: 6px;
            overflow: hidden
        }

        .action h4.action-heading .name {
            float: right;
            font-weight: normal;
            padding: 6px 0
        }

        .action h4.action-heading .method {
            padding: 6px 12px;
            margin-right: 12px;
            border-radius: 3px;
            display: inline-block
        }

        .action h4.action-heading .method.get {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.head {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.options {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.put {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.patch {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.post {
            color: #000;
            background-color: white
        }

        .action h4.action-heading .method.delete {
            color: #000;
            background-color: white
        }

        .action h4.action-heading code {
            color: #444;
            background-color: rgba(255, 255, 255, 0.7);
            border-color: transparent;
            font-weight: normal;
            word-break: break-all;
            display: inline-block;
            margin-top: 2px
        }

        .action dl.inner {
            padding-bottom: 2px
        }

        .action .title {
            border-bottom: 1px solid white;
            margin: 0 -12px -1px -12px;
            padding: 12px
        }

        .action.get {
            border-color: #3498db
        }

        .action.get h4.action-heading {
            color: white;
            background: #3498db;
            border-bottom-color: #3498db
        }

        .action.head {
            border-color: #3498db
        }

        .action.head h4.action-heading {
            color: white;
            background: #3498db;
            border-bottom-color: #3498db
        }

        .action.options {
            border-color: #3498db
        }

        .action.options h4.action-heading {
            color: white;
            background: #3498db;
            border-bottom-color: #3498db
        }

        .action.post {
            border-color: #18bc9c
        }

        .action.post h4.action-heading {
            color: white;
            background: #18bc9c;
            border-bottom-color: #18bc9c
        }

        .action.put {
            border-color: #f39c12
        }

        .action.put h4.action-heading {
            color: white;
            background: #f39c12;
            border-bottom-color: #f39c12
        }

        .action.patch {
            border-color: #f39c12
        }

        .action.patch h4.action-heading {
            color: white;
            background: #f39c12;
            border-bottom-color: #f39c12
        }

        .action.delete {
            border-color: #e74c3c
        }

        .action.delete h4.action-heading {
            color: white;
            background: #e74c3c;
            border-bottom-color: #e74c3c
        }</style>
</head>
<body class="">
<div id="nav-background"></div>
<div class="container-fluid triple">
    <div class="row">
        <nav>
            <h4 align="center">Мой Склад JSON API</h4>
            <div class="resource-group">
                <div class="heading">
                    <div class="chevron"><i class="open fa fa-angle-down"></i></div>
                    <a href="#общие-сведения">Общие Сведения</a></div>
                <div class="collapse-content" style="max-height: 0px;">
                    <ul>
                        <li><a href="#header-аутентификация">Аутентификация</a></li>
                        <li><a href="#header-ограничения">Ограничения</a></li>
                        <li><a href="#header-метаданные">Метаданные</a></li>
                        <li><a href="#header-обработка-ошибок">Обработка ошибок</a></li>
                    </ul>
                </div>
            </div>
            <h4 align="center">Сущности</h4>
            <div class="resource-group">
                <div class="heading">
                    <div class="chevron"><i class="open fa fa-angle-down"></i></div>
                    <a href="#контрагент">Контрагент</a></div>
                <div class="collapse-content" style="max-height: 0px;">
                    <ul>
                        <li><a href="#контрагент-контрагенты">Контрагенты</a>
                            <ul>
                                <li><a href="#контрагент-контрагенты-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить список Контрагентов</a>
                                </li>
                                <li><a href="#контрагент-контрагенты-post"><span class="badge post"><i
                                                    class="fa fa-plus"></i></span>Создать Контрагента</a></li>
                                <li><a href="#контрагент-контрагенты-delete"><span class="badge delete"><i
                                                    class="fa fa-times"></i></span>Удалить Контрагента</a></li>
                            </ul>
                        </li>
                        <li><a href="#контрагент-контрагент">Контрагент</a>
                            <ul>
                                <li><a href="#контрагент-контрагент-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить Контрагента</a></li>
                                <li><a href="#контрагент-контрагент-put"><span class="badge put"><i
                                                    class="fa fa-pencil"></i></span>Изменить Контрагента</a></li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="#контрагент-счета-контрагента-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить счета Контрагента</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="#контрагент-счет-контрагента-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить счет Контрагента</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <ul>
                                <li><a href="#контрагент-контактные-лица-контрагента-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Список контактных лиц</a></li>
                            </ul>
                        </li>
                        <li><a href="#контрагент-контактное-лицо">Контактное лицо</a>
                            <ul>
                                <li><a href="#контрагент-контактное-лицо-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить контактное лицо</a>
                                </li>
                                <li><a href="#контрагент-контактное-лицо-put"><span class="badge put"><i
                                                    class="fa fa-pencil"></i></span>Изменить контактное лицо</a></li>
                            </ul>
                        </li>
                        <li><a href="#контрагент-события-контрагента">События Контрагента</a>
                            <ul>
                                <li><a href="#контрагент-события-контрагента-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Список событий</a></li>
                                <li><a href="#контрагент-события-контрагента-post"><span class="badge post"><i
                                                    class="fa fa-plus"></i></span>Добавить событие</a></li>
                            </ul>
                        </li>
                        <li><a href="#контрагент-событие">Событие</a>
                            <ul>
                                <li><a href="#контрагент-событие-get"><span class="badge get"><i
                                                    class="fa fa-arrow-down"></i></span>Получить событие</a></li>
                                <li><a href="#контрагент-событие-put"><span class="badge put"><i
                                                    class="fa fa-pencil"></i></span>Изменить событие</a></li>
                                <li><a href="#контрагент-событие-delete"><span class="badge delete"><i
                                                    class="fa fa-times"></i></span>Удалить событие</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>

            <p style="text-align: center; word-wrap: break-word;"><a href="https:/">https://</a>
            </p>
        </nav>
        <div class="content">
            <div id="right-panel-background"></div>
            <div class="middle">
                <header><h1 id="top">МойСклад JSON API 1.1</h1></header>
            </div>
            <div class="right"><h5>API Endpoint</h5><a href="https://online.moysklad.ru/api/remap/1.1">https://online.moysklad.ru/api/remap/1.1</a>
            </div>
            <div class="middle"><p>API для манипуляции с сущностями и создания отчетов в онлайн-сервисе МойСклад.</p>
            </div>
            <div class="middle">
                <div id="общие-сведения-замена-ссылок-объектами-с-помощью-expand" class="resource"><h3
                            class="resource-heading">Замена ссылок объектами с помощью expand <a
                                href="#общие-сведения-замена-ссылок-объектами-с-помощью-expand" class="permalink">¶</a>
                    </h3>
                    <p>В JSON API, в составе сущностей можно встретить ссылки на связанные объекты.
                        Ссылки выводятся в формате <a
                                href="#header-%D0%BC%D0%B5%D1%82%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5">Метаданных</a>.
                        Для того, чтобы вместо ссылок получить связанные объекты,
                        не обязательно делать отдельные запросы для каждого из них. Вместо этого, вместе с запросом на
                        получение сущности, нужно передать параметр <strong>expand</strong>.
                        В качестве значения данного параметра нужно перечислить через запятую все необходимые
                        поля-ссылки,
                        на месте которых вы бы хотели видеть связанные объекты.
                        В результате запроса с таким параметром, в ответе вы получите объект с развернутыми вложенными
                        объектами вместо ссылок.
                        К примеру, в документах, имеющих в составе поле <strong>agent</strong>, вместо ссылки на
                        контрагента будет выведен объект со всеми полями сущности “Контрагент”, описанными <a
                                href="#header-%D0%B0%D1%82%D1%80%D0%B8%D0%B1%D1%83%D1%82%D1%8B-%D1%81%D1%83%D1%89%D0%BD%D0%BE%D1%81%D1%82%D0%B8-">тут</a>.
                        Максимальный уровень вложенности <strong>expand</strong> : 3.
                        Также <strong>expand</strong> можно применять для результатов операций создания и обновления.
                    </p>
                    <ul>
                        <li>Ниже показаны примеры использования <strong>expand</strong> на <a
                                    href="#%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82-%D0%B2%D0%BE%D0%B7%D0%B2%D1%80%D0%B0%D1%82-%D0%BF%D0%BE%D0%BA%D1%83%D0%BF%D0%B0%D1%82%D0%B5%D0%BB%D1%8F">Возврате
                                покупателя</a>. В примерах представлены только поля <strong>meta</strong> и <strong>demand</strong>.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="right">
                <div class="definition"><span class="method get">GET</span>&nbsp;<span class="uri"><span
                                class="hostname">https://online.moysklad.ru/api/remap/1.1</span>/entity/salesreturn/166909e6-4a99-11e6-8a84-bae500000089</span>
                </div>
                <div class="tabs">
                    <div class="tabs">
                        <div class="example-names"><span>Responses</span><span class="tab-button active">200</span>
                        </div>
                        <div class="tab" style="display: block;">
                            <div>
                                <div class="inner"><h5>Headers</h5>
                                    <pre><code><span class="hljs-attribute">Content-Type</span>: <span
                                                    class="hljs-string">application/json</span></code></pre>
                                    <div style="height: 1px;"></div>
                                    <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                    class="close">Скрыть</span><span class="open">Показать</span></span>
                                    </div>
                                    <pre class="code"><code>{
  <span class="hljs-string">"meta"</span>: {
    <span class="hljs-string">"href"</span>: <span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/salesreturn/166909e6-4a99-11e6-8a84-bae500000089"</span>,
    <span class="hljs-string">"metadataHref"</span>: <span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/salesreturn/metadata"</span>,
    <span class="hljs-string">"type"</span>: <span class="hljs-string">"salesreturn"</span>,
    <span class="hljs-string">"mediaType"</span>: <span class="hljs-string">"application/json"</span>
  },
  ...
  <span class="hljs-string">"demand"</span>: {
    <span class="hljs-string">"meta"</span>: {
      <span class="hljs-string">"href"</span>: <span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/demand/f99544d7-4a98-11e6-8a84-bae50000007f"</span>,
      <span class="hljs-string">"metadataHref"</span>: <span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/demand/metadata"</span>,
      <span class="hljs-string">"type"</span>: <span class="hljs-string">"demand"</span>,
      <span class="hljs-string">"mediaType"</span>: <span class="hljs-string">"application/json"</span>
    }
  }
}</code></pre>
                                    <div style="height: 1px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="middle">
                <div id="контрагент-контрагенты-get" class="action get">
                    <h4 class="action-heading">
                        <div class="name">Получить список Контрагентов</div>
                        <a href="#контрагент-контрагенты-get" class="method get">GET</a><code class="uri">/entity/counterparty</code>
                    </h4>
                    <p>Получить список всех Контрагентов.
                        Результат: Объект JSON, включающий в себя поля:</p>
                    <ul>
                        <li>
                            <p><strong>meta</strong> <a
                                        href="#header-%D0%BC%D0%B5%D1%82%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5">Метаданные</a>
                                о выдаче,</p>
                        </li>
                        <li>
                            <p><strong>context</strong> - <a
                                        href="#header-%D0%BC%D0%B5%D1%82%D0%B0%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5">Метаданные</a>
                                о сотруднике, выполнившем запрос.</p>
                        </li>
                        <li>
                            <p><strong>rows</strong> - Массив JSON объектов, представляющих собой Контрагентов.</p>
                        </li>
                    </ul>
                    <div class="title"><strong>URI Parameters</strong>
                        <div class="collapse-button"><span class="close">Скрыть</span><span class="open">Показать</span>
                        </div>
                    </div>
                    <div class="collapse-content">
                        <dl class="inner">
                            <dt>limit</dt>
                            <dd><code>number</code>&nbsp;<span>(optional)</span>&nbsp;<span
                                        class="text-info default"><strong>Default:&nbsp;</strong><span>25</span></span>&nbsp;<span
                                        class="text-muted example"><strong>Example:&nbsp;</strong><span>100</span></span>
                                <p>Максимальное количество сущностей для извлечения.</p>
                                <p>
                                    <code>Допустимые значения 1 - 100</code>
                                </p></dd>
                            <dt>offset</dt>
                            <dd><code>number</code>&nbsp;<span>(optional)</span>&nbsp;<span
                                        class="text-info default"><strong>Default:&nbsp;</strong><span>0</span></span>&nbsp;<span
                                        class="text-muted example"><strong>Example:&nbsp;</strong><span>40</span></span>
                                <p>Отступ в выдаваемом списке сущностей</p>
                            </dd>
                            <dt>updatedFrom</dt>
                            <dd><code>string</code>&nbsp;<span>(optional)</span>&nbsp;<span
                                        class="text-muted example"><strong>Example:&nbsp;</strong><span>2016-04-15 15:48:46</span></span>
                                <p>Один из <a
                                            href="#header-%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D0%B0%D1%86%D0%B8%D0%B8-%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D0%B8">параметров
                                        фильтрации выборки</a>.
                                    Формат строки : <code>ГГГГ-ММ-ДД ЧЧ:ММ:СС[.ммм]</code>, Часовой пояс:
                                    <code>MSK</code> (Московское время)</p>
                            </dd>
                            <dt>updatedTo</dt>
                            <dd><code>string</code>&nbsp;<span>(optional)</span>&nbsp;<span
                                        class="text-muted example"><strong>Example:&nbsp;</strong><span>2016-04-15 15:48:46</span></span>
                                <p>Один из <a
                                            href="#header-%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D0%B0%D1%86%D0%B8%D0%B8-%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D0%B8">параметров
                                        фильтрации выборки</a>.
                                    Формат строки : <code>ГГГГ-ММ-ДД ЧЧ:ММ:СС[.ммм]</code>, Часовой пояс:
                                    <code>MSK</code> (Московское время)</p>
                            </dd>
                            <dt>updatedBy</dt>
                            <dd><code>string</code>&nbsp;<span>(optional)</span>&nbsp;<span
                                        class="text-muted example"><strong>Example:&nbsp;</strong><span>admin@admin</span></span>
                                <p>Один из <a
                                            href="#header-%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D0%B0%D1%86%D0%B8%D0%B8-%D0%B2%D1%8B%D0%B1%D0%BE%D1%80%D0%BA%D0%B8">параметров
                                        фильтрации выборки</a>.
                                    Формат строки : <code>uid</code></p>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="definition"><span class="method post">POST</span>&nbsp;<span class="uri"><span
                                class="hostname">https://online.moysklad.ru/api/remap/1.1</span>/entity/counterparty</span>
                </div>
                <div class="tabs">
                    <div class="example-names"><span>Requests</span><span class="tab-button active">Пример 1</span><span
                                class="tab-button">Пример 2</span><span class="tab-button">Пример с доп полями</span>
                    </div>
                    <div class="tab" style="display: block;">
                        <div>
                            <div class="inner">
                                <div class="description"><p>Пример типичного запроса для создания Контрагента.</p>
                                </div>
                                <h5>Headers</h5>
                                <pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre>
                                <div style="height: 1px;"></div>
                                <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                class="close">Скрыть</span><span class="open">Показать</span></span>
                                </div>
                                <pre class="code"><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"ООО Радуга"</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"Сеть стройматериалов Радуга ЭКСПО"</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"rainbowCode"</span></span>,
  "<span class="hljs-attribute">externalCode</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"extRainbw"</span></span>,
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"raduga@stroi.ru"</span></span>,
  "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"+7 495 331 22 33"</span></span>,
  "<span class="hljs-attribute">fax</span>": <span class="hljs-value"><span class="hljs-string">"1257752"</span></span>,
  "<span class="hljs-attribute">actualAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Академика Миля дом 15 к 21"</span></span>,
  "<span class="hljs-attribute">legalTitle</span>": <span class="hljs-value"><span class="hljs-string">"Общество с ограниченой ответственностью \"Радуга\""</span></span>,
  "<span class="hljs-attribute">legalAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Авиастроителей д 93 к 12"</span></span>,
  "<span class="hljs-attribute">inn</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"125152124152"</span></span>,
  "<span class="hljs-attribute">kpp</span>": <span class="hljs-value"><span class="hljs-string">"12155521"</span></span>,
  "<span class="hljs-attribute">ogrn</span>": <span class="hljs-value"><span class="hljs-string">"1251512"</span></span>,
  "<span class="hljs-attribute">okpo</span>": <span class="hljs-value"><span class="hljs-string">"201355"</span></span>,
  "<span class="hljs-attribute">tags</span>": <span class="hljs-value">[
    <span class="hljs-string">"Строители"</span>,
    <span class="hljs-string">"Радуга"</span>,
    <span class="hljs-string">"Ремонт"</span>
  ]</span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/states/fb56c504-2e58-11e6-8a84-bae500000069"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"state"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">priceType</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"Цена летняя"</span>
</span>}</code></pre>
                                <div style="height: 1px;"></div>
                            </div>
                        </div>
                        <div class="tabs">
                            <div class="example-names"><span>Responses</span><span class="tab-button active">200</span>
                            </div>
                            <div class="tab" style="display: block;">
                                <div>
                                    <div class="inner">
                                        <div class="description"><p>Успешный запрос. Результат - JSON представление
                                                созданного Контрагента.</p>
                                        </div>
                                        <h5>Headers</h5>
                                        <pre><code><span class="hljs-attribute">Content-Type</span>: <span
                                                        class="hljs-string">application/json</span></code></pre>
                                        <div style="height: 1px;"></div>
                                        <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                        class="close">Скрыть</span><span
                                                        class="open">Показать</span></span></div>
                                        <pre class="code"><code>{
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000"</span></span>,
    "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata"</span></span>,
    "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"counterparty"</span></span>,
    "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
  </span>}</span>,
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"b80ea81b-7058-11e6-8a84-bae500000000"</span></span>,
  "<span class="hljs-attribute">accountId</span>": <span class="hljs-value"><span class="hljs-string">"1185513e-692c-11e6-8a84-bae500000001"</span></span>,
  "<span class="hljs-attribute">owner</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/12747f9e-692c-11e6-8a84-bae50000002a"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"employee"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">shared</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">group</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/11883c67-692c-11e6-8a84-bae500000002"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"group"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">version</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
  "<span class="hljs-attribute">updated</span>": <span class="hljs-value"><span class="hljs-string">"2016-09-01 18:28:22"</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"ООО Радуга"</span></span>,
  "<span class="hljs-attribute">externalCode</span>": <span class="hljs-value"><span class="hljs-string">"o7732zkki541HDkZZD1Yt3"</span></span>,
  "<span class="hljs-attribute">archived</span>": <span class="hljs-value"><span
                                                            class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">companyType</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"legal"</span></span>,
  "<span class="hljs-attribute">accounts</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/accounts"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"account"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">tags</span>": <span class="hljs-value">[]</span>,
  "<span class="hljs-attribute">contactpersons</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/contactpersons"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"contactperson"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">notes</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/notes"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"note"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/states/fb56c504-2e58-11e6-8a84-bae500000069"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"state"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">priceType</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"Цена летняя"</span>
</span>}</code></pre>
                                        <div style="height: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab" style="display: none;">
                        <div>
                            <div class="inner">
                                <div class="description"><p>Пример запроса на создание Контрагента с указанием только
                                        лишь его имени</p>
                                </div>
                                <h5>Headers</h5>
                                <pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre>
                                <div style="height: 1px;"></div>
                                <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                class="close">Скрыть</span><span class="open">Показать</span></span>
                                </div>
                                <pre class="code"><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"ООО Радуга"</span>
</span>}</code></pre>
                                <div style="height: 1px;"></div>
                            </div>
                        </div>
                        <div class="tabs">
                            <div class="example-names"><span>Responses</span><span class="tab-button active">200</span>
                            </div>
                            <div class="tab" style="display: block;">
                                <div>
                                    <div class="inner">
                                        <div class="description"><p>Успешный запрос. Результат - JSON представление
                                                созданного Контрагента.</p>
                                        </div>
                                        <h5>Headers</h5>
                                        <pre><code><span class="hljs-attribute">Content-Type</span>: <span
                                                        class="hljs-string">application/json</span></code></pre>
                                        <div style="height: 1px;"></div>
                                        <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                        class="close">Скрыть</span><span
                                                        class="open">Показать</span></span></div>
                                        <pre class="code"><code>{
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000"</span></span>,
    "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata"</span></span>,
    "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"counterparty"</span></span>,
    "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
  </span>}</span>,
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"b80ea81b-7058-11e6-8a84-bae500000000"</span></span>,
  "<span class="hljs-attribute">accountId</span>": <span class="hljs-value"><span class="hljs-string">"1185513e-692c-11e6-8a84-bae500000001"</span></span>,
  "<span class="hljs-attribute">owner</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/12747f9e-692c-11e6-8a84-bae50000002a"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"employee"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">shared</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">group</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/11883c67-692c-11e6-8a84-bae500000002"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"group"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">version</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
  "<span class="hljs-attribute">updated</span>": <span class="hljs-value"><span class="hljs-string">"2016-09-01 18:28:22"</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"ООО Радуга"</span></span>,
  "<span class="hljs-attribute">externalCode</span>": <span class="hljs-value"><span class="hljs-string">"o7732zkki541HDkZZD1Yt3"</span></span>,
  "<span class="hljs-attribute">archived</span>": <span class="hljs-value"><span
                                                            class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">created</span>": <span class="hljs-value"><span class="hljs-string">"2007-02-07 17:16:41"</span></span>,
  "<span class="hljs-attribute">companyType</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"legal"</span></span>,
  "<span class="hljs-attribute">accounts</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/accounts"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"account"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">tags</span>": <span class="hljs-value">[]</span>,
  "<span class="hljs-attribute">contactpersons</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/contactpersons"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"contactperson"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">notes</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/b80ea81b-7058-11e6-8a84-bae500000000/notes"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"note"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/states/fb56c504-2e58-11e6-8a84-bae500000069"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"state"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}
</span>}</code></pre>
                                        <div style="height: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab" style="display: none;">
                        <div>
                            <div class="inner">
                                <div class="description"><p>Пример запроса для создания Контрагента с доп полями в теле
                                        запроса.</p>
                                </div>
                                <h5>Headers</h5>
                                <pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span></code></pre>
                                <div style="height: 1px;"></div>
                                <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                class="close">Скрыть</span><span class="open">Показать</span></span>
                                </div>
                                <pre class="code"><code>{
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"ООО Овощ Экспресс"</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"Сеть доставки овощей"</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"ovoshexpressCode"</span></span>,
  "<span class="hljs-attribute">externalCode</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"extVagetable"</span></span>,
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"ovosh@delivery.ru"</span></span>,
  "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"+7 495 662 12 23"</span></span>,
  "<span class="hljs-attribute">fax</span>": <span class="hljs-value"><span class="hljs-string">"1052034"</span></span>,
  "<span class="hljs-attribute">actualAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Кузнечная д 331"</span></span>,
  "<span class="hljs-attribute">legalTitle</span>": <span class="hljs-value"><span class="hljs-string">"Общество с ограниченой ответственностью \"Овощ Экспресс\""</span></span>,
  "<span class="hljs-attribute">legalAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Достоевская д 93 к 12"</span></span>,
  "<span class="hljs-attribute">inn</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"1251521244152"</span></span>,
  "<span class="hljs-attribute">kpp</span>": <span class="hljs-value"><span
                                                    class="hljs-string">"121555212"</span></span>,
  "<span class="hljs-attribute">ogrn</span>": <span class="hljs-value"><span class="hljs-string">"1251552"</span></span>,
  "<span class="hljs-attribute">okpo</span>": <span class="hljs-value"><span class="hljs-string">"201323"</span></span>,
  "<span class="hljs-attribute">tags</span>": <span class="hljs-value">[
    <span class="hljs-string">"Овощи"</span>,
    <span class="hljs-string">"Еда"</span>,
    <span class="hljs-string">"Доставка"</span>
  ]</span>,
  "<span class="hljs-attribute">attributes</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d129ff5-2c8c-11e6-8a84-bae5000000f3"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Коэффициент скидки"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                        class="hljs-string">"double"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-number">0.75</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d12a9a5-2c8c-11e6-8a84-bae5000000f4"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Последний заключённый договор"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                        class="hljs-string">"time"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-string">"2016-06-07 12:52:33"</span>
    </span>},
    {
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d12b1e7-2c8c-11e6-8a84-bae5000000f5"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Партнёрское юрлицо"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                        class="hljs-string">"boolean"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/states/fb56c504-2e58-11e6-8a84-bae500000069"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"state"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}
</span>}</code></pre>
                                <div style="height: 1px;"></div>
                            </div>
                        </div>
                        <div class="tabs">
                            <div class="example-names"><span>Responses</span><span class="tab-button active">200</span>
                            </div>
                            <div class="tab" style="display: block;">
                                <div>
                                    <div class="inner">
                                        <div class="description"><p>Успешный запрос. Результат - JSON представление
                                                созданного Контрагента.</p>
                                        </div>
                                        <h5>Headers</h5>
                                        <pre><code><span class="hljs-attribute">Content-Type</span>: <span
                                                        class="hljs-string">application/json</span></code></pre>
                                        <div style="height: 1px;"></div>
                                        <div class="title"><strong>Body</strong><span class="collapse-code-button"><span
                                                        class="close">Скрыть</span><span
                                                        class="open">Показать</span></span></div>
                                        <pre class="code"><code>{
  "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/88fc07ac-2c8d-11e6-8a84-bae500000050"</span></span>,
    "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata"</span></span>,
    "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"counterparty"</span></span>,
    "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
  </span>}</span>,
  "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"88fc07ac-2c8d-11e6-8a84-bae500000050"</span></span>,
  "<span class="hljs-attribute">accountId</span>": <span class="hljs-value"><span class="hljs-string">"6270cd18-2c7f-11e6-8a84-bae500000001"</span></span>,
  "<span class="hljs-attribute">owner</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/faba7f37-2e58-11e6-8a84-bae500000028"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/employee/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"employee"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">shared</span>": <span class="hljs-value"><span class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">group</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/f97aa1fb-2e58-11e6-8a84-bae500000002"</span></span>,
      "<span class="hljs-attribute">metadataHref</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/group/metadata"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"group"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">version</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
  "<span class="hljs-attribute">updated</span>": <span class="hljs-value"><span class="hljs-string">"2016-06-07 11:55:08"</span></span>,
  "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"ООО Овощ Экспресс"</span></span>,
  "<span class="hljs-attribute">description</span>": <span class="hljs-value"><span class="hljs-string">"Сеть доставки овощей"</span></span>,
  "<span class="hljs-attribute">code</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"ovoshexpressCode"</span></span>,
  "<span class="hljs-attribute">externalCode</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"extVagetable"</span></span>,
  "<span class="hljs-attribute">archived</span>": <span class="hljs-value"><span
                                                            class="hljs-literal">false</span></span>,
  "<span class="hljs-attribute">created</span>": <span class="hljs-value"><span class="hljs-string">"2007-02-07 17:16:41"</span></span>,
  "<span class="hljs-attribute">legalTitle</span>": <span class="hljs-value"><span class="hljs-string">"Общество с ограниченой ответственностью \"Овощ Экспресс\""</span></span>,
  "<span class="hljs-attribute">legalAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Достоевская д 93 к 12"</span></span>,
  "<span class="hljs-attribute">actualAddress</span>": <span class="hljs-value"><span class="hljs-string">"г.Москва ул Кузнечная д 331"</span></span>,
  "<span class="hljs-attribute">inn</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"1251521244152"</span></span>,
  "<span class="hljs-attribute">kpp</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"121555212"</span></span>,
  "<span class="hljs-attribute">ogrn</span>": <span class="hljs-value"><span class="hljs-string">"1251552"</span></span>,
  "<span class="hljs-attribute">okpo</span>": <span class="hljs-value"><span class="hljs-string">"201323"</span></span>,
  "<span class="hljs-attribute">email</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"ovosh@delivery.ru"</span></span>,
  "<span class="hljs-attribute">phone</span>": <span class="hljs-value"><span
                                                            class="hljs-string">"+7 495 662 12 23"</span></span>,
  "<span class="hljs-attribute">fax</span>": <span class="hljs-value"><span class="hljs-string">"1052034"</span></span>,
  "<span class="hljs-attribute">attributes</span>": <span class="hljs-value">[
    {
      "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/attributes/0d129ff5-2c8c-11e6-8a84-bae5000000f3"</span></span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"attributemetadata"</span></span>,
        "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
      </span>}</span>,
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d129ff5-2c8c-11e6-8a84-bae5000000f3"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                class="hljs-string">"double"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Коэффициент скидки"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-number">0.75</span>
    </span>},
    {
      "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/attributes/0d12a9a5-2c8c-11e6-8a84-bae5000000f4"</span></span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"attributemetadata"</span></span>,
        "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
      </span>}</span>,
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d12a9a5-2c8c-11e6-8a84-bae5000000f4"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Последний заключённый договор"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                class="hljs-string">"time"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-string">"2016-06-07 12:52:33"</span>
    </span>},
    {
      "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
        "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/attributes/0d12b1e7-2c8c-11e6-8a84-bae5000000f5"</span></span>,
        "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span class="hljs-string">"attributemetadata"</span></span>,
        "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
      </span>}</span>,
      "<span class="hljs-attribute">id</span>": <span class="hljs-value"><span class="hljs-string">"0d12b1e7-2c8c-11e6-8a84-bae5000000f5"</span></span>,
      "<span class="hljs-attribute">name</span>": <span class="hljs-value"><span class="hljs-string">"Партнёрское юрлицо"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                class="hljs-string">"boolean"</span></span>,
      "<span class="hljs-attribute">value</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
    </span>}
  ]</span>,
  "<span class="hljs-attribute">accounts</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/88fc07ac-2c8d-11e6-8a84-bae500000050/accounts"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"account"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">tags</span>": <span class="hljs-value">[
    <span class="hljs-string">"доставка"</span>,
    <span class="hljs-string">"еда"</span>,
    <span class="hljs-string">"овощи"</span>
  ]</span>,
  "<span class="hljs-attribute">contactpersons</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/88fc07ac-2c8d-11e6-8a84-bae500000050/contactpersons"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"contactperson"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">notes</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/88fc07ac-2c8d-11e6-8a84-bae500000050/notes"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"note"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span></span>,
      "<span class="hljs-attribute">size</span>": <span class="hljs-value"><span class="hljs-number">0</span></span>,
      "<span class="hljs-attribute">limit</span>": <span class="hljs-value"><span class="hljs-number">100</span></span>,
      "<span class="hljs-attribute">offset</span>": <span class="hljs-value"><span class="hljs-number">0</span>
    </span>}
  </span>}</span>,
  "<span class="hljs-attribute">state</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">meta</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">href</span>": <span class="hljs-value"><span class="hljs-string">"https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata/states/fb56c504-2e58-11e6-8a84-bae500000069"</span></span>,
      "<span class="hljs-attribute">type</span>": <span class="hljs-value"><span
                                                                    class="hljs-string">"state"</span></span>,
      "<span class="hljs-attribute">mediaType</span>": <span class="hljs-value"><span class="hljs-string">"application/json"</span>
    </span>}
  </span>}
</span>}</code></pre>
                                        <div style="height: 1px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>/* eslint-env browser */
    /* eslint quotes: [2, "single"] */
    'use strict';

    /*
     Get a list of direct child elements by class name.
     */
    function childrenByClass(element, name) {
        var filtered = [];

        for (var i = 0; i < element.children.length; i++) {
            var child = element.children[i];
            if (child.className.indexOf(name) !== -1) {
                filtered.push(child);
            }
        }

        return filtered;
    }

    /*
     Collapse or show a request/response example.
     */
    function toggleCollapseButton(event) {
        var button = event.target.parentNode;
        var content = button.parentNode.nextSibling;

        if (content.style.maxHeight && content.style.maxHeight !== '0px') {
            // Currently showing, so let's hide it
            button.className = 'collapse-button';
            content.style.maxHeight = '0px';
        } else {
            // Currently hidden, so let's show it
            button.className = 'collapse-button show';
            if (content !== null) {
                content.style.maxHeight = content.children[0].offsetHeight + 12 + 'px';
            }
        }
    }

    function toggleCollapseCodeButton(event) {
        var button = event.target.parentNode;
        var content = button.parentNode.nextSibling;

        if (content.style.maxHeight && content.style.maxHeight !== '0px') {
            // Currently showing, so let's hide it
            button.className = 'collapse-code-button';
            content.style.padding = 0;
            content.style.maxHeight = '0px';
            content.style.overflow = 'hidden';
        } else {
            // Currently hidden, so let's show it
            button.className = 'collapse-code-button show';
            content.style.padding = 12;
            if (content !== null) {
                content.style.maxHeight = content.children[0].offsetHeight + 24 + 'px';
            }
            content.style.overflow = 'auto';
        }
    }

    function toggleTabButton(event) {
        var i, index;
        var button = event.target;

        // Get index of the current button.
        var buttons = childrenByClass(button.parentNode, 'tab-button');
        for (i = 0; i < buttons.length; i++) {
            if (buttons[i] === button) {
                index = i;
                button.className = 'tab-button active';
            } else {
                buttons[i].className = 'tab-button';
            }
        }

        // Hide other tabs and show this one.
        var tabs = childrenByClass(button.parentNode.parentNode, 'tab');
        for (i = 0; i < tabs.length; i++) {
            if (i === index) {
                tabs[i].style.display = 'block';
            } else {
                tabs[i].style.display = 'none';
            }
        }
    }

    /*
     Collapse or show a navigation menu. It will not be hidden unless it
     is currently selected or `force` has been passed.
     */
    function toggleCollapseNav(event) {
        var heading = event.target.parentNode;
        var content = heading.nextSibling;

        if (content.style.maxHeight && content.style.maxHeight !== '0px') {
            // Currently showing, so let's hide it, but only if this nav item
            // is already selected. This prevents newly selected items from
            // collapsing in an annoying fashion.
            content.style.maxHeight = '0px';

        } else {
            // Currently hidden, so let's show it
            if (content !== null) {
                content.style.maxHeight = content.children[0].offsetHeight + 12 + 'px';
            }
        }
    }

    /*
     Refresh the page after a live update from the server. This only
     works in live preview mode (using the `--server` parameter).
     */
    function refresh(body) {
        document.querySelector('body').className = 'preload';
        document.body.innerHTML = body;

        // Re-initialize the page
        init();

        document.querySelector('body').className = '';
    }

    /*
     Initialize the interactive functionality of the page.
     */
    function init() {
        var i, j;

        // Make collapse buttons clickable
        var buttons = document.querySelectorAll('.collapse-button');
        for (i = 0; i < buttons.length; i++) {
            buttons[i].onclick = toggleCollapseButton;
        }

        var buttons1 = document.querySelectorAll('.collapse-code-button');
        for (i = 0; i < buttons1.length; i++) {
            buttons1[i].onclick = toggleCollapseCodeButton;
        }

        var responseCodes = document.querySelectorAll('.example-names');
        for (i = 0; i < responseCodes.length; i++) {
            var tabButtons = childrenByClass(responseCodes[i], 'tab-button');
            // Show by default?
            toggleTabButton({target: tabButtons[0]});
            for (j = 0; j < tabButtons.length; j++) {
                tabButtons[j].onclick = toggleTabButton;
            }
        }

        // Make nav items clickable to collapse/expand their content.
        var navItems = document.querySelectorAll('nav .resource-group .heading');
        for (i = 0; i < navItems.length; i++) {
            navItems[i].onclick = toggleCollapseNav;
        }
    }

    // Initial call to set up buttons
    init();

    window.onload = function () {
        // Remove the `preload` class to enable animations
        document.querySelector('body').className = '';
    };
</script>
</body>
</html>