<?php
use frontend\widgets\FeedBackWidget;

echo FeedBackWidget::widget([
    'selector' => $selector ? true : false,
    'type' => $type
]);