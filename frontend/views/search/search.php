<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;


$formatter = \Yii::$app->i18n->messageFormatter;
$this->title = 'Инвестиционный портал Московской области';

function processContent($content, $phrase)
{
    $content = strip_tags($content, '<mark>');
    
    $positions = [];
    preg_match('/[а-яА-Яa-zA-Z0-9ёЁ]/u', $content, $positions, PREG_OFFSET_CAPTURE);
    
    $content = mb_substr($content, $positions[0][1]);
    return $content;
}

?>
<p class="searching__text-result">
    <?=$count?> 
    <?=$formatter->format(
        '{n, plural, one{разультат} few{результата} many{результатов} other{результатов}}',
        ['n' => intval($count)], \Yii::$app->language); ?> по запросу
</p>

<div class="search-result">
    <? if ($answers && count($answers)) : ?>
        <? foreach ($answers as $answer) : ?>
            <?
                $content = false;
                if (preg_match('/<mark>/um', $answer['_content'])) {
                    $content = processContent($answer['_content'], $phrase);
                } else if (preg_match('/<mark>/um', $answer['_header'])) {
                    $content = processContent($answer['_header'], $phrase);
                } else {
                    $content = $answer['_header'];
                }
            ?>

            <div class="search-result__content">
                <h3 class="search-result__title"><?=$answer['title']?></h3>
                <p class="search-result__text"><?=$content?></p>
                <a class="search-result__link" href="<?=Url::toRoute(['/page/'.$answer['slug']])?>"><?=Url::toRoute(['/page/'.$answer['slug']], true);?></a>
            </div>
        <? endforeach; ?>
    <? endif?>        
</div>
<div class="pagination p-search__pagination">
    <? if ($answers && count($answers)) : ?>
        <?=LinkPager::widget([
            'pagination' => $provider->pagination,
            'activePageCssClass' => 'pagination__marker_active',
            'pageCssClass' => 'pagination__marker',
            'prevPageLabel' => '',
            'nextPageLabel' => '',
            'prevPageCssClass' => 'hidden',
            'nextPageCssClass' => 'hidden',
        ]);?>
    <? endif?>        
</div>