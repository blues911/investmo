<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

$formatter = \Yii::$app->i18n->messageFormatter;
$this->title = 'Инвестиционный портал Московской области';

function processContent($content, $phrase)
{ 
    $content = strip_tags($content, '<mark>');   
    return $content;
}

?>
<div class="p-search">
    <div class="title-subpage title-subpage_no-gap">
        <div class="title-subpage__top" style="background-image:url(static/img/assets/p-stories/mobile-bg.jpg);">
            <div class="title-subpage__title">
                <div class="container">
                    <h1>
                        <span class="title-subpage__brand">Поиск</span>
                        <span class="title-subpage__divider">&nbsp;/&nbsp;</span>
                        <span class="title-subpage__title-box">по сайту</span>
                    </h1>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container">
        <p class="p-search__result">Результаты поиска</p>
            <div class="p-search__content">
                <div class="searching">
                    <?= Html::beginForm([
                        'search/search',
                        'id' => 'search-form',
                    ],
                    'post',
                    [
                        'enctype' => 'multipart/form-data',
                        'class' => 'searching__form',
                        'novalidate' => 'novalidate',
                    ]) ?>
                        <div class="search searching__field">
                            <?= Html::input('search', 'search', $phrase, [
                                'class' => 'search__input',
                                'placeholder' => 'Поиск',
                                'required' => 'required',
                            ]) ?>
                            <button class="search__btn search__btn_submit" type="submit">
                                <svg class="icon icon_search-1 search__icon" width="17.8px" height="17.3px">
                                    <use xlink:href="svg-symbols.svg#search-1"></use>
                                </svg>
                            </button>
                            <button class="search__btn search__btn_reset" type="reset">
                                <svg class="icon icon_reset search__icon" width="23px" height="23px">
                                    <use xlink:href="svg-symbols.svg#reset"></use>
                                </svg>
                            </button>
                        </div>
          
                        <div class="searching__filters">
                            <p class="searching__text">Сортировать по</p>
                            <div class="select searching__select">
                                <select class="select__widget" name="order">
                                    <option value="date" selected="selected">дате публикации</option>
                                    <option value="title">алфавиту</option>
                                </select>
                            </div>
                        </div>
                    <?= Html::endForm() ?>
                    <p class="searching__text-result">
                        <? if ($answers && count($answers)) : ?>
                            <?=$count?>
                            <?=$formatter->format(
                                '{n, plural, one{разультат} few{результата} many{результатов} other{результатов}}',
                                ['n' => intval($count)], \Yii::$app->language); ?> по запросу
                        <? endif; ?>
                    </p>
                </div>
                <div class="search-result">
                    <? if ($answers && count($answers)) : ?>
                        <? foreach ($answers as $answer) : ?>
                            <?
                                $content = false;
                                if (preg_match('/<mark>/um', $answer['_content'])) {
                                    $content = processContent($answer['_content'], $phrase);
                                } else if (preg_match('/<mark>/um', $answer['_header'])) {
                                    $content = processContent($answer['_header'], $phrase);
                                } else {
                                    $content = $answer['_header'];
                                }
                            ?>
                
                            <div class="search-result__content">
                                <h3 class="search-result__title"><?=$answer['title']?></h3>
                                <p class="search-result__text"><?=$content?></p>
                                <a class="search-result__link" href="<?=Url::toRoute(['/page/'.$answer['slug']])?>"><?=Url::toRoute(['/page/'.$answer['slug']], true);?></a>
                            </div>
                        <? endforeach; ?>
                    <? endif?>               
                </div>
                <div class="pagination p-search__pagination">
                    <? if ($answers && count($answers)) : ?>
                        <?=LinkPager::widget([
                            'pagination' => $provider->pagination,
                            'activePageCssClass' => 'pagination__marker_active',
                            'pageCssClass' => 'pagination__marker',
                            'prevPageLabel' => '',
                            'nextPageLabel' => '',
                            'prevPageCssClass' => 'hidden',
                            'nextPageCssClass' => 'hidden',
                        ]);?>
                    <? endif?>        
                </div>
            </div>
        </div>
    </div>
</div>