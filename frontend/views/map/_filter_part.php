<?
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Object;
use common\models\ObjectType;

foreach ($types as $type){
    $filter[$type->id] = json_decode($type->object_fields, true);
}
if($changed){
    $filter[0] = [];
    foreach ($filter as $key => $item){
        $filter[0] = array_merge($filter[0],$filter[$key]);
    }
    $res_filter = $filter[0];
}else{
    $section = (int)Yii::$app->request->get('section', 0);
    if($section != 8 && $section != 9 && $section != 10){
        $filter[0] = array_merge($filter[1],$filter[2],$filter[3],$filter[4]);
        $res_filter = $filter[0];
    }else{
        $res_filter = $filter[$section];
    }
}

?>
        <?php if(isset($res_filter['address_address']) && $res_filter['address_address']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Адрес</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input map-filter__input_width_full" type="text" name="ObjectMapSearch[address_address]"/>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(isset($res_filter['owner']) && $res_filter['owner']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Форма собственности</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[owner][]" value="<?=Object::OWNER_GOVERNMENT?>" id="owner-<?=Object::OWNER_GOVERNMENT?>" checked="checked"/>
                        <label class="checkbox__label" for="owner-<?=Object::OWNER_GOVERNMENT?>">
                            <span class="checkbox__text" title="<?=Object::getOwner(Object::OWNER_GOVERNMENT)?>"><?=Object::getOwner(Object::OWNER_GOVERNMENT)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[owner][]" value="<?=Object::OWNER_MUNICIPAL?>" id="owner-<?=Object::OWNER_MUNICIPAL?>" checked="checked"/>
                        <label class="checkbox__label" for="owner-<?=Object::OWNER_MUNICIPAL?>">
                            <span class="checkbox__text" title="<?=Object::getOwner(Object::OWNER_MUNICIPAL)?>"><?=Object::getOwner(Object::OWNER_MUNICIPAL)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[owner][]" value="<?=Object::OWNER_PRIVATE?>" id="owner-<?=Object::OWNER_PRIVATE?>" checked="checked"/>
                        <label class="checkbox__label" for="owner-<?=Object::OWNER_PRIVATE?>">
                            <span class="checkbox__text" title="<?=Object::getOwner(Object::OWNER_PRIVATE)?>"><?=Object::getOwner(Object::OWNER_PRIVATE)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[owner][]" value="<?=Object::OWNER_NOT_DISTRICT?>" id="owner-<?=Object::OWNER_NOT_DISTRICT?>" checked="checked"/>
                        <label class="checkbox__label" for="owner-<?=Object::OWNER_NOT_DISTRICT?>">
                            <span class="checkbox__text" title="<?=Object::getOwner(Object::OWNER_NOT_DISTRICT)?>"><?=Object::getOwner(Object::OWNER_NOT_DISTRICT)?></span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>
        <?php if(isset($res_filter['bargain_type']) && $res_filter['bargain_type']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Форма реализации</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[bargain_type][]" value="<?=Object::BARGAIN_TYPE_SALE?>" id="bargain_type-<?=Object::BARGAIN_TYPE_SALE?>" checked="checked"/>
                        <label class="checkbox__label" for="bargain_type-<?=Object::BARGAIN_TYPE_SALE?>">
                            <span class="checkbox__text" title="<?=Object::getBargain(Object::BARGAIN_TYPE_SALE)?>"><?=Object::getBargain(Object::BARGAIN_TYPE_SALE)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[bargain_type][]" value="<?=Object::BARGAIN_TYPE_RENT?>" id="bargain_type-<?=Object::BARGAIN_TYPE_RENT?>" checked="checked"/>
                        <label class="checkbox__label" for="bargain_type-<?=Object::BARGAIN_TYPE_RENT?>">
                            <span class="checkbox__text" title="<?=Object::getBargain(Object::BARGAIN_TYPE_RENT)?>"><?=Object::getBargain(Object::BARGAIN_TYPE_RENT)?></span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['bargain_price']) && $res_filter['bargain_price']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Стоимость, руб</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[bargain_price][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[bargain_price][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>
        <?php if(isset($res_filter['square']) && $res_filter['square']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Площадь, га</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[square][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[square][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['square_sqm']) && $res_filter['square_sqm']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Площадь, кв. м</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[square_sqm][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[square_sqm][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['cadastral_number']) && $res_filter['cadastral_number']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Кадастровый номер</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input map-filter__input_width_full" type="text" name="ObjectMapSearch[cadastral_number]"/>
                    </div>
                </div>
            </div>
        <?  } ?>
        <?php if(isset($res_filter['distance_to_moscow']) && $res_filter['distance_to_moscow']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Удаленность от Москвы, км</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[distance_to_moscow][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[distance_to_moscow][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>
        <?php if(isset($res_filter['square_target']) && $res_filter['square_target']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Назначение площади</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input map-filter__input_width_full" type="text" name="ObjectMapSearch[square_target]"/>
                    </div>
                </div>
            </div>
        <?  } ?>
<?php if((isset($res_filter['has_electricity_supply']) && $res_filter['has_electricity_supply']) || (isset($res_filter['has_gas_supply']) && $res_filter['has_gas_supply'])|| (isset($res_filter['has_water_supply']) && $res_filter['has_water_supply']) || (isset($res_filter['has_heat_supply']) && $res_filter['has_heat_supply']) || (isset($res_filter['has_water_removal_supply']) && $res_filter['has_water_removal_supply'])){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Подключения</div>
                <div class="map-filter__inner">
                    <?php if(isset($res_filter['has_electricity_supply']) && $res_filter['has_electricity_supply']){ ?>
                        <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                            <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_electricity_supply][]" id="has_electricity_supply" value="1"/>
                            <label class="checkbox__label" for="has_electricity_supply">
                                <span class="checkbox__text" title="">Электроснабжение</span>
                            </label>
                        </div>
                    <?  } ?>

                    <?php if(isset($res_filter['has_gas_supply']) && $res_filter['has_gas_supply']){ ?>
                        <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                            <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_gas_supply][]" id="has_gas_supply" value="1"/>
                            <label class="checkbox__label" for="has_gas_supply">
                                <span class="checkbox__text" title="">Газоснабжение</span>
                            </label>
                        </div>
                    <?  } ?>

                    <?php if(isset($res_filter['has_water_supply']) && $res_filter['has_water_supply']){ ?>
                        <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                            <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_water_supply][]" id="has_water_supply" value="1"/>
                            <label class="checkbox__label" for="has_water_supply">
                                <span class="checkbox__text" title="">Водоснабжение</span>
                            </label>
                        </div>
                    <?  } ?>

                    <?php if(isset($res_filter['has_heat_supply']) && $res_filter['has_heat_supply']){ ?>
                        <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                            <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_heat_supply][]" id="has_heat_supply" value="1"/>
                            <label class="checkbox__label" for="has_heat_supply">
                                <span class="checkbox__text" title="">Теплоснабжение</span>
                            </label>
                        </div>
                    <?  } ?>

                    <?php if(isset($res_filter['has_water_removal_supply']) && $res_filter['has_water_removal_supply']){ ?>
                        <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                            <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_water_removal_supply][]" id="has_water_removal_supply" value="1"/>
                            <label class="checkbox__label" for="has_water_removal_supply">
                                <span class="checkbox__text" title="">Водоотведение</span>
                            </label>
                        </div>
                    <?  } ?>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['land_category']) && $res_filter['land_category']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Категория земель</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[land_category][]" value="<?=Object::LAND_CATEGORY_SETTLEMENTS?>" id="land_category-<?=Object::LAND_CATEGORY_SETTLEMENTS?>" checked="checked"/>
                        <label class="checkbox__label" for="land_category-<?=Object::LAND_CATEGORY_SETTLEMENTS?>">
                            <span class="checkbox__text" title="<?=Object::getLandCategoryData(Object::LAND_CATEGORY_SETTLEMENTS)?>"><?=Object::getLandCategoryData(Object::LAND_CATEGORY_SETTLEMENTS)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[land_category][]" value="<?=Object::LAND_CATEGORY_INDUSTRIAL_PURPOSE?>" id="land_category-<?=Object::LAND_CATEGORY_INDUSTRIAL_PURPOSE?>" checked="checked"/>
                        <label class="checkbox__label" for="land_category-<?=Object::LAND_CATEGORY_INDUSTRIAL_PURPOSE?>">
                            <span class="checkbox__text" title="<?=Object::getLandCategoryData(Object::LAND_CATEGORY_INDUSTRIAL_PURPOSE)?>"><?=Object::getLandCategoryData(Object::LAND_CATEGORY_INDUSTRIAL_PURPOSE)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[land_category][]" value="<?=Object::LAND_CATEGORY_AGRICULTURE?>" id="land_category-<?=Object::LAND_CATEGORY_AGRICULTURE?>" checked="checked"/>
                        <label class="checkbox__label" for="land_category-<?=Object::LAND_CATEGORY_AGRICULTURE?>">
                            <span class="checkbox__text" title="<?=Object::getLandCategoryData(Object::LAND_CATEGORY_AGRICULTURE)?>"><?=Object::getLandCategoryData(Object::LAND_CATEGORY_AGRICULTURE)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[land_category][]" value="<?=Object::LAND_CATEGORY_RESERVE?>" id="land_category-<?=Object::LAND_CATEGORY_RESERVE?>" checked="checked"/>
                        <label class="checkbox__label" for="land_category-<?=Object::LAND_CATEGORY_RESERVE?>">
                            <span class="checkbox__text" title="Особо охраняемые территории">Особо охраняемые территории</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['has_buildings']) && $res_filter['has_buildings']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Наличие строений</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_buildings][]" value="1" id="has_buildings-1" checked="checked"/>
                        <label class="checkbox__label" for="has_buildings-1">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_buildings][]" value="0" id="has_buildings-0" checked="checked"/>
                        <label class="checkbox__label" for="has_buildings-0">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['has_road_availability']) && $res_filter['has_road_availability']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Доступ к участку с автодороги</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_road_availability][]" value="1" id="has_road_availability-1" checked="checked"/>
                        <label class="checkbox__label" for="has_road_availability-1">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_road_availability][]" value="0" id="has_road_availability-0" checked="checked"/>
                        <label class="checkbox__label" for="has_road_availability-0">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['has_railway_availability']) && $res_filter['has_railway_availability']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Присоединение к ж/д станции</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_railway_availability][]" value="1" id="has_railway_availability-1" checked="checked"/>
                        <label class="checkbox__label" for="has_railway_availability-1">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_railway_availability][]" value="0" id="has_railway_availability-0" checked="checked"/>
                        <label class="checkbox__label" for="has_railway_availability-0">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['has_charge']) && $res_filter['has_charge']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Наличие обременения</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_charge][]" value="1" id="has_charge-1" checked="checked"/>
                        <label class="checkbox__label" for="has_charge-1">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[has_charge][]" value="0" id="has_charge-0" checked="checked"/>
                        <label class="checkbox__label" for="has_charge-0">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['is_special_ecological_zone']) && $res_filter['is_special_ecological_zone']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Расположение в ОЭЗ</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[is_special_ecological_zone][]" value="1" id="is_special_ecological_zone-1" checked="checked"/>
                        <label class="checkbox__label" for="is_special_ecological_zone-1">
                            <span class="checkbox__text" title="Да">Да</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[is_special_ecological_zone][]" value="0" id="is_special_ecological_zone-0" checked="checked"/>
                        <label class="checkbox__label" for="is_special_ecological_zone-0">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['danger_class']) && $res_filter['danger_class']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Опасность производства</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[danger_class][]" value="<?=Object::DANGER_CLASS_1?>" id="danger_class-<?=Object::DANGER_CLASS_1?>" checked="checked"/>
                        <label class="checkbox__label" for="danger_class-<?=Object::DANGER_CLASS_1?>">
                            <span class="checkbox__text" title="<?=Object::getDangerClass(Object::DANGER_CLASS_1)?>"><?=Object::getDangerClass(Object::DANGER_CLASS_1)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[danger_class][]" value="<?=Object::DANGER_CLASS_2?>" id="danger_class-<?=Object::DANGER_CLASS_2?>" checked="checked"/>
                        <label class="checkbox__label" for="danger_class-<?=Object::DANGER_CLASS_2?>">
                            <span class="checkbox__text" title="<?=Object::getDangerClass(Object::DANGER_CLASS_2)?>"><?=Object::getDangerClass(Object::DANGER_CLASS_2)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[danger_class][]" value="<?=Object::DANGER_CLASS_3?>" id="danger_class-<?=Object::DANGER_CLASS_3?>" checked="checked"/>
                        <label class="checkbox__label" for="danger_class-<?=Object::DANGER_CLASS_3?>">
                            <span class="checkbox__text" title="<?=Object::getDangerClass(Object::DANGER_CLASS_3)?>"><?=Object::getDangerClass(Object::DANGER_CLASS_3)?></span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[danger_class][]" value="<?=Object::DANGER_CLASS_4?>" id="danger_class-<?=Object::DANGER_CLASS_4?>" checked="checked"/>
                        <label class="checkbox__label" for="danger_class-<?=Object::DANGER_CLASS_4?>">
                            <span class="checkbox__text" title="<?=Object::getDangerClass(Object::DANGER_CLASS_4)?>"><?=Object::getDangerClass(Object::DANGER_CLASS_4)?></span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['free_land']) && $res_filter['free_land']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Наличие свободных земельных участков</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[free_land][]" value="1" id="free_land-1" checked="checked"/>
                        <label class="checkbox__label" for="free_land-1>">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[free_land][]" value="0" id="free_land-0" checked="checked"/>
                        <label class="checkbox__label" for="free_land-0>">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['free_room']) && $res_filter['free_room']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Наличие свободных площадей</div>
                <div class="map-filter__inner">
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[free_room][]" value="1" id="free_room-1" checked="checked"/>
                        <label class="checkbox__label" for="free_room-1>">
                            <span class="checkbox__text" title="Есть">Есть</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart map-filter__checkbox">
                        <input class="checkbox__input" type="checkbox" name="ObjectMapSearch[free_room][]" value="0" id="free_room-0" checked="checked"/>
                        <label class="checkbox__label" for="free_room-0>">
                            <span class="checkbox__text" title="Нет">Нет</span>
                        </label>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['okved']) && $res_filter['okved']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">ОКВЭД</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input map-filter__input_width_full" type="text" name="ObjectMapSearch[okved]"/>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['flat']) && $res_filter['flat']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Этажность</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[flat][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[flat][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>

        <?php if(isset($res_filter['flat_height']) && $res_filter['flat_height']){ ?>
            <div class="map-filter__box" data-type="inputs">
                <div class="map-filter__sub-title">Высота потолка, м</div>
                <div class="map-filter__inner">
                    <div class="map-filter__inputs">
                        <input class="map-filter__input" type="text" placeholder="От" name="ObjectMapSearch[flat_height][]"/>
                        <input class="map-filter__input" type="text" placeholder="До" name="ObjectMapSearch[flat_height][]"/>
                    </div>
                </div>
            </div>
        <?  } ?>