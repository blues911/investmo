<?php
/* @var $this yii\web\View */
use frontend\assets\MapAsset;
use common\models\Object;
use frontend\widgets\FeedBackWidget;
use frontend\widgets\MapFeedBackWidget;
MapAsset::register($this);
$this->title = 'Инвестиционный портал Московской области';
$separatedIds = [];
foreach ($separated as $objectType) {
    $separatedIds[] = $objectType->id;
}

$js = <<<JS
    $(document).on('keydown','.map-filter__search-input',function(e){
        if(e.keyCode == 13){
             e.preventDefault();
            return false;
        }
    });
JS;
$this->registerJs($js);
?>

<div class="p-map">
    <div class="map-nav custom-scroll custom-scroll custom-scroll_theme_horizontal p-map__nav">
        <div class="map-nav__container">
            <div class="map-nav__items">
                <div class="map-nav__item-box map-nav__item <?= $section == 0 ? 'map-nav__item_active' : ''?>" data-id="0">
                    <div class="map-nav__box">
                        <div class="map-nav__sub-btn">Пром. площадки, ОЭЗ<sup class="map-nav__count"><?=$totalContainersCount?></sup>
                        </div>

                        <form class="map-nav__box-items map-nav__box-items_mod">
                            <?php foreach ($containers as $container): ?>
                                <div class="map-nav__box-item map-nav__box-item_contains_inner">
                                    <span class="map-nav__arrow"></span>
                                    <div class="map-nav__inner-items">
                                        <?php foreach ($children as $child): ?>
                                            <div class="checkbox checkbox_theme_standart checkbox_inner map-nav__checkbox">
                                                <input class="checkbox__input" type="checkbox" name="sub-<?=$container->id?>-<?=$child->id?>" id="sub-<?=$container->id?>-<?=$child->id?>"/>
                                                <label class="checkbox__label" for="sub-<?=$container->id?>-<?=$child->id?>">
                                                    <span class="checkbox__text" title="<?=$child->name?>">
                                                        <?=$child->name?> (<?= $container->getCountObjectsTypes($child->id,$container->id) ?>)
                                                    </span>
                                                </label>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="checkbox checkbox_theme_standart checkbox_main map-nav__checkbox">
                                        <input class="checkbox__input" type="checkbox" name="main-<?=$container->id?>" id="main-<?=$container->id?>" <?= ($section == null || $section != 0) ? 'checked' : '' ?>/>
                                        <label class="checkbox__label" for="main-<?=$container->id?>">
                                            <span class="checkbox__text" title="<?=$container->name?>">
                                                <?=$container->name?> (<?= $container->getCountObjects() ?>)
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </form>
                    </div>
                </div>

                <? foreach ($separated as $objectType) : ?>
                    <a class="map-nav__item <?= $section == $objectType->id ? 'map-nav__item_active' : ''?>" href="javascript:void(0)" data-id="<?=$objectType->id?>">
                        <span class="map-nav__text"><?=$objectType->name?>
                            <sup class="map-nav__count"><?=$objectType->getCountObjects()?></sup>
                        </span>
                    </a>
                <? endforeach; ?>
            </div>
            <div class="map-nav__points">
                <div class="map-nav__box">
                    <div class="map-nav__box-btn">Информационные слои
                    </div>
                    <div class="map-nav__box-items">
                        <div class="map-nav__box-item">
                            <div class="checkbox checkbox_theme_standart map-nav__checkbox"><input class="checkbox__input" type="checkbox" name="gas-points" id="gas-points"/>
                                <label class="checkbox__label" for="gas-points"><span class="checkbox__text" title="Точки газофикации">Точки газофикации<sup class="checkbox__count"><?=$gasPoints?></sup></span>
                                </label>
                            </div>
                        </div>
                        <div class="map-nav__box-item">
                            <div class="checkbox checkbox_theme_standart map-nav__checkbox"><input class="checkbox__input" type="checkbox" name="electricity-points" id="electricity-points"/>
                                <label class="checkbox__label" for="electricity-points"><span class="checkbox__text" title="Точки электрификации">Точки электрификации<sup class="checkbox__count"><?=$electricPoints?></sup></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map-nav__search-box"><span class="map-nav__search-btn"></span>
            </div>
        </div>
    </div>
    <div class="p-map__content">
        <div class="map p-map__map">
            <div class="map__map" id="map">
            </div>
            <div class="map-controls map__map-controls">
                <!-- поиск -->
                <button class="map-controls__btn map-controls__btn_search map__btn map__btn_search"></button>
                <!-- -->

                <!-- Увеличение масштаба -->
                <button class="map-controls__btn map-controls__btn_increase map__btn map__btn map__btn_increase"></button>
                <!-- -->

                <!-- Уменьшение масштаба -->
                <button class="map-controls__btn map-controls__btn_decrease map__btn map__btn map__btn_decrease"></button>
                <!-- -->

                <!-- Фильтр -->
                <button class="map-controls__btn map-controls__btn_filter map__btn map__btn map__btn_filter">
                    <svg class="map-controls__show-filter">
                        <use xlink:href="/svg-symbols.svg#filters"></use>
                    </svg>
                </button>
                <!-- -->

                <!-- хз -->
                <button class="map-controls__btn map-controls__btn_fav map__btn map__btn_fav"></button>
                <!-- -->

                <!-- Посмотреть список отфильтрованного -->
                <button class="map-controls__btn map-controls__btn_show-list">
                    <svg class="map-controls__show-list">
                        <use xlink:href="/svg-symbols.svg#list"></use>
                    </svg>
                </button>
                <!-- -->

                <!-- Подписка -->
                    <button class="btn btn_theme_light map-controls__btn map-controls__btn_subscribe map__btn map__btn_subscribe btn_open-popup" data-popup="subscribe">
                        <span class="btn__text">Оформить подписку</span>
                        <span class="btn__arrow"></span>
                    </button>
                <!-- -->

                <!-- Добавление объекта -->
                <button class="map-controls__btn map-controls__btn_add map__btn map__btn map__btn_add" data-popup="add-object"></button>
                <!-- -->
            </div>
            <?= $this->render('_filter',[
                    'types' => $types,
                    'changed' => false
            ]); ?>
            <div class="map__subscribe-box">
                <a data-popup="subscribe" class="btn btn_theme_light map-filter__go-subscribe btn_open-popup" href="javascript:void(0)">
                    <span class="btn__text">Оформить подписку</span>
                </a>
                <a data-popup="add-object" class="btn btn_theme_standart map__button map-filter__show-btn btn_open-popup" href="javascript:void(0)">
                    <span class="btn__text">Добавить объект</span>
                </a>
            </div>

            <?=MapFeedBackWidget::widget(['login' => Yii::$app->session->getFlash('login')])?>
            <form class="form form-info-object" id="form-info-object" action="/feed-back/create" method="POST" style="display:none;">
                <div class="form-info-object__inner"></div>
                <input type="hidden" name="type" value="1"/>
                <input type="hidden" name="selector" value="0"/>
                <div class="form__subtitle">Укажите информацию для обратной связи</div>
                <label class="form__label">Фамилия
                    <input class="form__input valid" type="text" name="lastname" aria-required="true" placeholder="Фамилия"/>
                </label>
                <label class="form__label">Имя
                    <input class="form__input valid" type="text" name="name" aria-required="true" placeholder="Имя"/>
                </label>
                <label class="form__label">Отчество
                    <input class="form__input valid" type="text" name="middlename" aria-required="true" placeholder="Отчество"/>
                </label>
                <label class="form__label">Ваш е-mail адрес
                    <input class="form__input valid" type="email" name="email" aria-required="true" placeholder="Ваш е-mail адрес"/>
                </label>
                <label class="form__label">и ваш телефон
                    <input class="form__input form__input_phone valid" type="text" name="phone" maxlength="18" aria-required="true" placeholder="и ваш телефон"/>
                </label><span class="form__label form__label_visible-mobile">Предполагаемый объем инвестиций</span>
                <radiogroup class="radio-group">
                    <div class="radio radio_theme_light radio_text_light">
                        <input class="radio__input" id="pickup1" type="radio" name="data[pickup]" value="от 5 млн. руб" checked="checked"/>
                        <label class="radio__label" for="pickup1"><span class="radio__text">от 5 млн. руб
                    <div class="radio__cnt"></div></span></label>
                    </div>
                    <div class="radio radio_theme_light radio_text_light">
                        <input class="radio__input" id="pickup2" type="radio" name="data[pickup]" value="от 50 млн. руб"/>
                        <label class="radio__label" for="pickup2"><span class="radio__text">от 50 млн. руб
                    <div class="radio__cnt"></div></span></label>
                    </div>
                    <div class="radio radio_theme_light radio_text_light">
                        <input class="radio__input" id="pickup3" type="radio" name="data[pickup]" value="от 200 млн. руб"/>
                        <label class="radio__label" for="pickup3"><span class="radio__text">от 200 млн. руб
                    <div class="radio__cnt"></div></span></label>
                    </div>
                    <div class="radio radio_theme_light radio_text_light">
                        <input class="radio__input" id="pickup4" type="radio" name="data[pickup]" value="от 1 млрд. руб"/>
                        <label class="radio__label" for="pickup4"><span class="radio__text">от 1 млрд. руб
                    <div class="radio__cnt"></div></span></label>
                    </div>
                </radiogroup>
                <div class="form__group">
                    <label class="form__label form__label_visible-mobile" for="object">Интересующий объект</label>
                    <input class="form__input form__input_object" id="object" type="text" value="" name="data[object]"/>
                </div>
                <div class="form__chooseDocs"><span class="form__label form__label_visible-mobile">Выберите документы для получения</span>
                    <select class="form__input-select" name="data[requested_documents][]" multiple="multiple" data-placeholder="Введите название документа или выберите из списка">
                        <option value="Заявка на реализацию проекта">Заявка на реализацию проекта</option>
                        <option value="Заявка на размещение объекта">Заявка на размещение объекта</option>
                        <option value="Анкета для получения поддержки">Анкета для получения поддержки</option>
                    </select>
                </div>
                <div class="form__chooseDocs"><span class="form__label form__label_visible-mobile">Укажите тип поддержки</span>
                    <select class="form__input-select" name="data[support_type][]" multiple="multiple" data-placeholder="Например, налоговая льгота">
                        <option value="Налоговая льгота">Налоговая льгота</option>
                        <option value="Имущественная поддержка">Имущественная поддержка</option>
                        <option value="Финансовая поддержка">Финансовая поддержка</option>
                    </select>
                </div>
                <div class="form__chooseDocs"><span class="form__label form__label_visible-mobile">Укажите отрасль</span>
                    <select class="form__input-select" name="data[branch][]" multiple="multiple" data-placeholder="Например, пищевая промышленность">
                        <option value="Аэрокосмическая промышленность">Аэрокосмическая промышленность</option>
                        <option value="Производство медицинского и научного оборудования">Производство медицинского и научного оборудования</option>
                        <option value="Производство промышленного оборудования">Производство промышленного оборудования</option>
                        <option value="Производство контрольных и измерительных приборов">Производство контрольных и измерительных приборов</option>
                        <option value="Автомобилестроение">Автомобилестроение</option>
                        <option value="Химия">Химия</option>
                        <option value="Производство пластиков">Производство пластиков</option>
                        <option value="Фармацевтика и биофармацевтика">Фармацевтика и биофармацевтика</option>
                        <option value="Производство строительных и отделочных материалов">Производство строительных и отделочных материалов</option>
                        <option value="Строительные и инжиниринговые работы">Строительные и инжиниринговые работы</option>
                        <option value="Пищевая промышленность">Пищевая промышленность</option>
                        <option value="Производство напитков">Производство напитков</option>
                        <option value="Производство мебели">Производство мебели</option>
                        <option value="гостиничный бизнес и туризм">гостиничный бизнес и туризм</option>
                    </select>
                </div>
                <div class="form__group">
                    <label class="form__label form__label_visible-mobile" for="message">Комментарий</label>
                    <textarea class="form__textarea form__textarea_message" id="message" name="data[comment]"></textarea>
                </div>
                <div id="myCaptcha"></div>
                <input type="hidden" name="type" value="0"/>
                <button class="btn btn-default" type="submit">Запросить консультацию</button>
            </form>
        </div>
        <div class="map-results p-map__results">
            <div class="map-results__box map-results__box_results custom-scroll custom-scroll custom-scroll_theme_standart">
                <div class="map-results__close">
                </div>
                <div class="map-results__title">Найдено&nbsp;<span class="map-result__count">0</span> объектов
                </div>
                <div class="map-results__items">
                </div>
            </div>
            <div class="map-results__box map-results__box_result custom-scroll custom-scroll custom-scroll_theme_standart">
                <form class="map-result map-results__map-result"><a class="map-result__back-link" href="javascript:void(0)">Назад к результатам</a>
                    <div class="map-result__dynamic-content">
                    </div>
                    <div class="map-result__submit-box"><a class="map-result__submit js-open-popup" href="#form-info-object">Отправить запрос</a><a class="map-result__more" href="javascript:void(0)">Подробнее</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="map-card p-map__map-card" id="map-card">
           <span class="map-card__close-btn">
              <svg class="icon icon_close-dark " width="17px" height="15px">
                  <use xlink:href="svg-symbols.svg#close-dark"></use>
              </svg>
            </span>        
            <div class="map-card__dynamic-content">
            </div>
            <div class="map-card__bottom"><a class="map-card__send js-open-popup" href="#form-info-object">Отправить запрос</a>
            </div>
        </div>
    </div>
</div>