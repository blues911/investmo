<?php

/* @var $this yii\web\View */
$this->title = Yii::t('app', 'Инвестиционный портал Московской области');
use frontend\widgets\StoryWidget;
use frontend\widgets\MapWidget;
use frontend\widgets\ContentBlockWidget;
use frontend\widgets\FeedBackWidget;
use frontend\widgets\NewsWidget;
?>
<div class="p-index">
    <section class="hero">
        <div class="hero__container container">
            <aside class="hero__sidebar">
                <div class="social social_theme_light social_theme_vertical hero__social">
                    <ul class="social__list">
                        <li class="social__item"><a class="social__link" target="_blank" href="https://vk.com/mininvest">
                                <svg class="icon icon_vk social__icon" width="18px" height="13px">
                                    <use xlink:href="/svg-symbols.svg#vk"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item"><a class="social__link" target="_blank" href="https://www.facebook.com/mii.mosreg.ru/">
                                <svg class="icon icon_fb social__icon" width="8px" height="17px">
                                    <use xlink:href="/svg-symbols.svg#fb"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item"><a class="social__link" target="_blank" href="https://www.instagram.com/mininvest/">
                                <svg class="icon icon_in social__icon" width="14px" height="15px">
                                    <use xlink:href="/svg-symbols.svg#in"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="hero__link-wrap"><a class="hero__link js-hero__link" href="#content"><?=Yii::t('app', 'Смотреть дальше')?></a>
                </div>
            </aside>
            <div class="hero__content">
                <h1 class="hero__title"><strong><?=Yii::t('app', 'Инвестировать</strong> в&nbsp;Московскую Область – <strong>просто!</strong>')?>
                </h1>
                <p class="hero__subtitle"><?=Yii::t('app', 'С вас инициатива, <span>об&nbsp;остальном&nbsp;позаботится</span> Московская&nbsp;Область')?>
                </p><a class="btn btn_theme_standart hero__btn" href="/map"><span class="btn__text"><?=Yii::t('app', 'Выбрать объект')?></span></a>
            </div>
        </div>
    </section>
    <div id="content"></div>
    <div class="p-index__wrapper">
        <?= ContentBlockWidget::set('content_theme_measures') ?>
        <div class="content content_theme_projects" >
            <div class="container">
                <div class="content__inner">
                    <h2 class="content__title"><?=Yii::t('app', '<strong>Удобно выбрать,</strong> просто начать')?>
                    </h2>
                    <div class="projects">
                        <?= ContentBlockWidget::set('content_theme_projects',['industry_parks' => $industry_parks_count, 'brownfields' => $brownfields_count, 'land_plots' => $land_plots_count, 'techno_parks' => $techno_parks_count]) ?>
                        <div class="projects__location">
                            <div class="projects__left">
                                <h4 class="projects__title"><?=Yii::t('app', 'Все объекты на единой интерактивной карте')?>
                                </h4>
                                <p class="projects__count"><?=Yii::t('app', 'более <strong>50</strong>')?>
                                </p>
                                <p class="projects__text"><?=Yii::t('app', 'индустриальных парков и браунфилдов, собранных на единой интерактивной карте портала')?>
                                </p><a class="projects__btn" href="/map"><?=Yii::t('app', 'Все объекты на карте')?></a>
                            </div>
                            <div class="projects__right">
                                <a class="projects__cecutient-mode-text" href="/map"><?=Yii::t('app', 'Перейти к объектам <br> на карте')?></a>
                                <div class="projects__map-wrap">
                                    <?= MapWidget::widget() ?>
                                    <svg class="icon icon_map projects__map" width="623px" height="589px">
                                        <use xlink:href="/svg-symbols.svg#map"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content content_theme_support">
            <div class="container">
                <div class="content__inner">
                    <h2 class="content__title"><?=Yii::t('app', '<strong>Помогаем</strong> лично')?>
                    </h2>
                    <div class="support">
                        <div class="support__row">
                            <div class="support__left">
                                <p class="support__subtitle"><?=Yii::t('app', 'При решении ваших проблем мы всегда используем индивидуальный подход')?>
                                </p>
                                <ul class="support__list">
                                    <li class="support__item">
                                        <div class="support__icon-wrap">
                                            <svg class="icon icon_search support__icon" width="42px" height="42px">
                                                <use xlink:href="/svg-symbols.svg#search"></use>
                                            </svg>

                                        </div>
                                        <p class="support__text"><?=Yii::t('app', 'Подбор проекта <br> по вашим запросам')?>
                                        </p>
                                    </li>
                                    <li class="support__item">
                                        <div class="support__icon-wrap">
                                            <svg class="icon icon_docs support__icon" width="100px" height="125px">
                                                <use xlink:href="/svg-symbols.svg#docs"></use>
                                            </svg>

                                        </div>
                                        <p class="support__text"><?=Yii::t('app', 'Предоставление типовых документов')?>
                                        </p>
                                    </li>
                                    <li class="support__item">
                                        <div class="support__icon-wrap">
                                            <svg class="icon icon_headphones support__icon" width="74px" height="80px">
                                                <use xlink:href="/svg-symbols.svg#headphones"></use>
                                            </svg>

                                        </div>
                                        <p class="support__text"><?=Yii::t('app', 'Профессиональные консультации')?>
                                        </p>
                                    </li>
                                    <li class="support__item support__item_center">
                                        <p class="support__text"><?=Yii::t('app','Телефоны для консультаций')?> <br> +7 (495) 109-07-07 <br> +7 (495) 668-00-99</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="support__right">
                                <?= FeedBackWidget::widget(['selector' => true]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content content_theme_stories">
            <div class="container">
                <div class="content__inner">
                    <h2 class="content__title"><?=Yii::t('app', '<strong>Истории,</strong> которыми мы гордимся')?></h2>
                    <?= StoryWidget::widget();?>
                </div>
            </div>
        </div>

        <?= ContentBlockWidget::set('content_theme_quote') ?>
        <?= ContentBlockWidget::set('content_theme_conditions') ?>

        <div class="content content_theme_news">
            <div class="container">
                <div class="content__inner">
                    <?= NewsWidget::widget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>