<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\FormAjaxAsset;

FormAjaxAsset::register($this);

$this->title = 'Смена Пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container" style="margin: 10rem auto">
    <div class="site-reset-password">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Пожалуйста введите Ваш новый пароль:</p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'options' => ['class' => 'form_ajax', 'data-message' => 'alert']]); ?>

                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>