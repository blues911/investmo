<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="content">
    <div class="container">
        <div class="content__inner">
            <div class="site-error">

                <h1><?= Html::encode($this->title) ?></h1>

                <div class="alert alert-danger">
                    <p>К сожалению, запрашиваемая страница не найдена</p>
                    <?/*= nl2br(Html::encode($message)) */?>
                    <a href="/">Вернуться на главную страницу</a>
                </div>
            </div>
        </div>
    </div>
</div>

