<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\assets\FormAjaxAsset;

FormAjaxAsset::register($this);

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container" style="margin: 10rem auto">
    <div class="site-request-password-reset">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>Пожалуйста, введите e-mail, используемый Вами для авторизации на портале</p>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form' , 'enableClientValidation'=> false, 'options' => ['class' => 'form_ajax', 'data-message' => 'alert']]); ?>

                <?= $form->field($model, 'email')->textInput(['required' => 'required'])->hint('') ?>

                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

