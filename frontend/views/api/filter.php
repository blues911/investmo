<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 23.05.2017
 * Time: 21:30
 */

$answer = ["type" => "FeatureCollection", "features" => []];
$k = 0;
$answer['name'] = $name;
foreach ($objects as $k => $object) {
    $answer['features'][] = [
        'type' => 'feature',
        "id" => $object->id,
        'geometry' => [
            'type' => "Point",
            'coordinates' => [$object->latitude, $object->longitude]
        ],
        'properties' => [
            "name" => $object->name ? $object->name : '',
            "owner" => $object->owner_name ? $object->owner_name : '',
            "address" => $object->address_address ? $object->address_address : '',
            "phone" => $object->phone ? $object->phone : '',
            'icons' => [],
            'details' => [
                ["title" => "Собственник",
                    "text" => $object->owner_name ? $object->owner_name : ''],
                ["title" => "Статус",
                    "text" => $object->bargain_type ? $object->bargain_type : ''],
                ["title" => "Площадь",
                    "text" => $object->square ? $object->square : ''],
                ["title" => "Стоимость",
                    "text" => $object->bargain_price ? $object->bargain_price : ''],
            ],
        ],
        'options' => ["preset" => $object->setObjectMapIcon()],

    ];

    if ($object->has_electricity_supply) {
        $answer['features'][$k]['properties']['icons'][] = [
            "className" => "electricity",
            "title" => "электричество"
        ];
    }

    if ($object->has_gas_supply) {
        $answer['features'][$k]['properties']['icons'][] = [
            "className" => "gas",
            "title" => "газ"
        ];
    }

    if ($object->getPhotos()->one() && $object->getPhotos()->one()->exists()) {
        if($object->getPhotos()->one()->exists('icon_map'))
            $answer['features'][$k]['properties']['preview'] = $object->getPhotos()->one()->getUrl('icon_map');
        else
            $answer['features'][$k]['properties']['preview'] = $object->getPhotos()->one()->getUrl();
    } else {
        $answer['features'][$k]['properties']['preview'] = '/no_image.svg';
    }
}

echo json_encode($answer, JSON_UNESCAPED_UNICODE);