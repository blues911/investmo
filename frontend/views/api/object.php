<?php

use common\models\Object;

/*'other': [
    {
        'title' => 'Свободные участки',
        'items': [
            'Участок 59 га', 'Участок 34 га', 'Участок 1 га', 'Участок 65 га'
        ]
    },
    {
        'title' => 'Свободные площади',
        'items': [
            'Корпус Г, 560 м2', 'Корпус Г, 50 м2', 'Корпус Г, 330 м2', 'Корпус Г, 60 м2'
        ]
    }
]*/

$answer = [
    'title' => $object->name,
    'object_type_id' => $object->object_type_id,
    'slider' => [],
    'details' => [],
    'other' => [],
    'residents' => [],
    'documents' => [],
];

if ($object->getPhotos()->all()) {
    foreach ($object->getPhotos()->all() as $photo) {
        if (!$photo->exists()) {
            continue;
        }
        $answer['slider'][] = [
            'img' => $photo->getUrl()
        ];
    }
}
if (!isset($answer['slider']) || empty($answer['slider'])) {
    $answer['slider'][] = [
        'img' => '/no_image.svg',
    ];
}


foreach ($object->children as $child) {
    $answer['residents'][] = [
        'img' => $object->getPhotos()->one() ? $child->getPhotos()->one()->getUrl() : '/no_image.svg',
        'text' => $child->name,
        'id' => $child->id
    ];
}


function formatBytes($size, $precision = 2)
{
    $base = log($size, 1024);
    $suffixes = array('', 'Kb', 'Mb', 'Gb', 'Tb');   
    return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
}

if ($object->getDocuments()->all()) {
    foreach ($object->getDocuments()->all() as $document) {
        if (!$document->exists()) {
            continue;
        }
        $answer['documents'][] = [
            'path' => '/uploads/objects_documents/'.$document->object_id.'/'.$document->file,
            'title' => $document->title,
            'format' => $document->format,
            'size' => formatBytes($document->size),
        ];
    }
}

Yii::$app->formatter->decimalSeparator = '.';
Yii::$app->formatter->thousandSeparator = ' ';
if ($object->latitude !== null && $object->longitude !== null) {
    $answer['details'][] = [
        'title' => 'Координаты',
        'longitude' => $object->longitude,
        'latitude' => $object->latitude
    ];
}
if ($object->description !== null && $object->description) {
    $answer['details'][] = [
        'title' => 'Общие сведения',
        'text' => $object->description
    ];
}

if ($object->address_address !== null && $object->address_address) {
    $answer['details'][] = [
        'title' => 'Адрес',
        'text' => $object->address_address
    ];
}

if ($object->phone !== null && $object->phone) {
    $answer['details'][] = [
        'title' => 'Контактный телефон',
        'text' => $object->phone
    ];
}

if ($object->owner_contacts !== null && $object->owner_contacts) {
    $answer['details'][] = [
        'title' => 'Контакты собственника',
        'text' => $object->owner_contacts
    ];
}
if ($object->management_company !== null && $object->management_company) {
    $answer['details'][] = [
        'title' => 'Управляющая компания',
        'text' => $object->management_company
    ];
}
if ($object->municipality !== null && $object->municipality) {
    $answer['details'][] = [
        'title' => 'Муниципальное образование',
        'text' => $object->municipality
    ];
}
if ($object->municipal_education_contacts !== null && $object->municipal_education_contacts) {
    $answer['details'][] = [
        'title' => 'Контакты муниципального образования',
        'text' => $object->municipal_education_contacts
    ];
}
if ($object->object_contact_face !== null && $object->object_contact_face) {
    $answer['details'][] = [
        'title' => 'Контактное лицо по объекту',
        'text' => $object->object_contact_face
    ];
}
if ($object->email && $object->email !== null) {
    $answer['details'][] = [
        'title' => 'E-mail',
        'text' => $object->email
    ];
}

if ($object->website && $object->website !== null) {
    $answer['details'][] = [
        'title' => 'Сайт',
        'text' => $object->website
    ];
}
if ($object->owner !== null && $object->owner) {
    $answer['details'][] = [
        'title' => 'Форма собственности',
        'text' => $object::getOwner($object->owner)
    ];
}

if ($object->owner_name !== null && $object->owner_name) {
    $answer['details'][] = [
        'title' => 'Собственник',
        'text' => $object->owner_name
    ];
}

if ($object->cadastral_number !== null && $object->cadastral_number) {
    $answer['details'][] = [
        'title' => 'Кадастровый номер',
        'text' => $object->cadastral_number
    ];
}

if ($object->industry_type !== null && $object->industry_type) {
    $answer['details'][] = [
        'title' => 'Сектор промышленности',
        'text' => $object::getIndustryTypes($object->industry_type)
    ];
}

if ($object->square_target !== null && $object->square_target) {
    $answer['details'][] = [
        'title' => 'Назначение площади',
        'text' => $object->square_target
    ];
}

if ($object->square !== null && $object->square) {
    $answer['details'][] = [
        'title' => 'Площадь, га',
        'text' => Yii::$app->formatter->asDecimal($object->square),
    ];
}

if ($object->square_sqm !== null && $object->square_sqm) {
    $answer['details'][] = [
        'title' => 'Площадь, кв.м',
        'text' => Yii::$app->formatter->asDecimal($object->square_sqm),
    ];
}

if ($object->distance_to_moscow !== null && $object->distance_to_moscow) {
    $answer['details'][] = [
        'title' => 'Расстояние до Москвы, км',
        'text' => Yii::$app->formatter->asDecimal($object->distance_to_moscow),
    ];
}

// Атрибуты наличия мощностей
if ($object->has_electricity_supply && $object->electricity_supply_capacity || $object->electricity_supply_capacity) {
    $name = 'Да, ' . $object->electricity_supply_capacity . 'МВт';
    $answer['details'][] = [
        'title' => 'Наличие электроснабжения',
        'text' => $name,
    ];
} elseif ($object->has_electricity_supply) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Наличие электроснабжения',
        'text' => $name,
    ];
}

if ($object->has_gas_supply && $object->gas_supply_capacity || $object->gas_supply_capacity) {
    $name = 'Да, ' . $object->gas_supply_capacity . 'куб.м/час';
    $answer['details'][] = [
        'title' => 'Наличие газоснабжения',
        'text' => $name,
    ];
} elseif ($object->has_gas_supply) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Наличие газоснабжения',
        'text' => $name,
    ];
}

if ($object->has_water_supply && $object->water_supply_capacity || $object->water_supply_capacity) {
    $name = 'Да, ' . $object->water_supply_capacity . 'куб.м/день';
    $answer['details'][] = [
        'title' => 'Наличие водоснабжения',
        'text' => $name,
    ];
} elseif ($object->has_water_supply) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Наличие водоснабжения',
        'text' => $name,
    ];
}

if ($object->has_water_removal_supply && $object->water_removal_supply_capacity || $object->water_removal_supply_capacity) {
    $name = 'Да, ' . $object->water_removal_supply_capacity . 'куб.м/день';
    $answer['details'][] = [
        'title' => 'Наличие водоотведения',
        'text' => $name,
    ];
} elseif ($object->has_water_removal_supply) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Наличие водоотведения',
        'text' => $name,
    ];
}

if ($object->has_heat_supply && $object->heat_supply_capacity || $object->heat_supply_capacity) {
    $name = 'Да, ' . $object->heat_supply_capacity . 'Гкал/час';
    $answer['details'][] = [
        'title' => 'Наличие теплоснабжения',
        'text' => $name,
    ];
} elseif ($object->has_heat_supply) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Наличие теплоснабжения',
        'text' => $name,
    ];
}
if ($object->can_use_water_drain && $object->power_water_drain || $object->power_water_drain) {
    $name = 'Да, ' . $object->power_water_drain . ' М3/час';
    $answer['details'][] = [
        'title' => 'Возможность подключения к ливневой канализации',
        'text' => $name,
    ];
} elseif ($object->can_use_water_drain) {
    $name = 'Да';
    $answer['details'][] = [
        'title' => 'Возможность подключения к ливневой канализации',
        'text' => $name,
    ];
}
if ($object->land_category !== null && $object->land_category) {
    $answer['details'][] = [
        'title' => 'Категория земель',
        'text' => $object::getLandCategoryData($object->land_category)
    ];
}

if ($object->land_status !== null && $object->land_status) {
    $answer['details'][] = [
        'title' => 'Статус участка',
        'text' => $object::getLandStatusData($object->land_status)
    ];
}


if ($object->vri !== null && $object->vri) {
    $answer['details'][] = [
        'title' => 'ВРИ',
        'text' => $object->vri
    ];
}
if ($object->area_config !== null && $object->area_config) {
    $answer['details'][] = [
        'title' => 'Конфигурация участка',
        'text' => $object->area_config
    ];
}
if ($object->location_feature !== null && $object->location_feature) {
    $answer['details'][] = [
        'title' => 'Характеристика местности',
        'text' => $object->location_feature
    ];
}
if ($object->buildings_count !== null && $object->buildings_count) {
    $answer['details'][] = [
        'title' => 'Число строений',
        'text' => $object->buildings_count
    ];
}


if ($object->distance_to_nearest_road !== null && $object->distance_to_nearest_road) {
    $answer['details'][] = [
        'title' => 'Расстояние до автодороги, км',
        'text' => $object->distance_to_nearest_road
    ];
}
if ($object->distance_to_major_transport_routes !== null && $object->distance_to_major_transport_routes) {
    $answer['details'][] = [
        'title' => 'Расстояние до крупных транспортных путей, км',
        'text' => $object->distance_to_major_transport_routes
    ];
}
if ($object->has_buildings !== null && $object->has_buildings) {
    $object->has_buildings ? $text = 'Да' : $text = 'Нет';
    $answer['details'][] = [
        'title' => 'Наличие строений',
        'text' => $text
    ];
}

if ($object->has_road_availability !== null && $object->has_road_availability) {
    $object->has_road_availability ? $text = 'Да' : $text = 'Нет';
    $answer['details'][] = [
        'title' => 'Наличие доступа к автодороге',
        'text' => $text
    ];
}

if ($object->has_railway_availability !== null && $object->has_railway_availability) {
    $object->has_railway_availability ? $text = 'Да' : $text = 'Нет';
    $answer['details'][] = [
        'title' => 'Возможность присоединения к грузовой ж/д станции',
        'text' => $text
    ];
}
if ($object->distance_to_rw_station !== null && $object->distance_to_rw_station) {
    $answer['details'][] = [
        'title' => 'Расстояние до ж/д станции, км',
        'text' => $object->distance_to_rw_station
    ];
}
if ($object->pipe_burial !== null && $object->pipe_burial) {
    $answer['details'][] = [
        'title' => 'Места захоронений, нефтяной трубопровод, проектируемые линии ж/д',
        'text' => $object->pipe_burial
    ];
}
if ($object->hotel_available !== null && $object->hotel_available) {
    $answer['details'][] = [
        'title' => 'Наличие гостиниц',
        'text' => $object->hotel_available
    ];
}
if ($object->workforse_available !== null && $object->workforse_available) {
    $answer['details'][] = [
        'title' => 'Наличие рабочей силы',
        'text' => $object->workforse_available
    ];
}
if ($object->available_communication_system !== null && $object->available_communication_system) {
    $answer['details'][] = [
        'title' => 'Наличие системы связи',
        'text' => $object->available_communication_system
    ];
}
if ($object->has_charge !== null && $object->has_charge) {
    $object->has_charge ? $text = 'Да' : $text = 'Нет';
    $answer['details'][] = [
        'title' => 'Наличие обременения',
        'text' => $text
    ];
}

if ($object->charge_type !== null && $object->charge_type != '') {
    $answer['details'][] = [
        'title' => 'Вид обременения',
        'text' => $object->charge_type
    ];
}

if ($object->danger_class !== null && $object->distance_to_nearest_road) {
    $answer['details'][] = [
        'title' => 'Класс опасности производства',
        'text' => $object::getDangerClass($object->danger_class)
    ];
}

if ($object->okved !== null && $object->okved) {
    $answer['details'][] = [
        'title' => 'ОКВЭД',
        'text' => $object->okved
    ];
}

if ($object->flat !== null) {
    $answer['details'][] = [
        'title' => 'Этажность',
        'text' => $object->flat
    ];
}

if ($object->flat_height !== null && $object->flat_height) {
    $answer['details'][] = [
        'title' => 'Высота потолка, м',
        'text' => Yii::$app->formatter->asDecimal($object->flat_height),
    ];
}

if ($object->bargain_price !== null && $object->bargain_price) {
    $answer['details'][] = [
        'title' => 'Стоимость, руб.',
        'text' => $object->bargain_price,
    ];
}

if ($object->bargain_type !== null && $object->bargain_type == Object::BARGAIN_TYPE_SALE || $object->bargain_type !== null && $object->bargain_type == Object::BARGAIN_TYPE_RENT) {
    $answer['details'][] = [
        'title' => 'Форма реализации',
        'text' => $object->bargain_type == Object::BARGAIN_TYPE_SALE ? 'Продажа' : 'Аренда'
    ];
}

$objectTemp = $object->getChildren()->all();
$haveFreeLand = false;
$haveFreeSpace = false;
$haveFreeOAZ = false;
for ($i = 0; $i < count($objectTemp); $i++) {
    if ($objectTemp[$i]->getType()->one()->code === 'TYPE_FREE_LAND' && !$haveFreeLand) {
        $answer['details'][] = [
            'title' => 'Наличие свободных земель',
            'text' => 'Да'
        ];
        $haveFreeLand = true;
    }
    if ($objectTemp[$i]->getType()->one()->code === 'TYPE_FREE_SPACE' && !$haveFreeSpace) {
        $answer['details'][] = [
            'title' => 'Наличие свободных земель',
            'text' => 'Да'
        ];
        $haveFreeSpace = true;
    }
    if ($objectTemp[$i]->getType()->one()->code === 'TYPE_SEZ' && !$haveFreeOAZ) {
        $answer['details'][] = [
            'title' => 'Наличие свободных земель',
            'text' => 'Да'
        ];
        $haveFreeOAZ = true;
    }
}

if (!$haveFreeOAZ) {
    if ($object->getType()->one()->code === 'TYPE_SEZ') {
        $answer['details'][] = [
            'title' => 'Расположение в ОЭЗ',
            'text' => 'Да',
        ];
        $haveFreeOAZ = true;
    }
}

if (!$haveFreeOAZ) {
    if ($object->is_special_ecological_zone !== null) {
        if ($object->is_special_ecological_zone) {
            $answer['details'][] = [
                'title' => 'Расположен в ОЭЗ',
                'text' => $object->is_special_ecological_zone ? 'Да' : 'Нет',
            ];
        }
    }
}
if ($object->property_status && $object->property_status !== null) {
    $answer['details'][] = [
        'title' => 'Статус объекта имущества',
        'text' => $object::getPropertyStatus($object->property_status)
    ];
}

if ($object->rent_end && $object->rent_end !== null) {
    $answer['details'][] = [
        'title' => 'Дата окончания действия договора аренды',
        'text' => date("d.m.Y", strtotime($object->rent_end))
    ];
}

if ($object->renter && $object->renter !== null) {
    $answer['details'][] = [
        'title' => 'Наименование арендополучателя',
        'text' => $object->renter
    ];
}

if ($object->worktime && $object->worktime !== null) {
    $answer['details'][] = [
        'title' => 'График работы',
        'text' => $object->worktime
    ];
}
if ($object->special_business_offer && $object->special_business_offer !== null) {
    $answer['details'][] = [
        'title' => 'Спецпредложения для бизнеса',
        'text' => $object->special_business_offer
    ];
}
echo json_encode($answer, JSON_UNESCAPED_UNICODE);