<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 23.05.2017
 * Time: 21:30
 */
use common\models\ConnectionPoint;

$answer = ["type" => "FeatureCollection", "features" => []];
$k = 0;
foreach ($points as $point) {
    $answer['features'][] = [
        'type' => 'feature',
        "id" => $point->id,
        'geometry' => [
            'type' => "Point",
            'coordinates' => [$point->latitude, $point->longitude]
        ],
        "options" => [],
    ];


    switch ($point->type) {
        case ConnectionPoint::TYPE_GAS:
            $answer['features'][$k]['options'] = [
                "preset" => "gas#icon"
            ];
            break;
        case ConnectionPoint::TYPE_ELECTRICITY:
            $answer['features'][$k]['options'] = [
                "preset" => "point_".$point->type."#icon"
            ];
            break;
    }

    $k++;
}

echo json_encode($answer, JSON_UNESCAPED_UNICODE);



