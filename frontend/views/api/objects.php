<?php

use common\models\Object;

$answer = [
    'type' => 'FeatureCollection',
    'features' => [],
];
foreach ($objects as $object) {
    $item = [
        'type' => 'Feature',
        'id' => $object->id,
        'geometry' => [
            'type' => 'Point',
            'coordinates' => [
                $object->latitude,
                $object->longitude,
            ],
        ],
        'properties' => [
            'icons' => [],
            'details' => [],
        ],
        'options' => [
            'preset' => 'standart#icon',
        ],
    ];

    $item['properties']['name'] = $object->name;
    if ($object->owner_name) {
        $item['properties']['owner_name'] = $object->owner_name;
        $item['properties']['details'][] = [
            'title' => 'Собственник',
            'text' => $object->owner_name,
        ];
    }
    if ($object->address_address) {
        $item['properties']['address'] = $object->address_address;
    }
    if ($object->phone) {
        $item['properties']['phone'] = $object->phone;
    }

    if (count($object->photos)) {
        $item['properties']['img'] = reset($object->photos)->getUrl();
        $item['properties']['preview'] = reset($object->photos)->getUrl();
    } else {
        $item['properties']['img'] = '/no_image.svg';
        $item['properties']['preview'] = '/no_image.svg';
    }

    if ($object->has_electricity_supply) {
        $item['properties']['icons'][] = [
            'className' => 'electricity',
            'title' => 'электричество',
        ];
    }

    if ($object->has_gas_supply) {
        $item['properties']['icons'][] = [
            'className' => 'gas',
            'title' => 'газ',
        ];
    }

    if ($object->bargain_type) {
        $item['properties']['details'][] = [
            'title' => 'Статус',
            'text' => $object->bargain_type == Object::BARGAIN_TYPE_SALE ? 'Продажа' : 'Аренда',
        ];
    }

    if ($object->square) {
        $item['properties']['details'][] = [
            'title' => 'Площадь',
            'text' => $object->square.' га',
        ];
    }

    if ($object->bargain_price) {
        $item['properties']['details'][] = [
            'title' => 'Стоимость',
            'text' => $object->bargain_price,
        ];
    } else {
        $item['properties']['details'][] = [
            'title' => 'Стоимость',
            'text' => 'По запросу',
        ];
    }

    $answer['features'][] = $item;
}


echo json_encode($answer, JSON_UNESCAPED_UNICODE);



