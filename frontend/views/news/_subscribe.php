<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 29.07.2017
 * Time: 19:17
 */

use yii\helpers\Url;
$js = <<<JS
   $(document).on('submit','.form_ajax',function () {
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var type = $(this).attr('method');
        $.ajax({
            url: url,
            data: data,
            type: type,
            dataType: 'json',
            success: function (result) {
                if (result.success == true) {
                    alert(result.text) ? '' : location.reload();
                } else {
                    alert(result.text.email[0]) ? '' : location.reload();
                }
            }
        });
        return false;
    });
JS;
$this->registerJs($js);
?>
<form class="form_ajax" action="<?=Url::to(['/news/subscribe'])?>" method="post">
    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
    <div class="subscribe__links">
        <a class="subscribe__link subscribe__link_rss" href="<?=Url::to(['/site/news-rss'])?>">RSS подписка</a>
    </div>
    <div class="subscribe__inputs">
        <input class="subscribe__input" type="text" placeholder="Ваш е-mail" name="SubscribeEmail[email]">
        <input class="subscribe__subscribe" type="submit" value="Подписаться">
    </div>
</form>
