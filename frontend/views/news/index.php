<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$formatter = \Yii::$app->formatter;

$this->title = 'Новости и мероприятия';
$this->params['breadcrumbs'][] = $this->title;
$js = <<<JS
   $(document).on('click','.pagination__marker',function () {
    if(navigator.userAgent.search(/Firefox/) > 0)
        $('html').animate({'scrollTop' : 200},'slow');
    else
        $('html body').animate({'scrollTop' : 200},'slow');
    });
JS;
$this->registerJs($js);
?>
<div class="p-news">
    <div class="top-box">
        <div class="container">
            <a class="top-box__back-link" href="/">Назад</a>
            <h1 class="top-box__title">Новости и мероприятия</h1>
            <div class="top-box__nav">
                <div class="top-box__menu">
                    <!--a class="top-box__link" href="/page/about">Об области</a>
                    <a class="top-box__link" href="/page/minister">Деятельность министерства</a>
                    <a class="top-box__link" href="/news">Новости и мероприятия</a-->
                </div>
                <div class="search-box search-box_theme_light top-box__search-box">
                    <div class="search-box__btn"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="news-box p-news__news-box">

        <?php $k = 1; foreach ($provider->getModels() as $article): ?>
        <?php if ($k < 4): ?>
        <div class="one-news news-box__one-news">
            <div class="one-news__pic"><img class="one-news__img" src="<?= $article->getFullImagePath() ?>" alt="" role="presentation"/>
            </div>
            <div class="one-news__content">
                <div class="one-news__date"><?= $formatter->asDate($article->updated_at, 'dd  MMMM yyyy') ?></div>
                <a href="<?=Url::toRoute(['/news/'.$article->id])?>">
                    <h3 class="one-news__title"><?= $article->title ?></h3>
                </a>
                <div class="one-news__text"><?= $article->short_desc ?></div>
            </div>
        </div>
                <?php endif; ?>
        <?php $k++; endforeach; ?>

    </div>
    <div class="subscribe p-news__subscribe">
        <div class="subscribe__container">
            <div class="subscribe__titles">
                <div class="subscribe__title"><b>Подписка</b> на новости
                </div>
                <div class="subscribe__sub-title">Будь в курсе инвестиционного <br> <b>развития области</b>
                </div>
            </div>
            <?=$this->render('_subscribe')?>
        </div>
    </div>

    <div class="news-box p-news__news-box">


        <?php $k = 1; foreach ($provider->getModels() as $article): ?>
            <?php if ($k > 3): ?>
                <div class="one-news news-box__one-news">
                    <div class="one-news__pic"><img class="one-news__img" src="<?= $article->getFullImagePath() ?>" alt="" role="presentation"/>
                    </div>
                    <div class="one-news__content">
                        <div class="one-news__date"><?= $formatter->asDate($article->updated_at, 'dd  MMMM yyyy') ?></div>
                        <a href="<?=Url::toRoute(['/news/'.$article->id])?>">
                            <h3 class="one-news__title"><?= $article->title ?></h3>
                        </a>
                        <div class="one-news__text"><?= $article->short_desc ?></div>
                    </div>
                </div>
            <?php endif; ?>
            <?php $k++; endforeach; ?>
        </div>


        <div class="pagination p-news__pagination">
            <?=LinkPager::widget([
                'pagination' => $provider->pagination,
                'activePageCssClass' => 'pagination__marker_active',
                'pageCssClass' => 'pagination__marker',
                'prevPageLabel' => '',
                'nextPageLabel' => '',
                'prevPageCssClass' => 'hidden',
                'nextPageCssClass' => 'hidden',
            ]);?>
        </div>
    </div>
<?php if($show_press): ?>
    <div class="press p-news__press">
        <div class="press__container">
            <h3 class="press__title"><b>Пресс-служба</b> <br> Министерства инвестиций и инноваций МО
            </h3>
            <ul class="press__contacts">
                <li class="press__contact"><a class="press__link press__link_mail" href="mailto:pr.mii.mosreg@gmail.com">pr.mii.mosreg@gmail.com</a>
                <li class="press__contact"><a class="press__link press__link_mail" href="mailto:priem.mii@mosreg.ru">priem.mii@mosreg.ru</a>
                </li>
                <li class="press__contact">
                    <a class="press__link press__link_phone" href="tel:+79852001770">+7 (985) 200 1770</a>
                    <a class="press__link press__link_phone" href="tel:+79257614262">+7 (925) 761 4262</a>
                </li>
                <li class="press__contact"><a class="press__link press__link_web" href="http://www.mii.mosreg.ru">www.mii.mosreg.ru</a>
                </li>
            </ul>
        </div>
    </div>
<?php endif; ?>
</div>