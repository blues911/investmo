<?php
use yii\helpers\Html;
$this->title = $model->title;
use yii\helpers\Url;
?>
<div class="p-content">
    <div class="title-subpage">
        <div class="title-subpage__top">
            <div class="title-subpage__title">
                <div class="container">
                    <h1><span class="title-subpage__brand">Новости и мероприятия</span></h1>
                </div>
            </div>

            <div class="title-subpage__company-contacts">
                <div class="container">
                    <div class="title-subpage__contacts"><a class="title-subpage__back-link" href="<?= Yii::$app->request->referrer ? Yii::$app->request->referrer : '/'?>">Назад</a></div>
                </div>
            </div>
        </div>
    </div>


    <div class="block-2 block-2_reverse">
        <div class="block-2__container">
            <div class="block-2__content">
                <div class="one-news__date"><?=Yii::$app->formatter->asDate($model->updated_at.Yii::$app->timeZone, 'dd LLLL yyyy')?></div>
                <h3 class="one-news__title"><?=$model->title?></h3>
                <div class="block-2__content-text">
                    <p class="block-2__text block-2__text_description">
                        <?=$model->short_desc?>
                    </p>
                </div>
            </div>

            <div class="block-2__pic">
                <img alt="" class="block-2__img" role="presentation" src="<?=$model->getFullImagePath()?>"/>
            </div>
        </div>
    </div>

    <div class="block-1">
        <div class="block-1__container">
            <div class="block-1__content">
                <div class="block-1__text">
                    <?=$model->full_desc?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="block-1__container share-wrap">
    <div class="share"><span class="share__text">Поделиться в соцсетях:</span>
        <div class="share__links">
            <a class="share__link social__link" href="http://vk.com/share.php?url=<?=Yii::$app->request->absoluteUrl?>&title=<?=$model->title?>&description=<?=$model->short_desc?>&image=<?=$model->getFullImagePath()?>">
                <i class="icon social__icon icon-vk"></i></a>
            <a class="share__link social__link" href="http://www.facebook.com/sharer.php?s=100&p[url]=<?=Yii::$app->request->absoluteUrl?>&p[title]=<?=$model->title?>&p[summary]=<?=$model->short_desc?>&p[images][0]=<?=$model->getFullImagePath()?>">
                <svg class="icon icon_fb social__icon" width="8px" height="17px">
                    <use xlink:href="/svg-symbols.svg#fb"></use>
                </svg>
            </a>
        </div>
    </div>
</div>
