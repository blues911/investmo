<?php
use yii\helpers\Url;
use frontend\widgets\StoryWidget;
$this->title = $model->title;
?>

<div class="p-stories">
    <div class="title-subpage">
        <div class="title-subpage__top" style="background-image:url(/static/img/assets/p-stories/mobile-bg.jpg);">
            <div class="title-subpage__title">
                <div class="container">
                    <h1>
                        <span class="title-subpage__brand"><?=$model->title?></span>
                        <span class="title-subpage__divider">/</span>
                        <span class="title-subpage__title-box">история успеха</span>
                    </h1>
                </div>
            </div>
            <div class="title-subpage__company-contacts">
                <div class="container">
                    <div class="title-subpage__contacts">
                        <a class="title-subpage__back-link" href="<?= Yii::$app->request->referrer ? Yii::$app->request->referrer : '/'?>">Назад</a>
                        <div class="title-subpage__contacts-box">
                            <? if (!empty($model->address)) : ?>
                                <a class="title-subpage__contacts-link title-subpage__contacts-link_address" href="javascript:void(0)"><?=$model->address?></a>
                            <? endif; ?>
                            <? if (!empty($model->phone)) : ?>
                                <a class="title-subpage__contacts-link title-subpage__contacts-link_phone" href="tel:<?=$model->phone?>"><?=$model->phone?></a>
                            <? endif; ?>
                            <? if (!empty($model->email)) : ?>
                                <a class="title-subpage__contacts-link title-subpage__contacts-link_mail" href="mailto:<?=$model->email?>"><?=$model->email?></a>
                            <? endif; ?>
                        </div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
    <div class="preview-box p-stories__preview-box">
        <div class="preview-box__pic">
            <img class="preview-box__img" src="<?=$model->backgroundImg?>" alt="" role="presentation"/>
            <div class="preview-box__info">
                <?php if($model->investments) { ?>
                <div class="preview-box__description">
                    <div class="preview-box__header">Общих инвестиций</div>
                    <div class="preview-box__count">
                        <div class="preview-box__number"><?=$model->investments?></div>
                        <div class="preview-box__units">млн. <br> <?=$model->getCurrency()?></div>
                    </div>
                </div>
                <?php } ?>
                <div class="preview-box__logo-box">
                    <img class="preview-box__logo" src="<?=$model->logoImg?>" alt="" role="presentation"/>
                </div>
            </div>
        </div>
        <div class="preview-box__content">
            <div class="preview-box__container">
                <div class="preview-box__details">
                    <div class="preview-box__box">
                        <div class="preview-box__sub-title">Дата запуска:</div>
                        <div class="preview-box__text"><?=Yii::$app->formatter->asDate($model->date_of_start, 'long')?></div>
                    </div>
                    <div class="preview-box__box">
                        <div class="preview-box__sub-title">Страна происхождения:</div>
                        <div class="preview-box__text"><?=$model->country_of_origin?></div>
                    </div>
                    <div class="preview-box__box">
                        <div class="preview-box__sub-title">Цель инвестирования:</div>
                        <div class="preview-box__text"><?=$model->investment_goal?></div>
                    </div>
                </div>
                <div class="preview-box__about">
                    <div class="preview-box__text preview-box__text_about"><?=$model->short_desc?></div>
                </div>
            </div>
        </div>
    </div>
    <?=$model->html?>
    
     <?= StoryWidget::widget(['view' => 'story', 'exclude' => $model->id, 'investor' => $model->investorTypes]);?>
</div>