<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use common\modules\PageComposer\widgets\BlockViewWidget;
use \frontend\widgets\PageConstructorContent;
$this->title = $title;
if($is_constructor){
    $constructor_content = null;
    if($blocks){
        foreach ($blocks as $block) {
            $constructor_content .= BlockViewWidget::widget(['block' => $block]);
        }
    }
    echo PageConstructorContent::widget(['hasBackground' => Yii::$app->controller->hasBackground,'header' => $header, 'content' => $constructor_content]);
}else{
    echo $content;
}
?>