<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 15:56
 */
use frontend\widgets\MenuWidget; ?>

<header class="header header_theme_static header_theme_simple header_theme_stories">
    <div class="header__container container"><a class="logo header__logo" href="/">
            <div class="logo__icon-wrap">
                <svg class="icon icon_logo logo__icon" width="37px" height="50px">
                    <use xlink:href="/svg-symbols.svg#logo"></use>
                </svg>

            </div>
            <div class="logo__text">
                <p class="logo__title">Инвестиционный портал
                </p>
                <div class="logo__subtitle">Московской области
                </div>
            </div>
        </a>
        <div class="header__control header__control_theme_burger">
            <button class="burger">
                <div class="burger__line">Открыть меню
                </div>
            </button>
        </div>
        <div class="header__right">
            <nav class="main-nav">
                <ul class="main-nav__list">
                    <li class="main-nav__item"><a class="main-nav__link active" href="javascript:void(0)">Инвесторам</a>
                    </li>
                    <li class="main-nav__item"><a class="main-nav__link" href="javascript:void(0)">Девелоперам</a>
                    </li>
                    <!--li class="main-nav__item"><a class="main-nav__link" href="javascript:void(0)">Владельцам</a>
                    </li-->
                    <li class="main-nav__item"><a class="main-nav__link" href="javascript:void(0)">Малому бизнесу</a>
                    </li>
                    <li class="main-nav__item"><a class="main-nav__link" href="javascript:void(0)">Об области</a>
                    </li>
                </ul>
            </nav>
            <div class="header__controls">
                <div class="header__select">
                    <select>
                        <option>Ru</option>
                        <option>En</option>
                        <option>Ge</option>
                    </select>
                </div>
                <a class="header__control header__control_theme_link" href="javascript:void(0)">
                    <svg class="icon icon_glasses control__icon" width="31px" height="14px">
                        <use xlink:href="/svg-symbols.svg#glasses"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</header>
