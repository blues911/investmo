<?php
use frontend\widgets\MenuWidget;
use yii\helpers\Url;

$headerClass = "";
#$slug = Yii::$app->request->resolve()[1]['slug'];

if (Yii::$app->controller->id == 'site') {
    $headerClass = "header_theme_simple";
} else if (Yii::$app->controller->id == 'search') {
    $headerClass = "header_theme_fixed";
} else if (Yii::$app->controller->id == 'news' && Yii::$app->controller->action->id == 'index') {
    $headerClass = "header_theme_simple header header_theme_light";
} else if (Yii::$app->controller->id == 'story') {
    $headerClass = "header_theme_static header_theme_simple header_theme_stories";
} else if (Yii::$app->controller->id == 'page' && Yii::$app->controller->hasBackground) {
    $headerClass = "header header_theme_simple header_theme_light";
} else if (Yii::$app->controller->id == 'page' && !Yii::$app->controller->hasBackground) {
    $headerClass = "header_theme_static header_theme_simple header_theme_stories";
} else if (Yii::$app->controller->id == 'news' && Yii::$app->controller->action->id == 'view') {
    $headerClass = "header_theme_static header_theme_simple header_no-line";
}
?>
<header class="header <?=$headerClass?>" data-class="<?=$headerClass?>">
    <div class="cecutient-header container">
        <div class="cecutient-header__left">
            <div class="cecutient-header__font"><a class="cecutient-header__font__item cecutient-header__font__item--md is-active" href="javascript:void(0)" data-size="md"><span>А</span></a><a class="cecutient-header__font__item cecutient-header__font__item--lg" href="javascript:void(0)" data-size="lg"><span>А</span></a><a class="cecutient-header__font__item cecutient-header__font__item--xl" href="javascript:void(0)" data-size="xl"><span>А</span></a>
            </div>
            <div class="cecutient-header__palette"><a class="cecutient-header__palette__item cecutient-header__palette__item--light is-active" href="javascript:void(0)" data-palette="light"><span></span></a><a class="cecutient-header__palette__item cecutient-header__palette__item--dark" href="javascript:void(0)" data-palette="dark"><span></span></a><a class="cecutient-header__palette__item cecutient-header__palette__item--sky" href="javascript:void(0)" data-palette="sky"><span></span></a><a class="cecutient-header__palette__item cecutient-header__palette__item--blue" href="javascript:void(0)" data-palette="blue"><span></span></a><a class="cecutient-header__palette__item cecutient-header__palette__item--brown" href="javascript:void(0)" data-palette="brown"><span></span></a>
            </div>
        </div>
        <div class="header__right">
            <div class="header__controls">
                <div class="header__off-image js-off-image">
                    <svg class="icon icon_off-image header__off-image-ico" width="44px" height="33px">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="../svg-symbols.svg#off-image"></use>
                    </svg>
                </div>
                
                <div class="header__select">
                    <select data-current-lang="<?=Yii::$app->language?>" data-url="<?=Url::toRoute(['/language/'], true)?>">
                        <option value="ru" <?=Yii::$app->language == 'ru' ? 'selected' : ''?>>Ru</option>
                        <option value="en" <?=Yii::$app->language == 'en' ? 'selected' : ''?>>En</option>
                    </select>
                </div>
                <div class="header__control header__control_theme_link" href="javascript:void(0)"><!--i class="icon icon-desktop"></i-->
                    <svg class="icon icon_desktop control__icon" width="200px" height="200px">
                        <use xlink:href="/svg-symbols.svg#desktop"></use>
                    </svg>
                    <!-- +icon({ icon: 'desktop', class: 'control__icon' })-->
                </div>
            </div>
        </div>
    </div>
    <div class="header__container container"><a class="logo header__logo" href="/">
            <div class="logo__icon-wrap">
                <svg class="icon icon_logo logo__icon" width="37px" height="50px">
                    <use xlink:href="/svg-symbols.svg#logo"></use>
                </svg>

            </div>
            <div class="logo__text">
            <p class="logo__title">Инвестиционный портал
            </p>
            <div class="logo__subtitle">Московской области
            </div>
            </div></a>
            <?php if (Yii::$app->controller->id != 'site'): ?>
                <div class="social social_theme_horizontal header__social">
                    <p class="social__title">Соцсети:
                    </p>
                    <ul class="social__list">
                        <li class="social__item"><a class="social__link" href="javascript:void(0)">
                        <svg class="icon icon_vk social__icon" width="200px" height="200px">
                            <use xlink:href="/svg-symbols.svg#vk"></use>
                        </svg>
                    </a>
                        </li>
                        <li class="social__item"><a class="social__link" href="javascript:void(0)">
                        <svg class="icon icon_fb social__icon" width="8px" height="17px">
                            <use xlink:href="/svg-symbols.svg#fb"></use>
                        </svg>
                    </a>
                        </li>
                        <li class="social__item"><a class="social__link" href="javascript:void(0)">
                        <svg class="icon icon_in social__icon" width="14px" height="15px">
                            <use xlink:href="/svg-symbols.svg#in"></use>
                        </svg>
                    </a>
                        </li>
                    </ul>
                </div>
            <?php endif; ?>
        <div class="header__control header__control_theme_burger">
            <button class="burger">
                <div class="burger__line"><?=Yii::t('app', 'Открыть меню')?>
                </div>
            </button>
        </div>
        <div class="header__right">
            <nav class="main-nav">
                <ul class="main-nav__list">
                    <?= MenuWidget::widget(['exclude_pages' => ['/page/contacts']]); ?>
                    <?php if (Yii::$app->controller->id == 'site'): ?>
                        <li class="main-nav__item">
                            <a class="main-nav__link main-nav__link_search" href="/search"></a>
                        </li>
                    <?php endif; ?>
                </ul>
            </nav>
            <div class="header__controls">
                <div class="header__select">
                    <select data-current-lang="<?=Yii::$app->language?>" data-url="<?=Url::toRoute(['/language/'], true)?>">
                        <option value="ru" <?=Yii::$app->language == 'ru' ? 'selected' : ''?>>Ru</option>
                        <option value="en" <?=Yii::$app->language == 'en' ? 'selected' : ''?>>En</option>
                    </select>
                </div>
                <a class="header__control header__control_theme_link" href="javascript:void(0)">
                    <svg class="icon icon_glasses control__icon" width="31px" height="14px">
                        <use xlink:href="/svg-symbols.svg#glasses"></use>
                    </svg>
                </a>
                <?php if (Yii::$app->controller->id == 'map'): ?>
                    <a class="btn btn_open-popup header__control" data-popup="add-object" href="javascript:void(0)">
                        <svg class="icon icon_user-lc control__icon">
                            <use xlink:href="/svg-symbols.svg#user-lc"></use>
                        </svg>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</header>