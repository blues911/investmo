<?php
/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use frontend\widgets\MenuWidget;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html <?php if (!Yii::$app->user->isGuest):?>class="logged-in"<?php endif;?>lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta content="" name="description">
    <meta content="" name="keywords">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
    <meta content="telephone=no" name="format-detection">
    <meta name="HandheldFriendly" content="true">
    <?php if (Yii::$app->controller->id == 'site') : ?>
        <meta name="cmsmagazine" content="29acd4fc7e5d743878768b6b1d51688b" /> 
    <?php endif; ?>
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    
    <script>
        (function (H) {
            H.className = H.className.replace(/\bno-js\b/, 'js')
        })(document.documentElement)</script>
    <?php $this->head() ?>
</head>
<body class="page">
<?php $this->beginBody() ?>
<?= $this->render('partial/main_header') ?>
<?= $content ?>

<? if (Yii::$app->controller->id != 'map') : ?>
    <footer class="footer">
        <div class="footer__container container">
            <div class="footer__left"><a class="logo logo_theme_light footer__logo" href="/">
                    <div class="logo__icon-wrap">
                        <svg class="icon icon_logo logo__icon" width="37px" height="50px">
                            <use xlink:href="/svg-symbols.svg#logo"></use>
                        </svg>
    
                    </div>
                    <div class="logo__text">
                        <p class="logo__title"><?=Yii::t('app', 'Инвестиционный портал')?>
                        </p>
                        <div class="logo__subtitle"><?=Yii::t('app', 'Московской области')?>
                        </div>
                    </div>
                </a>
                <p class="footer__copyright"><?=Yii::t('app', 'Любое копирование материалов с сайта без письменного разрешения владельца запрещено © 2017 Инвестиционный портал')?>
                </p>
            </div>
            <div class="footer__right">
                <nav class="main-nav main-nav_theme_inverse">
                    <ul class="main-nav__list">
                        <?= MenuWidget::widget(); ?>
                    </ul>
                </nav>
                <div class="footer__socials">
                    <div class="subscribe">
                        <div class="subscribe__message"></div>
                        <form class="subscribe-box subscribe__form subscribe-box_theme_dark footer__subscribe-box" action="news/subscribe">
                            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" />
                            <input
                                class="subscribe-box__input" type="email" name="SubscribeEmail[email]" placeholder="<?=Yii::t('app', 'Подписка на новости')?>" required="required"/>
                            <button class="subscribe-box__submit" type="submit"><?=Yii::t('app', 'Отправить')?>
                                <svg class="icon icon_arrow subscribe-box__icon" width="9px" height="14px">
                                    <use xlink:href="/svg-symbols.svg#arrow"></use>
                                </svg>
                            </button>
                        </form>
                    </div>
                    <div class="social social_theme_horizontal footer__social">
                        <p class="social__title"><?=Yii::t('app', 'Мы в соцсетях:')?>
                        </p>
                        <ul class="social__list">
                            <li class="social__item"><a class="social__link" target="_blank" href="https://vk.com/mininvest">
                                    <svg class="icon icon_vk social__icon" width="18px" height="13px">
                                        <use xlink:href="/svg-symbols.svg#vk"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item"><a class="social__link" target="_blank" href="https://www.facebook.com/mii.mosreg.ru/">
                                    <svg class="icon icon_fb social__icon" width="8px" height="17px">
                                        <use xlink:href="/svg-symbols.svg#fb"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item"><a class="social__link" target="_blank" href="https://www.instagram.com/mininvest/">
                                    <svg class="icon icon_in social__icon" width="14px" height="15px">
                                        <use xlink:href="/svg-symbols.svg#in"></use>
                                    </svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<? endif; ?>
<script>
    var onloadCallback = function() {
        widget = grecaptcha.render('myCaptcha', {
            'sitekey': '6LcmdTwUAAAAACO6Q7_UB1JS17ko2Y3tQWhWcDP-'
        });
    };
</script>
<?php $this->endBody() ?>

<!-- Yandex.Metrika -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter45801798 = new Ya.Metrika({
                    id:45801798,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/45801798" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika -->

<!-- GoogleAnalytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63952541-8', 'auto');
    ga('send', 'pageview');
</script>
<!-- /GoogleAnalytics -->
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</body>
</html>
<?php $this->endPage() ?>
