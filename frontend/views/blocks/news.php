<?php
use frontend\widgets\NewsWidget;
?>
<div class="content content_theme_news">
    <div class="container">
        <div class="content__inner">
            <?= NewsWidget::widget(); ?>
        </div>
    </div>
</div>