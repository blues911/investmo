<?php
use frontend\widgets\FeedBackWidget;
?>
<div class="content content_theme_support hash-block" id="support">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?=Yii::t('app', '<strong>Помогаем</strong> лично')?>
            </h2>
            <div class="support">
                <div class="support__row">
                    <div class="support__left">
                        <p class="support__subtitle"><?=Yii::t('app', 'При решении ваших проблем мы всегда используем индивидуальный подход')?>
                        </p>
                        <ul class="support__list">
                            <li class="support__item">
                                <div class="support__icon-wrap">
                                    <svg class="icon icon_search support__icon" width="42px" height="42px">
                                        <use xlink:href="/svg-symbols.svg#search"></use>
                                    </svg>

                                </div>
                                <p class="support__text"><?=Yii::t('app', 'Подбор проекта <br> по вашим запросам')?>
                                </p>
                            </li>
                            <li class="support__item">
                                <div class="support__icon-wrap">
                                    <svg class="icon icon_docs support__icon" width="100px" height="125px">
                                        <use xlink:href="/svg-symbols.svg#docs"></use>
                                    </svg>

                                </div>
                                <p class="support__text"><?=Yii::t('app', 'Предоставление типовых документов')?>
                                </p>
                            </li>
                            <li class="support__item">
                                <div class="support__icon-wrap">
                                    <svg class="icon icon_headphones support__icon" width="74px" height="80px">
                                        <use xlink:href="/svg-symbols.svg#headphones"></use>
                                    </svg>

                                </div>
                                <p class="support__text"><?=Yii::t('app', 'Профессиональные консультации')?>
                                </p>
                            </li>
                            <li class="support__item support__item_center">
                                <p class="support__text"><?=Yii::t('app','Телефоны для консультаций')?> <br> +7 (495) 109-07-07 <br> +7 (495) 668-00-99</p>
                            </li>
                        </ul>
                    </div>
                    <div class="support__right">
                        <?= FeedBackWidget::widget(['selector' => true]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>