<?php
use frontend\widgets\StoryWidget;
?>
<div id="stories" class="content content_theme_stories hash-block">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?=Yii::t('app', '<strong>Истории,</strong> которыми мы гордимся')?></h2>
            <?= StoryWidget::widget(['investor' => $investor]);?>
        </div>
    </div>
</div>