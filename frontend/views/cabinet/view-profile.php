<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 27.07.2017
 * Time: 16:23
 */
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="p-cabinet-profile p-cabinet">
    <div class="cabinet-profile">
        <div class="container">
            <div class="cabinet-menu">
                <ul class="cabinet-menu__list">
                    <!--li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/help'])?>">Помощь</a>
                    </li>
                    <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/edit-profile'])?>">Редактировать профиль</a>
                    </li-->
                    <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/'])?>">Ваши объекты</a>
                    </li>
                    <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/logout'])?>">Выйти</a>
                    </li>
                </ul>
                <div class="cabinet-menu__lc-ico">
                    <a href="/cabinet/view-profile">
                        <?php if ($user->getFullImageUrl('little')) : ?>
                            <img class="cabinet-menu__profile-ico" src="<?=$user->getFullImageUrl('little')?>" alt="">
                        <?php else: ?>
                            <svg class="icon icon_user-lc cabinet-menu__ico cabinet-menu__ico_user" width="20px" height="20px">
                                <use xlink:href="/svg-symbols.svg#user-lc"></use>
                            </svg>
                        <?php endif; ?>
                    </a>
                </div>
            </div>
            <div class="p-cabinet-profile__content p-cabinet__content">
                <div class="p-cabinet-profile__left p-cabinet__left">
                    <div class="object-menu">
                        <ul class="object-menu__list">
                            <li class="object-menu__item"><span class="object-menu__text">Профиль</span>
                            </li>
                            <li class="object-menu__item"><span class="object-menu__text"></span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="p-cabinet-profile__right p-cabinet__right">
                    <div class="profile">
                        <div class="profile__img-select" style="<?=($user->getFullImageUrl('default')) ? 'background-image: url('.$user->getFullImageUrl('default').'); background-size:cover' : '' ?>">

                        </div>
                        <div class="profile__data">
                            <p class="profile__name"><?=Html::encode($user->getFullUserName())?></p>
                            <div class="profile__row">
                                <p class="profile__company-title profile__title">Компания</p>
                                <p class="profile__company-name profile__text"><?=Html::encode($user->company_name)?></p>
                            </div>
                            <div class="profile__row">
                                <p class="profile__phone-title profile__title">Телефон</p>
                                <p class="profile__phone-number profile__text"><?=Html::encode($user->phone)?></p>
                            </div>
                            <div class="profile__row profile__row_email">
                                <p class="profile__email-title profile__title">Email</p>
                                <p class="profile__email-address profile__text"><a href="mailto:<?=Html::encode($user->email)?>"><?=Html::encode($user->email)?></a></p>
                            </div><a class="profile__edit btn btn_theme_standart" href="<?=Url::to(['/cabinet/edit-profile'])?>">Редактировать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
