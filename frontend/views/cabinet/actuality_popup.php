<div class="popup-success popup-success_absolute popup-success_visible">
    <A class="popup-success__close" href="javascript:void(0)"></A>
    <div class="popup-success__ico">
        <svg class="icon icon_success popup-success__ico-success" width="31px" height="26px">
            <use xlink:href="/svg-symbols.svg#<?=$success == 'true'?'success':'logo'?>"></use>
        </svg>
    </div>
    <div class="popup-success__title popup-success__title_margin">Объекты актуальны</div>
    <div class="popup-success__sub-title"><?=$success == 'true'?'Актуальность ваших объектов успешно подтверждена':'Ваши объекты не нуждаются в подтверждении актуальности'?></div>
</div>