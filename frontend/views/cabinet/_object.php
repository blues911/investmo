<?php
/**
 * Created by PhpStorm.
 * User: shakinm@gmail.com
 * Date: 25.07.2017
 * Time: 9:48
 */

use yii\helpers\Url;
use frontend\assets\ObjectAsset;

ObjectAsset::register($this);

?>
<div class="p-cabinet-add-object p-cabinet">
    <div class="container">
        <div class="cabinet-menu p-cabinet__cabinet-menu">
            <ul class="cabinet-menu__list">
                <!--li class="cabinet-menu__item"><a class="cabinet-menu__link"
                                                  href="<?= Url::to(['/cabinet/help']) ?>"><?= Yii::t('app', 'Помощь') ?></a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link"
                                                  href="<?= Url::to(['/cabinet/edit-profile']) ?>"><?= Yii::t('app', 'Редактировать профиль') ?> </a>
                </li-->
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/'])?>">Ваши объекты</a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link"
                                                  href="<?= Url::to(['/cabinet/logout']) ?>"><?= Yii::t('app', 'Выйти') ?></a>
                </li>
            </ul>
            <div class="cabinet-menu__lc-ico">
                <a href="/cabinet/view-profile">
                    <?php if ($user->getFullImageUrl('little')) : ?>
                        <img class="cabinet-menu__profile-ico" src="<?=$user->getFullImageUrl('little')?>" alt="">
                    <?php else: ?>
                        <svg class="icon icon_user-lc cabinet-menu__ico cabinet-menu__ico_user" width="20px" height="20px">
                            <use xlink:href="/svg-symbols.svg#user-lc"></use>
                        </svg>
                    <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="p-cabinet-add-object__content p-cabinet__content p-cabinet__content p-cabinet__content_position">
            <div class="p-cabinet-add-object__left p-cabinet__left">
                <div class="object-menu p-cabinet__object-menu">
                    <ul class="object-menu__list">
                        <li class="object-menu__item"><span
                                    class="object-menu__text"><?= ($object->id) ? Yii::t('app', 'Редактирование объекта') : Yii::t('app', 'Заявка на добавление объекта') ?></span>
                        </li>
                        <?php if ($object->id) : ?>
                            <li class="object-menu__item">
                                <a href="/cabinet/check-object-diff/" data-id="<?=$object->id?>" class="object-menu__link object-menu__link_close btn btn_theme_light" href="">закрыть</a>
                            </li>
                            <li class="object-menu__item">
                                <a class="object-menu__link object-menu__link_add btn btn_theme_standart" href="javascript:void(0)" data-save="true">Сохранить</a>
                            </li>
                        <?php else: ?>
                            <li class="object-menu__item"><a
                                        class="object-menu__link object-menu__link_delete btn btn_theme_light"
                                        href="<?= Url::toRoute('/cabinet/index') ?>">Отмена</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="p-cabinet-add-object__right p-cabinet__right p-cabinet__right p-cabinet__right_position">
                <div class="object-form">
                    <form class="object-form__form form_ajax" data-alert="true"
                          action="<?= ($object->id) ? Url::to(['/cabinet/update-object', 'id' => $object->id]) : Url::to(['/cabinet/create-object']) ?>" method="post"
                          enctype="multipart/form-data">
                        <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                               value="<?= Yii::$app->request->getCsrfToken() ?>"/>

                        <div class="object-form__step-box object-form__step-box_visible" data-number="1">
                            <div class="object-form__step">
                                <div class="object-form__step-number">
                                    <div class="object-form__step-digit">1
                                    </div>
                                </div>
                                <div class="object-form__step-text"><?= Yii::t('app', 'Общие сведения') ?>
                                </div>
                            </div>
                            <div class="object-form__next-step">
                                <div class="object-form__next-step-number">2. <?= Yii::t('app', 'Параметры объекта') ?>
                                </div>
                            </div>
                            <div class="object-form__data-wrapper">
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Название объекта') ?><span
                                                    class="object-form__required">*</span>
                                        </label><input
                                                class="object-form__input object-form__input_size-full object-form__required-input"
                                                type="text" name="Object[name]" placeholder="Укажите название объекта" value="<?=$object->name?>"/>
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Тип объекта') ?><span
                                                class="object-form__required">*</span>
                                        </label>
                                        <select class="object-form__select object-form__required-select"
                                                name="Object[object_type_id] required=" required
                                        placeholder="<?= Yii::t('app', 'Участок') ?>">
                                        <option class="object-form__option">
                                        </option>
                                        <?php foreach ($objectTypes as $objectType) { ?>
                                        <option class="object-form__option"
                                                value="<?= $objectType->id ?>" <?= ($object->object_type_id == $objectType->id) ? 'selected': '' ?> ><?= $objectType->name ?>
                                        </option>
                                        <?php } ?>


                                        </select>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Собственник') ?><span
                                                    class="object-form__required">*</span>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-input"
                                               type="text" placeholder="Укажите название или имя"
                                               name="Object[owner_name]" value="<?= $object->owner_name ?>"/>
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Контактный телефон') ?>
                                            <span class="object-form__required">*</span>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__input_phone object-form__required-input"
                                               type="text" placeholder="Укажите номер телефона"
                                               name="Object[phone]" value="<?= $object->phone ?>"/>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Площадь, га') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number"
                                               type="text" placeholder="Укажите площадь объекта в га"
                                               name="Object[square]" value="<?= $object->square ?>"/>
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Площадь, кв.м') ?></label>
                                        <input class="object-form__input object-form__input_size-full object-form__input object-form__number object-form__required-number"
                                               type="text" placeholder="Укажите площадь объекта в кв.м"
                                               name="Object[square_sqm]" value="<?= $object->square_sqm ?>"/>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col object-form__col_size-full">
                                        <label class="object-form__label"><?= Yii::t('app', 'Адрес объекта') ?><span
                                                class="object-form__required">*</span>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-input"
                                               type="text" placeholder="Укажите адрес"
                                               name="Object[address_address]" value="<?= $object->address_address ?>"/>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label">Широта
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number"
                                               type="text" placeholder="Укажите широту"
                                               name="Object[latitude]" value="<?= $object->latitude ?>"/>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
                                        <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                                            <use xlink:href="/svg-symbols.svg#info"></use>
                                        </svg>
                                             </span><span class="object-form__info-text">Координаты необходимы для отображения объекта на карте.</span></i>
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label">Долгота
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number"
                                               type="text" placeholder="Укажите долготу"
                                               name="Object[longitude]" value="<?= $object->longitude ?>"/>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
                                        <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                                            <use xlink:href="/svg-symbols.svg#info"></use>
                                        </svg>
                                             </span><span class="object-form__info-text">Координаты необходимы для отображения объекта на карте.</span></i>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col object-form__col_size-full">
                                        <label class="object-form__label"><?= Yii::t('app', 'Общие сведения об объекте') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full" type="text"
                                               placeholder="<?= Yii::t('app', 'Опишите объект') ?>"
                                               name="Object[description]" value="<?= $object->description ?>"
                                        />
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__added-photo">
                                        <div class="object-form__label"><?= Yii::t('app', 'Добавить фотографии объекта') ?>
                                        </div>
                                        <div class="object-form__photo-block">
                                            <div class="object-form__photo-preview"></div>
                                            <?php if (isset($photos)) : ?>
                                                <?php foreach ($photos as $photo) : ?>
                                                    <?php if ($photo->exists()) : ?>
                                                        <div class="object-form__wrap-photo">
                                                            <div class="object-form__photo-item">
                                                                <img class="object-form__thumb" src="<?=$photo->getUrl()?>">
                                                                <a class="object-form__delete-photo object-form__delete-photo_edit" href="javascript:void(0)" data-id="<?=$photo->id?>"></a>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <input class="object-form__file-input" type="file" name="Object[files][]"
                                                   id="photos" multiple/>
                                            <label class="object-form__photo-control" for="photos"><span
                                                        class="object-form__photo-content"><span
                                                            class="object-form__photo-icon-wrapper">
            <svg class="icon icon_photo-camera object-form__camera-icon" width="64px" height="50px">
                <use xlink:href="/svg-symbols.svg#photo-camera"></use>
            </svg>
        </span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="object-form__row object-form__row_buttons object-form__row_buttons_step-one">
                                    <div class="object-form__required-inputs-text"><span
                                                class="object-form__star">*</span> Отмечены поля, обязательные для
                                        заполнения
                                    </div>
                                    <div class="object-form__control-buttons object-form__control-buttons_step-one">
                                    <?php if ($object->id) : ?>
                                        <a class="object-form__continue object-form__continue_light btn btn_theme_standart"
                                                data-next="2" data-id="<?=$object->id?>"><?= Yii::t('app', 'Продолжить') ?></a>
                                    <?php else: ?>
                                        <a
                                                class="object-form__continue btn btn_theme_standart"
                                                data-next="2" data-id="<?=$object->id?>"><?= Yii::t('app', 'Продолжить') ?></a>
                                    <?php endif; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="object-form__step-box" data-number="2">
                            <div class="object-form__step">
                                <div class="object-form__step-number">
                                    <div class="object-form__step-digit">2
                                    </div>
                                </div>
                                <div class="object-form__step-text"><?= Yii::t('app', 'Параметры объекта') ?>
                                </div>
                            </div>
                            <div class="object-form__next-step">
                                <div class="object-form__next-step-number">
                                    3. <?= Yii::t('app', 'Дополнительные сведения') ?>
                                </div>
                            </div>
                            <div class="object-form__data-wrapper">
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"> <?= Yii::t('app', 'Тип предложения') ?><span
                                                    class="object-form__required">*</span>
                                        </label>
                                        <select class="object-form__select object-form__required-select"
                                                name="Object[bargain_type]"
                                                placeholder=" <?= Yii::t('app', 'Продажа') ?>">
                                            <!--option class="object-form__option">
                                            </option-->

                                            <?php
                                            if (isset($bargainTypes)) {
                                                foreach ($bargainTypes as $key => $bargainType) { ?>
                                                    <option class="object-form__option"
                                                            value="<?= $key ?>" <?= ($object->bargain_type == $key) ? 'selected' : '' ?>><?= $bargainType ?>
                                                    </option>
                                                <?php }
                                            } ?>


                                        </select>
                                    </div>
                                    <div class="object-form__col object-form__col_inventory">

                                        <label class="object-form__label"><?= Yii::t('app', 'Кадастровый номер') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full"
                                               type="text" placeholder="<?= Yii::t('app', 'Укажите кадастровый номер') ?>"
                                               name="Object[cadastral_number]" value="<?= $object->cadastral_number ?>"
                                        />
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
                                        <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                                            <use xlink:href="/svg-symbols.svg#info"></use>
                                        </svg>
                                             </span><span class="object-form__info-text">Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link" href="javascript:void(0)">Земельным кодексом РФ</a>.</span></i>


                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Стоимость, руб') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number"
                                               type="text"
                                               placeholder="<?= Yii::t('app', 'Укажите стоимость аренды/продажи') ?>"
                                               name="Object[bargain_price]" value="<?= $object->bargain_price ?>"
                                        />
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Форма собственности') ?>
                                            <span class="object-form__required">*</span>
                                        </label>
                                        <select class="object-form__select object-form__required-select"
                                                name="Object[owner]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <?php
                                            if (isset($proprietorShips)) {
                                                foreach ($proprietorShips as $key => $proprietorShip) { ?>
                                                    <option class="object-form__option"
                                                            value="<?= $key ?>" <?= ($object->owner == $key) ? 'selected' : '' ?>><?= $proprietorShip ?>
                                                    </option>
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Удалённость от Москвы') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number" type="text"
                                               placeholder="Укажите расстояние в км"
                                               name="Object[distance_to_moscow]"
                                               value="<?= $object->distance_to_moscow ?>"
                                        />
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Категория земель') ?>
                                        </label>
                                        <select class="object-form__select object-form__select_info"
                                                name="Object[land_category]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <?php
                                            if (isset($landCategoriesData)) {
                                                foreach ($landCategoriesData as $key => $landCategoryData) { ?>
                                                    <option class="object-form__option"
                                                            value="<?= $key ?>" <?= ($object->land_category == $key) ? 'selected' : '' ?>><?= $landCategoryData ?>
                                                    </option>
                                                <?php }
                                            } ?>
                                        </select>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
            <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                <use xlink:href="/svg-symbols.svg#info"></use>
            </svg>
        </span><span class="object-form__info-text">   Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link" href="javascript:void(0)">Земельным кодексом РФ</a>.</span></i>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Назначение площади') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full" type="text"
                                               name="Object[square_target]" value="<?= $object->square_target ?>"
                                               placeholder="Укажите назначение"
                                        />
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Вид разрешённого использования') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full" type="text"
                                               name="Object[vri]" value="<?= $object->vri ?>"
                                               placeholder="Укажите ВРИ"
                                        />
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
                                            <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                                                <use xlink:href="/svg-symbols.svg#info"></use>
                                            </svg>
                                        </span><span class="object-form__info-text">   Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link" href="javascript:void(0)">Земельным кодексом РФ</a>.</span></i>
                                    </div>
                                </div>
                                <div class="object-form__row object-form__row_position-checkbox">
                                    <label class="object-form__label"><?= Yii::t('app', 'Выберите подключенные коммуникации') ?>
                                    </label>
                                    <div class="object-form__wrap-checkbox object-form__wrap-checkbox_position">
                                        <div class="checkbox checkbox_theme_standart object-form__checkbox">
                                            <input type="hidden" name="Object[has_electricity_supply]" value="0"/>
                                            <input class="checkbox__input" type="checkbox"
                                                   name="Object[has_electricity_supply]" value="1"
                                                <?= $object->has_electricity_supply == 1 ? 'checked' : '' ?>
                                                   id="power-supply"/>
                                            <label class="checkbox__label" for="power-supply"><span
                                                        class="checkbox__text"
                                                        title="<?= Yii::t('app', 'Энергоснабжение') ?>"><?= Yii::t('app', 'Энергоснабжение') ?></span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox_theme_standart object-form__checkbox">
                                            <input type="hidden" name="Object[has_gas_supply]" value="0"/>
                                            <input class="checkbox__input" type="checkbox" name="Object[has_gas_supply]"
                                                   value="1"
                                                <?= $object->has_gas_supply == 1 ? 'checked' : '' ?> id="gas-supply"/>
                                            <label class="checkbox__label" for="gas-supply"><span class="checkbox__text"
                                                                                                  title="<?= Yii::t('app', 'Газоснабжение') ?>"><?= Yii::t('app', 'Газоснабжение') ?></span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox_theme_standart object-form__checkbox">
                                            <input type="hidden" name="Object[has_water_supply]" value="0"/>
                                            <input class="checkbox__input" type="checkbox"
                                                   name="Object[has_water_supply]" value="1"
                                                <?= $object->has_water_supply == 1 ? 'checked' : '' ?>
                                                   id="water-supply"/>
                                            <label class="checkbox__label" for="water-supply"><span
                                                        class="checkbox__text"
                                                        title="<?= Yii::t('app', 'Водоснабжение') ?>"><?= Yii::t('app', 'Водоснабжение') ?></span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox_theme_standart object-form__checkbox">
                                            <input type="hidden" name="Object[has_water_removal_supply]" value="0"/>
                                            <input class="checkbox__input" type="checkbox"
                                                   name="Object[has_water_removal_supply]" value="1"
                                                <?= $object->has_water_removal_supply == 1 ? 'checked' : '' ?>
                                                   id="drainage"/>
                                            <label class="checkbox__label" for="drainage"><span class="checkbox__text"
                                                                                                title="<?= Yii::t('app', 'Водоотведение') ?>"><?= Yii::t('app', 'Водоотведение') ?></span>
                                            </label>
                                        </div>
                                        <div class="checkbox checkbox_theme_standart object-form__checkbox">
                                            <input type="hidden" name="Object[has_heat_supply]" value="0"/>
                                            <input class="checkbox__input" type="checkbox"
                                                   name="Object[has_heat_supply]" value="1"
                                                <?= $object->has_heat_supply == 1 ? 'checked' : '' ?>
                                                   id="headt-supply"/>
                                            <label class="checkbox__label" for="headt-supply"><span
                                                        class="checkbox__text"
                                                        title="<?= Yii::t('app', 'Теплоснабжение') ?>"><?= Yii::t('app', 'Теплоснабжение') ?></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="object-form__row object-form__row_buttons object-form__row_buttons_step-two">
                                    <div class="object-form__required-inputs-text"><span
                                                class="object-form__star">*</span> Отмечены поля, обязательные для
                                        заполнения
                                    </div>
                                    <div class="object-form__control-buttons">
                                                <?php if ($object->id) : ?>
                                                    <a class="object-form__continue object-form__continue_light btn btn_theme_standart"
                                                            data-next="3" data-id="<?=$object->id?>"><?= Yii::t('app', 'Продолжить') ?></a>
                                                <?php else: ?>
                                                    <a
                                                            class="object-form__continue btn btn_theme_standart"
                                                            data-next="3" data-id="<?=$object->id?>"><?= Yii::t('app', 'Продолжить') ?></a>
                                                <?php endif; ?>
                                                <a
                                                class="object-form__step-back btn btn_theme_light btn_theme_light_arrow-left"
                                                data-prev="1" data-id="<?=$object->id?>"><?= Yii::t('app', 'На шаг назад') ?></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="object-form__step-box" data-number="3">
                            <div class="object-form__step">
                                <div class="object-form__step-number">
                                    <div class="object-form__step-digit">3
                                    </div>
                                </div>
                                <div class="object-form__step-text"><?= Yii::t('app', 'Дополнительные сведения') ?>
                                </div>
                            </div>
                            <div class="object-form__data-wrapper">
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Наличие строений') ?>
                                        </label>
                                        <select class="object-form__select" name="Object[has_buildings]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <option class="object-form__option" value="1" <?= ($object->has_buildings == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Да') ?>
                                            </option>
                                            <option class="object-form__option" value="0" <?= ($object->has_buildings == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Нет') ?>
                                            </option>
                                        </select>
                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Наличие обременения объекта') ?>
                                            <span class="object-form__required">*</span>
                                        </label>
                                        <select class="object-form__select object-form__select_info object-form__required-select"
                                                name="Object[has_charge]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <option class="object-form__option" value="1" <?= ($object->has_charge == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Да') ?>
                                            </option>
                                            <option class="object-form__option" value="0" <?= ($object->has_charge == 0) ? 'selected' : '' ?>><?= Yii::t('app', 'Нет') ?>
                                            </option>
                                        </select>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
            <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                <use xlink:href="/svg-symbols.svg#info"></use>
            </svg>
        </span><span class="object-form__info-text">   Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link">Земельным кодексом РФ</a>.</span></i>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col object-form__col_size-full">

                                        <label class="object-form__label">Вид обременения
                                        </label><input class="object-form__input object-form__input_size-full"
                                                       type="text" name="Object[charge_type]" placeholder="Укажите вид обременения" value="<?= $object->charge_type ?>"/>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Расстояние до автодороги, км') ?>
                                        </label>
                                        <input class="object-form__input object-form__input_size-full object-form__required-number"
                                               type="text" name="Object[distance_to_nearest_road]"
                                               placeholder="<?= Yii::t('app', 'Укажите расстояние') ?>"/>
                                        <!--select class="object-form__select" name="Object[distance_to_nearest_road]"
                                                placeholder="<?= Yii::t('app', 'Укажите расстояние') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <option class="object-form__option" value="2" <?= ($object->distance_to_nearest_road == 2) ? 'selected' : '' ?>>2 <?= Yii::t('app', 'км') ?>
                                            </option>
                                            <option class="object-form__option" value="5" <?= ($object->distance_to_nearest_road == 5) ? 'selected' : '' ?>>5 <?= Yii::t('app', 'км') ?>
                                            </option>
                                        </select-->
                                    </div>
                                    <div class="object-form__col">

                                        <label class="object-form__label"><?= Yii::t('app', 'Расположение в ОЭЗ') ?>
                                        </label>
                                        <select class="object-form__select object-form__select_info"
                                                name="Object[is_special_ecological_zone]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <option class="object-form__option" value="1" <?= ($object->is_special_ecological_zone == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Да') ?>
                                            </option>
                                            <option class="object-form__option" value="0" <?= ($object->is_special_ecological_zone == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Нет') ?>
                                            </option>
                                        </select>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
            <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                <use xlink:href="/svg-symbols.svg#info"></use>
            </svg>
        </span><span class="object-form__info-text">   Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link">Земельным кодексом РФ</a>.</span></i>

                                    </div>
                                </div>

                                <div class="object-form__row">
                                    <div class="object-form__col">


                                        <label class="object-form__label"><?= Yii::t('app', 'Возможность присоединения к грузовой ж/д станции') ?>
                                        </label>
                                        <select class="object-form__select" name="Object[has_railway_availability]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <option class="object-form__option" value="1" <?= ($object->has_railway_availability == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Да') ?>
                                            </option>
                                            <option class="object-form__option" value="0" <?= ($object->has_railway_availability == 1) ? 'selected' : '' ?>><?= Yii::t('app', 'Нет') ?>
                                            </option>
                                        </select>


                                    </div>
                                    <div class="object-form__col">
                                        <label class="object-form__label"><?= Yii::t('app', 'Класс опасности производства') ?>
                                        </label>
                                        <select class="object-form__select object-form__select_info"
                                                name="Object[danger_class]"
                                                placeholder="<?= Yii::t('app', 'Выберите') ?>">
                                            <option class="object-form__option">
                                            </option>
                                            <?php
                                            if (isset($dangerClasses)) {
                                                foreach ($dangerClasses as $key => $dangerClass) { ?>
                                                    <option class="object-form__option"
                                                            value="<?= $key ?>" <?= ($object->danger_class == $key) ? 'selected' : '' ?>><?= $dangerClass ?>
                                                    </option>
                                                <?php }
                                            } ?>
                                        </select>
                                        <i class="object-form__info"><span class="object-form__wrap-ico">
            <svg class="icon icon_info object-form__info-ico" width="5px" height="11px">
                <use xlink:href="/svg-symbols.svg#info"></use>
            </svg>
        </span><span class="object-form__info-text">   Предназначено для определения правового статуса земельного участка и его разрешённого использования. Установлено и регулируется <a
                                                        class="object-form__info-link">Земельным кодексом РФ</a>.</span></i>
                                    </div>
                                </div>
                                <div class="object-form__row">
                                    <div class="object-form__title"><?= Yii::t('app', 'Требуемые мощности') ?>
                                    </div>
                                </div>
                                <div class="object-form__row object-form__row_input">
                                    <div class="object-form__col object-form__col_input">
                                        <label class="object-form__label"><?= Yii::t('app', 'Энергоснабжение') ?>
                                        </label>
                                        <input class="object-form__input object-form__required-number" name="Object[electricity_supply_capacity]"
                                               placeholder="Укажите мощность в МВт"
                                               value="<?= $object->electricity_supply_capacity ?>" type="text"/>
                                    </div>
                                    <div class="object-form__col object-form__col_input">
                                        <label class="object-form__label"><?= Yii::t('app', 'Водоснабжение') ?>
                                        </label>
                                        <input class="object-form__input object-form__required-number" name="Object[water_supply_capacity]"
                                               placeholder="Укажите мощность в м3/день"
                                               value="<?= $object->water_supply_capacity ?>" type="text"/>
                                    </div>
                                </div>
                                <div class="object-form__row object-form__row_input">
                                    <div class="object-form__col object-form__col_input">
                                        <label class="object-form__label"><?= Yii::t('app', 'Газоснабжение') ?>
                                        </label>
                                        <input class="object-form__input object-form__required-number" name="Object[gas_supply_capacity]"
                                               placeholder="Укажите мощность в м3/час"
                                               value="<?= $object->gas_supply_capacity ?>" type="text"/>
                                    </div>
                                    <div class="object-form__col object-form__col_input">
                                        <label class="object-form__label"><?= Yii::t('app', 'Водоотведение') ?>
                                        </label>
                                        <input class="object-form__input object-form__required-number" name="Object[water_removal_supply_capacity]"
                                               placeholder="Укажите мощность в м3/день"
                                               value="<?= $object->water_removal_supply_capacity ?>" type="text"/>
                                    </div>
                                </div>
                                <div class="object-form__row object-form__row_input">
                                    <div class="object-form__col object-form__col_input">
                                        <label class="object-form__label">Теплоснабжение</label>
                                        <input class="object-form__input object-form__required-number" name="Object[heat_supply_capacity]" placeholder="Укажите мощность в ГКал/час"
                                               value="<?= $object->heat_supply_capacity ?>" type="text"/>
                                    </div>
                                </div>
                                <div class="object-form__row object-form__row_buttons object-form__row_buttons_step-three">
                                    <div class="object-form__required-inputs-text"><span
                                                class="object-form__star">*</span> Отмечены поля, обязательные для
                                        заполнения
                                    </div>
                                    <div class="object-form__control-buttons">
                                        <?php if (!$object->id) : ?>
                                        <button class="object-form__continue btn btn_theme_standart"
                                                type="submit"><?= Yii::t('app', 'Отправить заявку') ?>
                                        </button>
                                        <?php endif; ?>
                                        <a class="object-form__step-back btn btn_theme_light btn_theme_light_arrow-left"
                                           data-prev="2"><?= Yii::t('app', 'На шаг назад') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup-success popup-success popup-success_absolute">
    <A class="popup-success__close" href="javascript:void(0)"></A>
    <div class="popup-success__ico">
        <svg class="icon icon_success popup-success__ico-success" width="31px" height="26px">
            <use xlink:href="/svg-symbols.svg#success"></use>
        </svg>

    </div>
    <div class="popup-success__title popup-success__title_margin">Ваша заявка отправлена</div>
    <div class="popup-success__sub-title">Мы рассмотрим ее в течение 15-ти дней</div>
    <div class="popup-success__buttons">
        <a class="popup-success__button btn btn_theme_light" href="/cabinet">Не сохранять</a>
        <a class="popup-success__button btn btn_theme_standart popup-success__button_save" href="javascript:void(0)">Сохранить</a>
    </div>
</div>
