<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 27.07.2017
 * Time: 13:00
 */
use yii\helpers\Url;
use yii\helpers\Html;
$js = <<<JS
    /*
   $(document).on('submit','#edit-profile',function () {
        var formData = new FormData($('#edit-profile')[0]);
        var url = $(this).attr('action');
        var type = $(this).attr('method');
        var haveToAlert = $(this).attr('data-alert');
        $.ajax({
            url: url,
            contentType: false,
            processData: false,
            data: formData,
            type: type,
            success: function (res) {
                if(haveToAlert == 'true')
                    alert(res);
                location.href = '/cabinet/view-profile';
            }
        });
         return false;
    });*/
JS;
$this->registerJs($js);
?>
<div class="p-cabinet-edit-profile p-cabinet">
    <div class="container">
        <div class="cabinet-menu p-cabinet__cabinet-menu">
            <ul class="cabinet-menu__list">
                <!--li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/help'])?>">Помощь</a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/edit-profile'])?>">Редактировать профиль</a>
                </li-->
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/'])?>">Ваши объекты</a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/logout'])?>">Выйти</a>
                </li>
            </ul>
            <div class="cabinet-menu__lc-ico">
                <a href="/cabinet/view-profile">
                    <?php if ($user->getFullImageUrl('little')) : ?>
                        <img class="cabinet-menu__profile-ico" src="<?=$user->getFullImageUrl('little')?>" alt="">
                    <?php else: ?>
                        <svg class="icon icon_user-lc cabinet-menu__ico cabinet-menu__ico_user" width="20px" height="20px">
                            <use xlink:href="/svg-symbols.svg#user-lc"></use>
                        </svg>
                    <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="p-cabinet-edit-profile__content p-cabinet__content p-cabinet__content p-cabinet__content_position">
            <div class="p-cabinet-edit-profile__left p-cabinet__left">
                <div class="object-menu__text object-menu__text_bold">Редактирование профиля</div>
            </div>
            <div class="p-cabinet-edit-profile__right p-cabinet__right p-cabinet__right p-cabinet__right_position">
                <div class="edit-profile">
                    <form id="edit-profile" class="form_ajax" data-alert="true" action="<?=Url::to(['/cabinet/profile-update'])?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken()?>">
                        <div class="edit-profile__box">
                            <div class="edit-profile__img-select">
                                <input class="edit-profile__file-input" type="file" name="User[image]" id="photo"/>
                                <label class="edit-profile__photo-control" for="photo"><span class="edit-profile__photo-content">
                                    <span class="edit-profile__photo-icon-wrapper">
                                        <svg class="icon icon_photo-camera edit-profile__camera-icon" width="64px" height="50px">
                                            <use xlink:href="/svg-symbols.svg#photo-camera"></use>
                                        </svg>
                                    </span>
                                    <span class="edit-profile__photo-text">Загрузить фото</span></span>
                                </label>
                                <a href="javascript:void(0)" class="edit-profile__delete-photo"></a>
                            </div>
                            <div class="edit-profile__inputs">
                                <div class="edit-profile__input-row">
                                    <div class="edit-profile__message"></div>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="surname">Фамилия
                                        <span class="edit-profile__required"> *</span>
                                    </label><input class="edit-profile__input" type="text" name="User[last_name]" id="surname" value="<?=Html::encode($user->last_name)?>"/>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="name">Имя
                                        <span class="edit-profile__required"> *</span>
                                    </label><input class="edit-profile__input" type="text" name="User[first_name]" id="name" value="<?=Html::encode($user->first_name)?>"/>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="patronymic">Отчество
                                    </label><input class="edit-profile__input" type="text" name="User[third_name]" id="patronymic" value="<?=Html::encode($user->third_name)?>"/>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="company">Компания
                                    </label><input class="edit-profile__input" type="text" name="User[company_name]" id="company" value="<?=Html::encode($user->company_name)?>"/>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="phone">Телефон
                                    </label><input class="edit-profile__input edit-profile__input-phone" type="text" name="User[phone]" id="phone" value="<?=Html::encode($user->phone)?>"/>
                                </div>
                                <div class="edit-profile__input-row">
                                    <label class="edit-profile__input-label" for="email">Email
                                        <span class="edit-profile__required"> *</span>
                                    </label><input class="edit-profile__input" type="text" name="User[email]" id="email" value="<?=Html::encode($user->email)?>"/>
                                </div>
                                <div class="edit-profile__required-inputs-text"><span
                                        class="object-form__star">*</span> Отмечены поля, обязательные для
                                    заполнения
                                </div>
                                <div class="edit-profile__input-row edit-profile__input-row_buttons">
                                    <button class="edit-profile__save btn btn_theme_standart" type="submit">Сохранить</button>
                                    <a class="edit-profile__cancel btn btn_theme_standart" href="<?=Url::to(['/cabinet'])?>">Отмена</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>