<?php
use common\models\Object;
use common\models\ObjectRequest;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
$this->title = 'Кабинет пользователя';
$success = null;
if(Yii::$app->session->getFlash('success')){
    $success = Yii::$app->session->getFlash('success');
$js = <<<JS
    $('body').addClass('open-popup');
JS;
$this->registerJs($js);
}
$js = <<<JS
   $(document).on('click','.object-menu__link_actuality',function () {
        $.ajax({
            url: '/cabinet/make-actuality',
            type: 'GET',
            beforeSend:function() {
              $('body').prepend('<div class="preloader preloader_spinner"><span class="preloader__text">Ваш запрос обрабатывается...</span></div>');
            },
            success: function (res) {
                $('body').find('.preloader').remove();
                $('body').addClass('open-popup');
                $('body').append(res);
                 setTimeout(function() {
                        window.location = '/cabinet';
                    }, 2000);
            }
        });
         return false;
    });
JS;
$this->registerJs($js);
?>
<div class="p-cabinet">
    <div class="container">
        <div class="cabinet-menu p-cabinet__cabinet-menu">
            <ul class="cabinet-menu__list">
                <!--li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/help'])?>">Помощь</a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/edit-profile'])?>">Редактировать
                        профиль</a>
                </li-->
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/'])?>">Ваши объекты</a>
                </li>
                <li class="cabinet-menu__item"><a class="cabinet-menu__link" href="<?=Url::to(['/cabinet/logout'])?>">Выйти</a>
                </li>
            </ul>
            <div class="cabinet-menu__lc-ico">
                <a href="/cabinet/view-profile">
                    <?php if ($user->getFullImageUrl('little')) : ?>
                        <img class="cabinet-menu__profile-ico" src="<?=$user->getFullImageUrl('little')?>" alt="">
                    <?php else: ?>
                        <svg class="icon icon_user-lc cabinet-menu__ico cabinet-menu__ico_user" width="20px" height="20px">
                            <use xlink:href="/svg-symbols.svg#user-lc"></use>
                        </svg>
                    <?php endif; ?>
                </a>
            </div>
        </div>
        <div class="p-cabinet__content p-cabinet__content_position">
            <div class="p-cabinet__left">
                <div class="object-menu p-cabinet__object-menu">
                    <ul class="object-menu__list">
                        <!--li class="object-menu__item"><a class="object-menu__link" href="javascript:void(0)">Ваши
                                объекты</a>
                        </li-->
                        <li class="object-menu__item"><a class="object-menu__link object-menu__link_bold">Просмотр
                            ваших объектов</a>
                        </li>
                        <li class="object-menu__item"><a
                                class="object-menu__link object-menu__link_add btn btn_theme_standart"
                                href="<?= Url::to("/cabinet/create-object") ?>">Добавить объект</a>
                        </li>
                        <?php if($actuality){?>
                        <li class="object-menu__item_down">
                            <div class="object-menu__actuality-circle-wrap object-menu__item_b">!</div>
                            <div class="object-menu__line object-menu__item_b"></div>
                            <div class="object-menu__text_d object-menu__item_b">Пожалуйста, подтвердите актуальность размещенных Вами инвестиционных объектов, иначе объекты могут быть удалены</div>
                        </li>
                        <li class="object-menu__item">
                            <a class="object-menu__link object-menu__link_actuality btn btn_theme_standart" href="javascript:void(0)">Объекты актуальны</a>
                        </li>
                        <?php }?>
                    </ul>
                </div>
            </div>
            <div class="p-cabinet__right p-cabinet__right_position">
                <?php if ($objects) : ?>
                <? foreach ($objects as $object): ?>
                <div class="object-list">
                    <div class="object-list__image-box">
                        <?php $photos = $object->getPhotos()->all() ?>
                        <img class="object-list__image"
                             src="<?= ($photos && !empty($photos[0]->getUrl())) ? $photos[0]->getUrl() : '/static/img/content/cabinet/default-image-list.png' ?>"
                             alt="" role="presentation"/>
                    </div>
                    <div class="object-list__info-box">
                        <a class="object-list__close-link" data-id="<?=$object->id?>" href="javascript:void(0)">
                            <svg class="icon icon_close-lc object-list__close" width="17px" height="15px">
                                <use xlink:href="/svg-symbols.svg#close-lc"></use>
                            </svg>
                        </a><a class="object-list__link"
                               href="javascript:void(0)"><?= $object->type->name ?></a>
                        <div class="object-list__icon-box">
                            <!-- Электроснабжение иконка -->
                            <div class="object-list__ico-wrap <?=($object->has_electricity_supply) ? 'object-list__ico-wrap_visible' : '' ?>">
                                <svg class="icon icon_slash object-list__ico object-list__ico_slash"
                                     width="19px" height="32px">
                                    <use xlink:href="/svg-symbols.svg#slash"></use>
                                </svg>
                            </div>
                            <!-- -->

                            <!-- Газоснабжение -->
                            <div class="object-list__ico-wrap <?=($object->has_gas_supply) ? 'object-list__ico-wrap_visible' : ''?>">
                                <svg class="icon icon_flame object-list__ico object-list__ico_flame"
                                     width="18px" height="23px">
                                    <use xlink:href="/svg-symbols.svg#flame"></use>
                                </svg>
                            </div>
                            <!-- -->

                            <!-- Водоснабжение -->
                            <div class="object-list__ico-wrap <?=($object->has_water_supply) ? 'object-list__ico-wrap_visible' : ''?>">
                                <svg class="icon icon_flame object-list__ico object-list__ico_water"
                                     width="17px" height="23px">
                                    <use xlink:href="/svg-symbols.svg#icon_water"></use>
                                </svg>
                            </div>
                            <!-- -->

                            <!-- Водоотведение -->
                            <div class="object-list__ico-wrap <?=($object->has_water_removal_supply) ? 'object-list__ico-wrap_visible' : ''?>">
                                <svg class="icon icon_flame object-list__ico object-list__ico_clearing"
                                     width="22px" height="22px">
                                    <use xlink:href="/svg-symbols.svg#icon_clearing"></use>
                                </svg>
                            </div>
                            <!-- -->

                            <!-- Теплоснабжение -->
                            <div class="object-list__ico-wrap <?=($object->has_heat_supply) ? 'object-list__ico-wrap_visible' : ''?>">
                                <svg class="icon icon_flame object-list__ico object-list__ico_heating"
                                     width="12px" height="27px">
                                    <use xlink:href="/svg-symbols.svg#icon_heating"></use>
                                </svg>
                            </div>
                            <!-- -->
                        </div>
                        <div class="object-list__location-box">
                            <div class="object-list__address"><?= Html::encode($object->name) ?>
                            </div>
                            <div class="object-list__location"><?= Html::encode($object->address_address) ?>
                            </div>
                        </div>
                        <div class="object-list__params-box">
                            <p class="object-list__lease">
                                <?= ($object->bargain_type) ? $object->bargain[$object->bargain_type] : 'Форма реализации не указана' ?>
                            </p>
                            <p class="object-list__area">Площадь
                                <?php if ($object->square) : ?>
                                <?= $object->square ?> га
                                <?php elseif ($object->square_sqm): ?>
                                <?= $object->square_sqm ?> кв.м
                                <?php else: ?>
                                не указана
                                <?php endif; ?>
                            </p>
                            <p class="object-list__road">До ближайшей крупной
                                дороги <?= ($object->distance_to_nearest_road) ? $object->distance_to_nearest_road.' км' : 'расстояние не указано' ?>
                            </p>
                        </div>
                        <div class="object-list__links-box">
                            <?php switch ($object->request_status):
                            case ObjectRequest::STATUS_APPROVED: ?>
                            <span class="object-list__placed">
                                            <span class="object-list__circle-wrap">
                                                <svg class="object-list__placed-ico">
                                                    <use xlink:href="/svg-symbols.svg#placed-ico"></use>
                                                </svg>
                                            </span>
                                            Размещено
                                        </span>
                                <a class="object-list__view" href="<?=Yii::$app->getUrlManager()->getBaseUrl().'/map?ObjectMapSearch%5Baddress_address%5D=&ObjectMapSearch%5Bowner%5D%5B%5D=1&ObjectMapSearch%5Bowner%5D%5B%5D=2&ObjectMapSearch%5Bowner%5D%5B%5D=3&ObjectMapSearch%5Bbargain_type%5D%5B%5D=1&ObjectMapSearch%5Bbargain_type%5D%5B%5D=2&ObjectMapSearch%5Bbargain_price%5D%5B%5D=&ObjectMapSearch%5Bbargain_price%5D%5B%5D=&ObjectMapSearch%5Bsquare%5D%5B%5D=&ObjectMapSearch%5Bsquare%5D%5B%5D=&ObjectMapSearch%5Bcadastral_number%5D=&ObjectMapSearch%5Bdistance_to_moscow%5D%5B%5D=&ObjectMapSearch%5Bdistance_to_moscow%5D%5B%5D=&ObjectMapSearch%5Bsquare_target%5D=&ObjectMapSearch%5Bland_category%5D%5B%5D=1&ObjectMapSearch%5Bland_category%5D%5B%5D=2&ObjectMapSearch%5Bland_category%5D%5B%5D=3&ObjectMapSearch%5Bhas_buildings%5D%5B%5D=1&ObjectMapSearch%5Bhas_buildings%5D%5B%5D=0&ObjectMapSearch%5Bhas_road_availability%5D%5B%5D=1&ObjectMapSearch%5Bhas_road_availability%5D%5B%5D=0&ObjectMapSearch%5Bhas_railway_availability%5D%5B%5D=1&ObjectMapSearch%5Bhas_railway_availability%5D%5B%5D=0&ObjectMapSearch%5Bhas_charge%5D%5B%5D=1&ObjectMapSearch%5Bhas_charge%5D%5B%5D=0&ObjectMapSearch%5Bis_special_ecological_zone%5D%5B%5D=1&ObjectMapSearch%5Bis_special_ecological_zone%5D%5B%5D=0&ObjectMapSearch%5Bdanger_class%5D%5B%5D=1&ObjectMapSearch%5Bdanger_class%5D%5B%5D=2&ObjectMapSearch%5Bdanger_class%5D%5B%5D=3&ObjectMapSearch%5Bdanger_class%5D%5B%5D=4&ObjectMapSearch%5Bfree_land%5D%5B%5D=1&ObjectMapSearch%5Bfree_land%5D%5B%5D=0&ObjectMapSearch%5Bfree_room%5D%5B%5D=1&ObjectMapSearch%5Bfree_room%5D%5B%5D=0&ObjectMapSearch%5Bokved%5D=&ObjectMapSearch%5Bflat%5D%5B%5D=&ObjectMapSearch%5Bflat%5D%5B%5D=&ObjectMapSearch%5Bflat_height%5D%5B%5D=&ObjectMapSearch%5Bflat_height%5D%5B%5D=&section=0&main-'.$object->object_type_id.'=on&id='.$object->id?>" target="_blank">
                                <svg class="object-list__view-ico">
                                    <use xlink:href="/svg-symbols.svg#point"></use>
                                </svg>
                                Посмотреть на карте</a>
                            <?php break; ?>
                            <?php case ObjectRequest::STATUS_REJECTED: ?>
                            <span class="object-list__placed">
                                            <span class="object-list__circle-wrap object-list__circle-wrap_rejected"></span>
                                            Отклонено
                                        </span>
                            <?php break; ?>
                            <?php default: ?>
                            <span class="object-list__placed">
                                            <span class="object-list__circle-wrap object-list__circle-wrap_is-considered"></span>
                                            На рассмотрении
                                        </span>
                            <?php break; ?>
                            <?php endswitch; ?>
                            <a class="object-list__edit" href="<?= Url::to("/cabinet/update-object/".$object->id) ?>">
                            <svg class="object-list__edit-ico">
                                <use xlink:href="/svg-symbols.svg#edit-ico"></use>
                            </svg>
                            Редактировать</a>
                        </div>
                    </div>
                </div>
                <? endforeach; ?>
                <?php else: ?>
                <div class="object-list object-list_empty">
                    <div class="object-list__image-box">
                        <div class="object-list__image"></div>
                    </div>
                    <div class="object-list__info-box">
                        <p class="object-list__title">Вы пока не разместили ни одного объекта</p>
                        <p class="object-list__subtitle">Для добавления объекта нажмите на кнопку слева</p>
                    </div>
                </div>
                <?php endif ?>
            </div>
            <?php if($pagination->totalCount > 10) {?>
            <div class="pagination p-news__pagination">
                <?=LinkPager::widget([
                    'pagination' => $pagination,
                    'activePageCssClass' => 'pagination__marker_active',
                    'pageCssClass' => 'pagination__marker',
                    'prevPageLabel' => '',
                    'nextPageLabel' => '',
                    'prevPageCssClass' => 'hidden',
                    'nextPageCssClass' => 'hidden',
                ]);?>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="popup-success popup-success_absolute">
        <A class="popup-success__close" href="javascript:void(0)"></A>
        <div class="popup-success__ico">
            <svg class="icon icon_success popup-success__ico-success" width="31px" height="26px">
                <use xlink:href="/svg-symbols.svg#success"></use>
            </svg>

        </div>
        <div class="popup-success__title popup-success__title_margin">Вы уверены, что хотите удалить этот объект?</div>
        <div class="popup-success__sub-title"></div>
        <div class="popup-success__buttons">
            <a class="popup-success__button popup-success__button_confirmation btn btn_theme_light" href="javascript:void(0)">Подтвердить</a>
            <a class="popup-success__button popup-success__button_renouncement btn btn_theme_standart" href="javascript:void(0)">Отмена</a>
        </div>
    </div>
    <?php if($success) echo $this->render('actuality_popup',
        [
            'success' => $success
        ])?>
</div>
