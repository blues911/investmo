<?php
/**
 * Created by PhpStorm.
 * User: shakinm@gmail.com
 * Date: 25.06.2017
 * Time: 17:05
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Скрипт для работы с формой заявок
 */
class ObjectAsset extends AssetBundle
{

    public $sourcePath = '@root/frontend/views/cabinet/js';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $js = [
        'script.js',
    ];

    public $depends = [
        'frontend\assets\MainAsset',
    ];
}