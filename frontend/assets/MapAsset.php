<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 24.05.2017
 * Time: 16:28
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class MapAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];
    public $depends = [
        'frontend\assets\MainAsset',
    ];
}