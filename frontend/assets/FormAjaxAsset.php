<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 11.08.2017
 * Time: 14:59
 */

namespace frontend\assets;
use yii\web\AssetBundle;


class FormAjaxAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        '/static/js/form_ajax.js',
    ];
}