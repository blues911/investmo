<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 01.08.2017
 * Time: 15:10
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ApiAsset extends AssetBundle
{
    public $sourcePath = '@app/frontend/views/api';
    public $js = [
        'main.js',
        'api_project.js'
    ];
    public $depends = [
    ];
}