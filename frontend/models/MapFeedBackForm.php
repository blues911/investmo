<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 17.07.2017
 * Time: 22:53
 */


namespace frontend\models;

use Yii;
use yii\base\Model;

use common\models\MapFeedBack;
use common\models\MapFeedBackTerm;

class MapFeedBackForm extends Model
{
    public $key;
    public $email;
    public $min_square;
    public $max_square;
    public $bargain_type;
    public $type;
    public $min_bargain_price;
    public $max_bargain_price;
    public $min_distance_to_moscow;
    public $max_distance_to_moscow;

    public function rules(){
        return [
            ['email', 'email'],
            ['email', 'required'],
            [['min_bargain_price', 'max_bargain_price'], 'number'],
            [['min_square','max_square'], 'double'],
            [['min_distance_to_moscow', 'max_distance_to_moscow'],'integer'],
            [['type','bargain_type'], 'checkInteger'],
            [['key'], 'string']
        ];
    }
    public function checkInteger()
    {
        if($this->type =='')$this->type = [];
        if($this->bargain_type =='')$this->bargain_type = [];
        $arr = array_merge($this->type, $this->bargain_type);
        if($arr)
        {
            foreach ($arr as $num)
            {
                if(!is_numeric($num))
                    return false;
            }
        }
        return true;
    }
    public function save_map_feed_back_term($map_feedback_id){
        if(isset($this->bargain_type))
        {
            foreach ($this->bargain_type as $type){
                $map_feedback_term = new MapFeedBackTerm();
                $map_feedback_term->map_feed_back_id = $map_feedback_id;
                $map_feedback_term->term_id = $type;
                $map_feedback_term->term_type = 'bargain_type';
                if(!$map_feedback_term->save())
                    return false;
                $map_feedback_term->save();
            }
        }
        if(isset($this->type))
        {
            foreach ($this->type as $obj_type){
                $map_feedback_term = new MapFeedBackTerm();
                $map_feedback_term->map_feed_back_id = $map_feedback_id;
                $map_feedback_term->term_id = $obj_type;
                $map_feedback_term->term_type = 'type_object_id';
                if(!$map_feedback_term->save())
                    return false;
                $map_feedback_term->save();
            }
        }
        return true;
    }
    public function save_map_feed_back(){
        if (!$this->validate()) {
            return false;
        }
        $date = Date('Y-m-d H:m:s',time());
        $map_feedback = new MapFeedBack();
        $map_feedback->key = $this->key;
        $map_feedback->email = $this->email;
        $map_feedback->min_square = $this->min_square;
        $map_feedback->max_square = $this->max_square;
        $map_feedback->min_bargain_price = $this->min_bargain_price;
        $map_feedback->max_bargain_price = $this->max_bargain_price;
        $map_feedback->min_distance_to_moscow = $this->min_distance_to_moscow;
        $map_feedback->max_distance_to_moscow = $this->max_distance_to_moscow;
        $map_feedback->created_at = $date;
        $map_feedback->updated_at = $date;
        if($map_feedback->save() && $this->save_map_feed_back_term(Yii::$app->db->lastInsertID))
            return true;
        else
            return false;
    }
}