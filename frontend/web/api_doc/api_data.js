define({ "api": [
  {
    "type": "",
    "url": "{}",
    "title": "форма реализации (BARGAIN_TYPE)",
    "version": "0.1.0",
    "name": "BARGAIN_TYPE",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "BARGAIN_TYPE_SALE",
            "optional": false,
            "field": "1",
            "description": "<p>Продажа</p>"
          },
          {
            "group": "Parameter",
            "type": "BARGAIN_TYPE_RENT",
            "optional": false,
            "field": "2",
            "description": "<p>Аренда</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "",
    "url": "{}",
    "title": "сектор промышленности (INDUSTRY_TYPE)",
    "version": "0.1.0",
    "name": "INDUSTRY_TYPE",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_ELECTRICAL",
            "optional": false,
            "field": "1",
            "description": "<p>Электроэнергетика</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_FUEL",
            "optional": false,
            "field": "2",
            "description": "<p>Топливная промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_IRON",
            "optional": false,
            "field": "3",
            "description": "<p>Черная металлургия</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_NONFERROUS",
            "optional": false,
            "field": "4",
            "description": "<p>Цветная металлургия</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_CHEMISTRY",
            "optional": false,
            "field": "5",
            "description": "<p>Химия и нефтехимия</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_METAL_FABRICATION",
            "optional": false,
            "field": "6",
            "description": "<p>Машиностроение и металлообработка</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_WOOD",
            "optional": false,
            "field": "7",
            "description": "<p>Лесообрабатывающая промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_CONSTRUCTION_MATERIALS",
            "optional": false,
            "field": "8",
            "description": "<p>Промышленность стройматериалов</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_GLASS",
            "optional": false,
            "field": "9",
            "description": "<p>Стекольная промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_CONSUMERS_GOODS",
            "optional": false,
            "field": "10",
            "description": "<p>Легкая промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_FOOD",
            "optional": false,
            "field": "11",
            "description": "<p>Пищевая промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_MICROBIOLOGICAL",
            "optional": false,
            "field": "12",
            "description": "<p>Микробиологическая промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_AGRICULTURAL",
            "optional": false,
            "field": "13",
            "description": "<p>Сельскохозяйственная промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_MEDICINE",
            "optional": false,
            "field": "14",
            "description": "<p>Медицинская промышленность</p>"
          },
          {
            "group": "Parameter",
            "type": "INDUSTRY_TYPE_POLYGRAPHIC",
            "optional": false,
            "field": "15",
            "description": "<p>Полиграфическая промышленность</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "",
    "url": "{}",
    "title": "категория земель (LAND_CATEGORY)",
    "version": "0.1.0",
    "name": "LAND_CATEGORY",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "LAND_CATEGORY_SETTLEMENTS",
            "optional": false,
            "field": "1",
            "description": "<p>Населенный пункт</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_CATEGORY_INDUSTRIAL_PURPOSE",
            "optional": false,
            "field": "2",
            "description": "<p>Промышленного назначения</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_CATEGORY_AGRICULTURE",
            "optional": false,
            "field": "3",
            "description": "<p>Сельскохозяйственного назначения</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_CATEGORY_RESERVE",
            "optional": false,
            "field": "4",
            "description": "<p>Заповедник</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "",
    "url": "{}",
    "title": "статус участка (LAND_STATUS)",
    "version": "0.1.0",
    "name": "LAND_STATUS",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "LAND_STATUS_FREE",
            "optional": false,
            "field": "1",
            "description": "<p>Свободен</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_STATUS_PLANNED_BARGAIN",
            "optional": false,
            "field": "2",
            "description": "<p>Планируется на торги</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_STATUS_STARTED_BARGAIN",
            "optional": false,
            "field": "3",
            "description": "<p>Торги объявлены</p>"
          },
          {
            "group": "Parameter",
            "type": "LAND_STATUS_FINISHED_BARGAIN",
            "optional": false,
            "field": "4",
            "description": "<p>Торги прошли</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "",
    "url": "{}",
    "title": "тип объекта (OBJECT_TYPE_ID)",
    "version": "0.1.0",
    "name": "OBJECT_TYPE_ID",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "TYPE_INDUSTRIAL_PARK",
            "optional": false,
            "field": "1",
            "description": "<p>Индустриальные парки</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_TECHNOLOGICAL_PARK",
            "optional": false,
            "field": "2",
            "description": "<p>Технопарки</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_SEZ",
            "optional": false,
            "field": "3",
            "description": "<p>ОЭЗ</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_BROWNFIELDS",
            "optional": false,
            "field": "4",
            "description": "<p>Браунфилды</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_RESIDENT",
            "optional": false,
            "field": "5",
            "description": "<p>Резиденты</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_FREE_LAND",
            "optional": false,
            "field": "6",
            "description": "<p>Свободные земли</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_FREE_SPACE",
            "optional": false,
            "field": "7",
            "description": "<p>Свободные площади</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_COWORKING",
            "optional": false,
            "field": "8",
            "description": "<p>Коворкинги</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_LAND",
            "optional": false,
            "field": "9",
            "description": "<p>Земельные участки</p>"
          },
          {
            "group": "Parameter",
            "type": "TYPE_PROPERTY",
            "optional": false,
            "field": "10",
            "description": "<p>Объекты имущества</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "",
    "url": "{}",
    "title": "форма собственности (OWNER)",
    "version": "0.1.0",
    "name": "OWNER",
    "group": "Dictionaries",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "OWNER_GOVERNMENT",
            "optional": false,
            "field": "1",
            "description": "<p>Государственная</p>"
          },
          {
            "group": "Parameter",
            "type": "OWNER_MUNICIPAL",
            "optional": false,
            "field": "2",
            "description": "<p>Муниципальная</p>"
          },
          {
            "group": "Parameter",
            "type": "OWNER_PRIVATE",
            "optional": false,
            "field": "3",
            "description": "<p>Частная</p>"
          }
        ]
      }
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Dictionaries"
  },
  {
    "type": "post",
    "url": "/create",
    "title": "Создать объект",
    "version": "0.1.0",
    "name": "Create",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на создание объектов с указанием их параметров.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id системы.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>рабочий токен полученный при авторизации.</p>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>Данные для фильтра по контейнерам.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Request-example:",
          "content": " data =\n [\n     {\n          \"name\":\"ЗКО (Завод котельного оборудования)\", - название объекта (обязательный)\n          \"object_type_id\":4, - тип объекта (обязательный)\n          \"description\":\"\", - общие сведения\n          \"address_address\":\"г. Талдом\", - адрес (обязательный)\n          \"latitude\":56.717241, - широта (обязательный)\n          \"longitude\":37.538879, - долгота (обязательный)\n          \"owner\":3, - форма собственности\n          \"municipality\":\"\", - муниципальное образование\n          \"owner_name\":\"\", - наименование владельца\n          \"bargain_type\":null, - форма реализации\n          \"bargain_price\":null, - стоимость, руб\n          \"square\":2.48, - площадь, га\n          \"square_sqm\":null, - площадь, кв. м\n          \"cadastral_number\":\"50:01:0031105:24\", - кадастровый номер\n          \"industry_type\":1, - сектор промышленности\n          \"distance_to_moscow\":null, - расстояние до Москвы, км\n          \"square_target\":\"Промышленное производство\", - назначение площади\n          \"has_electricity_supply\":1, - наличие электроснабжения\n          \"has_gas_supply\":1, - наличие газоснабжения\n          \"has_water_supply\":1, - наличие водоснабжения\n          \"has_water_removal_supply\":1, - наличие водоотведения\n          \"has_heat_supply\":1, - наличие теплоснабжения\n          \"electricity_supply_capacity\":\"0.65\", - мощность электроснабжения, МВт\n          \"gas_supply_capacity\":\"\", - мощность газоснабжения, м3/час\n          \"water_supply_capacity\":\"\", - мощность водоснабжения, м3/день\n          \"water_removal_supply_capacity\":\"\", - мощность водоотведения, м3/день\n          \"heat_supply_capacity\":\"\", - мощность темплоснабжения, ГКал/час\n          \"land_category\":\"1\", - категория земель\n          \"land_status\":null, - статус земель\n          \"vri\":\"123\", - ВРИ\n          \"has_buildings\":1, - наличие строений\n          \"buildings_count\":0, - число строений\n          \"has_road_availability\":0, - наличие доступа к автодороге\n          \"distance_to_nearest_road\":null, - расстояние до автодороги\n          \"has_railway_availability\":1, - возможность присоединения к грузовой ж/д станции\n          \"has_charge\":0, - наличие обременения\n          \"charge_type\":\"\", - вид обременения\n          \"danger_class\":1, - класс опасности производства\n          \"okved\":\"\", - ОКВЭД\n          \"phone\":\"\", - контактный телефон\n          \"parent_id\":null, - родительский объект\n          \"flat\":null, - этажность\n          \"flat_height\":null, - высота этажа\n          \"photos\" : [ - массив фотографий, каждое фото представлено в виде base64-строки\n              \"==Photo_file_in_base64\"\n          ]\n     },\n     ...\n]",
          "type": "json"
        },
        {
          "title": "Success-Response-example:",
          "content": "[\n    1,\n    2,\n    3,\n    ...\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Validating-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" : \"Validating error\",\n  \"errors\": [\n      {\n         \"name\": \"can not be blank\"\n      },\n      ...\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Saving-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" : \"Saving error\",\n  \"errors\": [\n      {\n         \"name\": \"can not be blank\"\n      },\n      ...\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" => \"Auth error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/delete",
    "title": "Удалить объект",
    "version": "0.1.0",
    "name": "Delete",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на удаление созданных объектов</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id системы.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>рабочий токен полученный при авторизации.</p>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>Данные для фильтра по контейнерам.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Requset-example:",
          "content": "data =\n[\n     123,\n     321,\n     ...\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" => \"Auth error\"\n}",
          "type": "json"
        },
        {
          "title": "Search-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"404\",\n  \"error_text\" => \"Object with ID 123 is not found\"\n}",
          "type": "json"
        },
        {
          "title": "Delete-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"500\",\n  \"error_text\" => \"Deleting error\"\n}",
          "type": "json"
        },
        {
          "title": "Save-Children-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" => \"Child saving error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/get",
    "title": "Получить объекты",
    "version": "0.1.0",
    "name": "Get",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на получение объектов системы и их атрибутов. Условие для поиска составляется в зависимости от значения флага поиска search внутри параметра data.  Если search равен 1: поиск производится по всем полученным параметрам поиска их логическим умножением. Если search равен 0: поиск производится по полученным параметрам их логическим сложением.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<ul> <li>Id системы.</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<ul> <li>рабочий токен полученный при авторизации.</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<ul> <li>Данные для фильтра по объектам.</li> </ul>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Request-example:",
          "content": "data =\n{\n  \"search\": \"0\", - Флаг поиска (обязательный)\n  \"id\": \"12\", - Id объектов\n  \"created_at_before\": \"22.02.2017\" - Дата создания, от\n  \"created_at_after\": \"22.02.2016\" - Дата создания, до\n  \"updated_at_before\": \"22.02.2016\" - Дата изменения, от\n  \"updated_at_after\": \"22.02.2016\" - Дата изменения, до\n  \"like\": \"Text\" - Вхождение\n  \"limit\" : 10 - количество записей в выборке\n  \"offset\" : 0 - количество пропускаемых записей\n}",
          "type": "json"
        },
        {
          "title": "Success-Response-example:",
          "content": " [\n     {\n         \"id\":55, - id объекта\n         \"name\":\"ЗКО (Завод котельного оборудования)\", - название объекта\n         \"description\":\"\", - описание\n         \"latitude\":56.717241, - широта\n         \"longitude\":37.538879, - долгота\n         \"status\":2, - статус\n         \"address_address\":\"г. Талдом\", - адрес\n         \"owner\":3, - форма собственности\n         \"bargain_type\":null, - тип сделки\n         \"bargain_price\":null, - сумма сделки\n         \"square\":2.48, - площадь, га\n         \"square_sqm\":null, - площадь, кв. м\n         \"cadastral_number\":\"50:01:0031105:24\", - кадастровый номер\n         \"distance_to_moscow\":null, - дистанция до Москвы\n         \"square_target\":\"Промышленное производство\", - назначение площади\n         \"vri\":\"123\", - ВРИ\n         \"has_electricity_supply\":1, - электроснабжение\n         \"has_gas_supply\":1, - газоснабжение\n         \"has_water_supply\":1, - водоснабжение\n         \"has_water_removal_supply\":1, - водоотведение\n         \"has_heat_supply\":1, - теплоснабжение\n         \"electricity_supply_capacity\":\"0.65\", - мощность электроснабжения\n         \"gas_supply_capacity\":\"\", - мощность газоснабжения\n         \"water_supply_capacity\":\"\", - мощность водоснабжения\n         \"water_removal_supply_capacity\":\"\", - мощность водоотведения\n         \"heat_supply_capacity\":\"\", - мощность темплоснабжения\n         \"land_category\":\"1\", - категория земель\n         \"has_buildings\":1, - наличие строений\n         \"buildings_count\":null, - количество строений\n         \"has_road_availability\":0, - доступность дорог\n         \"distance_to_nearest_road\":null, - расстояние до ближайшей дороги\n         \"has_railway_availability\":1, - доступность железнодорожного транспорта\n         \"has_charge\":0, - наличие обременения\n         \"charge_type\":\"\", - тип обременения\n         \"is_special_ecological_zone\":null, - специальная экологическая зона\n         \"danger_class\":1, - класс опасности производства\n         \"phone\":\"\", - контактный телефон\n         \"parent_id\":null, - родительский объект\n         \"object_type_id\":4, - тип объекта\n         \"system_id\":1, - идентификатор системы\n         \"okved\":\"\", - ОКВЭД\n         \"owner_name\":\"\", - наименование владельца\n         \"flat\":null, - этажность\n         \"flat_height\":null, - высота этажа\n         \"industry_type\":1, - тип промышленности\n         \"municipality\":\"\", - муниципальное образование\n         \"land_status\":null, - статус земель\n         \"created_at\":\"2017-07-07 13:11:50\", - всемя\n         \"updated_at\":\"2017-08-14 10:24:55\"\n     },\n     ...\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Limit-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"The Search Flag can not be empty\"\n}",
          "type": "json"
        },
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Auth error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/get-object-container",
    "title": "Поиск контейнера по объекту",
    "version": "0.1.0",
    "name": "GetObjectContainer",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на получение контейнера по id объекта. Метод возвращает обязательные параметры объекта, необходимые для его идентификации,</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id системы.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>рабочий токен полученный при авторизации.</p>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>Данные для фильтра по контейнерам.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Requset-example:",
          "content": "data =\n{\n    \"object_id\": 1 - идентификатор объекта (обязательный)\n}",
          "type": "json"
        },
        {
          "title": "Success-Response-example:",
          "content": "{\n    \"id\":74,\n    \"name\":\"Ступино-1\",\n    \"address_address\":\"Ступинский район\",\n    \"object_type_id\":1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Not-Found-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"success\": \"false\",\n  \"error_code\": \"404\",\n  \"error_text\": \"There is no object with that id\"\n}",
          "type": "json"
        },
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Auth error\"\n}",
          "type": "json"
        },
        {
          "title": "No-parent-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"success\": \"false\",\n  \"error_code\": \"404,\n  \"error_text\": \"The object has no parent\"\n}",
          "type": "json"
        },
        {
          "title": "Bad-Request-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Request error: the object_id param is empty\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/get-objects-by-container",
    "title": "Получить объекты по контейнеру",
    "version": "0.1.0",
    "name": "GetObjectsByContainer",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на получение вложенных объектов по id контейнера. Метод возвращает обязательные параметры объектов, необходимые для их идентификации, включая id объектов.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id системы.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>рабочий токен полученный при авторизации.</p>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>Данные для фильтра по контейнерам.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Requset-example:",
          "content": "data =\n{\n  \"parent_id\": 1 - идентификатор контейнера (обязательный)\n}",
          "type": "json"
        },
        {
          "title": "Success-Response-example:",
          "content": "[\n    {\n        \"id\":74,\n        \"name\":\"Ступино-1\",\n        \"address_address\":\"Ступинский район\",\n        \"object_type_id\":1\n    },\n    ...\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "No-Children-Objects-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"success\": \"false\",\n  \"error_code\": \"404\",\n  \"error_text\": \"No child objects available\"\n}",
          "type": "json"
        },
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Auth error\"\n}",
          "type": "json"
        },
        {
          "title": "Bad-Request-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Request error: the parent_id param is empty\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/update",
    "title": "Изменить объект",
    "version": "0.1.0",
    "name": "Update",
    "group": "Object",
    "permission": [
      {
        "name": "авторизованным системам"
      }
    ],
    "description": "<p>Запрос на изменение созданных объектов с указанием параметров, которые необходимо изменить. Значения атрибутов должны быть пустыми, если их необходимо очистить.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Id системы.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>рабочий токен полученный при авторизации.</p>"
          },
          {
            "group": "Parameter",
            "type": "Json",
            "optional": false,
            "field": "data",
            "description": "<p>Данные для фильтра по контейнерам.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Request-example:",
          "content": " data =\n [\n     \"112233\" : { - 112233 - идентификатор объекта для изменения\n          \"name\":\"ЗКО (Завод котельного оборудования)\", - название объекта (обязательный)\n          \"object_type_id\":4, - тип объекта (обязательный)\n          \"description\":\"\", - общие сведения\n          \"address_address\":\"г. Талдом\", - адрес (обязательный)\n          \"latitude\":56.717241, - широта (обязательный)\n          \"longitude\":37.538879, - долгота (обязательный)\n          \"owner\":3, - форма собственности\n          \"municipality\":\"\", - муниципальное образование\n          \"owner_name\":\"\", - наименование владельца\n          \"bargain_type\":null, - форма реализации\n          \"bargain_price\":null, - стоимость, руб\n          \"square\":2.48, - площадь, га\n          \"square_sqm\":null, - площадь, кв. м\n          \"cadastral_number\":\"50:01:0031105:24\", - кадастровый номер\n          \"industry_type\":1, - сектор промышленности\n          \"distance_to_moscow\":null, - расстояние до Москвы, км\n          \"square_target\":\"Промышленное производство\", - назначение площади\n          \"has_electricity_supply\":1, - наличие электроснабжения\n          \"has_gas_supply\":1, - наличие газоснабжения\n          \"has_water_supply\":1, - наличие водоснабжения\n          \"has_water_removal_supply\":1, - наличие водоотведения\n          \"has_heat_supply\":1, - наличие теплоснабжения\n          \"electricity_supply_capacity\":\"0.65\", - мощность электроснабжения, МВт\n          \"gas_supply_capacity\":\"\", - мощность газоснабжения, м3/час\n          \"water_supply_capacity\":\"\", - мощность водоснабжения, м3/день\n          \"water_removal_supply_capacity\":\"\", - мощность водоотведения, м3/день\n          \"heat_supply_capacity\":\"\", - мощность темплоснабжения, ГКал/час\n          \"land_category\":\"1\", - категория земель\n          \"land_status\":null, - статус земель\n          \"vri\":\"123\", - ВРИ\n          \"has_buildings\":1, - наличие строений\n          \"buildings_count\":0, - число строений\n          \"has_road_availability\":0, - наличие доступа к автодороге\n          \"distance_to_nearest_road\":null, - расстояние до автодороги\n          \"has_railway_availability\":1, - возможность присоединения к грузовой ж/д станции\n          \"has_charge\":0, - наличие обременения\n          \"charge_type\":\"\", - вид обременения\n          \"danger_class\":1, - класс опасности производства\n          \"okved\":\"\", - ОКВЭД\n          \"phone\":\"\", - контактный телефон\n          \"parent_id\":null, - родительский объект\n          \"flat\":null, - этажность\n          \"flat_height\":null, - высота этажа\n          \"photos\" : [ - массив фотографий, каждое фото представлено в виде base64-строки\n              \"==Photo_file_in_base64\"\n          ]\n          или\n          \"photos\" : \"delete\" - удалить фото, ассоциированные с данным объектом\n     },\n     ...\n]",
          "type": "json"
        },
        {
          "title": "Success-Response-example:",
          "content": "[\n    1,\n    2,\n    3,\n    ...\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Validating-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" : \"Validating error at object:  123\",\n  \"errors\": [\n      {\n         \"name\": \"can not be blank\"\n      },\n      ...\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Saving-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\" : \"Saving at object: 321\",\n  \"errors\": [\n      {\n         \"name\": \"can not be blank\"\n      },\n      ...\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Auth-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Auth error\"\n}",
          "type": "json"
        },
        {
          "title": "Child-Saving-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"success\": \"false\",\n  \"error_code\": \"400\",\n  \"error_text\": \"Сhild saving error\",\n  \"errors\": [...]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "type": "post",
    "url": "/auth",
    "title": "Авторизация системы",
    "version": "0.1.0",
    "name": "auth",
    "group": "Object",
    "permission": [
      {
        "name": "всем системам"
      }
    ],
    "description": "<p>Запрос на авторизацию и получение системой рабочего токена.  До авторизации невозможны операции с объектами.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<ul> <li>Id системы.</li> </ul>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<ul> <li>Токен авторизации.</li> </ul>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Search-Error-Response:",
          "content": "HTTP/1.1 404 Bad Request\n {\n   \"success\": \"false\",\n   \"error_code\": \"404\",\n   \"error_text\": \"System is not exist\"\n }",
          "type": "json"
        },
        {
          "title": "Save-Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n {\n   \"success\": \"false\",\n   \"error_code\": \"500\",\n   \"error_text\": \"Processing error\"\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": \"true\",\n  \"work_token\": \"1231asasqbdfs32324bfsd\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "source/docs/api.js",
    "groupTitle": "Object"
  },
  {
    "version": "0.1.0",
    "type": "",
    "url": "",
    "filename": "source/docs/api.js",
    "group": "_srv_sites_zephyrlab_investmo_o_nagornov_investmo_zephyrlab_ddemo_ru_source_docs_api_js",
    "groupTitle": "_srv_sites_zephyrlab_investmo_o_nagornov_investmo_zephyrlab_ddemo_ru_source_docs_api_js",
    "name": ""
  }
] });
