<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SuccessStory */

$this->title = Yii::t('app', 'Create Success Story');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Success Stories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="success-story-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
