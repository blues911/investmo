<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 16:48
 */

namespace frontend\widgets;

use common\models\SuccessStory;
use yii\base\Widget;


class StoryWidget extends Widget
{
    public $amount;
    public $investor;
    public $view = 'main';
    public $exclude = false;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $stories = SuccessStory::find()->active()->investorType($this->investor)->exclude($this->exclude)->orderBy('id ASC')->limit($this->amount)->all();
        return $this->render($this->view, ['stories' => $stories]); 
    }
}