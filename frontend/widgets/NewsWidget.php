<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 12:43
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\News;


class NewsWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $news = News::find()->active()->orderBy('updated_at DESC')->all();
        return $this->render('news', ['news' => $news]);
    }
}