<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Menu;

/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 11:10
 */
class MenuWidget extends Widget
{
    public $exclude_pages;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $menuItems = Menu::find()->active()->lang();
        if($this->exclude_pages)
            $menuItems = $menuItems->andWhere(['NOT IN', 'url', $this->exclude_pages]);
        $menuItems = $menuItems->orderBy('sort')->all();
        return $this->render('menu', ['menuItems' => $menuItems]);
    }
}