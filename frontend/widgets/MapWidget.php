<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Cluster;

class MapWidget extends Widget
{
    public function init()
    {
        parent::init();
    }
    
    public function run()
    {
        $clusters = Cluster::find()->all();
        return $this->render('map', ['clusters' => $clusters]);
    }
}