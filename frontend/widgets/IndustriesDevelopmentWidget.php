<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 21:46
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Direction;

class IndustriesDevelopmentWidget extends Widget
{
    public $blocks;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        return $this->render('industries_development', ['blocks' => $this->blocks]);
    }
}