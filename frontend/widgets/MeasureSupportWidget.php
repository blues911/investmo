<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 16:25
 */

namespace frontend\widgets;


use common\models\MeasuresSupport;
use yii\base\Widget;
use common\models\InvestorType;

class MeasureSupportWidget extends Widget
{
    public $investor;
    public $title;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $investor = null;
        $single_investor = null;
        if (isset($this->investor) && !empty($this->investor)) {
            if(in_array("0", $this->investor))
                $arr = array_diff($this->investor, ["0"]);

            $allInvestors = InvestorType::find();
            if(!empty($arr))
            $allInvestors = $allInvestors->where(['IN', 'id', $this->investor]);
            $allInvestors = $allInvestors->all();
            foreach ($allInvestors as $investor) {
                $measures = MeasuresSupport::find()->where(['investor_type_id' => $investor->id])->all();
                $investors[$investor->id]['img'] = $investor->img;
                $investors[$investor->id]['text'] = $investor->text;
                $investors[$investor->id]['page_link'] = $investor->page_link;
                if($measures){
                    foreach ($measures as $key => $measure){
                        $investors[$investor->id]['measures'][$key]['name'] = $measure->name;
                        $investors[$investor->id]['measures'][$key]['page_link'] = $measure->page_link;
                    }

                }
            }

        } else {
            $singleInvestor = InvestorType::find()->where(['investor_type.id' => $this->investor])->all();
            $singleMeasure = MeasuresSupport::find()->where(['investor_type_id' => $singleInvestor->id])->all();
            foreach ($singleInvestor as $investor) {
                $single_investor['text'] = $investor->text;
                $single_investor['page_link'] = $investor->page_link;
                foreach ($singleMeasure as $key => $measure){
                    $single_investor['measures'][$key]['name'] = $singleMeasure->name;
                    $single_investor['measures'][$key]['page_link'] = $singleMeasure->page_link;
                }

            }
        }
        return $this->render('measure_support', [
            'investors' => $investors,
            'investor' => $single_investor,
            'title' => $this->title,
        ]);
    }

}