<?php

namespace frontend\widgets;

use yii\base\Widget;
use common\models\ContentBlock;

class ContentBlockWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function set($slug, $template = null)
    {

        if($template){
           $content = $result = ContentBlock::find()->slug($slug)->lang()->one()->content;
           foreach ($template as $key => $value){
               $replace[] = '/\{\{'.$key.'\}\}/';
               if($value > 1000){
                   $d= explode('.',$value/1000);
                   $t = \Yii::$app->language == 'ru' ? 'тыс.': 'K';
                   $s = isset($d[1]) ? substr($d[1],0,1) : 0;
                   $replacement[] = $d[0].'.'.$s.'<span style="font-size: 1.875rem;">'.$t.'</span>';
               }else{
                   $replacement[] = $value;
               }
           }
           $content = preg_replace($replace,$replacement,$content);
            return$content;
        }else{
            $result = ContentBlock::find()->slug($slug)->lang()->one();

            return $result->content;
        }
    }
}