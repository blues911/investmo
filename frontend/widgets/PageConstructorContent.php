<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 04.12.2017
 * Time: 18:19
 */

namespace frontend\widgets;


use yii\base\Widget;

class PageConstructorContent extends Widget
{
    public $hasBackground;
    public $header;
    public $content;
    public function run()
    {
        if($this->hasBackground){
            return $this->render('page_with_background', ['header' => $this->header, 'content' => $this->content]);
        }else{
            return $this->render('page_without_background', ['header' => $this->header, 'content' => $this->content]);
        }
    }
}