<?php

namespace frontend\widgets;

use common\models\FeedBack;
use yii\base\Widget;
use Yii;

class FeedBackWidget extends Widget
{
    public $model = false;
    public $layout = 'feedback';
    public $selector = false;
    public $hidden = false;
    public $cssClass = '';
    public $ignoreSession = false;
    public $type = 3;
    
    public function init()
    {
        parent::init();
        
        if (!$this->model) {
            $this->model = new FeedBack();
        }
    }

    public function run()
    {
        return $this->render($this->layout, [
            'model' => $this->model,
            'selector' => $this->selector,
            'type' => $this->type,
            'hidden' => $this->hidden,
            'class' => $this->cssClass,
        ]);
    }
}