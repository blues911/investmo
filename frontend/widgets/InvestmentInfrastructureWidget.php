<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 15:40
 */

namespace frontend\widgets;

use yii\base\Widget;
use common\models\Object;

class InvestmentInfrastructureWidget extends Widget
{
    public $blocks;
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('investment_infrastructure', ['blocks' => $this->blocks,

        ]);
    }

}