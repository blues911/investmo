<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 14:42
 */

namespace frontend\widgets;

use common\models\ForeignProjects;
use yii\base\Widget;

class ForeignProjectsWidget extends Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $projects = ForeignProjects::find()->all();
        return $this->render('foreign_projects', ['projects' => $projects]);
    }

}