<div class="content content_theme_projects p-investor__projects hash-block" id="map">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><strong>Карта</strong> объектов</h2>

            <div class="projects">
                <div class="projects__location">
                    <div class="projects__left">
                        <h4 class="projects__title projects__title_mobile">Хотите <b>быстро подобрать интересующий вас объект ?</b><br />
                            Вам поможет расширенный фильтр</h4>

                        <h4 class="projects__title">Расширенный фильтр поможет вам<br />
                            <b>быстро подобрать нужный проект</b></h4>

                        <div class="projects__advantages">
                            <div class="projects__adv-items projects__adv-items_mobile">
                                <?php if($blocks){
                                    foreach ($blocks as $block){ ?>
                                        <div class="projects__adv-item">
                                            <div class="projects__number"><?= $block['count']?></div>

                                            <div class="projects__adv-text"><?= $block['name']?></div>
                                        </div>
                                   <?php }
                                } ?>
                            </div>

                            <div class="projects__adv-items">
                                <?php if($blocks){
                                    foreach ($blocks as $block){ ?>
                                        <div class="projects__adv-item">
                                            <div class="projects__number"><?= $block['count']?></div>

                                            <div class="projects__adv-text"><?= $block['name']?></div>
                                        </div>
                                    <?php }
                                } ?>
                            </div>
                            <a class="projects__adv-btn projects__adv-btn_mobile" href="/map">Посмотреть все объекты на карте</a>

                            <div class="projects__buttons"><a class="projects__adv-btn" href="/page/investinfrastr">Подробнее об инвестиционной инфраструктуре</a></div>
                        </div>
                    </div>

                    <div class="projects__right">
                        <div class="projects__map-wrap">
                            <div class="projects__map-marker projects__map-marker_size_small">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_big">
                                <a href="map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_small">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_big">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_small">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_small">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_big">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <div class="projects__map-marker projects__map-marker_size_small">
                                <a href="/map" class="projects__map-count"></a>
                            </div>
                            <svg class="icon icon_map projects__map" height="589px" width="623px"> <use xlink:href="/svg-symbols.svg#map" xmlns:xlink="http://www.w3.org/1999/xlink"></use> </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>