<?php

use common\models\FeedBack;
/**
 * @var number $type
 */
?>
<div class="form__ico form__ico_1"></div>
<div class="form__ico form__ico_2"></div>
<div class="form__title"><?=Yii::t('app', 'Есть вопросы? <br> <b>Пишите!</b>')?></div>
<div class="form__text"><?=Yii::t('app', 'Вы хотели бы')?>
    <select class="form__select" id="feedback_select" name="type">
        <option class="form__option" value="<?=FeedBack::TYPE_REQUEST?>" <?= $type == FeedBack::TYPE_REQUEST ?'selected="selected"' : ''?>><?=Yii::t('app', 'реализовать инвестиционный проект')?></option>
        <option class="form__option" value="<?=FeedBack::TYPE_QUESTION?>" <?= $type == FeedBack::TYPE_QUESTION ?'selected="selected"' : ''?>><?=Yii::t('app', 'задать вопрос')?></option>
        <option class="form__option" value="<?=FeedBack::TYPE_ARGUE?>" <?= $type == FeedBack::TYPE_ARGUE ?'selected="selected"' : ''?>><?=Yii::t('app', 'подать жалобу')?></option>
      
    </select>
</div>