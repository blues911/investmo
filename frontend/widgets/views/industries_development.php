<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 11:30
 */
use \common\models\Direction;
?>

<div class="content content_theme_industry">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><strong>Развитие</strong> отраслей
            </h2>
            <div class="industry">
                <div class="industry__controls">
                    <?php if($blocks){
                        foreach ($blocks as $block){
                            $direction[$block['direction']] = Direction::findOne($block['direction']) ?>
                            <div class="industry__control">
                                <svg class="icon icon_engineering industry__icon" height="47px" width="96px"> <use xlink:href="/<?=$direction[$block['direction']]->slider_img?>" xmlns:xlink="http://www.w3.org/1999/xlink"></use> </svg>
                                <p class="industry__caption"><?= $direction[$block['direction']]->name?>
                                </p>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="industry__items">
                    <?php if($blocks){
                        foreach ($blocks as $block){ ?>
                            <div class="industry__item">
                                <div class="industry__inner">
                                    <div class="industry__content">
                                        <h3 class="industry__title"><?= $direction[$block['direction']]->name ?>
                                        </h3>
                                        <p class="industry__address"><?= $direction[$block['direction']]->address ?>
                                        </p>
                                        <div class="industry__desc">
                                            <p class="industry__text"><?= $direction[$block['direction']]->description ?>
                                            </p>
                                            <p class="industry__text"><b>Планируется привлечение инвестиций в следующих
                                                    направлениях:</b>
                                            </p>
                                            <ul class="industry__list">

                                            <?php $points = explode(';', $block['invest']);
                                            if($points){
                                                foreach ($points as $point) {
                                                    if ($point != '') { ?>
                                                        <li class="industry__list-item"><?= trim($point) ?>
                                                        </li>
                                                    <?php }
                                                }
                                                } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="industry__thumb"><img class="industry__img"
                                                                      src="<?= $direction[$block['direction']]->img ?>" alt=""
                                                                      role="presentation"/>
                                    </div>
                                </div>
                            </div>
                       <?php }
                    } ?>

                </div>
            </div>
        </div>
    </div>
</div>

