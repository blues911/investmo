<?php
use common\models\FeedBack;
use frontend\widgets\FeedBackWidget;
/**
 * @var FeedBackWidget $model
 * @var ActiveForm $form
 * @var FeedBack $model
 */
?>
<div class="form__ico form__ico_1">
</div>
<div class="form__title"><?=Yii::t('app', 'Запрос информации по объекту')?>
</div>
<div class="form__subtitle"><?=Yii::t('app', 'Укажите информацию для обратной связи')?>
</div>
<label class="form__label"><?=Yii::t('app', 'Фамилия')?>
    <input type="text" name="lastname" class="form__input valid" placeholder="<?=Yii::t('app', 'Фамилия')?>"/>
</label>
<label class="form__label"><?=Yii::t('app', 'Имя')?>
    <input type="text" name="name" class="form__input valid" placeholder="<?=Yii::t('app', 'Имя')?>"/>
</label>
<label class="form__label"><?=Yii::t('app', 'Отчество')?>
    <input type="text" name="middlename" class="form__input valid" placeholder="<?=Yii::t('app', 'Отчество')?>"/>
</label>
<div class="form__small-text">
    <?=Yii::t('app', 'Формат электронной почты')?>
</div>
<label class="form__label"><?=Yii::t('app', 'Ваш е-mail адрес')?>
    <input type="text" name="email" class="form__input valid" placeholder="user@example.com"/>
</label>
<label class="form__label"><?=Yii::t('app', 'и ваш телефон')?>
    <input type="text" name="phone" class="form__input form__input_phone valid" maxlength="18" placeholder="<?=Yii::t('app', 'и ваш телефон')?>"/>
</label>

<span class="form__label"><?=Yii::t('app', 'Предполагаемый объем инвестиций')?></span>
<RADIOGROUP class="radio-group">
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 5 млн. руб" id="pickup1" checked="checked"/>
        <label class="radio__label" for="pickup1">
            <span class="radio__text"><?=Yii::t('app', 'от 5 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 50 млн. руб" id="pickup2" />
        <label class="radio__label" for="pickup2">
            <span class="radio__text"><?=Yii::t('app', 'от 50 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 200 млн. руб" id="pickup3"/>
        <label class="radio__label" for="pickup3">
            <span class="radio__text"><?=Yii::t('app', 'от 200 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 1 млрд. руб" id="pickup4"/>
        <label class="radio__label" for="pickup4">
            <span class="radio__text"><?=Yii::t('app', 'от 1 млрд. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
</RADIOGROUP>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="object"><?=Yii::t('app', 'Интересующий объект')?>
    </label><input class="form__input form__input_object" type="text" id="object" value="" name="data[object]"/>
</div>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="message"><?=Yii::t('app', 'Комментарий')?>
    </label><textarea class="form__textarea form__textarea_message" id="message" name="data[comment]"></textarea>
</div>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="description"><?=Yii::t('app', 'суть проекта')?>
    </label><textarea class="form__textarea form__textarea_message" id="description" name="data[description]"></textarea>
</div>
<div class="form__docs-block">
    <div class="form__progress-bar">
        <div class="form__docs-progress"></div>
    </div>
    <div class="form__docs-message"></div>
    <div class="form__load-docs"></div>
    <div class="form__docs__wrap-input">
        <input class="form__docs" id="docs-form-0" name="files[]" multiple type="file"/>
        <label class="form__control-docs" for="docs-form-0"><?=Yii::t('app', 'Прикрепить файл')?></label>
    </div>
</div>

<div class="form__small-text">
    <?=Yii::t('app', 'Допустимые форматы')?><br/>
    <?=Yii::t('app', 'Максимальное кол-во файлов')?>
</div>
<div id="myCaptcha"></div>