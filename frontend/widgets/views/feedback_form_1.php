<?php
use common\models\FeedBack;
use frontend\widgets\FeedBackWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/**
 * @var FeedBackWidget $model
 * @var ActiveForm $form
 * @var FeedBack $model
 */
?>
<?php
/**
 * @var FeedBackWidget $feedback
 */
if((isset($selector) && $selector)):?>
    <?php
    /**
     * @var number $type
     */
    echo $this->render('feedback_selector', [
        'type' => $type
    ]);
    ?>
<?php endif;?>
<div class="form__subtitle"><?=Yii::t('app', 'Укажите информацию для обратной связи')?>
</div>
<label class="form__label"><?=Yii::t('app', 'Фамилия')?>
    <input type="text" name="lastname" class="form__input valid" placeholder="<?=Yii::t('app', 'Фамилия')?>"/>
</label>
<label class="form__label"><?=Yii::t('app', 'Имя')?>
    <input type="text" name="name" class="form__input valid" placeholder="<?=Yii::t('app', 'Имя')?>"/>
</label>
<label class="form__label"><?=Yii::t('app', 'Отчество')?>
    <input type="text" name="middlename" class="form__input valid" placeholder="<?=Yii::t('app', 'Отчество')?>"/>
</label>
<div class="form__small-text">
        <?=Yii::t('app', 'Формат электронной почты')?>
</div>
<label class="form__label"><?=Yii::t('app', 'Ваш е-mail адрес')?>
    <input type="text" name="email" class="form__input valid" placeholder="user@example.com"/>
</label>
<label class="form__label"><?=Yii::t('app', 'и ваш телефон')?>
    <input type="text" name="phone" class="form__input form__input_phone valid" maxlength="18" placeholder="<?=Yii::t('app', 'и ваш телефон')?>"/>
</label>

<span class="form__label form__label_visible-mobile"><?=Yii::t('app', 'Предполагаемый объем инвестиций')?></span>
<RADIOGROUP class="radio-group">
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 5 млн. руб" id="pickup1" checked="checked"/>
        <label class="radio__label" for="pickup1">
            <span class="radio__text"><?=Yii::t('app', 'от 5 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 50 млн. руб" id="pickup2" />
        <label class="radio__label" for="pickup2">
            <span class="radio__text"><?=Yii::t('app', 'от 50 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 200 млн. руб" id="pickup3"/>
        <label class="radio__label" for="pickup3">
            <span class="radio__text"><?=Yii::t('app', 'от 200 млн. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
    <div class="radio radio_theme_light radio_text_light">
        <input class="radio__input" type="radio" name="data[pickup]" value="от 1 млрд. руб" id="pickup4"/>
        <label class="radio__label" for="pickup4">
            <span class="radio__text"><?=Yii::t('app', 'от 1 млрд. руб')?>
                <div class="radio__cnt">
                </div>
            </span>
        </label>
    </div>
</RADIOGROUP>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="object"><?=Yii::t('app', 'Интересующий объект')?>
    </label><input class="form__input form__input_object" type="text" id="object" value="" name="data[object]"/>
</div>
<div class="form__chooseDocs"><span class="form__label form__label_visible-mobile"><?=Yii::t('app', 'Выберите документы для получения')?></span>
    <select class="form__input-select" name="data[requested_documents][]" multiple="multiple" data-placeholder="<?=Yii::t('app', 'Введите название документа или выберите из списка')?>">
        <option value="Заявка на реализацию проекта"><?=Yii::t('app', 'Заявка на реализацию проекта')?></option>
        <option value="Заявка на размещение объекта"><?=Yii::t('app', 'Заявка на размещение объекта')?></option>
        <option value="Анкета для получения поддержки"><?=Yii::t('app', 'Анкета для получения поддержки')?></option>
    </select>
</div>
<div class="form__chooseDocs"><span class="form__label form__label_visible-mobile"><?=Yii::t('app', 'Укажите тип поддержки')?></span>
    <select class="form__input-select" name="data[support_type][]" multiple="multiple" data-placeholder="<?=Yii::t('app', 'Например, налоговая льгота')?>">
        <option value="Налоговая льгота"><?=Yii::t('app', 'Налоговая льгота')?></option>
        <option value="Имущественная поддержка"><?=Yii::t('app', 'Имущественная поддержка')?></option>
        <option value="Финансовая поддержка"><?=Yii::t('app', 'Финансовая поддержка')?></option>
    </select>
</div>
<div class="form__chooseDocs"><span class="form__label form__label_visible-mobile"><?=Yii::t('app', 'Укажите отрасль')?></span>
    <select class="form__input-select" name="data[branch][]" multiple="multiple" data-placeholder="<?=Yii::t('app', 'Например, пищевая промышленность')?>">
        <option value="Аэрокосмическая промышленность"><?=Yii::t('app', 'Аэрокосмическая промышленность')?></option>
        <option value="Производство медицинского и научного оборудования"><?=Yii::t('app', 'Производство медицинского и научного оборудования')?></option>
        <option value="Производство промышленного оборудования"><?=Yii::t('app', 'Производство промышленного оборудования')?></option>
        <option value="Производство контрольных и измерительных приборов"><?=Yii::t('app', 'Производство контрольных и измерительных приборов')?></option>
        <option value="Автомобилестроение"><?=Yii::t('app', 'Автомобилестроение')?></option>
        <option value="Химия"><?=Yii::t('app', 'Химия')?></option>
        <option value="Производство пластиков"><?=Yii::t('app', 'Производство пластиков')?></option>
        <option value="Фармацевтика и биофармацевтика"><?=Yii::t('app', 'Фармацевтика и биофармацевтика')?></option>
        <option value="Производство строительных и отделочных материалов"><?=Yii::t('app', 'Производство строительных и отделочных материалов')?></option>
        <option value="Строительные и инжиниринговые работы"><?=Yii::t('app', 'Строительные и инжиниринговые работы')?></option>
        <option value="Пищевая промышленность"><?=Yii::t('app', 'Пищевая промышленность')?></option>
        <option value="Производство напитков"><?=Yii::t('app', 'Производство напитков')?></option>
        <option value="Производство мебели"><?=Yii::t('app', 'Производство мебели')?></option>
        <option value="гостиничный бизнес и туризм"><?=Yii::t('app', 'гостиничный бизнес и туризм')?></option>
    </select>
</div>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="message"><?=Yii::t('app', 'Комментарий')?>
    </label><textarea class="form__textarea form__textarea_message" id="message" name="data[comment]"></textarea>
</div>
<div class="form__docs-block">
    <div class="form__progress-bar">
        <div class="form__docs-progress"></div>
    </div>
    <div class="form__docs-message"></div>
    <div class="form__load-docs"></div>
    <div class="form__docs__wrap-input">
        <input class="form__docs" id="docs-form-1" name="files[]" multiple type="file"/>
        <label class="form__control-docs" for="docs-form-1"><?=Yii::t('app', 'Прикрепить файл')?></label>
    </div>
</div>

<div class="form__small-text">
    <?=Yii::t('app', 'Допустимые форматы')?><br/>
    <?=Yii::t('app', 'Максимальное кол-во файлов')?>
</div>
<div id="myCaptcha"></div>