<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 12:44
 */

use yii\helpers\Url;
 
$formatter = \Yii::$app->formatter;
?>

<h2 class="content__title"><?=Yii::t('app', '<strong>Новости</strong> инвестиций')?>
</h2>


<div class="news">
    <div class="news__list">
        <?php  if (isset($news[0]) && !empty($news[0])): ?>
            <div class="news__col news__col_theme_big">
                <div class="news-item">
                    <div class="news-item__inner">
                        <a class="news-item__thumb" href="<?=Url::toRoute(['/news/'.$news[0]->id])?>">
                            <img class="news-item__img" src="<?= $news[0]->getFullImagePath() ?>" alt="" role="presentation"/>
                        </a>
                        <div class="news-item__content">
                            <a class="news-item__link" href="<?=Url::toRoute(['/news/'.$news[0]->id])?>">
                                <span class="news-item__date"><?= $formatter->asDate($news[0]->updated_at, 'dd  MMMM yyyy') ?></span>
                                <h4 class="news-item__title"><?= $news[0]->title ?></h4>
                            </a>
                            <a class="news-item__link" href="<?=Url::toRoute(['/news/'.$news[0]->id])?>">
                                <p class="news-item__desc"><?= $news[0]->short_desc ?></p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="news__col">
            <?php for ($i = 1; $i < 3; $i++): ?>
                <?php if (isset($news[$i]) && !empty($news[$i])): ?>
                    <div class="news-item">
                        <div class="news-item__inner">
                            <div class="news-item__content">
                                <a class="news-item__link" href="<?=Url::toRoute(['/news/'.$news[$i]->id])?>">
                                    <span class="news-item__date"><?= $formatter->asDate($news[$i]->updated_at, 'dd  MMMM yyyy') ?></span>
                                    <h4 class="news-item__title"><?= $news[$i]->title ?></h4>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endfor; ?>
        </div>
    </div>
    <a class="btn btn_theme_default news__btn" href="<?=Url::toRoute(['/news']);?>"><span class="btn__text"><?=Yii::t('app', 'Посмотреть все')?></span></a>
</div>
