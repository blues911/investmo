<div class="p-content" style="">
    <div class="title-subpage title-subpage_js_inited">
        <div class="title-subpage__top">
            <div class="title-subpage__title">
                <div class="container">
                    <h1><span class="title-subpage__brand"><?=$header?></span></h1>
                </div>
            </div>

            <div class="title-subpage__company-contacts">
                <div class="container">
                    <div class="title-subpage__contacts"><a class="title-subpage__back-link" href="javascript:void(0)">Назад</a></div>
                </div>
            </div>
        </div>
    </div>
<?=$content ?>
</div>