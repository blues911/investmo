<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 15:43
 */
use \common\models\ObjectType;
?>
<div class="top-box">
    <div class="container">
        <div class="info-box__components">
            <div class="info-box__components-top">
                <h3 class="info-box__components-title"><b>Составляющие</b> инвестиционной инфраструктуры
                </h3>
                <ul class="info-box__components-list">
                    <?php if($blocks){
                        foreach ($blocks as $block){ ?>
                            <li class="info-box__components-item"><b><?= $block['count'] ?></b> <?= ObjectType::getTypeName($block['object_type_id']) ?>
                            </li>
                      <?php  }
                    } ?>
                </ul>
            </div>
            <a class="btn btn_theme_light info-box__components-btn" href="<?=$block['btn_link']?>"><?=$block['btn_text']?></a>
        </div>
    </div>
</div>