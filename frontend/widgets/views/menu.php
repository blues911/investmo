<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.05.2017
 * Time: 11:15
 */
foreach ($menuItems as $menuItem): ?>
    <?php $slug = explode('/',$menuItem->url);?>
    <li class="main-nav__item"><a class="main-nav__link<?php if(isset(Yii::$app->request->resolve()[1]['slug']) && Yii::$app->request->resolve()[1]['slug'] == $slug[2]):?> active<?php endif;?>" href="<?=$menuItem->url?>"><?= $menuItem->title ?></a></li>
<?php endforeach; ?>