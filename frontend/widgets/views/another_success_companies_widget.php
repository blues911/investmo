<?php

/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.06.2017
 * Time: 12:04
 */ ?>
<div class="slide-box p-stories__slide-box">
    <div class="slide-box__container">
        <div class="slide-box__title">Другие <b>успешные компании</b>
        </div>
        <div class="slide-box__items">
            <?php foreach ($stories as $story): ?>
            <a class="slide-box__item" href="javascript:void(0)" style="background-image:url(<?= $story->getMainImg()?>)">
                <span class="slide-box__shadow"></span>
                <span class="slide-box__text"><?=$story->title?></span>
            </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
