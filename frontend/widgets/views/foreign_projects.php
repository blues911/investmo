<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 14:43
 */
use yii\helpers\Url; ?>
<div class="container">
    <div class="collaboration__projects">
        <h3 class="collaboration__title"><b>Проекты</b> с участием иностранного капитала
        </h3>

        <div class="collaboration__projects-slider">
            <?php foreach ($projects as $project): ?>
                <div class="collaboration__item"
                     style="background-image: url(<?= Url::to($project->img, true); ?>)">
                    <div class="collaboration__item-inner">
                        <div class="collaboration__location">
                            <div class="collaboration__country"><img class="collaboration__flag"
                                                                     src="<?= Url::to($project->country_img, true); ?>"
                                                                     al="" alt="" role="presentation"/>
                                <p class="collaboration__country-name"><?= $project->country ?>
                                </p>
                            </div>
                        </div>
                        <p class="collaboration__place"><?= $project->company ?>
                        </p>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>