<?php
use common\models\FeedBack;
use frontend\widgets\FeedBackWidget;
use yii\widgets\ActiveForm;
/**
 * @var FeedBackWidget $model
 * @var ActiveForm $form
 * @var FeedBack $model
 */
?>
<?php
/**
 * @var FeedBackWidget $feedback
 */
if((isset($selector) && $selector)):?>
    <?php
    /**
     * @var number $type
     */
    echo $this->render('feedback_selector', [
        'type' => $type
    ]);
    ?>
<?php endif;?>
<label class="form__label">Ваше имя
    <input type="text" name="name" class="form__input valid" aria-required="true" placeholder="Ваше имя"/>
</label>
<label class="form__label">Ваш е-mail адрес
    <input type="text" name="email" class="form__input valid" aria-required="true" placeholder="Ваш е-mail адрес"/>
</label>
<label class="form__label">и ваш телефон
    <input type="text" name="phone" class="form__input form__input_phone valid" maxlength="18" aria-required="true" placeholder="и ваш телефон"/>
</label>
<div class="form__group">
    <label class="form__label form__label_visible-mobile" for="message">Опишите ситуацию
    </label><textarea class="form__textarea form__textarea_message" id="message" name="data[reason]"></textarea>
</div>
<div class="form__docs-block">
    <div class="form__progress-bar">
        <div class="form__docs-progress"></div>
    </div>
    <div class="form__docs-message"></div>
    <div class="form__load-docs"></div>
    <div class="form__docs__wrap-input">
        <input class="form__docs" id="docs-form-4" name="files[]" multiple type="file"/>
        <label class="form__control-docs" for="docs-form-4"><?=Yii::t('app', 'Прикрепить файл')?></label>
    </div>
</div>

<div class="form__small-text">
    <?=Yii::t('app', 'Допустимые форматы')?><br/>
    <?=Yii::t('app', 'Максимальное кол-во файлов')?>
</div>
<div id="myCaptcha"></div>