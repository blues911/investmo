<?
use yii\helpers\Url;

?>

<div class="stories">

    <?php $k=1; $total = count($stories); foreach ($stories as $story): ?>
        <div class="stories__item<?php if($k==1):?> stories__item_state_active<?php endif;?>">
            <div class="stories__row">
                <div class="stories__thumb">
                    <a class="stories__link-img" href="<?=Url::toRoute(['/story/'.$story->id])?>">
                        <img class="stories__img"
                             src="<?= $story->getMainImg()?>" alt=""
                             role="presentation"/>
                    </a>
                    <div class="stories__counter">
                        <div class="stories__counter-item"><?=$k ?>
                        </div>
                        <div class="stories__counter-item"><?=$total ?>
                        </div>
                    </div>
                </div>
                <div class="stories__content">
                    <a class="stories__link-desc" href="<?=Url::toRoute(['/story/'.$story->id])?>">
                        <h3 class="stories__title"><?= $story->title ?>
                        </h3>
                        <p class="stories__desc">
                            <?= $story->short_desc ?>
                        </p>
                        <p class="stories__info"><strong><?=Yii::t('app', 'Дата запуска')?></strong> <?= \Yii::$app->formatter->asDate($story->date_of_start, 'short') ?>
                        </p>
                        <p class="stories__info"><strong><?=Yii::t('app', 'Страна происхождения')?>:</strong> <?= $story->country_of_origin ?></p>
                    </a>
                    <a class="stories__btn" href="<?=Url::toRoute(['/story/'.$story->id])?>"><?=Yii::t('app', 'Подробнее')?></a>
                </div>
            </div>
        </div>
        <?php $k++; endforeach; ?>

    <ul class="stories__controls">

        <?php $i=1; foreach ($stories as $story): ?>
            <li class="stories__control<?php if($i==1):?> stories__control_active<?php endif;?>"><div class="stories__c-text"><?=$story->title?>
                </div><img class="stories__logo"
                           src="<?= $story->getLogoImg()?>"
                           alt="" role="presentation"/>
            </li>
            <?php $i++; endforeach; ?>

        <li class="stories__control stories__control_theme_link stories__control_btn">
            <a class="stories__link stories__link_btn" href="/stories/">
                <span class="stories__link-arrow">
                    <svg class="icon icon_arrow stories__icon" width="9px" height="14px">
                        <use xlink:href="/svg-symbols.svg#arrow"></use>
                    </svg>
                </span>
                <!--span class="stories__text stories__text_hidden" data-open="Меньше проектов"><?=Yii::t('app', 'Больше проектов')?></span-->
                <span class="stories__text stories__text_visible" data-open="СКРЫТЬ"><?=Yii::t('app', 'Посмотреть другие кейсы')?></span>
            </a>
        </li>

    </ul>

</div>
