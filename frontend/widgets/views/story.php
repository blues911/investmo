<?

use yii\helpers\Url;

?>
<div class="slide-box p-stories__slide-box">
    <div class="slide-box__container">
        <div class="slide-box__title">
            Другие <b>успешные компании</b>
        </div>
        <div class="slide-box__items">
            <? foreach ($stories as $story) : ?>
                <a class="slide-box__item" href="<?=Url::toRoute(['/story/'.$story->id])?>" style="background-image:url(<?= $story->getMainImg()?>)">
                    <span class="slide-box__shadow"></span><span class="slide-box__text"><?= $story->title ?></span>
                </a>
            <? endforeach; ?>
        </div>
    </div>
</div>