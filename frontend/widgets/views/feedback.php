<?php
use frontend\widgets\FeedBackWidget;
use yii\helpers\Html;
use yii\helpers\Url;
if (!Yii::$app->request->isAjax) {
    $selectorVal = $selector ? 1 : 0;
$script = <<<JS
    $(document).on('change', '#feedback_select', function () {
        var type = $(this).val();
        var form = $(this).closest('form');
        
        $.ajax({
            url: '/feed-back/form',
            method: 'post',
            data: {
                type : type,
                selector : $selectorVal
            },
            success: function (html) {
                
                form.replaceWith($('<div>' + html + '</div>').find('form'));
            }
        });
    });
JS;

$this->registerJS($script);
}

?>

<form id="feedback-form" action="<?=Url::toRoute(['/feed-back/create'])?>" method="POST" enctype="multipart/form-data" class="form form_js_inited <?=$class?>" <?=$hidden ? 'style="display:none;"' : ''?>>
    <input type="hidden" name="type" value="<?=$type?>">
    <input type="hidden" name="selector" value="<?=$selector ? 1 : 0 ?>">
    <?= $this->render('feedback_form_' . $type, [
        'model' => $model,
        'selector' => $selector,
        'type' => $type,
    ]); ?>
    
    <?
        if (!$selector) {
            echo Html::hiddenInput('type', $selectorVal);
        }
    ?>
<?
    if ($type != 2) {
        echo Html::submitButton(Yii::t('app', 'Запросить консультацию'), ['class' => 'btn btn-default']);
    } else {
        echo Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-default']);
    }
?>
</form>