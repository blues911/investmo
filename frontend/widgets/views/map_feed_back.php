<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 11:55
 */
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$js = <<<JS
   $(document).on('submit','.form_ajax',function () {
        var data = $(this).serialize();
        var url = $(this).attr('action');
        var type = $(this).attr('method');
        var message = $(this).find('.popup-base__message');
        var haveToAlert = $(this).attr('data-alert');
        $.ajax({
            url: url,
            data: data,
            type: type,
            success: function (res) {
               if(haveToAlert == 'true') {
                   if(message)
                        message.html(res);
                   else 
                    alert(res);
               }
            }
        });
         return false;
    });
JS;
$this->registerJs($js);
?>
<div class="popup-base popup-base_authorization <?=$login == 'true' ? 'popup-base_visible' : null ;?>">
    <div class="popup-base__header">
        <p class="popup-base__title">Авторизация</p><a class="popup-base__close" href="javascript:void(0)"></a>
    </div>
    <div class="popup-base__body">
        <?php $form = ActiveForm::begin(['id' => 'form-signup', 'action' => '/site/login', 'options' => ['class' => 'popup-base__form form_ajax', 'data-alert' => 'true']]); ?>
        <div class="popup-base__box">
            <div class="input-base">
                <input name="LoginForm[email]" class="input-base__input" type="text" placeholder="Email" required/>
            </div>
            <div class="input-base">
                <input name="LoginForm[password]" class="input-base__input" type="password" placeholder="Пароль" required/>
            </div>
            <div class="popup-base__message"></div>
            <div class="button-base">
                <button name="login-button" class="button-base__button" type="submit">Войти</button>
            </div>
        </div>
        <div class="popup-base__box popup-base__box_flex"><span class="popup-base__label">Войти через портал</span><a class="popup-base__link" href="/saml/login"><img class="popup-base__ico" src="static/img/content/gosuslugi/gosuslugi.png"/></a></div>
        <div class="popup-base__box popup-base__box_links"><a data-popup="registration" class="btn btn_open-popup popup-base__register" href="javascript:void(0)">Регистрация</a><a class="popup-base__reestablish" href="/site/request-password-reset">Забыли пароль</a></div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="popup-base popup-base_registration">
    <div class="popup-base__header">
        <p class="popup-base__title">Регистрация</p><a class="popup-base__close" href="javascript:void(0)"></a>
    </div>
    <div class="popup-base__body">
        <?php $form = ActiveForm::begin(['id' => 'form-signup1', 'action' => '/site/registration', 'options'=>['class' => 'popup-base__form form_ajax', 'data-alert' => 'true']]); ?>
        <div class="popup-base__box">
            <div class="input-base">
                <input name="User[email]" class="popup-base__input input-base__input" type="text" placeholder="Email" required/>
            </div>
            <div class="input-base">
                <input name="User[fio]" class="popup-base__input input-base__input" type="text" placeholder="ФИО"/>
            </div>
            <div class="input-base">
                <input name="User[password]" class="popup-base__input input-base__input" type="password" placeholder="Пароль" required/>
            </div>
            <div class="input-base">
                <input name="User[password_repeat]" class="popup-base__input input-base__input" type="password" placeholder="Повторите пароль" required/>
            </div>
            <div class="popup-base__message"></div>
            <div class="button-base">
                <button class="button-base__button" type="submit">Зарегистрироваться</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="popup-base popup-base_subscribe popup-base_feedback">
    <div class="popup-base__header">
        <p class="popup-base__title">Подписка</p><a class="popup-base__close" href="javascript:void(0)"></a>
    </div>
    <div class="popup-base__body">
        <form id="feedback-form" class="popup-base__form form_ajax" action="<?=Url::toRoute(['/map-feed-back/save-feed-back'])?>" method="post" data-alert="true">
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_only-text">Укажите адрес почты, на которую хотите получать информацию о появлении новых объектов.</label>
                <div class="input-base">
                    <input class="popup-base__input input-base__input" name="MapFeedBackForm[email]" type="text" placeholder="Email" required/>
                </div>
            </div>
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_only-text">Укажите параметры интересующих объектов</label>
            </div>
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_openable">Тип объекта</label>
                <div class="popup-base__inner">
                    <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[type][]" value="1" id="industrial-park"/>
                        <label class="checkbox__label" for="industrial-park"><span class="checkbox__text" title="Индустриальные парки">Индустриальные парки</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[type][]" value="2" id="techno-park"/>
                        <label class="checkbox__label" for="techno-park"><span class="checkbox__text" title="Технопарки">Технопарки</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[type][]" value="3" id="oez"/>
                        <label class="checkbox__label" for="oez"><span class="checkbox__text" title="ОЭЗ">ОЭЗ</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[type][]" value="4" id="braundfields"/>
                        <label class="checkbox__label" for="braundfields"><span class="checkbox__text" title="Браунфилды">Браунфилды</span>
                        </label>
                    </div>
                    <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[type][]" value="9" id="areas"/>
                        <label class="checkbox__label" for="areas"><span class="checkbox__text" title="Земельные участки">Земельные участки</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_openable">Площадь, га</label>
                <div class="popup-base__inner">
                    <div class="popup-base__between">
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[min_square]" type="text" placeholder="от"/>
                        </div>
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[max_square]" type="text" placeholder="до"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-base__box">
                <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input"  type="checkbox" name="MapFeedBackForm[bargain_type][]" value="2" id="lease"/>
                    <label class="checkbox__label" for="lease"><span class="checkbox__text" title="Аренда">Аренда</span>
                    </label>
                </div>
            </div>
            <div class="popup-base__box">
                <div class="checkbox checkbox_theme_standart popup-base__checkbox"><input class="checkbox__input" type="checkbox" name="MapFeedBackForm[bargain_type][]" id="sale"/>
                    <label class="checkbox__label" for="sale"><span class="checkbox__text" title="Продажа">Продажа</span>
                    </label>
                </div>
            </div>
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_openable">Стоимость, руб</label>
                <div class="popup-base__inner">
                    <div class="popup-base__between">
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[min_bargain_price]" type="text" placeholder="от"/>
                        </div>
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[max_bargain_price]" type="text" placeholder="до"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-base__box">
                <label class="popup-base__label popup-base__label_openable">Удалённость от Москвы</label>
                <div class="popup-base__inner">
                    <div class="popup-base__between">
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[min_distance_to_moscow]" type="text" placeholder="от"/>
                        </div>
                        <div class="input-base">
                            <input class="popup-base__input input-base__input" name="MapFeedBackForm[max_distance_to_moscow]" type="text" placeholder="до"/>
                        </div>
                    </div>
                </div>
                <div class="popup-base__message"></div>
            </div>
            <div class="popup-base__box">
                <div class="button-base">
                    <button class="button-base__button" type="submit">Подписаться</button>
                </div>
            </div>
        </form>
    </div>
</div>
