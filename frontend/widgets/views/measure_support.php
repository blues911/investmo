<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 16:38
 */ ?>
<?php if (isset($investor)): ?>
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?=$title?></h2>

            <div class="measures">
                <div class="measures__content">
                    <div class="measures__content-item measures__content-item_state_active">
                        <ul class="measures-list">
                            <?php foreach ($investor['measures'] as $measure): ?>
                                <li class="measures-list__item"><a class="measures-list__link"
                                                                   href="<?=$measure['page_link']?>"><span
                                                class="measures-list__text"><?= $measure['name'] ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <a class="btn btn_theme_standart measures__btn" href="<?=$investor['page_link']?>"><span
                                    class="btn__text">Посмотреть все</span></a></div>
                </div>
            </div>
        </div>
    </div>
<?php else: ?>
    <?php if (isset($investors)): ?>
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?=$title?></h2>
            <div class="measures">
                <header class="measures__header">
                    <ul class="measures__controls">
                        <?php $k = 0;
                        foreach ($investors as $key => $investor): ?>
                            <li class="measures__control<?php if ($k == 0): ?> measures__control_active<?php endif; ?>">
                                <a class="measures__link" href="javascript:void(0)"
                                   style="background-image: url(/uploads/investor_type/<?=$key.'/'. $investor['img'] ?>)">
                                    <div class="measures__link-inner">
                                        <h3 class="measures__title"><?= $investor['text'] ?>

                                        </h3>
                                        <p class="measures__subtitle">
                                        </p>
                                    </div>
                                </a>
                            </li>
                            <?php $k++; endforeach; ?>

                    </ul>
                </header>
                <div class="measures__content">
                    <?php $k = 0;
                    foreach ($investors as $investor): ?>
                        <div class="measures__content-item<?php if ($k == 0): ?> measures__content-item_state_active <?php endif; ?>">
                            <ul class="measures-list">

                                <?php
                                if(isset($investor['measures'][0])){
                                    foreach ($investor['measures'] as $measure): ?>

                                        <li class="measures-list__item"><a class="measures-list__link" href="<?=$measure['page_link']?>"><span
                                                        class="measures-list__text"><?= $measure['name'] ?></a>
                                        </li>

                                    <?php endforeach;
                                } ?>


                            </ul>
                            <a class="btn btn_theme_standart measures__btn" href="<?=$investor['page_link']?>"><span
                                        class="btn__text">Посмотреть все</span></a>
                        </div>
                        <?php $k++;endforeach; ?>

                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
<?php endif; ?>