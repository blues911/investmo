<?php

namespace frontend\widgets;

use common\models\InvestHelpers;
use common\models\ObjectType;
use yii\base\Widget;
use common\models\Cluster;
use common\models\Object;

class NewMapWidget extends Widget
{
    public $investorType;

    public function init()
    {
        parent::init();
    }
    private function getTypeText($id, $count){
        $type_arr = [
            1 => 'Индустриальны'.InvestHelpers::setEndingWord($count,'й','х','х').'<br />парк'.InvestHelpers::setEndingWord($count,'','а','ов'),
            2 => 'Технопарк'.InvestHelpers::setEndingWord($count,'','а','ов'),
            3 => 'Объект'.InvestHelpers::setEndingWord($count,'','а','ов').'<br />ОЭЗ',
            4 => 'Браунфилд'.InvestHelpers::setEndingWord($count,'','ов','ов'),
            5 => 'Резидент'.InvestHelpers::setEndingWord($count,'','а','ов'),
            6 => 'Свободн'.InvestHelpers::setEndingWord($count,'ая','ые','ых').'<br />зем'.InvestHelpers::setEndingWord($count,'ля','и','ель'),
            7 => 'Свободн'.InvestHelpers::setEndingWord($count,'ая','ые','ых').'<br />площад'.InvestHelpers::setEndingWord($count,'ь','и','ей'),
            8 => '<br />Коворкинг'.InvestHelpers::setEndingWord($count,'','а','ов'),
            9 => 'Земельн'.InvestHelpers::setEndingWord($count,'ый','ых','ых').'<br />участ'.InvestHelpers::setEndingWord($count,'ок','ков','ков'),
            10 => 'Объект'.InvestHelpers::setEndingWord($count,'','а','ов').'<br />имущества'
            ];
        return $type_arr[$id];
    }
    public function run()
    {
        if($this->investorType){
            foreach ($this->investorType as $item){
                $count[$item] = Object::find()->active()->andWhere(['object_type_id' => $item])->filterRequestStatus()->count();
                $blocks[] = [
                    'name' => $this->getTypeText($item, $count[$item]),
                    'count' => $count[$item]
                ];
            };
        }
        $clusters = Cluster::find()->all();

        return $this->render('new_map',
            ['clusters' => $clusters,
                'blocks' => $blocks]);
    }
}