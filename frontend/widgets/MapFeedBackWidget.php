<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 1:49
 */

namespace frontend\widgets;
use Yii;
use yii\base\Widget;

class MapFeedBackWidget extends Widget
{
    public $model;
    public $count;
    public $login;

    public function init()
    {
        parent::init();
    }

    public function run(){
        return $this->render('map_feed_back',[
            'count' => $this->count,
            'login' => $this->login
        ]);
    }
}