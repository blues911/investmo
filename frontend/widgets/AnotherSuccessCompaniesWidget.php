<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.06.2017
 * Time: 11:51
 */

namespace frontend\widgets;

use common\models\SuccessStory;
use yii\base\Widget;

class AnotherSuccessCompaniesWidget extends Widget
{
    public $storyId;
    public $investorTypeId;
    public $test;
    public $best;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $stories = SuccessStory::find()->where(['is_active' => 1])->andWhere(['investor_type_id' => $this->investorTypeId])->andWhere(['NOT IN', 'id', $this->storyId])->orderBy('created_at DESC')->all();

        return $this->render('another_success_companies_widget', ['stories' => $stories]);
    }
}