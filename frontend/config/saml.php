<?php

$urlManager = Yii::$app->urlManager;
$spBaseUrl = $urlManager->getHostInfo().''.$urlManager->getBaseUrl();

return [
    'debug' => true,
    'sp' => [
        'entityId' => $spBaseUrl.'/saml/metadata',
        //'entityId' => $spBaseUrl.'/login/metadata.php',
        'assertionConsumerService' => [
            'url' => $spBaseUrl.'/saml/acs',
            //'url' => $spBaseUrl.'/login/index.php?acs',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
        ],
        'singleLogoutService' => [
            'url' => $spBaseUrl.'/saml/sls',
            //'url' => $spBaseUrl.'/login/index.php?slo',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified',
        'x509cert' => 'MIIDIDCCAgigAwIBAgIVANlXzr3atx8p/zkPgdRs35b2uPFrMA0GCSqGSIb3DQEB
                        BQUAMBgxFjAUBgNVBAMMDXNzby5tb3NyZWcucnUwHhcNMTUwMjEwMTM0MzExWhcN
                        MzUwMjEwMTM0MzExWjAYMRYwFAYDVQQDDA1zc28ubW9zcmVnLnJ1MIIBIjANBgkq
                        hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx1ICIOZPR6GGlWKRCHwDLY/HDDxYmXam
                        XtA+bFT2YpFQFcU39GKjS0i2DAuv5hQAJimYEhL2HhSqsfWAedGz7i65OcpAHMX2
                        TRvEs9Yi19HytD02r6HZjUAyDI8F8unZTsJy0hQuI01uM2/xq1FjZJFHtSU2Qcj8
                        cfTyU1BHNLCN/7mSW0CYX7xLFOxuapmQ6Mh9uqfVUxnTHAGz01JR8Gny+eIVLF0s
                        Pq/JxTg0OwUolVR8/EhiMRA+0I5wNUaBVx50CJ/TagtuOGS+XJwEzkZdwsdPQOiQ
                        H47Sgd993ruM37dXhH3rHlb0PizyKFNB+vflEQBucqv5M07cFM9XbwIDAQABo2Ew
                        XzAdBgNVHQ4EFgQUc5z6ryeOlof9RU19YfCD4HIm0WQwPgYDVR0RBDcwNYINc3Nv
                        Lm1vc3JlZy5ydYYkaHR0cHM6Ly9zc28ubW9zcmVnLnJ1L2lkcC9zaGliYm9sZXRo
                        MA0GCSqGSIb3DQEBBQUAA4IBAQBiBb0CSMgpRTrMm327bIOib8Xnm9UQce9T9C8P
                        A9D71otVxkPZjQmuM5fUHCCg6p/M9akadpkokSi9jcYEYNraMxME0UIQtpaML0xj
                        g/LqtXq3dNPOzS/J8yzemnbLv30bRTROcTaj9EfbQbquaUIP0Kh34zmTPRrEU9HG
                        WA4eD1JZakr9Z3bg+XD98PAitZP1wcnqdVXcc0rApSbgUDvUs89S+G/OENiH9Ojt
                        UUh8RF3O/mVTIltmLqwYl5Gy2sVlI82xOEFiOezzYwzu7K1j8Qe3CsPLXAmKgM+f
                        3tIvSZurYSBlvpf856Ew9nxjhNxKQKmgawDMChZ54Z1r12xO',
    ],
    'idp' => [
        'entityId' => 'https://sso.mosreg.ru/shibboleth',
        'singleSignOnService' => [
            'url' => 'https://sso.mosreg.ru//profile/SAML2/Redirect/SSO',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        'singleLogoutService' => [
            'url' => 'https://sso.mosreg.ru//profile/SAML2/Redirect/SSO',
            'binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
        ],
        'x509cert' => 'MIIDIDCCAgigAwIBAgIVANlXzr3atx8p/zkPgdRs35b2uPFrMA0GCSqGSIb3DQEB
    BQUAMBgxFjAUBgNVBAMMDXNzby5tb3NyZWcucnUwHhcNMTUwMjEwMTM0MzExWhcN
    MzUwMjEwMTM0MzExWjAYMRYwFAYDVQQDDA1zc28ubW9zcmVnLnJ1MIIBIjANBgkq
    hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx1ICIOZPR6GGlWKRCHwDLY/HDDxYmXam
    XtA+bFT2YpFQFcU39GKjS0i2DAuv5hQAJimYEhL2HhSqsfWAedGz7i65OcpAHMX2
    TRvEs9Yi19HytD02r6HZjUAyDI8F8unZTsJy0hQuI01uM2/xq1FjZJFHtSU2Qcj8
    cfTyU1BHNLCN/7mSW0CYX7xLFOxuapmQ6Mh9uqfVUxnTHAGz01JR8Gny+eIVLF0s
    Pq/JxTg0OwUolVR8/EhiMRA+0I5wNUaBVx50CJ/TagtuOGS+XJwEzkZdwsdPQOiQ
    H47Sgd993ruM37dXhH3rHlb0PizyKFNB+vflEQBucqv5M07cFM9XbwIDAQABo2Ew
    XzAdBgNVHQ4EFgQUc5z6ryeOlof9RU19YfCD4HIm0WQwPgYDVR0RBDcwNYINc3Nv
    Lm1vc3JlZy5ydYYkaHR0cHM6Ly9zc28ubW9zcmVnLnJ1L2lkcC9zaGliYm9sZXRo
    MA0GCSqGSIb3DQEBBQUAA4IBAQBiBb0CSMgpRTrMm327bIOib8Xnm9UQce9T9C8P
    A9D71otVxkPZjQmuM5fUHCCg6p/M9akadpkokSi9jcYEYNraMxME0UIQtpaML0xj
    g/LqtXq3dNPOzS/J8yzemnbLv30bRTROcTaj9EfbQbquaUIP0Kh34zmTPRrEU9HG
    WA4eD1JZakr9Z3bg+XD98PAitZP1wcnqdVXcc0rApSbgUDvUs89S+G/OENiH9Ojt
    UUh8RF3O/mVTIltmLqwYl5Gy2sVlI82xOEFiOezzYwzu7K1j8Qe3CsPLXAmKgM+f
    3tIvSZurYSBlvpf856Ew9nxjhNxKQKmgawDMChZ54Z1r12xO',
    ],
    'security' => [
        'authnRequestsSigned' => true,
        'wantAssertionsSigned' => true,
        'wantAssertionsEncrypted' => true,
        'requestedAuthnContext' => [
            'sx:sec:saml:esia:EsiaAuthAL10',
            'sx:sec:saml:esia:EsiaAuthAL15',
            'sx:sec:saml:esia:EsiaAuthAL20',
            'sx:sec:saml:esia:EsiaAuthAL30',
        ],
    ],
    'organization' => [
        'en-US' => [
            'name' => 'Инвестиционный портал МО',
            'displayname' => 'Инвестиционный портал МО',
            'url' => 'http://invest.mosreg.ru/',
        ],
    ],
    'contactPerson' => [
        'technical' => [
            'givenName' => 'Инвестиционный портал МО',
            'emailAddress' => 'bdo@zephyrlab.ru',
        ],
        'support' => [
            'givenName' => 'Инвестиционный портал МО',
            'emailAddress' => 'bdo@zephyrlab.ru',
        ],
    ],
];