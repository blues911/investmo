<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'controllerNamespace' => 'frontend\controllers',
    'aliases' => [
        '@root' => '@yii/../../..'
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
            'timeout' => 86400
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js'=>[]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'scriptUrl'=>'/index.php',
            'baseUrl' => '/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'GET,POST /' => 'site/index',
                'GET,POST /index.php' => 'site/index',
                'GET,POST /cabinet/create-object' => 'cabinet/create-object',
                'GET,POST /cabinet/update-object/<id:\d+>' => 'cabinet/update-object',
                'GET /cabinet/delete-object/<id:\d+>' => 'cabinet/delete-object',
                'GET,POST /cabinet/check-object-diff/<id:\d+>' => 'cabinet/check-object-diff',
                'GET,POST feedback' => 'feed-back/index',
                'language/<lang:\w+>' => 'site/change-language',
                'page/<slug:\w+>' => 'page/index',
                'story/<id:\d+>' => 'story/view',
                'news/<id:\d+>' => 'news/view',
                'GET,POST /api/land' => 'api/filter',
                'GET,POST /api/warehouse' => 'api/filter',
                'GET,POST /api/factory' => 'api/filter',
                'GET,POST /api/technopark' => 'api/filter',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'saml' => [
            'class' => 'asasmoyo\yii2saml\Saml',
            'configFileName' => '@frontend/config/saml.php', // OneLogin_Saml config file (Optional)
        ],
    ],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*'],
        ],
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['*'],
        ],
    ],
    'on beforeAction' => function ($event) {
        $language = Yii::$app->session->get('language', 'ru');
        Yii::$app->language = $language;
    },
    'params' => $params,
];
