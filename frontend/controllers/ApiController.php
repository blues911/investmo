<?php

namespace frontend\controllers;

use common\models\ConnectionPoint;
use common\models\Object;
use common\models\ObjectDocument;
use common\models\search\ObjectMapSearch;
use common\models\ObjectType;
use common\models\ObjectPhoto;
use Yii;
use yii\db\Exception;

class ApiController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionObject()
    {
        $request = Yii::$app->request;
        if (!isset($request)) {
            throw new Exception('Id cant be empty');
        }
        $id = $request->get('id');
        $object = Object::findOne(['id' => $id]);

        return $this->renderAjax('object', ['object' => $object]);
    }
    public function actionFilter()
    {
        $searchModel = new ObjectMapSearch();
        $page = Yii::$app->request->get('page');

        $section = Yii::$app->request->get('section');
        if ($section > 0) {
            $sections = intval($section);
        } else {
            $containers = ObjectType::find()->isContainer()->all();
            $children = ObjectType::find()->isChild()->all();

            $sections = [];

            foreach ($containers as $container) {
                $main = Yii::$app->request->get('main-'.$container->id, false);
                if ($main == 'on') {
                    $sections[$container->id] = [];
                }


                foreach ($children as $child) {
                    $sub = Yii::$app->request->get('sub-' . $container->id . '-' . $child->id, false);
                    if ($sub == 'on') {
                        if ($main)
                            $sections[$container->id][$container->id][] = $child->id;
                        else
                            $sections[0][$container->id][] = $child->id;
                    }
                }
            }
        }
        
        $searchModel->sections = $sections;
        $searchModel->page = $page;
        $name = isset(Yii::$app->request->get('ObjectMapSearch')['name']) ? Yii::$app->request->get('ObjectMapSearch')['name'] : null;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderAjax('filter', ['objects' => $dataProvider->models, 'name' => $name]);
    }

    public function actionElectricity()
    {
        $points = ConnectionPoint::find()->active()->andWhere(['type' => ConnectionPoint::TYPE_ELECTRICITY])->all();
        return $this->renderAjax('point', ['points' => $points]);
    }

    public function actionGas()
    {
        $points = ConnectionPoint::find()->active()->andWhere(['type' => ConnectionPoint::TYPE_GAS])->all();
        return $this->renderAjax('point', ['points' => $points]);
    }
}
