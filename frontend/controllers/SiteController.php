<?php

namespace frontend\controllers;

use common\models\News;
use common\models\Object;
use common\models\ObjectDocument;
use common\models\ObjectRequest;
use common\models\SoapArip;
use common\models\Systems;
use Yii;
use common\models\ActivateUser;
use common\models\MailTool;
use common\models\User;
use frontend\assets\AppAsset;
use yii\base\InvalidParamException;
use yii\helpers\Html;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use yii\helpers\Url;
use yii\helpers\StringHelper;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        AppAsset::register(Yii::$app->view);
        $actions = parent::actions();
        $actions['error'] = ['class' => 'yii\web\ErrorAction',];
        return $actions;
    }

    public function actionChangeLanguage($lang)
    {
        Yii::$app->language = $lang;
        Yii::$app->session->set('language', $lang);
        
        return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $object_counts = Object::find()->select(['count(id) count','object_type_id'])
            ->where(['IN','object_type_id',[1,2,4,9]])
            ->andWhere(['status' => Object::STATUS_PUBLISHED])
            ->andWhere(['request_status' => ObjectRequest::STATUS_APPROVED])
            ->groupBy(['object_type_id'])->asArray()->all();
        if($object_counts){
            foreach ($object_counts as $count)
                $count_arr[$count['object_type_id']] = $count['count'];
        }
        return $this->render('index',
            [
                'industry_parks_count' => isset($count_arr[1]) ? $count_arr[1] : 0,
                'brownfields_count' => isset($count_arr[4]) ? $count_arr[4] : 0,
                'land_plots_count' => isset($count_arr[9]) ? $count_arr[9] : 0,
                'techno_parks_count' => isset($count_arr[2]) ? $count_arr[2] : 0
            ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/cabinet');
        }else{
            if(isset($_POST['LoginForm'])){

                $model = new LoginForm(['scenario' => 'use_email']);
                if ($model->load(Yii::$app->request->post()) && $model->login()) {
                    return $this->redirect('/cabinet');
                } else {
                    return 'Неверно введен Email или Пароль';
                }
            }else{
                return $this->redirect('/map');
            }

        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionRegistration()
    {
        $error = 'Регистрация отклонена, попробуйте изменить данные';
        $model = new User(['scenario' => 'front_user']);
        if(isset($_POST['User'])){
            if ($model->load(Yii::$app->request->post())) {
                $transaction = Yii::$app->db->beginTransaction();
                $user_id = $model->saveUser();
                if($user_id){
                    $activate = new ActivateUser();
                    $activate->user_id = $user_id;
                    $activate->key = md5($model->email . time());
                    if($activate->save()){
                        $transaction->commit();
                        $data['key'] = $activate->key;
                        $data['email'] = $model->email;
                        MailTool::sendMail($model->email,
                                        $data,
                                        'RegistrationActivate-html',
                                        'Подтверждение регистрации на Инвестиционном портале МО');
                        return 'На указанный e-mail отправлено письмо для подтверждения регистрации';
                    }
                }
                $transaction->rollBack();
                return $error;
            }else{
                return $error;
            }
        }else{
            return $this->redirect('/map');
        }


    }
    public function actionActivation(){
        if(isset($_GET['email']) && isset($_GET['key'])){
            $email = Yii::$app->request->get('email');
            $key = Yii::$app->request->get('key');
            $user = User::findOne(['email' => $email]);
            if($user){
                if(ActivateUser::deleteAll(['user_id' => $user->id, 'key' => $key])){
                    $user->status = 10;
                    $user->save();
                    MailTool::sendMail($email,
                        null,
                        'RegistrationSuccess-html',
                        'Регистрация на Инвестиционном портале МО подтверждена');
                    if(Yii::$app->user->login($user))
                        return $this->redirect(['/cabinet']);
                }else{
                    return $this->redirect(['/']);
                }
            }else{
                return $this->render('blank',[
                   'message' => 'Вы не зарегистированны на портале.'
                ]);
            }

        }
        return $this->goHome();
    }
    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if(isset($_POST['PasswordResetRequestForm'])){
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail()) {
                    return json_encode(['message'=>'На указанный Email адрес отправлена ссылка для восстановления пароля',
                        'handler' => 'alert',
                        'location' => '/']);
                } else {
                    return json_encode(['message'=> 'Произошла ошибка, попробуйте повторить запрос позже',
                        'handler' => 'alert',
                        ]);
                }
            }else{
                return json_encode(['message'=> 'Введеный Email не принадлежит ни одному из пользователей портала',
                    'handler' => 'alert'
                ]);
            }
        }else{
            return $this->render('requestPasswordResetToken', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $model = new ResetPasswordForm($token);
        if(isset($_POST['ResetPasswordForm'])){
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                return json_encode(['message'=> 'Ваш пароль успешно изменен',
                    'handler' => 'alert',
                    'location' => '/map']);
            }else{
                return json_encode(['message'=> 'Ваш пароль не обновлен, возможно ссылка на востановление пароля устарела',
                    'handler' => 'alert'
                    ]);
            }
        }else{
            return $this->render('resetPassword', [
                'model' => $model,
            ]);
        }
    }
    public function actionNewsRss()
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => News::find()
                ->orderBy(['updated_at' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 20
            ],
        ]);

        $response = Yii::$app->getResponse();
        $headers = $response->getHeaders();

        $headers->set('Content-Type', 'application/rss+xml; charset=utf-8');

        $response->content = \Zelenin\yii\extensions\Rss\RssView::widget([
            'dataProvider' => $dataProvider,
            'channel' => [
                'title' => 'Новости и мероприятия',
                'link' => Url::toRoute('/news', true),
                'description' => 'Новости и мероприятия на инвестиционном портале Московской облости',
                'language' => Yii::$app->language,
                'image'=> function ($widget, \Zelenin\Feed $feed) {
                    $feed->addChannelImage(Yii::$app->urlManager->hostInfo.'/static/img/assets/logo/logo.png', Yii::$app->urlManager->hostInfo, 88, 88, 'Инвестиционный портал Московской облости');
                },
            ],
            'items' => [
                'title' => function ($model, $widget, \Zelenin\Feed $feed) {
                    return $model->title;
                },
                'description' => function ($model, $widget, \Zelenin\Feed $feed) {
                    return Html::img($model->getFullImagePath()).'</br>'.$model->short_desc.'</br>'.Html::a('Читать дальше →',Yii::$app->urlManager->hostInfo.'news/'.$model->id);
                },
                'link' => function ($model, $widget, \Zelenin\Feed $feed) {
                    return Yii::$app->urlManager->hostInfo.'news/'.$model->id;
                },
                'guid' => function ($model, $widget, \Zelenin\Feed $feed) {
                    return Yii::$app->urlManager->hostInfo.'news/'.$model->id;
                },
                'pubDate' => function ($model, $widget, \Zelenin\Feed $feed) {
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s', $model->updated_at);
                    return $date->format(DATE_RSS);
                },
            ]
        ]);
    }
}
