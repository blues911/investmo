<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use common\models\User;

class SamlController extends Controller
{
    // Remove CSRF protection
    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'login' => [
                'class' => 'asasmoyo\yii2saml\actions\LoginAction'
            ],
            'acs' => [
                'class' => 'asasmoyo\yii2saml\actions\AcsAction',
                'successCallback' => [$this, 'callback'],
                'successUrl' => Url::to('/cabinet'),
            ],
            'metadata' => [
                'class' => 'asasmoyo\yii2saml\actions\MetadataAction'
            ],
            'slo' => [
                'class' => 'asasmoyo\yii2saml\actions\LogoutAction',
                'returnTo' => Url::to('/map/'),
            ],
            'sls' => [
                'class' => 'asasmoyo\yii2saml\actions\SlsAction',
                'successUrl' => Url::to('/map/'),
            ],
        ];
    }
    
    public function callback($attributes)
    {
        if (!isset($attributes['socId']) || empty($attributes['socId'])) {
             Yii::$app->getSession()->setFlash('error', 'soc_id_not_found');
             Yii::error('emptry auth redirect', 'mosreg');
             return false;
        }

        $esia = [
            'socId' => $attributes['socId'][0],
            'email' => isset($attributes['email']) ? $attributes['email'][0] : null,
            'firstName' => $attributes['firstName'][0],
            'secondName' => $attributes['secondName'][0]
        ];

        $user = $this->getUser($esia['socId']);

        if (!$user) {
            $user = $this->createUser($esia);
        }

        if (!$user) {
            Yii::$app->getSession()->setFlash('error', 'auth_failed');
            Yii::error('auth failed :'.$esia['socId'], 'mosreg');
            return false;
        }
        
        return Yii::$app->user->login($user);
    }

    protected function getUser($id)
    {
        return User::findByEsia($id);
    }
    
    protected function createUser($esia)
    {
        $user = new User();
        $user->username = $esia['socId'];
        $user->email = $esia['email'];
        $user->first_name = $esia['firstName'];
        $user->last_name = $esia['secondName'];
        $user->status = 10;
        $user->esia_id = $esia['socId'];
        $user->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->security->generateRandomString());
        $user->auth_key = Yii::$app->security->generateRandomString();

        if (!$user->save()) {
            Yii::$app->getSession()->setFlash('error', 'user_creation_failed');
            Yii::error('can`t create user: '.$esia['socId'].PHP_EOL.var_export($user->errors, true), 'mosreg');
            return false;
        }

        return $user;
    }
}
