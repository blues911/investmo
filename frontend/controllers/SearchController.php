<?php

namespace frontend\controllers;

use frontend\assets\AppAsset;
use Yii;
use yii\sphinx\Query;
use common\models\Page;
use yii\data\ActiveDataProvider;

class SearchController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        AppAsset::register(Yii::$app->view);
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $searchPhrase = Yii::$app->request->get('search');
        $order = Yii::$app->request->post('order', false);
        
        $provider = false;
        $answers = false;
        $count = false;
        
        if (!empty($searchPhrase)) {
            $arr = explode(' ', $searchPhrase);
            if(count($arr) > 1)
                $searchPhrase = '*' .implode('* *', $arr). '*';
            else
                $searchPhrase =  '*' .$searchPhrase. '*';
            $query = new Query;
            $query = $query
                ->select([
                    '*',
                    'SNIPPET(title, \'' . $searchPhrase . '\', \'before_match=<mark>\', \'after_match=</mark>\') AS _title',
                    'SNIPPET(header, \'' . $searchPhrase . '\', \'before_match=<mark>\', \'after_match=</mark>\') AS _header',
                    'SNIPPET(content, \'' . $searchPhrase . '\', \'before_match=<mark>\', \'after_match=</mark>\') AS _content'
                ])
                ->from('investmo_index')
                ->match($searchPhrase)
                ->snippetOptions(['before_match' => '<mark>', 'after_match' => '</mark>']);


            if ($order == 'date') {
                $query = $query->orderBy(['created_at' => SORT_DESC]);
            } else if ($order == 'title') {
                $query = $query->orderBy(['title' => SORT_ASC]);
            }

            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                    'route' => '/search/search/',
                ]
            ]);

            $count = $query->count();
            $answers = $provider->getModels();
        }

        return $this->render('index', [
            'phrase' => $searchPhrase,
            'answers' => $answers,
            'provider' => $provider,
            'count' => $count,
        ]);
    }
    
    public function actionSearch()
    {
        $searchPhrase = Yii::$app->request->post('search');
        $order = Yii::$app->request->post('order', false);
        
        $provider = false;
        $answers = false;
        $count = false;
        
        if (!empty($searchPhrase)) {
            $arr = explode(' ', $searchPhrase);
            if(count($arr) > 1)
                $searchPhrase = '*' .implode('* *', $arr). '*';
            else
                $searchPhrase =  '*' .$searchPhrase. '*';
            $query = new Query;
            $query = $query
                ->select([
                    '*',
                    'SNIPPET(title, \''.$searchPhrase.'\', \'before_match=<mark>\', \'after_match=</mark>\') AS _title',
                    'SNIPPET(header, \''.$searchPhrase.'\', \'before_match=<mark>\', \'after_match=</mark>\') AS _header',
                    'SNIPPET(content, \''.$searchPhrase.'\', \'before_match=<mark>\', \'after_match=</mark>\') AS _content'
                ])
                ->from('investmo_index')
                ->match($searchPhrase)
                ->snippetOptions(['before_match' => '<mark>', 'after_match' => '</mark>']);
            
            if ($order == 'date') {
                $query = $query->orderBy(['created_at' => SORT_DESC]);
            } else if ($order == 'title') {
                $query = $query->orderBy(['title' => SORT_ASC]);
            }
            
            $provider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                    'route' => '/search/search/',
                ]
            ]);
            
            $count = $query->count();
            $answers = $provider->getModels();
            
        }

        return $this->renderPartial('search', [
            'phrase' => $searchPhrase,
            'answers' => $answers,
            'provider' => $provider,
            'count' => $count,
        ]);
    }

}
