<?php

namespace frontend\controllers;

use common\models\InvestHelpers;
use common\models\ObjectRequest;
use common\modules\PageComposer\models\PageBlock;
use FailDependenciesPrimitiveParam\IncorrectDependenciesClass;
use frontend\assets\AppAsset;
use frontend\assets\MapAsset;
use yii\web\Controller;
use common\models\Object;
use common\models\Page;
use Yii;

class PageController extends Controller
{
    public $hasBackground = false;
    /**
     * @inheritdoc
     */
    public function actions()
    {
        AppAsset::register(Yii::$app->view);

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex($slug)
    {
        $page = Page::find()
            ->where(['slug' => $slug])
            ->andWhere(['is_active' => '1'])->andWhere(['lang' => Yii::$app->language])
            ->one();
        if(!$page){
            $page = Page::find()
                ->where(['slug' => $slug])
                ->andWhere(['is_active' => '1'])->andWhere(['lang' => 'ru'])
                ->one();
        }
        $cache = Yii::$app->cache;
        if ($page) {
            $blocks = null;
            if($page->is_constructor){
                $blocks = PageBlock::find()->where(['page_id' => $page->id])->andWhere(['class' => Page::className()])->orderBy('sort ASC')->all();
                if ($blocks === false) {
                    $cache->set(Page::className() . '_' . $page->id . '_data', $blocks);
                }
            }
            $this->hasBackground = $page->has_background;
            if($slug == 'investors' || $slug == 'developers' || $slug == 'business'){

                $object_counts = Object::find()->select(['count(id) count','object_type_id'])
                    ->where(['IN','object_type_id',[1,2,4,8,9,10]])
                    ->andWhere(['status' => Object::STATUS_PUBLISHED])
                    ->andWhere(['request_status' =>ObjectRequest::STATUS_APPROVED])
                    ->groupBy(['object_type_id'])->asArray()->all();
                if($object_counts){
                    foreach ($object_counts as $count)
                        $count_arr[$count['object_type_id']] = $count['count'];
                }
                $template_count = ['!industry_parks' => isset($count_arr[1]) ? $count_arr[1] : 0,
                '!brownfields' => isset($count_arr[4]) ? $count_arr[4] : 0,
                '!land_plots' => isset($count_arr[9]) ? $count_arr[9] : 0,
                '!techno_parks' => isset($count_arr[2]) ? $count_arr[2] : 0,
                '!coworkings' => isset($count_arr[8]) ? $count_arr[8] : 0,
                '!object_estates' => isset($count_arr[10]) ? $count_arr[10] : 0];
                $template_text = ['!industry_parks_text' => 'Индустриальны'.InvestHelpers::setEndingWord($template_count['!industry_parks'],'й','х','х').'<br />парк'.InvestHelpers::setEndingWord($template_count['!industry_parks'],'','а','ов'),
                    '!brownfields_text' => 'Браунфилд'.InvestHelpers::setEndingWord($template_count['!brownfields'],'','ов','ов'),
                    '!land_plots_text' => 'Земельн'.InvestHelpers::setEndingWord($template_count['!land_plots'],'ый','ых','ых').'<br />участ'.InvestHelpers::setEndingWord($template_count['!land_plots'],'ок','ков','ков'),
                    '!coworkings_text' => '<br />Коворкинг'.InvestHelpers::setEndingWord($template_count['!coworkings'],'','а','ов'),
                    '!techno_parks_text' => 'Технопарк'.InvestHelpers::setEndingWord($template_count['!techno_parks'],'','а','ов'),
                    '!object_estates_text' => 'Объект'.InvestHelpers::setEndingWord($template_count['!object_estates'],'','а','ов').'<br />имущества'];
                $template = array_merge($template_count , $template_text);
                foreach ($template as $key => $value){
                    $replace[] = '/\{\{'.$key.'\}\}/';
                    $replacement[] = $value;
                }
                $page->content = preg_replace($replace,$replacement,$page->content);
            }
            $content = null;
            if(!$page->is_constructor)
                $content = $this->prepareContent($page->content);
            return $this->render('index', [
                'title' => $page->title,
                'header' => $page->header,
                'content' => $content,
                'blocks' => $blocks,
                'is_constructor' => $page->is_constructor,
                'slug' => $slug
            ]);
        } else {
            throw new \yii\web\NotFoundHttpException();
        }
    }

    private function prepareContent($content)
    {
        $str = $content;
        preg_match_all('#\{{([^!].*?)\}}#', $str, $match);
        foreach ($match[1] as $k => $v) {
            $template = $v;
            if (mb_strpos($v, 'stories:') !== false) {
                $template = 'stories';
                $parts = explode(':', $v);
                if (count($parts) > 1) {
                    $parts = explode(',', $parts[1]);
                    $match[1][$k] = $this->renderFile("@frontend/views/blocks/{$template}.php", ['investor' => $parts]);
                    continue;
                }
            } 
            $match[1][$k] = $this->renderFile("@frontend/views/blocks/{$template}.php");
        }
        $matches = array_combine($match[0], $match[1]);
        return strtr($str, $matches);
    }
}
