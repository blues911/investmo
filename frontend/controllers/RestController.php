<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 29.06.2017
 * Time: 14:24
 */

namespace frontend\controllers;

use common\models\InvestHelpers;
use common\models\ObjectDocument;
use common\models\ObjectRequest;
use common\models\SoapArip;
use common\models\Systems;
use common\models\User;
use Yii;
use yii\db\Exception;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\Object;
use common\models\ObjectPhoto;
use common\models\Systems as System;
use yii\helpers\ArrayHelper;
use yii\filters\auth\HttpBearerAuth;

class RestController extends Controller
{
    private function cleanResponce($responce, $fields = false, $merge = false)
    {
        $filter = $fields === false ? ['id', 'name', 'address_address', 'object_type_id'] : $fields;
        if (ArrayHelper::isAssociative($responce)) {
            if ($fields && $merge) {
                $filter = array_keys($responce) + $fields;
            }
            
            return ArrayHelper::filter($responce, $filter);
        } else {
            $result = [];
            foreach ($responce as $item) {
                if ($fields && $merge) {
                    $filter = array_keys($item) + $fields;
                }
                $result[] = ArrayHelper::filter($item, $filter);
            }
            
            return $result;
        }
        
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function actionAuth()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');

        $system = System::findOne(['id' => $id]);
        if (!$system) {
            $errorsData = [
                "success" => false,
                "error_code" => 404,
                "error_text" => "System is not exist"
            ];
            return $errorsData;
        }

        $workToken = false;
        if ($system->auth_token == $token) {
            $system->last_enter = date('Y-m-d H:i:s');
            if (! $system->save()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 500,
                    "error_text" => "Processing error"
                ];
                return $errorsData;
            }
            $workToken = $system->work_token;
        } else {
            return false;
        }

        $successData = [
            "success" => true,
            "work_token" => $workToken
        ];

        return $successData;
    }

    public function actionGet()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));

        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }

        if (isset($data->search)) {

            $query = Object::find()->active()->where(["system_id" => $id]);

            if ($data->search == 1) {
                if (!empty($data->created_at_before)) {
                    $query->andFilterWhere(['<', 'created_at', $data->created_at_before]);
                }

                if (!empty($data->created_at_after)) {
                    $query->andFilterWhere(['>', 'created_at', $data->created_at_after]);
                }

                if (!empty($data->updated_at_before)) {
                    $query->andFilterWhere(['<', 'updated_at', $data->updated_at_before]);
                }

                if (!empty($data->updated_at_after)) {
                    $query->andFilterWhere(['>', 'updated_at', $data->updated_at_after]);
                }

                if (!empty($data->objects)) {
                    $query->andFilterWhere(['IN', 'ID', $data->objects]);
                }

                if (!empty($data->like)) {
                    $query->andFilterWhere(['LIKE', 'description', $data->like]);
                    $query->orFilterWhere(['LIKE', 'address_area', $data->like]);
                }

            } elseif ($data->search == 0) {
                if (!empty($data->created_at_before)) {
                    $query->orFilterWhere(['<', 'created_at', $data->created_at_before]);
                }

                if (!empty($data->created_at_after)) {
                    $query->orFilterWhere(['>', 'created_at', $data->created_at_after]);
                }

                if (!empty($data->updated_at_before)) {
                    $query->orFilterWhere(['<', 'updated_at', $data->updated_at_before]);
                }

                if (!empty($data->updated_at_after)) {
                    $query->orFilterWhere(['>', 'updated_at', $data->updated_at_after]);
                }

                if (!empty($data->objects)) {
                    $query->orFilterWhere(['IN', 'ID', $data->objects]);
                }

                if (!empty($data->like)) {
                    $query->andFilterWhere(['LIKE', 'description', $data->like]);
                    $query->orFilterWhere(['LIKE', 'address_area', $data->like]);
                }

            }

        } else {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "The Search Flag can not be empty"
            ];
            return $errorsData;
        }

        $limit = (isset($data->limit) && $data->limit > 0) ? intval($data->limit) : 10;
        $offset = (isset($data->offset) && $data->offset > 0) ? intval($data->offset) : 0;
        
        return $this->cleanResponce(ArrayHelper::toArray($query->offset($offset)->limit($limit)->all()), ['!user_id', '!request_status', '!status_comment'], true);
    }

    public function actionGetObjectContainer()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));

        if (! $this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }

        if (empty($data->object_id)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Request error: the object_id param is empty"
            ];
            return $errorsData;
        }
            
        $object = Object::find()->active()->where(['id' => $data->object_id])->one();
        if (!$object) {
             $errorsData = [
                "success" => false,
                "error_code" => 404,
                "error_text" => "There is no object with that id"
            ];
            return $errorsData;
        }
        
        if ($object->parent) {
            return $this->cleanResponce(ArrayHelper::toArray($object->parent));
        } else {
            $errorsData = [
                "success" => false,
                "error_code" => 404,
                "error_text" => "The object has no parent"
            ];
            return $errorsData;
        }
    }

    public function actionGetObjectsByContainer()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));

        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }
        
        if (empty($data->parent_id)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Request error: the parent_id param is empty"
            ];
            return $errorsData;
        }

        $children = Object::find()->active()->where(['parent_id' => $data->parent_id])->all();

        if (count($children) == 0) {
            $errorsData = [
                "success" => false,
                "error_code" => 404,
                "error_text" => "No child objects available"
            ];
            return $errorsData;
        }

        return $this->cleanResponce(ArrayHelper::toArray($children));
    }

    public function actionCreate()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'), true);

        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }

        $objects = [];
        $objectsIDs = [];
        foreach ($data as $dataID => $objectData) {

            $objects[$dataID] = new Object();

            $now = date('Y-m-d H:i:s');
            $objects[$dataID]->attributes = $objectData;
            $objects[$dataID]->system_id = $id;

            if (!$objects[$dataID]->validate()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Validating error",
                    "errors" => $objects[$dataID]->errors,
                ];
                return $errorsData;
            }

            if (!$objects[$dataID]->save()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Saving error",
                    "errors" => $objects[$dataID]->errors,
                ];
                return $errorsData;
            }

            if (is_array($objectData['photos'])) {
                $this->savePhotos($objectData['photos'], $objects[$dataID]->id);
            }

            $objectsIDs[] = $objects[$dataID]->id;
        }

        return $objectsIDs;
    }


    public function actionUpdate()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));

        //return Object::find()->where(['id' => $data[0]->id])->exists();
        //exit;
        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }

        $objects = [];
        $objectsIDs = [];
        foreach ($data as $dataID => $objectData) {

            $objectID = $objectData->id;
            $systemID = $objectData->system_id;
            $object = Object::findOne(["id" => $objectID, "system_id" => $systemID]);
            if (! $object) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 404,
                    "error_text" => "Not Found"
                ];
                return $errorsData;
            }

            $objects[$objectID] = $object;
            $now = date('Y-m-d H:i:s');

            foreach ($objectData as $attributeName => $attributeValue) {
                if (isset($objects[$objectID]->$attributeName)) {
                    $objects[$objectID]->$attributeName = $attributeValue;
                }
            }

            if (! $objects[$objectID]->validate()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Validating error at object: ".$objectID,
                    "errors" => $objects[$objectID]->errors,
                ];
                return $errorsData;
            }

            if (! $objects[$objectID]->save()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Saving at object: ".$objectID,
                    "errors" => $objects[$objectID]->errors,
                ];
                return $errorsData;
            }

            if (is_array($objectData->photos) && !empty($objectData->photos)) {
                $this->savePhotos($objectData->photos, $objectID);
            }
            if (is_string($objectData->photos) && $objectData->photos == 'delete') {
                $this->deletePhotos($objectID);
            }


            $objectsIDs[] = $objects[$objectID]->id;

            if (isset($objectData->children)) {
                foreach ($objectData->children as $child) {
                    $child->parent_id = $objects[$objectID]->id;
                    if (!$child->save()) {
                        $errorsData = [
                            "success" => false,
                            "error_code" => 400,
                            "error_text" => "Сhild saving error",
                            "errors" => $child->errors,
                        ];
                        return $errorsData;
                    }
                }
            }
        }

        return $objectsIDs;
    }
    public function actionAripObjectsList(){
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $success = 0;
        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }
        $soap = new SoapArip(['url' => "http://185.120.188.170/Arip/Services/CommonService.svc?wsdl"]);
        foreach ($soap->getSoapObjects_id() as $arip_id){
            $success++;
            $arip_object_ids[] = $arip_id;
        }
        $work_token = $this->setWorkToken($id,$token);
        $response = [
            "success" => true,
            "work_token" => $work_token,
            "data" => [
                'success' => [
                    "total" => $success,
                    "arip_objects" => $arip_object_ids
                ]
            ],
        ];
        return $response;
    }
    public function actionAripObjectsResponse(){
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));
        $success = 0;
        $error = 0;
        $error_log = null;
        $arip_response = null;
        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }
        $soap = new SoapArip(['url' => "http://185.120.188.170/Arip/Services/CommonService.svc?wsdl", 'response' => true]);
        if(isset($data->objects)){
            foreach ($data->objects as $object_id){
                $soap_object = $soap->getSoapObject($object_id);
                if($soap_object){
                    $success++;
                    $arip_response[$object_id] = $soap_object;
                }else{
                    $error++;
                    $error_log[$object_id] = 'Объект отсутствует в системе АРИП';
                }
            }
        }else{
            foreach ($soap->getSoapObjects_id() as $arip_id){
                $soap_object = $soap->getSoapObject($arip_id);
                if($soap_object->RealObject){
                    $success++;
                    $arip_response[$arip_id] = $soap_object;
                }else{
                    $error++;
                    $error_log[$arip_id] = 'Объект отсутствует в системе АРИП';
                }
            }
        }
        $work_token = $this->setWorkToken($id,$token);
        $response = [
            "success" => true,
            "work_token" => $work_token,
            "data" => [
                'success' => [
                    "total" => $success,
                    "arip_response" => $arip_response
                ],
                "error" => [
                    "total" => $error,
                    "error_log" => $error_log
                ]
            ],
        ];
        return $response;
    }
    private function deleteObject($object){

        $obj_docs = ObjectDocument::find()->where(['object_id' => $object->id])->all();
        if($obj_docs){
            foreach ($obj_docs as $doc)
                $doc->remove($doc->id);

            InvestHelpers::RDir(Yii::getAlias('@frontend') . '/web/uploads/objects_documents/' . $object->id);
        }
        $obj_pics = ObjectPhoto::find()->where(['object_id' => $object->id])->all();
        if ($obj_pics) {
            foreach ($obj_pics as $pic){
                $pic->removeFile();
                $pic->delete();
            }
            InvestHelpers::RDir(Yii::getAlias('@frontend') . '/web/uploads/objects/' . $object->id);
        }
        $object->delete();
        return true;
    }
    private function handlerObjectDocPhoto($obj_id, $documents, $presentation){
        $object_documents = ObjectDocument::find()->where(['object_id' => $obj_id])->all();
        $object_photos = ObjectPhoto::find()->where(['object_id' => $obj_id])->all();

        $object_document_arr = [];
        $object_document_input_arr = [];
        $object_photo_arr = [];
        $object_photo_input_arr = [];
        if($object_documents){
            foreach ($object_documents as $object_document) {
                $object_document_arr[preg_replace('/\s/', '_', $object_document->title)] = $object_document->id;
            }
        }

        if ($documents) {
            if(is_array($documents)){
                foreach ($documents as $Document) {
                    $object_document_input_arr[preg_replace('/\s/', '_', $Document->FileName)] = $Document;
                }
            }else{
                $object_document_input_arr[preg_replace('/\s/', '_', $documents->FileName)] = $documents;
            }

        }
        if($object_photos){
            if(is_array($object_photos)){
                foreach ($object_photos as $object_photo) {
                    $object_photo_arr[preg_replace('/\s/', '_', $object_photo->title)] = $object_photo->id;
                }
            }else{
                $object_photo_arr[preg_replace('/\s/', '_', $object_photos->title)] = $object_photos->id;
            }
        }
        if ($presentation) {
            if(is_array($presentation)){
                foreach ($presentation as $Presentation) {
                    if ($Presentation->Kind->Id != 1000118)
                        $object_document_input_arr[preg_replace('/\s/', '_', $Presentation->FileName)] = $Presentation;
                    else
                        $object_photo_input_arr[preg_replace('/\s/', '_', $Presentation->FileName)] = $Presentation;
                }
            }else{
                if ($presentation->Kind->Id != 1000118)
                    $object_document_input_arr[preg_replace('/\s/', '_', $presentation->FileName)] = $presentation;
                else
                    $object_photo_input_arr[preg_replace('/\s/', '_', $presentation->FileName)] = $presentation;
            }
        }

        foreach (array_diff(array_keys($object_document_arr), array_keys($object_document_input_arr)) as $del) {
            $model = new ObjectDocument;
            $model->remove($object_document_arr[$del]);
        }

        foreach (array_diff(array_keys($object_document_input_arr), array_keys($object_document_arr)) as $add) {
            ObjectDocument::uploadFromUrl($object_document_input_arr[$add]->Url, $obj_id, $object_document_input_arr[$add]->FileName);
        }

        foreach (array_diff(array_keys($object_photo_arr), array_keys($object_photo_input_arr)) as $del) {
            $model = ObjectPhoto::findOne($object_photo_arr[$del]);
            $model->removeFile();
            $model->delete();
        }

        foreach (array_diff(array_keys($object_photo_input_arr), array_keys($object_photo_arr)) as $add) {
            ObjectPhoto::uploadFromUrl($object_photo_input_arr[$add]->Url, $obj_id, $object_photo_input_arr[$add]->FileName);
        }
    }
    public function actionAripObjects(){
        set_time_limit(180);

        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));
        $success = 0;
        $error = 0;
        $success_log = null;
        $error_log = null;
        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }
        $soap = new SoapArip(['url' => "http://185.120.188.170/Arip/Services/CommonService.svc?singleWsdl"]);
        $date = date('Y-m-d H:i:s');
            if(isset($data->delete)){
                foreach ($data->delete as $delete_id){
                    $object = Object::find()->where(['external_id' => $delete_id])->andWhere(['system_id' => 2])->one();
                    if($object){
                        $this->deleteObject($object);
                        $success++;
                        $success_log[$delete_id] = 'Успешно удалён';
                    }else{
                        $error++;
                        $error_log[$delete_id] = 'Объект отсутствует в базе ИнвестМО';
                    }
                }
            }
            if (isset($data->insert)){
                foreach ($data->insert as $object_id){
                    $created = false;

                    $object = Object::find()->where(['external_id' => $object_id])->andWhere(['system_id' => 2])->one();
                    $soap_object = $soap->getSoapObject($object_id);

                    if((!$soap_object && $object) || ($soap_object->HaveToDelete && $object)){
                        $this->deleteObject($object);
                        $success++;
                        $success_log[$object_id] = 'Успешно удалён';
                    }else{
                        if(!$object){
                            $object = new Object();
                            $object->created_at = $date;
                            $created = true;
                        }
                        if($soap_object && !$soap_object->HaveToDelete){
                            $object->attributes = $soap_object->attributes;
                            $object->updated_at = $date;
                            $object->actuality_date = $date;
                            $object->user_id = Systems::find()->where(['id' => 2])->one()->admin;
                            $object->request_status = ObjectRequest::STATUS_APPROVED;
                            if($object->validate() && $object->save()){
                                $obj_id = \Yii::$app->db->lastInsertID != 0 ? \Yii::$app->db->lastInsertID : $object->id;
                                $this->handlerObjectDocPhoto($obj_id, $soap_object->Document, $soap_object->Presentation);
                                $success++;
                                if($created)
                                    $success_log[$object_id] = 'Успешно добавлен';
                                else
                                    $success_log[$object_id] = 'Успешно обновлён';
                            }else{
                                $error++;
                                foreach ($object->getErrors() as $key => $value)
                                    $error_log[$object_id] = $key.' - '.$value[0];
                            }
                        }else{
                            if($soap_object->HaveToDelete){
                                $error++;
                                $error_log[$object_id] = 'Объект не подходит для импорта в базу ИнвестМО';
                            }else{
                                $error++;
                                $error_log[$object_id] = 'Объект отсутствует в системе АРИП';
                            }
                        }
                    }
                }
            }
        $work_token = $this->setWorkToken($id,$token);
        $response = [
            "success" => true,
            "work_token" => $work_token,
            "data" => [
                'success' => [
                    "total" => $success,
                    "success_log" => $success_log
                ],
                "error" => [
                    "total" => $error,
                    "error_log" => $error_log
                    ]
            ],
        ];
        return $response;
    }
    public function actionDelete()
    {
        $token = Yii::$app->request->post('token');
        $id = Yii::$app->request->post('id');
        $data = json_decode(Yii::$app->request->post('data'));

        if (!$this->authSystem($id, $token)) {
            $errorsData = [
                "success" => false,
                "error_code" => 400,
                "error_text" => "Auth error"
            ];
            return $errorsData;
        }

        foreach ($data as $objectID) {
            $object = Object::findOne(["id" => $objectID, "system_id" => $id]);
            if (! $object) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 404,
                    "error_text" => "Object with ID ".$objectID." is not found",
                ];
                return $errorsData;
            }

            if (!$object->delete()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 500,
                    "error_text" => "Deleting error",
                    "errors" => $object->errors,
                ];
                return $errorsData;
            }

            $children = Object::find(["parent_id" => $object->id]);
            foreach ($children as $child) {
                $child->parent_id = null;
                if (!$child->save()) {
                    $errorsData = [
                        "success" => false,
                        "error_code" => 500,
                        "error_text" => "Child saving error",
                        "errors" => $child->errors,
                    ];
                    return $errorsData;
                }
            }
        }

        $successData = [
            "success" => true
        ];
        return $successData;
    }
    private function setWorkToken($id,$token){
        $system = System::findOne(['id' => $id]);
        if (!$system || $system->work_token != $token) {
            return false;
        }
        $work_token = md5($token.time());
        $system->work_token = $work_token;
        $system->token_created_at = date('Y-m-d H:i:s');
        if($system->save())
            return $work_token;
        else
            return $token;
    }
    private function authSystem($id, $token)
    {
        $system = System::findOne(['id' => $id]);
        if (!$system) {
            return false;
        }

        if ($system->work_token != $token) {
            return false;
        }

        return true;
    }

    private function savePhotos($photos, $objectID)
    {
        foreach ($photos as $photo) {
            $mime = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'];
            // check equal api hash
            $exists = ObjectPhoto::find()->where(['api_hash' => md5(base64_decode($photo))])->exists();
            if ($exists) continue;
            $apiHash = md5(base64_decode($photo));
            $file = base64_decode($photo);
            // validate mime
            $finfoMType = finfo_buffer(finfo_open(), $file, FILEINFO_MIME_TYPE);
            if (!in_array($finfoMType, $mime)) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Photo validation error",
                    "errors" => "Not alloved photo format",
                ];
                return $errorsData;
            }
            // save photo
            $fFormat = explode('/', $finfoMType);
            $fileName = substr(md5(mt_rand()), 0, 18).'.'.$fFormat[1];
            $dirPath = Yii::getAlias('@frontend').'/web/uploads/objects/'. $objectID;
            if (!is_dir($dirPath)) {
                mkdir($dirPath, 0777, true);
            }
            $filePath = $dirPath.'/'.$fileName;
            $objectPhoto = new ObjectPhoto;
            $objectPhoto->img = $fileName;
            $objectPhoto->object_id = $objectID;
            $objectPhoto->api_hash = $apiHash;
            if(!$objectPhoto->save()) {
                $errorsData = [
                    "success" => false,
                    "error_code" => 400,
                    "error_text" => "Photo saving error",
                ];
                return $errorsData;
            }
            file_put_contents($filePath, $file);
        }
    }

    private function deletePhotos($id)
    {
        $object = Object::findOne($id);
        if ($object->getPhotos()->all()) {
            foreach ($object->getPhotos()->all() as $photo) {
                $photo->removeFile();
                $photo->delete();
            }
        }
    }
}