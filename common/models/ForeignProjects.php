<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "foreign_projects".
 *
 * @property integer $id
 * @property string $country
 * @property string $company
 * @property string $country_img
 * @property string $img
 */
class ForeignProjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foreign_projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'company', 'country_img', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country' => 'Country',
            'company' => 'Company',
            'country_img' => 'Country Img',
            'img' => 'Img',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ForeignProjectsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ForeignProjectsQuery(get_called_class());
    }
}
