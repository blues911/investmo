<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "{{%page}}".
 *
 * @property integer $id
 * @property string $is_active
 * @property string $title
 * @property string $slug
 * @property string $header
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_deleted
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'content'], 'string'],
            [['has_background','is_constructor'], 'boolean'],
            [['title', 'slug', 'header', 'lang'], 'required'],
            [['created_at', 'updated_at', 'is_deleted'], 'integer'],
            [['title', 'slug', 'header','lang'], 'string', 'max' => 255],
            [['slug', 'lang'], 'unique', 'targetAttribute' => ['slug', 'lang']]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_active' => 'Is Active',
            'title' => 'Имя',
            'slug' => 'Название в URL',
            'header' => 'Шапка',
            'content' => 'Контент',
            'has_background' => 'Есть ли фоновая картинка',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'is_deleted' => 'Удалено?',
            'lang' => 'Язык',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => 1
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\queries\PageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return parent::find()->where(['is_deleted' => 0]);
    }
}
