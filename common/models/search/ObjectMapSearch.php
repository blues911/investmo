<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Object;
use yii\db\Query;

/**
 * ObjectSearch represents the model behind the search form about `common\models\Object`.
 */
class ObjectMapSearch extends Object
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['land_status', 'id', 'status', 'owner', 'bargain_type', 'distance_to_moscow', 'has_electricity_supply', 'has_gas_supply', 'has_water_supply', 'has_water_removal_supply', 'has_heat_supply', 'proprietorship_type', 'has_buildings', 'buildings_count', 'has_road_availability', 'distance_to_nearest_road', 'has_railway_availability', 'has_charge', 'is_special_ecological_zone', 'danger_class', 'parent_id', 'object_type_id', 'system_id', 'flat', 'industry_type'], 'safe'],
            [['municipality', 'name', 'description', 'address_address', 'cadastral_number', 'square_target', 'electricity_supply_capacity', 'gas_supply_capacity', 'water_supply_capacity', 'water_removal_supply_capacity', 'heat_supply_capacity', 'land_category', 'vri', 'charge_type', 'phone', 'okved', 'owner_name'], 'safe'],
            [['latitude', 'longitude', 'bargain_price', 'square', 'flat_height', 'square_sqm'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public $sections = false;
    public $page;
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Object::find()->filterRequestStatus();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if ($this->sections) {
            $totalSections = [];
            $subParent = [];
            $subSection = [];
            if (is_array($this->sections)) {
                foreach ($this->sections as $main => $subs) {
                    if($main != 0)
                        $totalSections[] = $main;

                    foreach ($subs as $sub_main => $sub){
                        $subParent[] = $sub_main;
                        foreach ($sub as $item)
                            $subSection[] = $item;
                    }
                }
            } else {
                $totalSections = $this->sections;
            }
            if($subParent){
                $subQuery = (new Query())->select(['id'])->from(Object::tableName())->where(['in','object_type_id', $subParent]);
                $query->andFilterWhere(['in', 'parent_id', $subQuery])->andWhere(['in','object_type_id',$subSection])->orWhere(['in','object_type_id', $totalSections]);
            }else{
                $query->andFilterWhere(['in','object_type_id',$totalSections]);
            }

        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $inAttributes = [
            'one' => [
                'has_electricity_supply',
                'has_gas_supply',
                'has_water_supply',
                'has_water_removal_supply',
                'has_heat_supply'
            ],
            'two' => [
                'has_buildings',
                'has_road_availability',
                'has_railway_availability',
                'has_charge',
                'is_special_ecological_zone',
                'bargain_type'
            ],
            'four' =>[
                'owner',
                'danger_class',
                'land_category'
            ]
        ];

        // grid filtering conditions
        $query->andFilterWhere([
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'status' => Object::STATUS_PUBLISHED,
        ]);
        foreach ($inAttributes['one'] as $name) {
            if ($this->$name) {
                $query->andFilterWhere([$name => 1]);
            }
        }
        foreach ($inAttributes['two'] as $name) {
            $arr = $this->$name;
            if($this->$name && !isset($arr[1])){
                $query->andFilterWhere(['IN',$name, $arr]);
            }
        }
        foreach ($inAttributes['four'] as $name) {
            $arr = $this->$name;
            if($this->$name && !isset($arr[3])){
                $query->andFilterWhere(['IN', $name, $arr]);
            }
        }

        $buildings_count = $this->buildings_count;
        $bargain_price = $this->bargain_price;
        $square = $this->square;
        $square_sqm = $this->square_sqm;
        $distance_to_moscow = $this->distance_to_moscow;
        $flat = $this->flat;
        $flat_height = $this->flat_height;
        $distance_to_nearest_road = $this->distance_to_nearest_road;
        $buildings_count[0]       = empty($this->buildings_count[0]) ?    0 : intval($this->buildings_count[0]);
        $bargain_price[0]         = empty($this->bargain_price[0]) ?      0 : intval($this->bargain_price[0]);
        $square[0]                = empty($this->square[0]) ?             0 : doubleval($this->square[0]);
        $square_sqm[0]            = empty($this->square_sqm[0]) ?         0 : doubleval($this->square_sqm[0]);
        $distance_to_moscow[0]    = empty($this->distance_to_moscow[0]) ? 0 : doubleval($this->distance_to_moscow[0]);
        $flat[0]                  = empty($this->flat[0]) ?               0 : intval($this->flat[0]);
        $flat_height[0]           = empty($this->flat_height[0]) ?        0 : doubleval($this->flat_height[0]);
        $distance_to_nearest_road[0] = empty($this->distance_to_nearest_road[0]) ? 0 : doubleval($this->distance_to_nearest_road[0]);
        
        $buildings_count[1]       = empty($this->buildings_count[1]) ?    4294967295 : intval($this->buildings_count[1]);
        $bargain_price[1]         = empty($this->bargain_price[1]) ?      4294967295 : intval($this->bargain_price[1]);
        $square[1]                = empty($this->square[1]) ?             4294967295 : doubleval($this->square[1]);
        $square_sqm[1]            = empty($this->square_sqm[1]) ?         4294967295 : doubleval($this->square_sqm[1]);
        $distance_to_moscow[1]    = empty($this->distance_to_moscow[1]) ? 4294967295 : doubleval($this->distance_to_moscow[1]);
        $flat[1]                  = empty($this->flat[1]) ?               4294967295 : intval($this->flat[1]);
        $lat_height[1]           = empty($this->flat_height[1]) ?        4294967295 : doubleval($this->flat_height[1]);
        $distance_to_nearest_road[1] = empty($this->distance_to_nearest_road[1]) ? 4294967295 : doubleval($this->distance_to_nearest_road[1]);

        if (!empty($this->buildings_count[0]) || !empty($this->buildings_count[1])) {
            $query->andFilterWhere(['between', 'buildings_count', $buildings_count[0], $buildings_count[1]]);
        }
        if (!empty($this->bargain_price[0]) || !empty($this->bargain_price[1])) {
            $query->andFilterWhere(['between', 'bargain_price', $bargain_price[0], $bargain_price[1]]);
        }
        if (!empty($this->square[0]) || !empty($this->square[1])) {
            $query->andFilterWhere(['between', 'square', $square[0], $square[1]]);
        }
        if (!empty($this->square_sqm[0]) || !empty($this->square_sqm[1])) {
            $query->andFilterWhere(['between', 'square_sqm', $square_sqm[0], $square_sqm[1]]);
        }
        if (!empty($this->distance_to_moscow[0]) || !empty($this->distance_to_moscow[1])) {
            $query->andFilterWhere(['between', 'distance_to_moscow', $distance_to_moscow[0], $distance_to_moscow[1]]);
        }
        if (!empty($this->flat[0]) || !empty($this->flat[1])) {
            $query->andFilterWhere(['between', 'flat', $flat[0], $flat[1]]);
        }
        if (!empty($this->flat_height[0]) || !empty($this->flat_height[1])) {
            $query->andFilterWhere(['between', 'flat_height', $flat_height[0], $flat_height[1]]);
        }
        if (!empty($this->distance_to_nearest_road[0]) || !empty($this->distance_to_nearest_road[1])) {
            $query->andFilterWhere(['between', 'distance_to_nearest_road', $distance_to_nearest_road[0], $distance_to_nearest_road[1]]);
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'municipality', $this->municipality])
            
             	
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'address_address', $this->address_address])
            ->andFilterWhere(['like', 'cadastral_number', $this->cadastral_number])
            ->andFilterWhere(['like', 'square_target', $this->square_target])
            ->andFilterWhere(['like', 'electricity_supply_capacity', $this->electricity_supply_capacity])
            ->andFilterWhere(['like', 'gas_supply_capacity', $this->gas_supply_capacity])
            ->andFilterWhere(['like', 'water_supply_capacity', $this->water_supply_capacity])
            ->andFilterWhere(['like', 'water_removal_supply_capacity', $this->water_removal_supply_capacity])
            ->andFilterWhere(['like', 'heat_supply_capacity', $this->heat_supply_capacity])
            ->andFilterWhere(['like', 'vri', $this->vri])
            ->andFilterWhere(['like', 'charge_type', $this->charge_type])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'okved', $this->okved])
            ->andFilterWhere(['like', 'owner_name', $this->owner_name]);

        //print_r($query->createCommand()->rawSql);die;    
        if($this->page){
            $query->offset($this->page * 50)->limit(50);
        }
        return $dataProvider;
    }
}
