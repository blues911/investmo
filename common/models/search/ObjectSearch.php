<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Object;

/**
 * ObjectSearch represents the model behind the search form about `common\models\Object`.
 */
class ObjectSearch extends Object
{
    public $addedOnly = false;
    public $username;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'owner', 'bargain_type', 'distance_to_moscow', 'has_electricity_supply', 'has_gas_supply', 'has_water_supply', 'has_water_removal_supply', 'has_heat_supply', 'has_buildings', 'buildings_count', 'has_road_availability', 'distance_to_nearest_road', 'has_railway_availability', 'has_charge', 'is_special_ecological_zone', 'danger_class', 'parent_id', 'object_type_id', 'system_id', 'flat', 'industry_type','external_id','system_id'], 'integer'],
            [['name', 'description',  'address_address', 'cadastral_number', 'square_target', 'electricity_supply_capacity', 'gas_supply_capacity', 'water_supply_capacity', 'water_removal_supply_capacity', 'heat_supply_capacity', 'land_category', 'vri', 'charge_type', 'phone', 'okved', 'owner_name','user_id'], 'safe'],
            [['latitude', 'longitude', 'bargain_price', 'square', 'flat_height'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Object::find()->filterRequestStatus();
        if ($this->addedOnly) {
            $query = $query->processing();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'object_type_id',
                'status',
                'latitude',
                'longitude',
                'username',
                'external_id',
                'system_id'
            ]
        ]);
        $this->load($params);

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->innerJoin('user', '(object.user_id = user.id)');
        // grid filtering conditions
        $query->andFilterWhere([
            'object.id' => $this->id,
            'object.latitude' => $this->latitude,
            'object.longitude' => $this->longitude,
            'object.status' => $this->status,
            'object.owner' => $this->owner,
            'object.bargain_type' => $this->bargain_type,
            'object.bargain_price' => $this->bargain_price,
            'object.square' => $this->square,
            'object.distance_to_moscow' => $this->distance_to_moscow,
            'object.has_electricity_supply' => $this->has_electricity_supply,
            'object.has_gas_supply' => $this->has_gas_supply,
            'object.has_water_supply' => $this->has_water_supply,
            'object.has_water_removal_supply' => $this->has_water_removal_supply,
            'object.has_heat_supply' => $this->has_heat_supply,
            'object.has_buildings' => $this->has_buildings,
            'object.buildings_count' => $this->buildings_count,
            'object.has_road_availability' => $this->has_road_availability,
            'object.distance_to_nearest_road' => $this->distance_to_nearest_road,
            'object.has_railway_availability' => $this->has_railway_availability,
            'object.has_charge' => $this->has_charge,
            'object.is_special_ecological_zone' => $this->is_special_ecological_zone,
            'object.danger_class' => $this->danger_class,
            'object.parent_id' => $this->parent_id,
            'object.object_type_id' => $this->object_type_id,
            'object.system_id' => $this->system_id,
            'object.flat' => $this->flat,
            'object.flat_height' => $this->flat_height,
            'object.industry_type' => $this->industry_type,
            'external_id' => $this->external_id,
            'system_id' => $this->system_id
        ]);
        $query->andFilterWhere(['like', 'object.name', $this->name])
            ->andFilterWhere(['like', 'object.description', $this->description])
            ->andFilterWhere(['like', 'object.address_address', $this->address_address])
            ->andFilterWhere(['like', 'object.cadastral_number', $this->cadastral_number])
            ->andFilterWhere(['like', 'object.square_target', $this->square_target])
            ->andFilterWhere(['like', 'object.electricity_supply_capacity', $this->electricity_supply_capacity])
            ->andFilterWhere(['like', 'object.gas_supply_capacity', $this->gas_supply_capacity])
            ->andFilterWhere(['like', 'object.water_supply_capacity', $this->water_supply_capacity])
            ->andFilterWhere(['like', 'object.water_removal_supply_capacity', $this->water_removal_supply_capacity])
            ->andFilterWhere(['like', 'object.heat_supply_capacity', $this->heat_supply_capacity])
            ->andFilterWhere(['like', 'object.land_category', $this->land_category])
            ->andFilterWhere(['like', 'object.vri', $this->vri])
            ->andFilterWhere(['like', 'object.charge_type', $this->charge_type])
            ->andFilterWhere(['like', 'object.phone', $this->phone])
            ->andFilterWhere(['like', 'object.okved', $this->okved])
            ->andFilterWhere(['like', 'object.owner_name', $this->owner_name]);

        if($this->user_id){
                $query->andFilterWhere(['like', 'user.username', $this->user_id]);
        }
        return $dataProvider;
    }
}
