<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ObjectRequest;
use yii\db\Query;

/**
 * ObjectSearch represents the model behind the search form about `common\models\ObjectRequest`.
 */
class ObjectRequestSearch extends ObjectRequest
{
    public $object_type_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'request_status','object_type_id'], 'integer'],
            [['name', 'request_comment','updated_at'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObjectRequest::find()->isRequest();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        // grid filtering conditions
        $strdate = preg_replace(
            [
                '/январ[ь|я]+/',
                '/феврал[ь|я]+/',
                '/март(а)?/',
                '/апрел[ь|я]+/',
                '/ма[й|я]+/',
                '/июн[ь|я]+/',
                '/июл[ь|я]+/',
                '/август(а)?/',
                '/сентябр[ь|я]+/',
                '/октябр[ь|я]+/',
                '/ноябр[ь|я]+/',
                '/декабр[ь|я]+/'],['january','february','march','april','may','june','jule','august','september','october','november','december'], $this->updated_at);
        $date = date('Y-m-d', strtotime($strdate));
        if($strdate){
            $query->andFilterWhere(['LIKE','updated_at' , $date]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'request_status' => $this->request_status,
            'request_comment' => $this->request_comment,
            'object_type_id' => $this->object_type_id,
            'is_back' => 0
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'request_comment', $this->request_comment]);

        return $dataProvider;
    }
}
