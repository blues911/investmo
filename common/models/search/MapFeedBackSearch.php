<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MapFeedBack;

/**
 * MapFeedBackSearch represents the model behind the search form about `common\models\MapFeedBack`.
 */
class MapFeedBackSearch extends MapFeedBack
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'min_distance_to_moscow', 'max_distance_to_moscow', 'is_deleted'], 'integer'],
            [['email', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['min_square', 'max_square', 'min_bargain_price', 'max_bargain_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MapFeedBack::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'min_square' => $this->min_square,
            'max_square' => $this->max_square,
            'min_bargain_price' => $this->min_bargain_price,
            'max_bargain_price' => $this->max_bargain_price,
            'min_distance_to_moscow' => $this->min_distance_to_moscow,
            'max_distance_to_moscow' => $this->max_distance_to_moscow,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'is_deleted' => $this->is_deleted,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
