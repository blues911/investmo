<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "{{%cluster}}".
 *
 * @property integer $id
 * @property string $title
 * @property double $map_x
 * @property double $map_y
 * @property double $latitude
 * @property double $longitude
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_deleted
 */
class Cluster extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cluster}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'map_x', 'map_y', 'latitude', 'longitude'], 'required'],
            [['map_x', 'map_y', 'latitude', 'longitude'], 'number'],
            [['quantity', 'created_at', 'updated_at', 'is_deleted'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Имя',
            'map_x' => 'Метка X',
            'map_y' => 'Метка Y',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'quantity' => 'Размер',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'is_deleted' => 'Удалено?',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => 1
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\queries\ClusterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return parent::find()->where(['is_deleted' => 0]);
    }
}
