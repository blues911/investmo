<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "systems".
 *
 * @property integer $id
 * @property string $name
 * @property string $auth_token
 * @property string $work_token
 * @property string $token_created_at
 * @property string $admin
 * @property string $last_enter
 */
class Systems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'systems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token_created_at', 'last_enter'], 'safe'],
            [['name', 'auth_token', 'work_token', 'admin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'auth_token' => 'Auth Token',
            'work_token' => 'Work Token',
            'token_created_at' => 'Token Created At',
            'admin' => 'Admin',
            'last_enter' => 'Last Enter',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SystemsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SystemsQuery(get_called_class());
    }
    public static function getSystemsList(){
        $systems = self::find()->all();
        if($systems){
            foreach ($systems as $system)
                $system_list[$system->id] = $system->name;
            return $system_list;
        }
    }
}
