<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\models\InvestorType;
use common\models\StoryInvestorType;

/**
 * This is the model class for table "success_story".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $logo
 * @property string $background_img
 * @property integer $investments
 * @property string $date_of_start
 * @property string $country_of_origin
 * @property string $investment_goal
 * @property string $short_desc
 * @property string $full_desc
 * @property string $html
 * @property string $created_at
 * @property string $updated_at
 * @property mixed $currency
 * @property string $backgroundImg
 * @property string $logoImg
 * @property string $main_img
 * @property string $deleted_at
 */
class SuccessStory extends \yii\db\ActiveRecord
{
    public $logoImage;
    public $backgroundImage;
    public $mainImage;
    public $investor_type_ids;
    
    
    const CURRENCY = ["USD", "EURO"];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'success_story';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'investments'], 'integer'],
            [['date_of_start', 'created_at', 'updated_at', 'deleted_at', 'currency'], 'safe'],
            [['short_desc', 'full_desc', 'html'], 'string'],
            [['title', 'address', 'phone', 'email', 'logo', 'background_img', 'main_img', 'country_of_origin', 'investment_goal'], 'string', 'max' => 255],
            [['logoImage', 'backgroundImage', 'mainImage'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['investor_type_ids'], 'each', 'rule' => ['exist', 'targetClass' => InvestorType::className(), 'targetAttribute' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'is_active' => 'Is Active',
            'address' => 'Адрес',
            'phone' => 'Телефон',
            'email' => 'Email',
            'logo' => 'Логотип',
            'background_img' => 'Фоновая картинка',
            'main_img' => 'Основная картинка',
            'investments' => 'Вложения',
            'currency' => 'Валюта',
            'date_of_start' => 'Дата старта',
            'country_of_origin' => 'Страна происхожнения',
            'investment_goal' => 'Цель инвестирования',
            'short_desc' => 'Краткое описание',
            'full_desc' => 'Полный текст',
            'html' => 'Html',
            'investor_type_id' => 'Тип инвестора',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'deleted_at' => 'Дата удаления',
        ];
    }
    
    public function saveInvestorTypeIds()
    {
        StoryInvestorType::deleteAll(['story_id' => $this->id]);
        $investorTypes = InvestorType::find()->all();
        foreach ($investorTypes as $type) {
            $relation = new StoryInvestorType();
            $relation->story_id = $this->id;
            $relation->type_id = $type->id;
            $relation->save();
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestorTypes()
    {
         return $this->hasMany(InvestorType::className(), ['id' => 'type_id'])->viaTable('story_investor_type', ['story_id' => 'id']);
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function upload($oldAttributes = false)
    {
        if ($this->validate()) {
            $dir = Yii::getAlias('@frontend') . '/web/uploads/story/' . $this->id;
            
            if ($this->logoImage) {
                $logoImageName = md5(time());
                if (!file_exists($dir)) {
                    FileHelper::createDirectory($dir);
                }
                
                $this->removeImage($this->logo);
                
                $this->logoImage->saveAs($dir . '/' . $logoImageName . '.' . $this->logoImage->extension);
                $this->logo = $logoImageName . '.' . $this->logoImage->extension;
                $this->logoImage = null;
            }

            if ($this->backgroundImage) {
                $backgroundImageName = md5(time() + 1);
                if (!file_exists($dir)) {
                    FileHelper::createDirectory($dir);
                }
                
                $this->removeImage($this->background_img);
                
                $this->backgroundImage->saveAs($dir . '/' . $backgroundImageName . '.' . $this->backgroundImage->extension);
                $this->background_img = $backgroundImageName . '.' . $this->backgroundImage->extension;
                $this->backgroundImage = null;
            }

            if ($this->mainImage) {
                $mainImageName = md5(time() + 2);
                if (!file_exists($dir)) {
                    FileHelper::createDirectory($dir);
                }
                
                $this->removeImage($this->main_img);
                
                $this->mainImage->saveAs($dir . '/' . $mainImageName . '.' . $this->mainImage->extension);
                $this->main_img = $mainImageName . '.' . $this->mainImage->extension;
                $this->mainImage = null;
            }
            
            $this->logo = empty($this->logo) ? $oldAttributes['logo'] : $this->logo;
            $this->background_img = empty($this->background_img) ? $oldAttributes['background_img'] : $this->background_img;
            $this->main_img = empty($this->main_img) ? $oldAttributes['main_img'] : $this->main_img;
            
            return true;
        } else {
            return false;
        }
    }

    public function removeImage($oldImage = false)
    {
        if ($oldImage && file_exists($oldImage)) {
            unlink(Yii::getAlias('@frontend') . '/web/uploads/story/' . $this->id . "/" . $oldImage);
        }
    }

    public function getLogoImg()
    {
        if (!$this->logo) {
            return null;
        }
        return '/uploads/story/' . $this->id . '/' . $this->logo;
    }

    public function getBackgroundImg()
    {
        if (!$this->background_img) {
            return null;
        }
        return '/uploads/story/' . $this->id . '/' . $this->background_img;
    }

    public function getMainImg()
    {
        if (!$this->main_img) {
            return null;
        }
        return '/uploads/story/' . $this->id . '/' . $this->main_img;
    }

    public function getCurrency()
    {
        return self::CURRENCY[$this->currency];
    }
    
    public function afterFind()
    {
        parent::afterFind();
    }
    
    public function beforeSave($insert)
    {
        $this->saveInvestorTypeIds();
        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\queries\ObjectTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SuccessStoryQuery(get_called_class());
    }
}
