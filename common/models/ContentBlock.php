<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * This is the model class for table "{{%content_block}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_deleted
 */
class ContentBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'content'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at', 'is_deleted'], 'integer'],
            [['title', 'slug'], 'string', 'max' => 255],
            [['lang'], 'string', 'max' => 2],
            [['slug', 'lang'], 'unique', 'targetAttribute' => ['slug', 'lang']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Имя',
            'slug' => 'Slug',
            'content' => 'Контент',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'is_deleted' => 'Удалено?',
            'lang' => 'Язык',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDeleteBehavior' => [
                'class' => SoftDeleteBehavior::className(),
                'softDeleteAttributeValues' => [
                    'is_deleted' => 1
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     * @return \common\models\queries\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ContentBlockQuery(get_called_class());
    }
    
}
