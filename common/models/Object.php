<?php

namespace common\models;

use Yii;
use yii\db\Query;
use common\models\ObjectPhoto;
use common\models\ObjectDocument;
use common\models\ObjectRequest;
use yii\helpers\Html;

/**
 * This is the model class for table "object".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $latitude
 * @property double $longitude
 * @property integer $status
 * @property integer $request_status
 * @property integer $type
 * @property string $address_address
 * @property integer $owner
 * @property integer $bargain_type
 * @property string $bargain_price
 * @property double $square
 * @property string $cadastral_number
 * @property integer $distance_to_moscow
 * @property string $square_target
 * @property integer $has_electricity_supply
 * @property integer $has_gas_supply
 * @property integer $has_water_supply
 * @property integer $has_water_removal_supply
 * @property integer $has_heat_supply
 * @property string $electricity_supply_capacity
 * @property string $gas_supply_capacity
 * @property string $water_supply_capacity
 * @property string $water_removal_supply_capacity
 * @property string $heat_supply_capacity
 * @property integer $land_category
 * @property string $vri
 * @property integer $has_buildings
 * @property integer $buildings_count
 * @property integer $has_road_availability
 * @property integer $distance_to_nearest_road
 * @property integer $has_railway_availability
 * @property integer $has_charge
 * @property string $charge_type
 * @property integer $is_special_ecological_zone
 * @property integer $danger_class
 * @property string $phone
 * @property string $user_id
 * @property string $created_at
 * @property string $photos
 * @property integer $only_type_fields
 */
class Object extends \yii\db\ActiveRecord
{
    const STATUS_PROCESSION = 0; //На обработке
    const STATUS_DRAFT = 1; //Черновик
    const STATUS_PUBLISHED = 2; //Опубликован

    const OWNER_GOVERNMENT = 1; //Государственная
    const OWNER_MUNICIPAL = 2; //Муниципальная
    const OWNER_PRIVATE = 3; //Частная
    const OWNER_NOT_DISTRICT = 4; //Неразграниченная

    const BARGAIN_TYPE_SALE = 1; //Продажа
    const BARGAIN_TYPE_RENT = 2; // Аренда

    const PROPRIETORSHIP_TYPE_STATE = 1;//Государственная собственность
    const PROPRIETORSHIP_TYPE_PUBLIC = 2;//Муниципальная собственность
    const PROPRIETORSHIP_TYPE_PRIVATE = 3;//Частная собственность


    const LAND_CATEGORY_SETTLEMENTS = 1;// Населенный пункт
    const LAND_CATEGORY_INDUSTRIAL_PURPOSE = 2;// Промышленного назначения
    const LAND_CATEGORY_AGRICULTURE = 3;// Сельскохозяйственного назначения
    const LAND_CATEGORY_RESERVE = 4;// Заповедник
    
    const LAND_STATUS_FREE = 1;
    const LAND_STATUS_PLANNED_BARGAIN = 2;
    const LAND_STATUS_STARTED_BARGAIN = 3;
    const LAND_STATUS_FINISHED_BARGAIN = 4;

    const DANGER_CLASS_1 = 1; // I тип
    const DANGER_CLASS_2 = 2; // II тип
    const DANGER_CLASS_3 = 3; // III тип
    const DANGER_CLASS_4 = 4; //IV тип

    const INDUSTRY_TYPE_ELECTRICAL = 1;
    const INDUSTRY_TYPE_FUEL = 2;
    const INDUSTRY_TYPE_IRON = 3;
    const INDUSTRY_TYPE_NONFERROUS = 4;
    const INDUSTRY_TYPE_CHEMISTRY = 5;
    const INDUSTRY_TYPE_METAL_FABRICATION = 6;
    const INDUSTRY_TYPE_WOOD = 7;
    const INDUSTRY_TYPE_CONSTRUCTION_MATERIALS = 8;
    const INDUSTRY_TYPE_GLASS = 9;
    const INDUSTRY_TYPE_CONSUMERS_GOODS = 10;
    const INDUSTRY_TYPE_FOOD = 11;
    const INDUSTRY_TYPE_MICROBIOLOGICAL = 12;
    const INDUSTRY_TYPE_AGRICULTURAL = 13;
    const INDUSTRY_TYPE_MEDICINE = 14;
    const INDUSTRY_TYPE_POLYGRAPHIC = 15;

    const VRI_TYPE_AGRICULTURE = 1; // 1.0 Сельскохозяйственное использование
    const VRI_TYPE_SETTLEMENT = 2; // 2.0 Жилая застройка
    const VRI_TYPE_BUILDING = 3; // 3.0 Общественное использование объектов капитального строительства
    const VRI_TYPE_ENTERPRISE = 4; // 4.0 Предпринимательство
    const VRI_TYPE_RECREATION = 5; // 5.0 Отдых (рекреация)
    const VRI_TYPE_INDUSTRY = 6; // 6.0 Производственная деятельность
    const VRI_TYPE_TRANSPORT = 7; // 7.0 Транспорт
    const VRI_TYPE_DEFENCE = 8; // 8.0 Обеспечение обороны и безопасности
    const VRI_TYPE_NATURE = 9; // 9.0 Деятельность по особой охране и изучению природы
    const VRI_TYPE_FOREST = 10; // 10.0 Использование лесов
    const VRI_TYPE_WATER = 11; // 11.0 Водные объекты
    const VRI_TYPE_PUBLIC = 12; // 12.0 Земельные участки (территории) общего пользования
    const VRI_TYPE_GARDEN_1 = 13; // 13.1 Ведение огородничества
    const VRI_TYPE_GARDEN_2 = 14; // 13.2 Ведение садоводства
    const VRI_TYPE_GARDEN_3 = 15; // 13.3 Ведение дачного хозяйства

    const PROPERTY_STATUS_FREE = 1; // Объект свободен
    const PROPERTY_STATUS_RENT = 2; // Объект в аренде

    const SCENARIO_CREATE_OBJECT_BY_USER = 'create_by_user';

    public $isObjectRequest = false;
    public $photos = [];
    public $documents = [];
    public $username;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['latitude', 'longitude', 'bargain_price', 'square', 'square_sqm', 'distance_to_nearest_road', 'flat_height','distance_to_rw_station'], 'number'],
            [['status', 'owner', 'bargain_type', 'distance_to_moscow', 'has_electricity_supply', 'has_gas_supply', 'has_water_supply',
                'has_water_removal_supply', 'has_heat_supply', 'has_buildings', 'buildings_count', 'has_road_availability',
                'has_railway_availability', 'has_charge', 'is_special_ecological_zone', 'danger_class', 'property_status', 'only_type_fields',
                'can_use_water_drain'], 'integer'],
            [['address_address', 'municipality', 'name', 'square_target', 'charge_type'], 'string', 'max' => 511],
            [['electricity_supply_capacity', 'gas_supply_capacity',
                'water_supply_capacity', 'water_removal_supply_capacity', 'heat_supply_capacity', 'phone', 'renter', 'worktime',
                'area_config','location_feature',
                'power_water_drain','pipe_burial','hotel_available','workforse_available','available_communication_system','distance_to_major_transport_routes'
                ,'owner_contacts', 'municipal_education_contacts', 'object_contact_face'], 'string', 'max' => 255],
            [['cadastral_number'], 'string', 'max' => 255],
            [['okved', 'owner_name', 'vri'], 'string', 'max' => 255],
            [['land_category', 'request_comment','management_company','special_business_offer'], 'string'],
            ['cadastral_number', 'unique'],
            //[['file_id'], 'integer'],
            //[['file_id'], 'default', 'value' => static::getCurrentFileId()],
            //[['file_id'], 'unique'],
            [['object_type_id'], 'required'],

            [['land_status', 'parent_id', 'object_type_id', 'system_id', 'flat', 'industry_type','is_back','external_id'], 'integer'],

            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Object::className(), 'targetAttribute' => ['parent_id' => 'id']],
            [['object_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ObjectType::className(), 'targetAttribute' => ['object_type_id' => 'id']],
            [['system_id'], 'exist', 'skipOnError' => true, 'targetClass' => Systems::className(), 'targetAttribute' => ['system_id' => 'id']],
            ['photos', 'checkPhotos'],
            ['status', 'default', 'value' => 0],
            ['request_status', 'default', 'value' => 0],
            [['email'], 'validateEmailsFromString', 'skipOnEmpty' => true],
            [['website'], 'validateURL', 'skipOnEmpty' => true],
            [['rent_end'], 'date', 'format' => 'php:d.m.Y', 'skipOnEmpty' => true],
         [
            [
                'name',
                'object_type_id',
                'owner_name',
                'phone',
                'address_address',
                'bargain_type',
                'owner',
                'has_charge',

            ], 'required', 'on' => self::SCENARIO_CREATE_OBJECT_BY_USER],
            [['documents'], 'file', 'extensions' => 'doc, docx, pdf, ppt, pptx, rar, zip', 'maxFiles' => 10, 'skipOnEmpty' => true],
        ];


    }
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!empty($this->rent_end)) {
            $this->rent_end = date('Y-m-d H:i:s', strtotime($this->rent_end));
        }

        // handle ObjectRequest changes
        if (!$this->isNewRecord && $this->isObjectRequest) {
            // change status for approved request
            if ($this->request_status == ObjectRequest::STATUS_APPROVED) {
                $this->status = Object::STATUS_PUBLISHED;
            }
        }
        if($this->longitude == null || $this->latitude == null){
            $api = new \Yandex\Geo\Api();
            $api->setQuery($this->address_address);
            $api->setLimit(1)->setLang(\Yandex\Geo\Api::LANG_RU)->load();
            $response = $api->getResponse();
            $collection = $response->getList();
            if($collection){
                $this->latitude = $this->latitude == null ? $collection[0]->getLatitude() : $this->latitude;
                $this->longitude = $this->longitude == null ? $collection[0]->getLongitude(): $this->longitude;
            }
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        // handle ObjectRequest changes
        if ($this->isObjectRequest) {
            // send email about request status changes
            if ($user = User::findOne($this->user_id)) {
                switch ($this->request_status) {
                    case ObjectRequest::STATUS_REJECTED:
                        Yii::$app->mailer->compose('Object-rejected-html', ['model' => $this])
                            ->setTo($user->email)
                            ->setFrom(Yii::$app->params['emailFrom'])
                            ->setSubject('Заявка на добавление объекта "' . $this->name . '" отклонена.')
                            ->send();
                        break;
                    case ObjectRequest::STATUS_APPROVED:
                        Yii::$app->mailer->compose('Object-approved-html', ['model' => $this])
                            ->setTo($user->email)
                            ->setFrom(Yii::$app->params['emailFrom'])
                            ->setSubject('Заявка на добавление объекта "' . $this->name . '" одобрена.')
                            ->send();
                        break;
                    default:
                        break;
                }
            }
        }

        if ($insert) {
            // initialize documents folder
            $tmpId = Yii::$app->session->get('object_documents_tmp_id');
            if ($tmpId) {
                $documents = ObjectDocument::find()->where(['tmp_id' => $tmpId])->all();
                if (!empty($documents)) {
                    $objectId = $this->id;
                    $tmpDir = Yii::getAlias('@frontend').'/web/uploads/objects_documents/'.$tmpId;
                    $dir = Yii::getAlias('@frontend').'/web/uploads/objects_documents/'.$objectId;
                    rename($tmpDir, $dir);
                    foreach ($documents as $document) {
                        $document->object_id = $objectId;
                        $document->tmp_id = NULL;
                        $document->save();
                    }
                    Yii::$app->session->remove('object_documents_tmp_id');
                }
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
    public static function checkActuality(){
        $date = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30);
        $actuality = ActualityObject::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['>','date', $date])->andWhere(['status' => 0])->one();
        if($actuality)
            return true;
        return false;
    }
    public static function getActualityUsers(){
        $date = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30);
        $user_ids = (new Query())->select('user_id')->from('object')->where(['<','actuality_date', $date])->andWhere(['is_back' => 0])->groupBy('user_id');
        $actuality_users = User::find()->where(['IN','id',$user_ids])->all();
        return $actuality_users;
    }
    public function setObjectMapIcon(){
        if(in_array($this->object_type_id,[1,2,3,4]))
            return "type_a#icon";
        elseif (in_array($this->object_type_id,[5,6,7,8,9,10]))
            return "type_".$this->object_type_id."#icon";
        else
            return "standart#icon";
    }
    public function getObjectSystemName(){
        $system = Systems::findOne($this->system_id);
        return $system->name;
    }
    public function validateEmailsFromString()
    {
        $explode = explode(',', $this->email);
        $valid = true;
        $row = [];
        // validate email
        foreach ($explode as $key => $email) {
            $row[] = trim($email);
            if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
                $valid = false;
            }
        }
        // check duplicates
        if (count(array_unique($row)) < count($row)) {
            $valid = false;
        }
        if (!$valid) {
            $this->addError('email', 'Строка содержит неправильный e-mail адрес или дубликаты.');
        }
        return $valid;
    }

    public function validateURL()
    {
        $pattern = '#((https?)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i';
        if (!preg_match($pattern, $this->website)) {
            $this->addError('website', 'Неверный формат сайта.');
        }
        return true;
    }

    public function checkPhotos(){
        $valid_ext = ['png', 'jpg', 'jpeg', 'gif'];
        if ($this->photos == 'delete') {
            return true;
        }
        if (count($this->photos) && $this->photos[0] != '') {
            foreach ($this->photos as $photo){
                if(!in_array($photo->extension, $valid_ext)) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getLandCategory()
    {
        return json_decode($this->land_category);
    }

    public function setLandCategory($value)
    {
        $this->land_category = json_encode($value);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название объекта',
            'system_id' => 'Система объектов',
            'parent_id' => 'Родительский объект',
            'object_type_id' => 'Тип объекта',
            'description' => 'Общие сведения',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'status' => 'Статус на сайте',
            'request_status' => 'Статус заявки',
            'address_address' => 'Адрес',
            'owner' => 'Форма собственности',
            'owner_name' => 'Собственник',
            'bargain_type' => 'Форма реализации',
            'bargain_price' => 'Стоимость, руб.',
            'square' => 'Площадь, га',
            'square_sqm' => 'Площадь, кв.м',
            'cadastral_number' => 'Кадастровый номер',
            'industry_type' => 'Сектор промышленности',
            'distance_to_moscow' => 'Расстояние до Москвы, км',
            'square_target' => 'Назначение площади',
            'has_electricity_supply' => 'Наличие электроснабжения',
            'has_gas_supply' => 'Наличие газоснабжения',
            'has_water_supply' => 'Наличие водоснабжения',
            'has_water_removal_supply' => 'Наличие водоотведения',
            'has_heat_supply' => 'Наличие теплоснабжения',
            'electricity_supply_capacity' => 'Мощность электроснабжения, МВт',
            'gas_supply_capacity' => 'Мощность газоснабжения, м3/час',
            'water_supply_capacity' => 'Мощность водоснабжения, м3/день',
            'water_removal_supply_capacity' => 'Мощность водоотведения, м3/день',
            'heat_supply_capacity' => 'Мощность теплоснабжения, ГКал/час',
            'land_category' => 'Категория земель',
            'vri' => 'ВРИ',
            'has_buildings' => 'Наличие строений',
            'buildings_count' => 'Число строений',
            'has_road_availability' => 'Наличие доступа к автодороге',
            'distance_to_nearest_road' => 'Расстояние до автодороги, км',
            'has_railway_availability' => 'Возможность присоединения к грузовой ж/д станции',
            'has_charge' => 'Наличие обременения',
            'charge_type' => 'Вид обременения',
            'is_special_ecological_zone' => 'Расположение в ОЭЗ',
            'danger_class' => 'Класс опасности производства',
            'okved' => 'ОКВЭД',
            'flat' => 'Этажность',
            'flat_height' => 'Высота потолка, м',
            'phone' => 'Контактный телефон',
            'photos' => 'Фотографии',
            'documents' => 'Документы',
            'municipality' => 'Муниципальное образование',
            'land_status' => 'Статус участка',
            'request_comment' => 'Комментарий', // к Заявке
            'created_at' => 'Дата создания',
            'property_status' => 'Статус объекта имущества',
            'rent_end' => 'Дата окончания действия договора аренды',
            'renter' => 'Наименование арендополучателя',
            'email' => 'E-mail',
            'website' => 'Сайт',
            'worktime' => 'График работы',
            'only_type_fields' => 'Только поля, настроенные для выбранного типа объекта',
            'area_config' => 'Конфигурация участка',
            'location_feature' => 'Характеристика местности',
            'can_use_water_drain' => 'Возможность подключения к ливневой канализации',
            'power_water_drain' => 'Мощность ливневой канализации, м3/час',
            'distance_to_rw_station' => 'Расстояние до ж/д станции, км',
            'pipe_burial' => 'Места захоронений, нефтяной трубопровод, проектируемые линии ж/д',
            'hotel_available' => 'Наличие гостиниц',
            'workforse_available' => 'Наличие рабочей силы',
            'available_communication_system' => 'Наличие системы связи',
            'distance_to_major_transport_routes' => 'Расстояние до крупных транспортных путей, км',
            'special_business_offer' => 'Спецпредложения для бизнеса',
            'owner_contacts' => 'Контакты собственника',
            'municipal_education_contacts' => 'Контакты муниципального образования',
            'object_contact_face' => 'Контактное лицо по объекту',
            'management_company' => 'Управляющая компания',
            'username' => 'Логин создавшего пользователя',
            'useremail' => 'E-mail создавшего пользователя',
            'is_back' => 'Создано в бэкенде?',
            'external_id' => 'Внешний id',
        ];
    }
    public static function getYesNoAnswer($question){
        if($question !== null){
            if($question === 0)
                return 'Нет';
            if($question === 1)
                return 'Да';
        }
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhotos()
    {
        return $this->hasMany(ObjectPhoto::className(), ['object_id' => 'id']);
    }
    public function getObjectPhotos(){
        return ObjectPhoto::findAll(['object_id' => $this->id]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Object::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Object::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(ObjectType::className(), ['id' => 'object_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getObjectUser()
    {
        return User::find()->where(['id' => $this->user_id])->one();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(ObjectDocument::className(), ['object_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ObjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectQuery(get_called_class());
    }

    public static function getStatus($key = null)
    {
        $answer = [
            static::STATUS_PROCESSION => 'На обработке',
            static::STATUS_DRAFT => 'Черновик',
            static::STATUS_PUBLISHED => 'Опубликован'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }
    public static function getOwner($key = null)
    {
        $answer = [
            static::OWNER_GOVERNMENT => 'Государственная',
            static::OWNER_MUNICIPAL => 'Муниципальная',
            static::OWNER_PRIVATE => 'Частная',
            static::OWNER_NOT_DISTRICT => 'Неразграниченная'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public static function getBargain($key = false)
    {
        $answer = [
            static::BARGAIN_TYPE_SALE => 'Продажа',
            static::BARGAIN_TYPE_RENT => 'Аренда'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public static function getProprietorship($key = false)
    {
        $answer = [
            static::PROPRIETORSHIP_TYPE_STATE => 'Государственная собственность',
            static::PROPRIETORSHIP_TYPE_PUBLIC => 'Муниципальная собственность',
            static::PROPRIETORSHIP_TYPE_PRIVATE => 'Частная собственность'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }
    
    public static function getLandStatusData($key = false)
    {
        $answer = [
            static::LAND_STATUS_FREE => 'Свободен',
            static::LAND_STATUS_PLANNED_BARGAIN => 'Планируется на торги',
            static::LAND_STATUS_STARTED_BARGAIN => 'Торги объявлены',
            static::LAND_STATUS_FINISHED_BARGAIN => 'Торги прошли',
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public static function getLandCategoryData($key = false)
    {
        $answer = [
            static::LAND_CATEGORY_SETTLEMENTS => 'Населенный пункт',
            static::LAND_CATEGORY_INDUSTRIAL_PURPOSE => 'Промышленного назначения',
            static::LAND_CATEGORY_AGRICULTURE => 'Сельскохозяйственного назначения',
            static::LAND_CATEGORY_RESERVE => 'Земли особо охраняемых территорий и объектов',
        ];

        if ($key) {
            if (is_array($key)) {
                $result = [];
                foreach ($key as $item) {
                    $result[] = $answer[$item];
                }
                return implode(',', $result);
            }
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public static function getDangerClass($key = false)
    {
        $answer = [
            static::DANGER_CLASS_1 => 'I класс',
            static::DANGER_CLASS_2 => 'II класс',
            static::DANGER_CLASS_3 => 'III класс',
            static::DANGER_CLASS_4 => 'IV класс'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public static function getIndustryTypes($key = false)
    {
        $answer = [
            static::INDUSTRY_TYPE_ELECTRICAL => 'Электроэнергетика',
            static::INDUSTRY_TYPE_FUEL => 'Топливная промышленность',
            static::INDUSTRY_TYPE_IRON => 'Чёрная металлургия',
            static::INDUSTRY_TYPE_NONFERROUS => 'Цветная металлургия',
            static::INDUSTRY_TYPE_CHEMISTRY => 'Химия и нефтехимия',
            static::INDUSTRY_TYPE_METAL_FABRICATION => 'Машиностроение и металлообработка',
            static::INDUSTRY_TYPE_WOOD => 'Лесообрабатывающая промышленность',
            static::INDUSTRY_TYPE_CONSTRUCTION_MATERIALS => 'Промышленность стройматериалов',
            static::INDUSTRY_TYPE_GLASS => 'Стекольная промышленность',
            static::INDUSTRY_TYPE_CONSUMERS_GOODS => 'Лёгкая промышленность',
            static::INDUSTRY_TYPE_FOOD => 'Пищевая промышленность',
            static::INDUSTRY_TYPE_MICROBIOLOGICAL => 'Микробиологическая промышленность',
            static::INDUSTRY_TYPE_AGRICULTURAL => 'Сельскохозяйственная промышленность',
            static::INDUSTRY_TYPE_MEDICINE => 'Медицинская промышленность',
            static::INDUSTRY_TYPE_POLYGRAPHIC => 'Полиграфическая промышленность',
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }
    public function getObjectTypeName(){
        return ObjectType::findOne($this->object_type_id)->name;
    }

    public static function getPropertyStatus($key = false)
    {
        $answer = [
            self::PROPERTY_STATUS_FREE => 'Объект свободен',
            self::PROPERTY_STATUS_RENT => 'Объект в аренде'
        ];

        if ($key) {
            if($key != 'no')
                return $answer[$key];
        } else {
            return $answer;
        }
    }

    public function getTempId()
    {
        if (!isset($this->tempId)) {
            $this->tempId = md5(time());
        }

        return $this->tempId;
    }

    public function setTempId($value)
    {
        $this->tempId = $value;
    }

    public function updateTempPhotos($id)
    {
        $tmpId = Yii::$app->session->get('tmp_id');
        if ($tmpId) {
            $objectPhotos = ObjectPhoto::find()->where(['tmp_id' => $tmpId])->all();
            if (!empty($objectPhotos)) {
                $properDir = Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id;
                $tmpDir = Yii::getAlias('@frontend') . '/web/uploads/objects/' . $tmpId;
                rename($tmpDir, $properDir);
                foreach ($objectPhotos as $objectPhoto) {
                    $objectPhoto->object_id = $id;
                    $objectPhoto->tmp_id = null;
                    $objectPhoto->save();
                }
                Yii::$app->session->remove('tmp_id');
            }
        }
        return true;

    }

    public static function getNewsObject($news_sender_period_id, $map_feed_back_id = null, $is_sent = null)
    {
        if($map_feed_back_id)
            $map_feed_back = MapFeedBack::findOne($map_feed_back_id);

        $map_feed_back_term = MapFeedBackTerm::findAll(['map_feed_back_id' => $map_feed_back_id]);

        if($map_feed_back_term){
            foreach ($map_feed_back_term as $term)
                $term_array[$term->term_type] = $term->term_id;
        }

        $objects = self::find()->select(['object.*'])
            ->innerJoin('map_feed_back')
            ->innerJoin('news_sender_reg', '(news_sender_reg.map_feed_back_id = map_feed_back.id)')
            ->innerJoin('news_sender_period', '(news_sender_period.id = news_sender_reg.news_sender_period)')
            ->where(['news_sender_period.id' => $news_sender_period_id])
            ->andWhere(['object.status' => 2])
            ->andWhere('object.updated_at BETWEEN news_sender_period.start_date and news_sender_period.end_date');
        if($map_feed_back->min_square)
            $objects = $objects->andWhere(['>', 'object.square', $map_feed_back->min_square]);
        if($map_feed_back->max_square)
            $objects = $objects->andWhere(['<', 'object.square', $map_feed_back->max_square]);
        if($map_feed_back->min_bargain_price)
            $objects = $objects->andWhere(['>', 'object.bargain_price', $map_feed_back->min_bargain_price]);
        if($map_feed_back->max_bargain_price)
            $objects = $objects->andWhere(['<', 'object.bargain_price', $map_feed_back->max_bargain_price]);
        if($map_feed_back->min_distance_to_moscow)
            $objects = $objects->andWhere(['>', 'object.distance_to_moscow', $map_feed_back->min_distance_to_moscow]);
        if($map_feed_back->max_distance_to_moscow)
            $objects = $objects->andWhere(['<', 'object.distance_to_moscow', $map_feed_back->max_distance_to_moscow]);
        if(!empty($term_array['bargain_type']))
           $objects = $objects->andWhere(['IN','object.bargain_type', $term_array['bargain_type']]);
        if(!empty($term_array['type_object_id']))
            $objects = $objects->andWhere(['IN','object.object_type_id', $term_array['type_object_id']]);
        
        if ($map_feed_back_id) {
            $objects = $objects->andWhere(['map_feed_back.id' => $map_feed_back_id]);
        }
        
        if ($is_sent !== null) {
            $objects = $objects->andWhere(['news_sender_reg.is_sent' => $is_sent]);
        }
        return $objects->groupBy('object.id')
            ->orderBy('news_sender_period.id DESC')
            ->all();
    }
    
    public static function getCurrentFileId()
    {
        $result = Yii::$app->db->createCommand('
            SELECT
                MAX(file_id) as max
            FROM '.static::tableName().'
        ')->queryScalar();
        
        $result = intval($result);
        
        if (!$result) {
            return 0;
        }
        
        return $result;
    }
    public function getTypeName(){
        return ObjectType::findOne(['id' => $this->object_type_id])->name;
    }
    public static function checkObjectGroups($user_id,$url = null, $type = null){
        $user_groups = UserInGroup::find()->where(['user_id' => Yii::$app->user->identity->id])->asArray()->all();
        if($user_groups){
            foreach ($user_groups as $group)
                $user_group_list[] = $group['group_id'];
        }else{
            $user_group_list = [];
        }
        $object_groups =  UserInGroup::find()->where(['user_id' => $user_id])->asArray()->all();

        if($object_groups){
            foreach ($object_groups as $group)
                $object_group_list[] = $group['group_id'];
        }else{
            $object_group_list = [];
        }
        $result = array_intersect($user_group_list, $object_group_list);
        if(!empty($result) || Yii::$app->user->identity->status == 40 || Yii::$app->user->identity->id == $user_id){
            if($type)
                return Html::a('<span class="glyphicon '.$type.'"></span>', $url,['data-pjax'=>'0','data-method'=>'post',  'data-confirm' => $type == 'glyphicon-trash' ?'Вы уверены, что хотите удалить этот элемент?':null]);
            else
                return true;
        }
        else{
            return false;
        }

    }
}
