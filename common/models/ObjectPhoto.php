<?php

namespace common\models;

use function GuzzleHttp\Psr7\str;
use Yii;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "object_photo".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $tmp_id
 * @property \yii\db\ActiveQuery $object
 * @property string $url
 * @property string $img
 */
class ObjectPhoto extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id'], 'integer'],
            [['img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'img' => 'Img',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Object::className(), ['id' => 'object_id']);
    }

    public static function saveFile($files, $entity)
    {
        $object_id = $entity->id ? $entity->id : null;
        foreach ($files as $file) {
            $objectPhoto = new self;
            $objectPhoto->object_id = $object_id;
            $objectPhoto->tmp_id = $entity->id ? null : static::createTmpId();
            $path = $objectPhoto->object_id ? $objectPhoto->object_id : $objectPhoto->tmp_id;
            $objectPhoto->img = PictureTool::saveFile($file, 'objects', $path, ['icon_map' => [80, 80]], true)[0];
            $objectPhoto->save();
        }
        return true;
    }
    public static function uploadFromUrl($url, $id, $file_input_name = null){
        if(@fopen($url, "r")){
            $objectId = ($id == 'false') ? NULL : $id;
            $file = pathinfo($url);
            $randomStr = md5(mt_rand());
            $tmpId = self::createTmpId(false);
            $dirName = ($objectId) ?: $tmpId;
            $dirPath = Yii::getAlias('@frontend').'/web/uploads/objects/'. $dirName;
            if (!is_dir($dirPath)) {
                mkdir($dirPath, 0777, true);
            }
            $file_name = $randomStr.'.'.$file['extension'];
            $filePath = $dirPath.'/'.$file_name;
            $file_content = InvestHelpers::getCurlData($url);
            file_put_contents($filePath, $file_content);
            $extension = strtolower($file['extension']);
            if($extension == 'tif' || $extension == 'tiff' || $extension == 'bmp'){
                $filePathRaw = $filePath;
                $fileName = md5(mt_rand());
                $filePathJpg = Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id . "/". $fileName . '.jpg';
                exec('convert '.$filePathRaw.' '.$filePathJpg);
                if(file_get_contents($filePathRaw) != ''){
                    $count = 0;
                    if($extension == 'tif' || $extension == 'tiff'){
                        $exif = exif_read_data($filePathRaw);
                        $count = isset($exif['NewSubFile']) ? $exif['NewSubFile'] : 0;
                    }
                    unlink($filePathRaw);
                    if($count >0){
                        for($i=1;$i< $count+2;$i++){
                            if(file_get_contents( Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id . "/". $fileName.'-'.$i.'.jpg') != '') {
                                $objectPhoto = new self;
                                $objectPhoto->object_id = $objectId;
                                $objectPhoto->title = $file_input_name;
                                $objectPhoto->tmp_id = ($objectId) ? NULL : $tmpId;
                                $path = $objectPhoto->object_id ? $objectPhoto->object_id : $objectPhoto->tmp_id;
                                $objectPhoto->img = PictureTool::saveFile($fileName . '-' . $i . '.jpg', 'objects', $path, ['icon_map' => [80, 80]], true, true)[0];
                                $objectPhoto->save(false);
                            }
                        }
                    }else{
                        if(file_get_contents( Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id . "/". $fileName . '.jpg') != ''){
                            $objectPhoto = new self;
                            $objectPhoto->object_id = $objectId;
                            $objectPhoto->title = $file_input_name;
                            $objectPhoto->tmp_id = ($objectId) ? NULL : $tmpId;
                            $path = $objectPhoto->object_id ? $objectPhoto->object_id : $objectPhoto->tmp_id;
                            $objectPhoto->img = PictureTool::saveFile($fileName.'.jpg', 'objects', $path, ['icon_map' => [80, 80]], true, true)[0];
                            $objectPhoto->save(false);
                        }
                    }
                }
            }else{
                if(file_get_contents( Yii::getAlias('@frontend') . '/web/uploads/objects/' . $id . "/". $file_name) != '') {
                    $objectPhoto = new self;
                    $objectPhoto->object_id = $objectId;
                    $objectPhoto->title = $file_input_name;
                    $objectPhoto->tmp_id = ($objectId) ? NULL : $tmpId;
                    $path = $objectPhoto->object_id ? $objectPhoto->object_id : $objectPhoto->tmp_id;
                    $objectPhoto->img = PictureTool::saveFile($file_name, 'objects', $path, ['icon_map' => [80, 80]], true, true)[0];
                    $objectPhoto->save(false);
                }
            }

        }
    }
    public function removeFile()
    {
        $filePath = Yii::getAlias('@frontend') . '/web/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . "/" . $this->img;
        $fileIconDir = Yii::getAlias('@frontend') . '/web/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . '/icon_map';
        $fileIconPath = Yii::getAlias('@frontend') . '/web/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . '/icon_map/' . $this->img;
        if(is_dir($fileIconDir)){
            if (is_file($fileIconPath)) {
                unlink($fileIconPath);
            }
        }
        if (is_file($filePath)) {
            unlink($filePath);
        }
    }

    public static function createTmpId($user = true)
    {
        $tmpId = Yii::$app->session->get('tmp_id');
        if (!isset($tmpId)) {
            $tmp_blank = $user ? \Yii::$app->user->id : time();
            Yii::$app->session->set('tmp_id', md5($tmp_blank));
        }
        return Yii::$app->session->get('tmp_id');
    }
    
    public function exists($size = null)
    {
        if($size)
            return file_exists(Yii::getAlias('@frontend') . '/web/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . "/" .$size."/". $this->img);
        else
            return file_exists(Yii::getAlias('@frontend') . '/web/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . "/" . $this->img);
    }


    public function getUrl($size = null)
    {
        if($size){
            return '/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . '/' . $size . '/' . $this->img;
        }else{
            return '/uploads/objects/' . (($this->object_id) ?: $this->tmp_id) . '/' . $this->img;
        }

    }
}
