<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 *
 * @property null|\common\models\User $user
 */
class LoginForm extends Model
{
    public $email;
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;

    public function scenarios()
    {
        return [
            'default' => ['username', 'rememberMe', 'password'],
            'backend' => ['username', 'rememberMe', 'password'],
            'use_email' => ['email', 'rememberMe', 'password']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            ['email', 'required', 'on' => 'use_email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword', 'on' => ['default', 'use_email']],
            ['password', 'validateBackendPassword', 'on' => 'backend'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }
    public function validateBackendPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getBackendUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {

        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * @return User|null|\yii\db\ActiveRecord
     */
    public function getBackendUser(){
        if ($this->_user === null) {
            $this->_user = User::find()->where(['username' => $this->username])
            ->andWhere('status > 10')->one();
        }

        return $this->_user;
    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            if($this->scenario == 'use_email')
                $this->_user = User::findOne(['email' => $this->email]);
            else
                $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
