<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "api_constants".
 *
 * @property integer $id
 * @property string $title
 * @property string $values
 */
class ApiConstants extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_constants';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['values'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'values' => 'Values',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ApiConstantsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ApiConstantsQuery(get_called_class());
    }
}
