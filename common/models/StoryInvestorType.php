<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "story_investor_type".
 *
 * @property integer $id
 * @property integer $story_id
 * @property integer $type_id
 *
 * @property InvestorType $type
 * @property SuccessStory $story
 */
class StoryInvestorType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'story_investor_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['story_id', 'type_id'], 'required'],
            [['story_id', 'type_id'], 'integer'],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvestorType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['story_id'], 'exist', 'skipOnError' => true, 'targetClass' => SuccessStory::className(), 'targetAttribute' => ['story_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'story_id' => 'Story ID',
            'type_id' => 'Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(InvestorType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStory()
    {
        return $this->hasOne(SuccessStory::className(), ['id' => 'story_id']);
    }
}
