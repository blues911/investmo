<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%subscribe_email}}".
 *
 * @property integer $id
 * @property string $email
 */

class SubscribeEmail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscribe_email}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => 'Заполните E-mail.'],
            ['email', 'email'],
            [['key'],'string'],
            ['email', 'unique','targetClass' => '\common\models\SubscribeEmail', 'message' => 'E-mail уже используется в рассылке.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Почта',
            'key' => 'Key'
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\SubscribeEmailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\SubscribeEmailQuery(get_called_class());
    }

    public function saveSubscribe()
    {
        $this->key = md5($this->email . time());
        if(!$this->save()) {
            return false;
        }
        return true;
    }

    public static function sendNewsEmail($model,  $subscribe, $host, $theme = 'SubscribeEmail-html')
    {
        Yii::$app->mailer->compose($theme,['model' => $model,
            'email' => $subscribe->email,
            'key' => $subscribe->key,
            'host' => $host])
            ->setTo($subscribe->email)
            ->setFrom('noreply.investmo@gmail.com')
            ->setSubject('Новостная рассылка Инвестиционного портала')
            ->send();
        return true;
    }
}
