<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_in_group".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $group_id
 */
class UserInGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_in_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id'], 'required'],
            [['user_id', 'group_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'group_id' => 'Группа',
        ];
    }
    public function saveUserGroups(){
            if(empty($this->group_id[0])) $this->group_id = [];
            $groups = self::find()->where(['user_id' => $this->user_id])->asArray()->all();
            if($groups){
                foreach ($groups as $group)
                    $base_groups[] = $group['group_id'];
            }else
                {
                    $base_groups = [];
                }
            foreach (array_diff($base_groups,$this->group_id) as $del){
                if(!UserInGroup::deleteAll(['user_id' => $this->user_id, 'group_id' => $del]))
                    return false;
            }
            foreach (array_diff($this->group_id,$base_groups) as $add)
            {
                $user_in_group = new UserInGroup();
                $user_in_group->user_id = $this->user_id;
                $user_in_group->group_id = $add;
                if(!$user_in_group->save())
                    return false;
            }
            return true;
    }
    public function saveGroupUsers(){
        if(empty($this->user_id[0])) $this->user_id = [];
        $users = self::find()->where(['group_id' => $this->group_id])->asArray()->all();
        if($users){
            foreach ($users as $user)
                $base_users[] = $user['user_id'];
        }else
        {
            $base_users = [];
        }
        foreach (array_diff($base_users,$this->user_id) as $del){
            if(!UserInGroup::deleteAll(['group_id' => $this->group_id, 'user_id' => $del]))
                return false;
        }
        foreach (array_diff($this->user_id,$base_users) as $add)
        {
            $user_in_group = new UserInGroup();
            $user_in_group->group_id = $this->group_id;
            $user_in_group->user_id = $add;
            if(!$user_in_group->save())
                return false;
        }
        return true;
    }
}