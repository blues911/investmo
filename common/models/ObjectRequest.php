<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "object".
 *
 * @property integer $request_status
 * @property string $request_comment
 *
 * @property ObjectType $objectType
 */
class ObjectRequest extends \yii\db\ActiveRecord
{
    const STATUS_REVIEW = 1; // На рассмотрении
    const STATUS_APPROVED = 2; // Размещено
    const STATUS_REJECTED = 3; // Отклонено

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_status'], 'integer'],
            [['request_comment'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'request_status' => 'Статус',
            'request_comment' => 'Коментарий',
            'object_type_id' => 'Тип объекта',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата поступления',
        ];
    }
    public function getObjectTypeName(){
        return ObjectType::findOne($this->object_type_id)->name;
    }
    /**
     * @inheritdoc
     * @return \common\models\queries\ObjectRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectRequestQuery(get_called_class());
    }
}
