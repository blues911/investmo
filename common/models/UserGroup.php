<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "user_group".
 *
 * @property integer $id
 * @property string $name
 */
class UserGroup extends \yii\db\ActiveRecord
{
    public $users;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Группа пользователей',
            'users' => 'Участники'
        ];
    }
    public function getGroupUsers(){
        $user_query = (new Query())->select('user_id')->from('user_in_group')->where(['group_id' => $this->id]);
        $users = User::find()->where(['IN','id',$user_query])->all();
        return $users;
    }
}
