<?php

namespace common\models;

use Yii;
use common\models\queries\DirectionQuery;


/**
 * This is the model class for table "direction".
 *
 * @property integer $id
 * @property string $name
 * @property string $address
 * @property string $img
 * @property string $slider_img
 * @property string $description
 * @property string $set_of_points
 * @property integer $investor_type_id
 */
class Direction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'direction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set_of_points'], 'string'],
            [['investor_type_id'], 'integer'],
            [['name', 'address', 'img', 'slider_img', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'address' => 'Address',
            'img' => 'Img',
            'slider_img' => 'Slider Img',
            'description' => 'Description',
            'set_of_points' => 'Set Of Points',
            'investor_type_id' => 'Investor Type ID',
        ];
    }

    /**
     * @inheritdoc
     * @return DirectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DirectionQuery(get_called_class());
    }
}
