<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 27.07.2017
 * Time: 17:09
 */

namespace common\models;

use Yii;

class PictureTool
{
    public static function checkImg($file){
        if(in_array($file->extension,['png','jpg','jpeg','gif']))
            return true;
        else
            return false;
    }
    public static function saveFile( $files, $type, $id , $sizes = null, $crop = false , $url = false)
    {
        $created_images = [];
        if(is_array($files)){
            foreach ($files as $file){
               $created_images[] = self::vipSave( $file, $type, $id , $sizes, $crop, $url);
            }
        }else{
            $created_images[] = self::vipSave( $files, $type, $id , $sizes, $crop, $url);
        }
        return $created_images;
    }
    private static function vipSave( $file, $type, $id , $sizes = null, $crop = false, $url){
        if($url){
            $file_name = $file;
        }else{
            $file_name = md5(mt_rand()) . '.' . $file->extension;
        }
        if (!is_dir(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id)) {
            mkdir(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id, 0775, true);
        }
        if(!$url)
            $file->saveAs(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id . "/" . $file_name);
        if($sizes)
            self::resizeImage($file_name, $sizes, $type, $id, $crop);
        return $file_name;
    }
    public static function removeFile($file_name, $type, $id, $size_path = array())
    {
        foreach ($size_path as $path){
            if($path)
                $path .= '/';
            if(is_file(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id . "/" . $path . $file_name))
                 unlink(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id . "/" . $path . $file_name);
        }
    }
    public static function getExtension($file_name)
    {
        $array_type = explode('.',$file_name);
        return strtolower(array_pop($array_type));
    }
    public static function resizeImage($file_name, $sizes = array(), $type, $id, $crop = false) {
        foreach ($sizes as $size_name => $size)
        {
            list($w_i, $h_i) = getimagesize(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id . "/" . $file_name);
            $img = imagecreatefromstring(file_get_contents(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/'.$id.'/'. $file_name));
            $size_name = $size_name == 'original' ? null: $size_name;
            $size_path = $size_name.'/';
            if (!file_exists(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id .'/'. $size_name)) {
                mkdir(Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/' . $id.'/'. $size_name, 0775, true);
            }
            if(is_array($size)){
                $width = ($w_i/$h_i >= $size[0]/$size[1]) ? (int)($w_i * $size[1] / $h_i) : $size[0];
                $height = ($h_i/$w_i >= $size[1]/$size[0]) ? (int)($h_i * $size[0] / $w_i):  $size[1];
            }else{
                $width = $w_i >= $h_i ? (int)($w_i * $size / $h_i) : $size;
                $height = $h_i >= $w_i ? (int)($h_i * $size / $w_i) : $size;
            }
            $img_o = imagecreatetruecolor($width, $height);

            imagecopyresampled($img_o, $img, 0, 0, 0, 0, $width, $height, $w_i, $h_i);
            if($crop){
                if(is_array($size)){
                    $img_1 = imagecreatetruecolor($size[0], $size[1]);
                    $x = (int)($width - $size[0])/2;
                    $y = (int)($height - $size[1])/2;
                    imagecopyresampled($img_1, $img_o, 0, 0, $x, $y, $width, $height, $width, $height);
                }else{
                    $img_1 = imagecreatetruecolor($size, $size);
                    $x = (int)($w_i - $size)/2;
                    $y = (int)($h_i - $size)/2;
                    imagecopyresampled($img_1, $img, 0, 0, $x, $y, $size, $size, $size, $size);
                }

                imagejpeg($img_1,Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/'.$id.'/' . $size_path . $file_name, 100);
                imagedestroy($img_o);
                imagedestroy($img_1);
                imagedestroy($img);
            }else{
                imagejpeg($img_o,Yii::getAlias('@frontend') . '/web/uploads/'.$type.'/'.$id.'/' . $size_path . $file_name, 100);
                imagedestroy($img_o);
                imagedestroy($img);
            }
        }
    }
}