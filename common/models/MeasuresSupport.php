<?php

namespace common\models;

use Yii;
use common\models\InvestorType;

/**
 * This is the model class for table "measures_support".
 *
 * @property integer $id
 * @property string $name
 * @property integer $investor_type_id
 */
class MeasuresSupport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measures_support';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['investor_type_id'], 'integer'],
            [['name', 'page_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'investor_type_id' => 'Investor Type ID',
            'page_link' => 'Ссылка на страницу'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvestor()
    {
        return $this->hasOne(InvestorType::className(), ['id' => 'investor_type_id']);
    }
}
