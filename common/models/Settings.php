<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 */
class Settings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 60],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
        ];
    }
    public static function getSetting($name){
        return self::findOne(['name' => $name]);
    }
    public static function getSettings($name){
        return self::findAll(['name' => $name]);
    }
    public static function getSettingValue($name)
    {
        return self::getSetting($name)->value;
    }
    public static function deleteSetting($name){
        $setting = self::findOne(['name' => $name]);
        if($setting)
            return $setting->delete();
    }
    public static function saveSetting($name, $value){
        $setting = self::findOne(['name' => $name]);
        if($setting){
            $setting->value = $value;
            if($setting->save())
                return true;
            else
                return false;
        }else{
            $setting = new Settings();
            $setting->name = $name;
            $setting->value = $value;
            if($setting->save())
                return true;
            else
                return false;
        }
    }
    public static function  saveMultiSettings($name, $input_settings){
        if(empty($input_settings[0])) $input_settings = [];
        $settings = self::getSettings($name);
        if($settings){
            foreach ($settings as $setting)
                $base_settings[] = $setting->value;
        }else{
            $base_settings = [];
        }

        foreach (array_diff($base_settings,$input_settings) as $del){
            if(!self::deleteAll(['name' => $name, 'value' => $del]))
                return false;
        }
        foreach (array_diff($input_settings,$base_settings) as $add)
        {
            $new_setting = new Settings();
            $new_setting->name = $name;
            $new_setting->value = $add;
            if(!$new_setting->save())
                return false;
        }
        return true;
    }
}