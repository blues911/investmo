<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 27.07.2017
 * Time: 18:41
 */

namespace common\models;


use yii\db\ActiveRecord;

class UserPhoto extends ActiveRecord
{
    public static function getImgName($id){
        return self::findOne($id)->img;
    }
}