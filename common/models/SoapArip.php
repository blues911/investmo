<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 23.11.2017
 * Time: 14:38
 */

namespace common\models;

use \SoapClient;
use yii\base\Model;

class SoapArip extends Model{
    public $url;
    protected $client;
    protected $kind_type;
    protected $kind_id;
    protected $kind_name;
    protected $kind_owner;
    protected $BuildingCharacteristic;
    protected $RoomCharacteristic;
    protected $LandCharacteristic;
    protected $AddressBase;
    protected $ObjectConclusion;
    protected $Stage;
    protected $ParticipantContact;
    protected $ObjectContactFace;
    protected $Restrictions;
    public $response = false;
    public $HaveToDelete = false;
    public $Document;
    public $Presentation;
    public $system_id = 2;
    public $external_id;
    public $name;
    public $description;
    public $bargain_type;
    public $bargain_price;
    public $object_type_id;
    public $latitude;
    public $longitude;
    public $address_address;
    public $owner;
    public $land_category;
    public $land_status;
    public $square_sqm;
    public $square_target;
    public $cadastral_number;
    public $flat;
    public $owner_name;
    public $has_charge;
    public $charge_type;
    public $has_buildings;
    public $owner_contacts;
    public $municipality;
    public $vri;
    public $municipal_education_contacts;
    public $object_contact_face;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->client = new SoapClient($this->url, [
            'exceptions' => false]);
    }

    protected function checkType(){
        if(
            ($this->kind_id == 1000001 || $this->kind_id == 1000002 || $this->kind_id == 1000003) &&
            (!is_array($this->BuildingCharacteristic))
        ){
            return '1_2_3';
        }elseif(
            ($this->kind_id == 1000001 || $this->kind_id == 1000002 || $this->kind_id == 1000003) &&
            (is_array($this->BuildingCharacteristic))
        ){
            return '4';
        }elseif(
            ($this->kind_id == 1000004) &&
            (!is_array($this->RoomCharacteristic))
        ){
            return '5';
        }elseif(
            ($this->kind_id == 1000004 && is_array($this->RoomCharacteristic)) ||
            ($this->kind_id == 1000005)
        ){
            return '6_7';
        }elseif($this->kind_id == 1000006){
            return '8';
        }elseif($this->kind_id == 1000060){
            return '9';
        }
    }
    public function getSoapObjects_id(){
        $result = $this->client->__soapCall("GetObjectPurchaseList",[],["soapaction" => "http://tempuri.org/ICommonService/GetObjectPurchaseList"]);
        $objects = $result->GetObjectPurchaseListResult->Data->RegistryNumberEntityBase;
        $objects_id = [];
        if($objects){
            foreach ($objects as $object) {
                $objects_id[] = $object->Id;
            }
        }
        return $objects_id;
    }
    public function getSoapObject($id){
        $object_result = $this->client->__soapCall("GetObjectPurchase",
            [array('id' => $id)],
            ["soapaction" => "http://tempuri.org/ICommonService/GetObjectPurchase"]);

        if($this->response){
            return $object_result;
        }
        if(isset($object_result->GetObjectPurchaseResult) && $object_result->GetObjectPurchaseResult->Status != 'Error'){
            $this->HaveToDelete = false;
            $soap_object = isset($object_result->GetObjectPurchaseResult->Data) ? $object_result->GetObjectPurchaseResult->Data : null;
            $this->Restrictions = (array)$soap_object->Restrictions;
            $this->kind_id = isset($soap_object->Kind->Id) ? $soap_object->Kind->Id : null;
            $this->kind_type = $this->checkType();
            $this->kind_name = isset($soap_object->Kind->Name) ? $soap_object->Kind->Name : null;
            $this->kind_owner = isset($soap_object->Rights->ObjectRight->OwnershipKind->Id) ? $soap_object->Rights->ObjectRight->OwnershipKind->Id : null;
            $this->BuildingCharacteristic = isset($soap_object->BuildingCharacteristics->BuildingCharacteristic) ? $soap_object->BuildingCharacteristics->BuildingCharacteristic : null;
            $this->RoomCharacteristic = isset($soap_object->RoomCharacteristics->RoomCharacteristic) ? $soap_object->RoomCharacteristics->RoomCharacteristic : null;
            $this->LandCharacteristic = isset($soap_object->LandCharacteristics->LandCharacteristic) ? $soap_object->LandCharacteristics->LandCharacteristic : null;
            $this->AddressBase = isset($soap_object->Addresses->AddressBase) ? $soap_object->Addresses->AddressBase : null;
            $this->ObjectConclusion = isset($soap_object->Conclusions->ObjectConclusion) ? $soap_object->Conclusions->ObjectConclusion : null;
            $this->Document = isset($soap_object->PurchaseDocuments->Document) ? $soap_object->PurchaseDocuments->Document : null;
            $this->Presentation = isset($soap_object->PresentationDocuments->Document) ? $soap_object->PresentationDocuments->Document : null;
            $this->Stage = isset($soap_object->Stage) ? $soap_object->Stage : null;
            $this->ParticipantContact = isset($soap_object->MunicipalityContacts->ParticipantContact) ? $soap_object->MunicipalityContacts->ParticipantContact : null;
            $this->ObjectContactFace = isset($soap_object->ObjectContactFace) ? $soap_object->ObjectContactFace : null;
            $this->external_id = $id;
            $this->square_sqm = $this->getObjectSquare();
            $this->name = $this->getObjectName();
            $this->description = isset($soap_object->Name) ? $soap_object->Name : null;
            $this->bargain_price = $this->getObjectBargainPrice();
            $this->bargain_type = $this->getObjectBargainType();
            $this->object_type_id = $this->kind_id == 1000060 ? 9 : 10;
            $this->latitude = $this->getObjectLatitude();
            $this->longitude = $this->getObjectLongitude();
            $this->address_address = $this->getObjectAddress();
            $this->municipality = $this->getObjectMunicipality();
            $this->land_category = $this->getObjectLandCategory();
            $this->land_status = $this->getObjectLandStatus();
            $this->square_target = $this->getObjectSquareTarget();
            $this->cadastral_number = $this->getObjectCadastralNumber();
            $this->flat = $this->getObjectFlat();
            $this->owner_name = $this->getObjectOwnerName();
            $this->has_charge = (!empty($this->Restrictions)) ? 1 : 0;
            $this->charge_type = $this->getObjectChargeType();
            $this->has_buildings = $this->getHasBuilding();
            $this->owner = $this->getOwner();
            $this->owner_contacts = $this->getOwnerContacts();
            $this->vri = $this->getObjectVRI();
            $this->municipal_education_contacts = $this->getMunicipalityContacts();
            $this->object_contact_face = $this->getObjectContactFace();
            return $this;
        }else{
            sleep(20);
            return false;
        }
    }
    public function getObjectChargeType(){
        $restriction_arr = [];
        if(isset($this->Restrictions['ObjectRestriction'])){
            if(is_array($this->Restrictions['ObjectRestriction'])){
                foreach ($this->Restrictions['ObjectRestriction'] as $restriction){
                    $restriction_arr[] = $restriction->Kind->Name;
                }
                return implode(', ', $restriction_arr);
            }else{
                return $this->Restrictions['ObjectRestriction']->Kind->Name;
            }
        }
        else{
            return null;
        }
    }
    public function getObjectAddress(){
        if(is_array($this->AddressBase)){
            if(isset($this->AddressBase[0]->Description))
                return $this->AddressBase[0]->Description;
            else
                return null;
        }else{
            if(isset($this->AddressBase->Description))
                return $this->AddressBase->Description;
            else
                return null;
        }
    }
    public function getObjectLongitude(){
        if(is_array($this->AddressBase)){
            if(isset($this->AddressBase[0]->Longitude))
                return $this->AddressBase[0]->Longitude;
            else
                return null;
        }else{
            if(isset($this->AddressBase->Longitude))
                return $this->AddressBase->Longitude;
            else
                return null;
        }
    }
    public function getObjectLatitude(){
        if(is_array($this->AddressBase)){
            if(isset($this->AddressBase[0]->Latitude))
                return $this->AddressBase[0]->Latitude;
            else
                return null;
        }else{
            if(isset($this->AddressBase->Latitude))
                return $this->AddressBase->Latitude;
            else
                return null;
        }
    }
    public function getObjectBargainPrice(){
        if(is_array($this->ObjectConclusion)){
            if(isset($this->ObjectConclusion[0]->PaymentAmount))
                return $this->ObjectConclusion[0]->PaymentAmount;
            else
                return null;
        }else{
            if(isset($this->ObjectConclusion->PaymentAmount))
                return $this->ObjectConclusion->PaymentAmount;
            else
                return null;
        }
    }
    public function getObjectContactFace(){
        $contacts = [];
        if($this->ObjectContactFace != null){
            $contacts[] = isset($this->ObjectContactFace->FullName) ? $this->ObjectContactFace->FullName : null;
            $contacts[] = isset($this->ObjectContactFace->Phone) ? $this->ObjectContactFace->Phone : null;
            $contacts[] = isset($this->ObjectContactFace->Email) ? $this->ObjectContactFace->Email : null;
            return implode(', ', array_diff($contacts, array(null, '-', '--')));
        }
        return null;
    }
    public function getMunicipalityContacts(){
        if($this->ParticipantContact != null){
            $contacts = [];
            $municipality_contacts = [];
            if(is_array($this->ParticipantContact)){
                for($i = 0; $i < 2; $i++){
                    if(isset($this->ParticipantContact[$i])){
                        $contacts[$i][] = isset($this->ParticipantContact[$i]->FullName) ? $this->ParticipantContact[$i]->FullName : null;
                        $contacts[$i][] = isset($this->ParticipantContact[$i]->Phone) ? $this->ParticipantContact[$i]->Phone : null;
                        $contacts[$i][] = isset($this->ParticipantContact[$i]->Email) ? $this->ParticipantContact[$i]->Email : null;
                        $municipality_contacts[] = implode(', ', array_diff($contacts[$i], array(null, '-', '--')));
                    }
                }
                return implode(', ', array_diff($municipality_contacts, array(null, '-', '--')));
            }else{
                $contacts[] = isset($this->ParticipantContact->FullName) ? $this->ParticipantContact->FullName : null;
                $contacts[] = isset($this->ParticipantContact->Phone) ? $this->ParticipantContact->Phone : null;
                $contacts[] = isset($this->ParticipantContact->Email) ? $this->ParticipantContact->Email : null;
                return implode(', ', array_diff($contacts, array(null, '-', '--')));
            }

        }
        return null;
    }
    public function getObjectVRI(){
        $vri = [];
        if($this->kind_type == '1_2_3'){
            if(isset($this->BuildingCharacteristic->AllowedUse->Name))
                return $this->BuildingCharacteristic->AllowedUse->Name;
            else
                return null;
        }elseif ($this->kind_type == '4' || $this->kind_type == '8'){
            if (is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->AllowedUse->Name))
                        $vri[] = $BuildingCharacteristic->AllowedUse->Name;
                }
                return implode(', ', array_diff($vri, array(null)));
            }else{
                if(isset($this->BuildingCharacteristic->AllowedUse->Name))
                    return $this->BuildingCharacteristic->AllowedUse->Name;
            }
        }elseif ($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic)){
                if(isset($this->LandCharacteristic[0]->AllowedUse->Name))
                    return $this->LandCharacteristic[0]->AllowedUse->Name;
                else
                    return null;
            }else{
                if(isset($this->LandCharacteristic->AllowedUse->Name))
                    return $this->LandCharacteristic->AllowedUse->Name;
                else
                    return null;
            }
        }
        return null;
    }
    public function getObjectBargainType(){
        $kind_bargain_type = null;
        if(is_array($this->ObjectConclusion)){
            if(isset($this->ObjectConclusion[0]->PurchaseKind->Id))
                $kind_bargain_type =  $this->ObjectConclusion[0]->PurchaseKind->Id;
        }else{
            if(isset($this->ObjectConclusion->PurchaseKind->Id))
                $kind_bargain_type =  $this->ObjectConclusion->PurchaseKind->Id;
        }
        if($kind_bargain_type == 1000003 || $kind_bargain_type == 1000006 || $kind_bargain_type == 1000018)
            return 1;
        if($kind_bargain_type == 1000002 || $kind_bargain_type == 1000005 || $kind_bargain_type == 1000008)
            return 2;
        return null;
    }
    public function getObjectLandStatus(){
        if(isset($this->Stage->State->Id) && $this->Stage->State->Id == 1000009){
            $this->HaveToDelete = false;
            return 3;
        }else{
            $this->HaveToDelete = true;
            return null;
        }
    }
    public function getObjectOwnerName(){
        if(is_array($this->ObjectConclusion)){
            if(isset($this->ObjectConclusion[0]->Author->Name))
                return $this->ObjectConclusion[0]->Author->Name;
            else
                return null;
        }else{
            if(isset($this->ObjectConclusion->Author->Name))
                return $this->ObjectConclusion->Author->Name;
            else
                return null;
        }
    }
    public function getObjectMunicipality(){
        if(is_array($this->AddressBase)){
            if(isset($this->AddressBase[0]->Oktmo))
                return $this->AddressBase[0]->Oktmo->Name;
            else
                return null;
        }else{
            if(isset($this->AddressBase->Oktmo))
                return $this->AddressBase->Oktmo->Name;
            else
                return null;
        }
    }
    protected function getOwner(){
        if($this->kind_owner == 1000001 || $this->kind_owner == 1000005 || $this->kind_owner == 1000025 || $this->kind_owner == 1000032)
            return 1;
        elseif($this->kind_owner == 1000004)
            return 2;
        elseif($this->kind_owner == 1000033)
            return 3;
        elseif($this->kind_owner == 1000034)
            return 4;
        else
            return null;
    }
    public function getObjectFlat(){
        $flat = null;
        if($this->kind_type == '1_2_3'){
            if(isset($this->BuildingCharacteristic->Floors))
                $flat = $this->BuildingCharacteristic->Floors;
        }elseif($this->kind_type == '4' || $this->kind_type == '8'){
            if(is_array($this->BuildingCharacteristic)){
                if(isset($this->BuildingCharacteristic[0]->Floors))
                    $flat = $this->BuildingCharacteristic[0]->Floors;
            }else{
                if(isset($this->BuildingCharacteristic->Floors))
                    $flat = $this->BuildingCharacteristic->Floors;
            }
        }
        return $flat;
    }
    public function getObjectCadastralNumber(){
        $cadastral = [];
        if($this->kind_type == '1_2_3'){
            if(isset($this->BuildingCharacteristic->CadastrNumber))
                return $this->BuildingCharacteristic->CadastrNumber;
            else
                return null;
        }elseif($this->kind_type == '4'){
            if (is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->CadastrNumber))
                        $cadastral[] = $BuildingCharacteristic->CadastrNumber;
                }
            }else{
                if(isset($this->BuildingCharacteristic->CadastrNumber))
                    $cadastral[] = $this->BuildingCharacteristic->CadastrNumber;
            }
            return implode(', ', array_diff($cadastral, array(null)));
        }elseif($this->kind_type == '5'){
            if(isset($this->RoomCharacteristic->CadastrNumber))
                return $this->RoomCharacteristic->CadastrNumber;
            else
                return null;
        }elseif($this->kind_type == '6_7'){
            if(is_array($this->RoomCharacteristic)){
                foreach ($this->RoomCharacteristic as $RoomCharacteristic){
                    if(isset($RoomCharacteristic->CadastrNumber))
                        $cadastral[] = $RoomCharacteristic->CadastrNumber;
                }
            }else{
                if(isset($this->RoomCharacteristic->CadastrNumber))
                    $cadastral[] = $this->RoomCharacteristic->CadastrNumber;
            }
            return implode(', ', array_diff($cadastral, array(null)));
        }elseif($this->kind_type == '8'){
            if(is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->CadastrNumber))
                        $cadastral[] = $BuildingCharacteristic->CadastrNumber;
                }
            }else{
                if(isset($this->BuildingCharacteristic->CadastrNumber))
                    $cadastral[] = $this->BuildingCharacteristic->CadastrNumber;
            }
            if(is_array($this->LandCharacteristic)){
                foreach ($this->LandCharacteristic as $LandCharacteristic){
                    if(isset($LandCharacteristic->CadastrNumber))
                        $cadastral[] = $LandCharacteristic->CadastrNumber;
                }
            }else{
                if(isset($this->LandCharacteristic->CadastrNumber))
                    $cadastral[] = $this->LandCharacteristic->CadastrNumber;
            }
            return implode(', ', array_diff($cadastral, array(null)));
        }elseif($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic))
                return $this->LandCharacteristic[0]->CadastrNumber;
            else
                return $this->LandCharacteristic->CadastrNumber;
        }
    }
    public function getObjectSquareTarget(){
        $square_target = [];
        if($this->kind_type == '1_2_3'){
            if(isset($this->BuildingCharacteristic->TargetPurpose))
                return $this->BuildingCharacteristic->TargetPurpose;
            else
                return null;
        }elseif($this->kind_type == '4'){
            if (is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->TargetPurpose))
                        $square_target[] = $BuildingCharacteristic->TargetPurpose;
                }
            }else{
                if(isset($this->BuildingCharacteristic->TargetPurpose))
                    $square_target[] = $this->BuildingCharacteristic->TargetPurpose;
            }
            return implode(', ', array_diff($square_target, array(null)));
        }elseif($this->kind_type == '5'){
            if(isset($this->RoomCharacteristic->TargetPurpose))
                return $this->RoomCharacteristic->TargetPurpose;
            else
                return null;
        }elseif($this->kind_type == '6_7'){
            if(is_array($this->RoomCharacteristic)){
                foreach ($this->RoomCharacteristic as $RoomCharacteristic){
                    if(isset($RoomCharacteristic->TargetPurpose))
                        $square_target[] = $RoomCharacteristic->TargetPurpose;
                }
            }else{
                if(isset($this->RoomCharacteristic->TargetPurpose))
                    $square_target[] = $this->RoomCharacteristic->TargetPurpose;
            }
            return implode(', ', array_diff($square_target, array(null)));
        }elseif($this->kind_type == '8'){
            if(is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->TargetPurpose))
                        $square_target[] = $BuildingCharacteristic->TargetPurpose;
                }
            }else{
                if(isset($this->BuildingCharacteristic->TargetPurpose))
                    $square_target[] = $this->BuildingCharacteristic->TargetPurpose;
            }
            if(is_array($this->RoomCharacteristic)){
                foreach ($this->RoomCharacteristic as $RoomCharacteristic){
                    if(isset($RoomCharacteristic->TargetPurpose))
                        $square_target[] = $RoomCharacteristic->TargetPurpose;
                }
            }else{
                if(isset($this->RoomCharacteristic->TargetPurpose))
                    $square_target[] = $this->RoomCharacteristic->TargetPurpose;
            }
            return implode(', ', array_diff($square_target, array(null)));
        }elseif($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic)){
                if (isset($this->LandCharacteristic[0]->TargetPurpose))
                    return $this->LandCharacteristic[0]->TargetPurpose;
                else
                    return null;
            }else {
                if (isset($this->LandCharacteristic->TargetPurpose))
                    return $this->LandCharacteristic->TargetPurpose;
                else
                    return null;
            }
        }
    }
    public function getObjectSquare(){
        $TotalSquare = 0;
        if($this->kind_type == '1_2_3'){
            if(isset($this->BuildingCharacteristic->TotalSquare))
                $TotalSquare += (float)$this->BuildingCharacteristic->TotalSquare;
        }elseif($this->kind_type == '4'){
            if(is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->TotalSquare))
                        $TotalSquare += (float)$BuildingCharacteristic->TotalSquare;
                }
            }else{
                if(isset($this->BuildingCharacteristic->TotalSquare))
                    $TotalSquare += (float)$this->BuildingCharacteristic->TotalSquare;
            }
        }elseif($this->kind_type == '5'){
            if(isset($this->RoomCharacteristic->TotalSquare))
                $TotalSquare += $this->RoomCharacteristic->TotalSquare;
        }elseif($this->kind_type == '6_7'){
            if(is_array($this->RoomCharacteristic)){
                foreach ($this->RoomCharacteristic as $RoomCharacteristic){
                    if(isset($RoomCharacteristic->TotalSquare))
                        $TotalSquare += (float)$RoomCharacteristic->TotalSquare;
                }
            }else{
                if(isset($this->RoomCharacteristic->TotalSquare))
                    $TotalSquare += (float)$this->RoomCharacteristic->TotalSquare;
            }
        }elseif($this->kind_type == '8'){
            if(is_array($this->BuildingCharacteristic)){
                foreach ($this->BuildingCharacteristic as $BuildingCharacteristic){
                    if(isset($BuildingCharacteristic->TotalSquare))
                        $TotalSquare += (float)$BuildingCharacteristic->TotalSquare;
                }
            }else{
                if(isset($this->BuildingCharacteristic->TotalSquare))
                    $TotalSquare += (float)$this->BuildingCharacteristic->TotalSquare;
            }
        }elseif($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic)){
                if(isset($this->LandCharacteristic[0]->LandSquare))
                    $TotalSquare += (float)$this->LandCharacteristic[0]->LandSquare;
            }else{
                if(isset($this->LandCharacteristic->LandSquare))
                    $TotalSquare += (float)$this->LandCharacteristic->LandSquare;
            }
        }
        return $TotalSquare;
    }
    public function getObjectLandCategory(){
        if($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic)){
                if(isset($this->LandCharacteristic[0]->LandCategory->Id))
                    $LandCategoryId = $this->LandCharacteristic[0]->LandCategory->Id;
                else
                    $LandCategoryId = null;
            }else{
                if(isset($this->LandCharacteristic->LandCategory->Id))
                    $LandCategoryId = $this->LandCharacteristic->LandCategory->Id;
                else
                    $LandCategoryId = null;
            }
            if($LandCategoryId == 1000002)
                return '1';
            elseif ($LandCategoryId == 1000003)
                return '2';
            elseif ($LandCategoryId == 1000001)
                return '3';
            elseif ($LandCategoryId == 1000004)
                return '4';
        }
        return '';
    }
    public function getOwnerContacts(){
        $contacts[] = isset($this->ObjectConclusion->Author->Phone) ? 'Тел: '.$this->ObjectConclusion->Author->Phone : null;
        $contacts[] = isset($this->ObjectConclusion->Author->Email) ? 'Email: '.$this->ObjectConclusion->Author->Email : null;
        return implode(', ', array_diff($contacts, array(null)));
    }
    public function getHasBuilding(){
        if($this->kind_type == '9'){
            if(is_array($this->LandCharacteristic)){
                if(isset($this->LandCharacteristic[0]->IsBuilt) && $this->LandCharacteristic[0]->IsBuilt)
                    return 1;
                else
                    return 0;
            }else{
                if(isset($this->LandCharacteristic->IsBuilt) && $this->LandCharacteristic->IsBuilt)
                    return 1;
                else
                    return 0;
            }
        }
        return 0;
    }
    public function getObjectName(){
        $object_name = [];
        if($this->kind_type == '1_2_3'){
            $object_name[] = $this->kind_name;
            if(isset($this->BuildingCharacteristic->CadastrNumber))
                $object_name[] = $this->BuildingCharacteristic->CadastrNumber;
        }elseif($this->kind_type == '4'){
            $object_name[] = 'Комплекс зданий';
        }elseif($this->kind_type == '5'){
            $object_name[] = $this->kind_name;
            if(isset($this->RoomCharacteristic->CadastrNumber))
                $object_name[] = $this->RoomCharacteristic->CadastrNumber;
        }elseif($this->kind_type == '6_7'){
            $object_name[] = 'Комплекс помещений';
        }elseif($this->kind_type == '8'){
            $object_name[] = 'Комплекс зданий и помещений';
        }elseif($this->kind_type == '9'){
            $object_name[] = $this->kind_name;
            if(is_array($this->LandCharacteristic)){
                if(isset($this->LandCharacteristic[0]->CadastrNumber))
                    $object_name[] = $this->LandCharacteristic[0]->CadastrNumber;
            }else{
                if(isset($this->LandCharacteristic->CadastrNumber))
                    $object_name[] = $this->LandCharacteristic->CadastrNumber;
            }
        }
        $TotalSquare = $this->square_sqm;
        $object_name[] = ($TotalSquare != 0) ? $TotalSquare. ' кв.м' : null;
        return implode(', ', array_diff($object_name, array(null)));
    }
}