<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 24.07.2017
 * Time: 12:20
 */

namespace common\models;


use yii\db\ActiveRecord;

class NewsSenderReg extends ActiveRecord
{
    public static function tableName()
    {
        return 'news_sender_reg';
    }
    public static function getAllNotSentPeriods($map_feed_back_id, $offset){
        return self::find()->where(['map_feed_back_id' => $map_feed_back_id, 'is_sent' => 0])->offset($offset)->limit(50)->all();
    }
}