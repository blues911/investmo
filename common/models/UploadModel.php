<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.10.2017
 * Time: 14:52
 */

namespace common\models;


use yii\base\Model;

class UploadModel extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'csv'],
        ];
    }
}