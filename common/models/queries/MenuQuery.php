<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\ConnectionPoint]].
 *
 * @see \common\models\ConnectionPoint
 */
class MenuQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }
    
    public function lang($lang = false)
    {
        if ($lang === false) {
            return $this->andWhere(['lang' => \Yii::$app->language]);
        }
        
        if ($lang === null) {
            return $this;
        }
        
        return $this->andWhere(['lang' => $lang]);
    }

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
