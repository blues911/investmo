<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\Systems]].
 *
 * @see \common\models\Systems
 */
class SystemsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \common\models\Systems[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Systems|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
