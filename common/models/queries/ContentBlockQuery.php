<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\ConnectionPoint]].
 *
 * @see \common\models\ConnectionPoint
 */
class ContentBlockQuery extends \yii\db\ActiveQuery
{
    public function slug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }
    
    public function active()
    {
        return $this->andWhere(['is_active' => true, 'is_deleted' => false]);
    }
    
    public function lang($lang = false)
    {
        if ($lang === false) {
            return $this->andWhere(['lang' => \Yii::$app->language]);
        }
        
        if ($lang === null) {
            return $this;
        }
        
        return $this->andWhere(['lang' => $lang]);
    }

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
