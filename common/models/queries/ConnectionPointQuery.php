<?php

namespace common\models\queries;

use common\models\ConnectionPoint;

/**
 * This is the ActiveQuery class for [[\common\models\ConnectionPoint]].
 *
 * @see \common\models\ConnectionPoint
 */
class ConnectionPointQuery extends \yii\db\ActiveQuery
{

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ConnectionPoint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active()
    {
        return $this->andWhere(['status' => ConnectionPoint::STATUS_PUBLISHED]);
    }

    public function gas()
    {
        return $this->andWhere(['type' => ConnectionPoint::TYPE_GAS]);
    }

    public function electric()
    {
        return $this->andWhere(['type' => ConnectionPoint::TYPE_ELECTRICITY]);
    }



}
