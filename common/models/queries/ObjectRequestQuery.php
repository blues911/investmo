<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\ObjectRequest]].
 *
 * @see \common\models\ObjectRequest
 */
class ObjectRequestQuery extends \yii\db\ActiveQuery
{
    public function isRequest()
    {
        return $this->andWhere(['not', ['request_status' => 0]]);
    }

    /**
     * @inheritdoc
     * @return \common\models\ObjectRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ObjectRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
