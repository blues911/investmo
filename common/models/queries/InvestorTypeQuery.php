<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 16:51
 */

namespace common\models\queries;


class InvestorTypeQuery extends \yii\db\ActiveQuery
{
    public function measures()
    {
        return $this::joinWith('investor_type.*, me');
    }

    public function countMeasure()
    {
        return parent::select('COUNT(measures_support.id) AS count');
    }

}