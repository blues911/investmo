<?php

namespace common\models\queries;

use common\models\Object;
use common\models\ObjectRequest;

/**
 * This is the ActiveQuery class for [[\common\models\Object]].
 *
 * @see \common\models\Object
 */
class ObjectQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return \common\models\Object[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Object|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function fileId($fileId)
    {
        if (!$fileId) {
            return $this;
        }
        
        return $this->andWhere(['file_id' => $fileId]);
    }

    public function type($type)
    {
        if (!$type) {
            return $this;
        }

        return $this->andWhere(['object_type_id' => $type->id]);
    }
    
    public function processing()
    {
        return $this->andWhere('status = '.Object::STATUS_PROCESSION.' AND user_id > 0');
    }

    public function active()
    {
        return $this->andWhere(['status' => Object::STATUS_PUBLISHED]);
    }

    public function filterRequestStatus()
    {
        return $this->andWhere(['request_status' => ObjectRequest::STATUS_APPROVED]);
    }

    public function activeA()
    {
        return $this->andWhere(['a.status' => Object::STATUS_PUBLISHED]);
    }

    public function isContainer()
    {
        $this->leftJoin('object_type t', 't.id = object_type_id');
        return $this->andWhere(['t.is_container' => true, 'parent_id' => null]);
    }

    public function isChild()
    {
        $this->leftJoin('object_type t', 't.id = object_type_id');
        return $this->andWhere(['t.is_child' => true, 'parent_id IS NOT' => null]);
    }

    public function findChild($type1, $type2)
    {
        $this->leftJoin('object b', 'a.parent_id = b.id');
        return $this->andWhere(['a.object_type_id' => $type1, 'b.object_type_id' => $type2]);
    }
}
