<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\ObjectType]].
 *
 * @see \common\models\ObjectType
 */
class ObjectTypeQuery extends \yii\db\ActiveQuery
{
    public function code($code)
    {
        return $this->andWhere(['code' => $code]);
    }
    
    public function isContainer()
    {
        return $this->andWhere(['is_container' => true]);
    }
    
    public function isChild()
    {
        return $this->andWhere(['is_child' => true]);
    }
    
    public function isSeparated()
    {
        return $this->andWhere(['is_child' => false, 'is_container' => false]);
    }

    /**
     * @inheritdoc
     * @return \common\models\ObjectType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\ObjectType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
