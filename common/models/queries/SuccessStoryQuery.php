<?php

namespace common\models\queries;

/**
 * This is the ActiveQuery class for [[\common\models\SuccessStory]].
 *
 * @see \common\models\SuccessStory
 */
class SuccessStoryQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }

    public function investorType($types)
    {
        if (!$types || empty($types)) {
            return $this;
        }
        
        $typesIds = [];
        if (is_array($types) && is_object(reset($types))) {
            foreach ($types as $type) {
                $typesIds[] = $type->id;
            }
        } else if (!is_array($types) && is_object($types)) {
            $typesIds[] = $types->id;
        } else if (is_array($types)) {
            $typesIds = array_values($types);
        } else if (intval($types) > 0) {
            $typesIds[] = intval($types);
        }
        
        
        $this->joinWith(['investorTypes']);
        return $this->andWhere(['type_id' => $typesIds]);
    }
    
    public function exclude($stories)
    {
        if (!$stories || empty($stories)) {
            return $this;
        }
        
        return $this->andWhere(['not in', 'success_story.id', $stories]);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\SuccessStory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\SuccessStory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
