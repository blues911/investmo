<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "api_methods".
 *
 * @property integer $id
 * @property string $title
 * @property integer $request_type
 * @property string $url
 * @property string $request
 * @property string $response
 */
class ApiMethods extends \yii\db\ActiveRecord
{

    const  REQUEST_TYPE_GET = 0;
    const  REQUEST_TYPE_POST = 1;
    const  REQUEST_TYPE_PUT = 2;
    const  REQUEST_TYPE_DELETE = 3;


    public static function getRequestType($key = false) {
        $answer = [
            static::REQUEST_TYPE_GET => 'GET',
            static::REQUEST_TYPE_POST => 'POST',
            static::REQUEST_TYPE_PUT => 'PUT',
            static::REQUEST_TYPE_DELETE => 'DELETE',

        ];

        if ($key) {
            return $answer[$key];
        } else {
            return $answer;
        }

    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_methods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['request_type'], 'integer'],
            [['request', 'response'], 'string'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'request_type' => 'Request Type',
            'url' => 'Url',
            'request' => 'Request',
            'response' => 'Response',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ApiMethodsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ApiMethodsQuery(get_called_class());
    }
}
