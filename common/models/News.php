<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property integer $is_active
 * @property string $short_desc
 * @property string $full_desc
 * @property string $img
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class News extends \yii\db\ActiveRecord
{
    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['short_desc', 'full_desc'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['created_at', 'updated_at'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['title', 'img'], 'string', 'max' => 255],
            [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'is_active' => 'Is Active',
            'short_desc' => 'Краткое описание',
            'full_desc' => 'Полный текст',
            'img' => 'Изображение',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата публикации',
            'deleted_at' => 'Дата удаления',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\NewsQuery(get_called_class());
    }

    public function upload($oldImage = null)
    {
        if ($this->validate()) {
            $imageName = md5(time());
            $dir = Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id;
            if (!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $this->img = PictureTool::saveFile($this->image, 'news', $this->id, ['original' => [690,425]], true)[0];
            $this->image = null;
            $this->save();
            if ($oldImage) {
                $this->removeImage($oldImage);
            }

            return true;
        } else {
            return false;
        }
    }

    public function removeImage($oldImage = null)
    {
        if ($oldImage) {
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id . "/" . $oldImage)) {
                unlink(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id . "/" . $oldImage);
            }
        } else {
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id . "/" . $this->img)) {
                unlink(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id . "/" . $this->img);
            }
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id)) {
                FileHelper::removeDirectory(Yii::getAlias('@frontend') . '/web/uploads/news/' . $this->id);
            }
        }

    }

    public function getFullImagePath($size = null)
    {
        $size = isset($size) ? $size.'/': null;
        return Yii::$app->urlManager->hostInfo.'/uploads/news/' . $this->id . '/' . $size.$this->img;
    }
    
    public static function getPeriodNews($news_feedback_sender_period, $feedback_id = null, $is_sent = null){
        $news = self::find()
            ->innerJoin('subscribe_email')
            ->innerJoin('news_feedback_sender_reg', '(news_feedback_sender_reg.feedback_id = subscribe_email.id)')
            ->innerJoin('news_feedback_sender_period', '(news_feedback_sender_period.id = news_feedback_sender_reg.news_feedback_sender_period)')
            ->where(['news_feedback_sender_period.id' => $news_feedback_sender_period])
            ->andWhere('news.created_at BETWEEN news_feedback_sender_period.start_date AND news_feedback_sender_period.end_date')
            ->andWhere(['news.is_active' => 1]);

        if($feedback_id)
            $news = $news->andWhere(['subscribe_email.id' => $feedback_id]);

        if ($is_sent !== null)
            $news = $news->andWhere(['news_feedback_sender_reg.is_sent' => $is_sent]);

        $news = $news->groupBy('news.id')
            ->orderBy('news.id DESC')
            ->limit(5)
            ->all();
        return $news;
    }
}
