<?php

namespace common\models;

use dstotijn\yii2jsv\JsonSchemaValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\models\FeedbackFile;

/**
 * FeedBack model
 * @property integer $id
 * @property string $name
 * @property string $data JSON string to validate
 * @property string $email
 * @property string $phone
 * @property \DateTime $created_at,
 * @property \DateTime $updated_at,
 * @property \DateTime $deleted_at,
 * @property boolean $is_deleted
 */
class FeedBack extends ActiveRecord
{
    public $files = [];
    
    const TYPE_ARGUE = 2;
    const TYPE_REQUEST = 1;
    const TYPE_QUESTION = 3;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'deleted_at', 'data'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['type'], 'integer'],
            [['name', 'email', 'lastname', 'middlename'], 'string', 'max' => 255, 'tooLong' => (Yii::$app->language == 'en') ? 'Maximum string length 255 chars' : 'Максимальная длинна строки 255 символов'],
            [['phone'], 'string', 'max' => 20, 'tooLong' => (Yii::$app->language == 'en') ? 'Maximum string length 20 chars' : 'Максимальная длинна строки 20 символов'],
            [['name', 'email', 'lastname', 'middlename', 'phone'], 'required', 'message' => (Yii::$app->language == 'en') ? 'Required field' : 'Поле обязательно для заполнения'],
            [['email'], 'email', 'message' => (Yii::$app->language == 'en') ? 'Incorrect e-mail' : 'Неверный адрес'],
            [['files'], 'safe'],
            [['files'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'jpg, jpeg, png, gif, doc, docx, pdf',
                'maxFiles' => 11
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'lastname' => 'Фамилия',
            'middlename' => 'Отчество',
            'data' => 'Data',
            'email' => 'Email',
            'phone' => 'Телефон',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'deleted_at' => 'Удалено',
            'is_deleted' => 'Удален',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(FeedbackFile::className(), ['feedback_id' => 'id']);
    }

    public function afterSave()
    {
        $this->uploadFiles($this->files, $this->id);
    }

    protected function uploadFiles($files, $feedbackID = null)
    {
        if (!empty($files)) {
            foreach ($files as $file) {
                $randomStr = substr(md5(mt_rand()), 0, 12);
                $item = new FeedbackFile;
                $item->feedback_id = $feedbackID;
                $item->file = $randomStr.'.'.$file->extension;
                $item->title = $file->baseName;
                $item->format = $file->extension;
                $item->size = $file->size;
                $item->mime = $file->type;
                $item->save(false);

                $dirPath = Yii::getAlias('@frontend').'/web/uploads/feedback_files/'.$feedbackID;
                $filePath = $dirPath.'/'.$randomStr.'.'.$file->extension;

                if (!is_dir($dirPath)) {
                    mkdir($dirPath, 0777, true);
                }

                $file->saveAs($filePath);
            }
        }
    }

    /**
     * @return bool
     */
    public function sendEmail()
    {
        if (isset(Yii::$app->params['email']) && isset(Yii::$app->params['emailFrom'])) {
            Yii::$app->mailer->compose('FeedBack-html', ['model' => $this])
                ->setTo(Yii::$app->params['email'])
                ->setFrom(Yii::$app->params['emailFrom'])
                ->setSubject('Новое обращение')
                ->send();
            Yii::$app->mailer->compose('FeedBack-confirm', ['model' => $this])
                ->setTo($this->email)
                ->setFrom(Yii::$app->params['emailFrom'])
                ->setSubject('Ваше обращение принято')
                ->send();
            return true;
        }
        return false;
    }
    private $additionalAttributes = [
        'pickup' => 'Предполагаемый объем инвестиций',
        'object' => 'Объект',
        'message' => 'Комментарий',
        'comment' => 'Комментарий',
        'description' => 'Описание',
        'document_type_1' => 'Тип документа',
        'document_type_2' => 'Тип документа',
        'requested_documents' => 'Необходимые документы',
        'support_type' => 'Тип поддержки',
        'branch' => 'Отрасль',
        'reason' => 'Причина обращения',
        'question' => 'Вопрос',
    ];
    
    public function getTextType()
    {
        switch ($this->type) {
            case self::TYPE_ARGUE:
                return 'Жалоба';
            case self::TYPE_QUESTION:
                return 'Вопрос';
            case self::TYPE_REQUEST:
                return 'Заявка';
        }
        
        return 'Не определен';
    }
    
    public function getDataTranslation($attr)
    {
        if (isset($this->additionalAttributes[$attr])) {
            return $this->additionalAttributes[$attr];
        }
        return null;
    }
}