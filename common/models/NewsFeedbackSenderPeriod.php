<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 24.07.2017
 * Time: 22:27
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class NewsFeedbackSenderPeriod extends ActiveRecord
{
    public static function getLastDate(){
        return self::find()->max('end_date');
    }
    public static function makeNewDate(){
        $new_date = new self();
        $new_date->end_date = date('Y-m-d H:i:s', time());
        $new_date->start_date = self::getLastDate();
        $new_date->save();
        return Yii::$app->db->lastInsertID;
    }
    public static function getPeriods($feedback_id = null, $is_sent = null){
        $periods = self::find()->select(['news_feedback_sender_period.*']);
        if($feedback_id || $is_sent !== null)
            $periods = $periods->innerJoin('news_feedback_sender_reg','(news_feedback_sender_reg.news_feedback_sender_period = news_feedback_sender_period.id)');
        if($feedback_id)
            $periods = $periods->where(['news_feedback_sender_reg.feedback_id' => $feedback_id]);
        if($is_sent !== null)
            $periods = $periods->andWhere(['news_feedback_sender_reg.is_sent' => $is_sent]);
        $periods = $periods->orderBy('news_feedback_sender_period.id DESC')->limit(100)->all();
        return $periods;
    }
}