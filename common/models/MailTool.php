<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 28.07.2017
 * Time: 18:11
 */

namespace common\models;

use Yii;

class MailTool
{
    public static function sendMail($email, $data, $html, $subject, $from = 'no-reply@dev.digitalwand.ru'){
        Yii::$app->mailer->compose($html, ['data' => $data])
            ->setTo($email)
            ->setFrom($from)
            ->setSubject($subject)
            ->send();
        return true;
    }
}