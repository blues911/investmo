<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 24.07.2017
 * Time: 22:56
 */

namespace common\models;


use yii\db\ActiveRecord;

class NewsFeedbackSenderReg extends ActiveRecord
{
    public static function tableName()
    {
        return 'news_feedback_sender_reg';
    }
    public static function getAllNotSentPeriods($feedback_id, $offset){
        return self::find()->where(['feedback_id' => $feedback_id, 'is_sent' => 0])->offset($offset)->limit(50)->all();
    }
}