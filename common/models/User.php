<?php
namespace common\models;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\IdentityInterface;
/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $authKey
 * @property string $password write-only password
 * @property string $first_name
 * @property string $last_name
 * @property string $third_name
 * @property string $company_name
 * @property string $phone
 * @property string $image
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $fio;
    public $password;
    public $password_repeat;

    const STATUS_DELETED = 0;
    const ROLE_USER = 10;
    const ROLE_MODERATOR = 20;
    const ROLE_ADMIN = 30;
    const ROLE_SUPERADMIN = 40;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
     public function scenarios()
     {
         return [
             'default' => ['email','username','first_name', 'last_name', 'third_name', 'status','phone','company_name'],
             'create' => ['email','username','first_name', 'last_name', 'third_name', 'status','phone','company_name','password_hash'],
             'profile' => ['last_name','first_name', 'third_name', 'company_name', 'phone', 'email', 'image'],
             'password' => ['password_hash'],
             'front_user' => ['email','fio', 'password', 'password_repeat'],
         ];
     }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот Email уже занят.'],
            [['username'], 'unique', 'targetClass' => '\common\models\User', 'message' => 'Этот логин уже занят.'],
            [['username','email'], 'required', 'on' => 'create'],
            ['status', 'default', 'value' => self::ROLE_USER, 'on' => 'create'],
            ['status', 'in', 'range' => [self::ROLE_ADMIN, self::ROLE_SUPERADMIN,self::ROLE_USER]],
            [['username', 'first_name', 'last_name', 'third_name'], 'string', 'max' => 24],
            ['phone', 'string'],
            ['company_name','string', 'max' => 255],
            ['password_hash', 'required', 'on' => 'password'],
            ['password_hash', 'required', 'on' => 'create'],
            [['image'],'file', 'extensions' => 'png, jpg, jpeg, gif', 'skipOnEmpty' => true],
            [['esia_id'], 'unique', 'skipOnEmpty' => true],
            ['password', 'compare', 'compareAttribute' => 'password_repeat', 'on' => 'front_user', 'skipOnEmpty' => false]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'email' => 'Почта',
            'status' => 'Статус',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'third_name' => 'Отчество',
            'company_name' => 'Компания',
            'phone' => 'Телефон',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'deleted_at' => 'Дата удаления',
        ];
    }
    public static function getStatus($key = null)
    {
        $answer = [
            static::ROLE_USER => 10,
            static::ROLE_ADMIN => 30
        ];
        if ($key) {
            return $answer[$key];
        } else {
            return $answer;
        }
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /**
     * @param $user
     */
    public function savePassword($user)
    {
        if ($user->validateUserPassword($this->user_password))
        {
            $user->setUserPassword($this->new_password);
            $user->save();
        }
    }
    public function getFullUserName(){
        $full_name = $this->last_name ? $this->last_name.' ':'';
        $full_name .= $this->first_name ? $this->first_name.' ':'';
        $full_name .= $this->third_name ? $this->third_name:'';
        return $full_name;
    }
    public function getFullImageUrl($size)
    {
        $img = UserPhoto::findOne($this->image);
        if($img){
            $path = Yii::getAlias('@frontend').'/web/uploads/users/'.$this->id."/".$size."/".$img->img;
            $file = '/uploads/users/'.$this->id."/".$size."/".$img->img;
            return (is_file($path)) ? $file : null;
        }
    }
    
    private function getFullNameByFIO()
    {
        if (!empty($this->fio)){
            $arr = $arr = explode(' ', $this->fio);
            if(!isset($arr[2]))
                $arr[2] = '';
            elseif (!isset($arr[1]))
                $arr[1] = '';
            elseif (!isset($arr[0]))
                $arr[0] = '';
            $this->last_name = $arr[0];
            $this->first_name = $arr[1];
            $this->third_name = $arr[2];
        }
    }

    private function getUsernameByEmail(){
        $arr = explode('@', $this->email);
        $this->username = array_shift($arr);
    }

    public function saveUser()
    {
        $time = time();
        $this->getFullNameByFIO();
        $this->getUsernameByEmail();
        $this->status = 0;
        $this->created_at = $time;
        $this->updated_at = $time;
        $this->setPassword($this->password);
        $this->generateAuthKey();
        
        if (!$this->validate()) {
            return null;
        }
        
        if ($this->save()) {
            return Yii::$app->db->lastInsertID;
        }
    }
    
    public static function findByEsia($id)
	{
		return static::findOne(['esia_id' => $id]);
	}
    public function getUserGroups(){
	    $user_query = (new Query())->select('group_id')->from('user_in_group')->where(['user_id' => $this->id]);
	    $groups = UserGroup::find()->where(['IN','id', $user_query])->all();
	    return $groups;
    }
    public static function checkSuperAdmin($user,$url = null, $type = null){
        if($user->status != 40 || Yii::$app->user->identity->status == 40){
            if($type)
                return Html::a('<span class="glyphicon '.$type.'"></span>', $url,['data-pjax'=>'0','data-method'=>'post',  'data-confirm' => $type == 'glyphicon-trash' ?'Вы уверены, что хотите удалить этот элемент?':null]);
            else
                return true;
        }else{
            return false;
        }
    }
}
