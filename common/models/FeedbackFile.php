<?php

namespace common\models;

use Yii;
use common\models\Feedback;

/**
 * This is the model class for table "{{%feedback_file}}".
 *
 * @property integer $id
 * @property integer $feedback_id
 * @property string $file
 * @property string $title
 * @property string $format
 * @property string $size
 * @property string $mime
 */
class FeedbackFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['feedback_id', 'file', 'title', 'format', 'size', 'mime'], 'required'],
            [['feedback_id'], 'integer'],
            [['file', 'title', 'format', 'size', 'mime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback ID',
            'file' => 'File',
            'title' => 'Title',
            'format' => 'Format',
            'size' => 'Size',
            'mime' => 'Mime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFeedback()
    {
        return $this->hasOne(Feedback::className(), ['id' => 'feedback_id']);
    }

    public function exists()
    {
        return file_exists(Yii::getAlias('@frontend').'/web/uploads/feedback_files/'.$this->feedback_id ."/".$this->file);
    }
}
