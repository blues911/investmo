<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%object_document}}".
 *
 * @property integer $id
 * @property integer $object_id
 * @property string $title
 * @property string $mime
 * @property string $size
 * @property string $created_at
 */
class ObjectDocument extends \yii\db\ActiveRecord
{
    public $files = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%object_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file', 'title', 'format', 'size', 'mime'], 'required'],
            [['object_id'], 'integer'],
            [['file', 'title', 'format', 'size', 'mime', 'tmp_id'], 'string', 'max' => 255],
            [['files'], 'safe'],
            [['files'], 'file', 'extensions' => 'doc, docx, pdf, ppt, pptx, rar, zip', 'maxFiles' => 10, 'skipOnEmpty' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Object ID',
            'file' => 'File',
            'title' => 'Title',
            'format' => 'Format',
            'size' => 'Size',
            'mime' => 'MIME',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Object::className(), ['id' => 'object_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ObjectDocumentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectDocumentQuery(get_called_class());
    }

    /**
     * Uploads files.
     * @param array $files
     * @param mix $id object id or 'false'
     */
    public function upload($files, $id)
    {
        if (!empty($files)) {
            foreach($files as $file) {
                $objectId = ($id == 'false') ? NULL : $id;
                $randomStr = substr(md5(mt_rand()), 0, 12);
                $tmpId = self::createTmpId();
                // create document
                $document = new self;
                $document->object_id = $objectId;
                $document->file = $randomStr.'.'.$file->extension;
                $document->title = $file->baseName;
                $document->format = $file->extension;
                $document->size = $file->size;
                $document->mime = $file->type;
                $document->tmp_id = ($objectId) ? NULL : $tmpId;
                $document->save(false);
                // move file
                $dirName = ($objectId) ?: $tmpId;
                $dirPath = Yii::getAlias('@frontend').'/web/uploads/objects_documents/'. $dirName;
                if (!is_dir($dirPath)) {
                    mkdir($dirPath, 0777, true);
                }
                $filePath = $dirPath.'/'.$randomStr.'.'.$file ->extension;
                $file->saveAs($filePath);

                return [
                    'initialPreview' => [
                        '<div class="file-preview-other"><span class="file-other-icon"><i class="glyphicon glyphicon-file"></i></span></div>'
                    ],
                    'initialPreviewConfig' => [
                        [
                            'caption' => $document->title, 
                            'size' => $document->size,
                            'url' => Url::to(['object/delete-document', 'id' => $document->id])
                        ],
                    ],
                    'append' => true
                ];
            }
        }
    }
    public static function uploadFromUrl($url, $id, $file_name = null){
        if(@fopen($url, "r")){
            $objectId = ($id == 'false') ? NULL : $id;
            $file = pathinfo($url);
            $randomStr = substr(md5(mt_rand()), 0, 12);
            $tmpId = self::createTmpId();
            $dirName = ($objectId) ?: $tmpId;
            $dirPath = Yii::getAlias('@frontend').'/web/uploads/objects_documents/'. $dirName;
            if (!is_dir($dirPath)) {
                mkdir($dirPath, 0777, true);
            }
            $filePath = $dirPath.'/'.$randomStr.'.'.$file['extension'];
            $file_content = InvestHelpers::getCurlData($url);
            file_put_contents($filePath, $file_content);
            // create document
            $document = new self;
            $document->object_id = $objectId;
            $document->file = $randomStr.'.'.$file['extension'];
            $document->title = $file_name ? $file_name : $file['filename'];
            $document->format = $file['extension'];
            $document->size = filesize($filePath);
            $document->mime = InvestHelpers::getMimeTypeByExtension($file['extension']);
            $document->tmp_id = ($objectId) ? NULL : $tmpId;
            $document->save(false);
        }
    }
    /**
     * Deletes document from DB and related file.
     * @param integer $id 
     */
    public function remove($id)
    {
        $document = $this->findOne($id);

        $dirName = ($document->object_id) ?: $document->tmp_id;
        $dirPath = Yii::getAlias('@frontend').'/web/uploads/objects_documents/'.$dirName;
        $filePath = $dirPath.'/'.$document->file;

        if (is_file($filePath)) {
            unlink($filePath);
        }

        $document->delete();
    }

    public static function createTmpId()
    {
        $session = Yii::$app->session;
        if (!$session->has('object_documents_tmp_id')) {
            $session->set('object_documents_tmp_id', strtotime(date("Y-m-d H:i:s")));
        }
        return $session->get('object_documents_tmp_id');
    }

    public function exists()
    {
        return file_exists(Yii::getAlias('@frontend').'/web/uploads/objects_documents/'.(($this->object_id) ?: $this->tmp_id)."/".$this->file);
    }
}
