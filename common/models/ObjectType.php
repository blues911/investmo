<?php

namespace common\models;

use Yii;
use common\models\Object;
use yii\db\Query;

/**
 * This is the model class for table "object_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property integer $is_container
 * @property integer $is_child
 *
 * @property Object[] $objects
 */
class ObjectType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_container', 'is_child'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 512],
            [['object_fields'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Символьный код',
            'description' => 'Описание',
            'is_container' => 'Может содержать другие объекты',
            'is_child' => 'Может быть вложен',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        // prepare object fields list
        $postFields = Yii::$app->request->post('objectFields');
        foreach ($postFields as $slug => $value) {
            if ($value == 'on') {
                $res_fields[$slug] = 1;
            } else {
                $res_fields[$slug] = 0;
            }
        }
        $this->object_fields = json_encode($res_fields);
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Object::className(), ['object_type_id' => 'id']);
    }


    public function getCountObjects()
    {
        return $this->hasMany(Object::className(), ['object_type_id' => 'id'])->active()->filterRequestStatus()->count();
    }

    public function getCountObjectsTypes($type1, $type2)
    {
        $parent = (new Query())
            ->select(['id'])
            ->from(Object::tableName())
            ->where(['object_type_id' => $type2]);
        return Object::find()->activeA()->where(['object_type_id' => $type1])->andWhere(['in', 'parent_id', $parent])->count();
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ObjectTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ObjectTypeQuery(get_called_class());
    }

    public static function getObjectFields()
    {
        $object = new Object();
        $fields = $object->attributeLabels();
        $excludeFields = [
            'id',
            'type',
            'created_at',
            'only_type_fields',
        ];
        foreach ($fields as $slug => $name) {
            if (in_array($slug, $excludeFields)) {
                unset($fields[$slug]);
            }
        }
        return $fields;
    }
    public static function getTypeNameList(){
        $types = self::find()->select(['id','name'])->asArray()->all();
        foreach ($types as $type)
            $list[$type['id']] = $type['name'];
        return $list;
    }
    public static function getTypeName($id){
        return self::findOne($id)->name;
    }
}
