<?php

namespace common\models;

use Yii;
use common\models\SuccessStory;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "investor_type".
 *
 * @property integer $id
 * @property string $img
 * @property string $text
 */
class InvestorType extends \yii\db\ActiveRecord
{
    public $count;
    public $measures;
    public $name;
    public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'investor_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'string'],
            [['img', 'page_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Картинка',
            'text' => 'Название',
            'page_link' => 'Ссылка на страницу'
        ];
    }

        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStories()
    {
        return $this->hasMany(SuccessStory::className(), ['id' => 'story_id'])->viaTable('story_investor_type', ['type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\InvestorTypeQuery(get_called_class());
    }
    public function getFullImagePath($size = null)
    {
        $size = isset($size) ? $size.'/': null;
        return Yii::$app->urlManager->hostInfo.'/uploads/investor_type/' . $this->id . '/' . $size.$this->img;
    }

    public function upload($oldImage = null)
    {
        if ($this->validate()) {
            $dir = Yii::getAlias('@frontend') . '/web/uploads/investor_type';
            if (!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $dir = Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id;
            if (!file_exists($dir)) {
                FileHelper::createDirectory($dir);
            }
            $this->img = PictureTool::saveFile($this->image, 'investor_type', $this->id, ['original' => [640,480]], true)[0];
            $this->image = null;
            $this->save();
            if ($oldImage) {
                $this->removeImage($oldImage);
            }

            return true;
        } else {
            return false;
        }
    }

    public function removeImage($oldImage = null)
    {
        if ($oldImage) {
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id . "/" . $oldImage)) {
                unlink(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id . "/" . $oldImage);
            }
        } else {
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id . "/" . $this->img)) {
                unlink(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id . "/" . $this->img);
            }
            if (file_exists(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id)) {
                FileHelper::removeDirectory(Yii::getAlias('@frontend') . '/web/uploads/investor_type/' . $this->id);
            }
        }

    }
}
