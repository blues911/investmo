<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.07.2017
 * Time: 21:23
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class NewsSenderPeriod extends ActiveRecord
{
    public static function getLastDate(){
        return self::find()->max('end_date');
    }
    public static function makeNewDate(){
        $new_date = new self();
        $new_date->end_date = date('Y-m-d H:i:s', time());
        $new_date->start_date = self::getLastDate();
        $new_date->save();
        return Yii::$app->db->lastInsertID;
    }
    public static function getPeriods($map_feed_back_id = null, $is_sent = null){
        $periods = self::find()->select(['news_sender_period.*']);
        if($map_feed_back_id || $is_sent !== null)
            $periods = $periods->innerJoin('news_sender_reg','(news_sender_reg.news_sender_period = news_sender_period.id)');
        if($map_feed_back_id)
            $periods = $periods->where(['news_sender_reg.map_feed_back_id' => $map_feed_back_id]);
        if($is_sent !== null)
            $periods = $periods->andWhere(['news_sender_reg.is_sent' => $is_sent]);
        $periods = $periods->orderBy('news_sender_period.id DESC')->limit(50)->all();
        return $periods;
    }
}