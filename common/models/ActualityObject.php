<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "actuality_object".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 */
class ActualityObject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'actuality_object';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['date'], 'safe'],
            [['status'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'status' => 'Статус подтверждения'
        ];
    }
    public static function checkUser($user_id){
        $date = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 30);
        $users = self::find()->where(['>','date',$date])->andWhere(['user_id' => $user_id])->count();
        if($users > 0)
            return false;
        return true;
    }
}