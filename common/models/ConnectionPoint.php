<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "connection_point".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $latitude
 * @property double $longitude
 * @property integer $status
 * @property integer $type
 * @property string $capacity
 */
class ConnectionPoint extends \yii\db\ActiveRecord
{
    const STATUS_PROCESSION = 0; //На обработке
    const STATUS_DRAFT = 1; //Черновик
    const STATUS_PUBLISHED = 2; //Опубликован

    const TYPE_ELECTRICITY = 1; //Технопарки и браунфилды
    const TYPE_GAS = 2; // Заводские цеха
    const TYPE_WATER = 3; // Склады
    const TYPE_WATER_REMOVAL = 4; // Земельный участок
    const TYPE_HEAT = 5; // Земельный участок

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'connection_point';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['latitude', 'longitude'], 'number'],
            [['status', 'type'], 'integer'],
            [['name'], 'string', 'max' => 511],
            [['capacity'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'latitude' => 'Широта',
            'longitude' => 'Долгота',
            'status' => 'Статус',
            'type' => 'Тип',
            'capacity' => 'Мощность',
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\queries\ConnectionPointQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\queries\ConnectionPointQuery(get_called_class());
    }

    public static function getStatus()
    {
        return [
            static::STATUS_PROCESSION => 'На обработке',
            static::STATUS_DRAFT => 'Черновик',
            static::STATUS_PUBLISHED => 'Опубликован'
        ];
    }

    public static function getType()
    {
        return [
            static::TYPE_ELECTRICITY => 'Электричество',
            static::TYPE_GAS => 'Газ',
            static::TYPE_WATER => 'Вода',
            static::TYPE_WATER_REMOVAL => 'Вода ',
            static::TYPE_HEAT => 'Отопление'
        ];
    }
}
