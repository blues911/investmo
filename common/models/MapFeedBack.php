<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
<<<<<<< HEAD
 * Date: 18.07.2017
 * Time: 14:02
=======
 * Date: 19.07.2017
 * Time: 12:21
>>>>>>> 12690_admin_feedback_sender
 */

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class MapFeedBack extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Почта',
            'min_square' => 'Мин.Площадь',
            'max_square' => 'Макс.Площадь',
            'min_bargain_price' => 'мин.Стоимость',
            'max_bargain_price' => 'макс.Стоимость',
            'min_distance_to_moscow' => 'Мин.Дистанция до Москвы, км',
            'max_distance_to_moscow' => 'Макс.Дистанция до Москвы, км',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления'
        ];
    }
    public static function tableName()
    {
        return '{{%map_feed_back}}';
    }
    public static function sendEmail($model, $theme = 'MapFeedBack-confirm')
    {
        if (isset(Yii::$app->params['adminEmail'])) {
            Yii::$app->mailer->compose('MapFeedBack-html',['model' => $model])
                ->setTo(Yii::$app->params['adminEmail'])
                ->setFrom(Yii::$app->params['emailFrom'])
                ->setSubject('Новая Подписка на обновление объектов ')
                ->send();
            Yii::$app->mailer->compose($theme,['model' => $model])
                ->setTo($model->email)
                ->setFrom(Yii::$app->params['emailFrom'])
                ->setSubject('Подписка на инвестиционные объекты оформлена')
                ->send();
            return true;
        }
        return false;
    }
    public static function sendNewsEmail($model,  $map_feed_back, $host, $theme = 'Object-Sender-Html')
    {
            Yii::$app->mailer->compose($theme,['model' => $model, 'host' => $host, 'map_feed_back' => $map_feed_back])
                ->setTo($map_feed_back->email)
                ->setFrom(Yii::$app->params['emailFrom'])
                ->setSubject('Новые инвестиционные объекты')
                ->send();
            return true;
    }
    public static function getCount(){
        return self::find()->count();
    }
}