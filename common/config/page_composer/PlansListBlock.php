<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 19:37
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\InfoField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;

class PlansListBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Планы и программы (перечень)');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new StringField('title', 'Подзаголовок'),
            new TextareaField('pointed', 'Описание'),
            new RepeaterField('points', 'Направления',[
                new TextareaField('text','Текст пункта'),
            ]),
            new InfoField('Вы можете либо указать ссылку на документ, либо загрузить документ. Если документ загружен, ссылка на документ будет игнорироваться.',null),
            new RepeaterField('files', 'Документы',[
                new StringField('file_name', 'Название документа'),
                new StringField('file_link_text', 'Ссылка на Документ'),
                new FileField('file_link', 'Файл'),
            ]),
        ];
    }
}