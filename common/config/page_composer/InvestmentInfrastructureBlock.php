<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 15:49
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\WidgetField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\SelectField;
use yii\helpers\ArrayHelper;
use common\models\ObjectType;

class InvestmentInfrastructureBlock extends AbstractBlock
{
    private $select;
    public function __construct()
    {

        $this->setName(__CLASS__);
        $this->setDescription('Составляющие инвестиционной инфраструктуры');
        $this->select = ArrayHelper::map(ObjectType::find()->all(), 'id', 'name');
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new WidgetField('name', 'Имя'),
            new RepeaterField('blocks','Блок',[
                new SelectField('object_type_id', 'Тип объекта', $this->select),
                new StringField('count','Количество'),
                new StringField('btn_text','Текст кнопки'),
                new StringField('btn_link','Ссылка'),
            ]),
        ];
    }

}