<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:41
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\InfoField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;
use common\modules\PageComposer\fields\WidgetField;

class InvestStrategyResultBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Результат инвест стратегии');
    }

    public function getFields()
    {
        return [
            new StringField('name', 'Заголовок'),
            new RepeaterField('results', 'Результаты', [
                new TextareaField('name', 'Текст пункта'),
            ]),
            new InfoField('Вы можете либо указать ссылку на документ, либо загрузить документ. Если документ загружен, ссылка на документ будет игнорироваться.',null),
            new RepeaterField('documents', 'Документы', [
                new StringField('file_name', 'Название документа'),
                new StringField('file_link_text', 'Ссылка на документ'),
                new FileField('file_link', 'Файл'),
            ]),
        ];
    }

}