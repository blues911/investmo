<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 15:01
 */

namespace common\config\page_composer;

use common\models\ObjectType;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\MultiselectField;
use common\models\InvestorType;
use yii\helpers\ArrayHelper;


class MapWidgetBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Карта объектов');
        $types = ObjectType::find()->all();
        $array = ArrayHelper::toArray($types);
        $this->select = ArrayHelper::map($array, 'id', 'name');

        parent::__construct();

    }

    public function getFields()
    {
        return [
            new MultiselectField('name', 'Имя', $this->select),
        ];
    }

}