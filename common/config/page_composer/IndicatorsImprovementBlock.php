<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:44
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;

class IndicatorsImprovementBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('История успеха - Улучшение показателей');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new StringField('start_year', 'Год запуска'),
            new StringField('current_year', 'Текущий год'),
            new RepeaterField('blocks', 'Блоки', [
                new StringField('measure', 'Единица измерения'),
                new StringField('previous', 'Предыдущие показатели'),
                new StringField('current', 'Текущие показатели'),
                new StringField('label', 'Подпись'),
                new StringField('height', 'Высота диаграммы'),

            ]),
        ];;
    }

}