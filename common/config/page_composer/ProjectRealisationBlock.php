<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 13:57
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\TextareaField;


class ProjectRealisationBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('История успеха - Реализация проекта');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new StringField('title', 'Подзаголовок'),
            new TextareaField('text', 'Текст'),
            new FileField('img', 'Изображение'),
        ];;
    }
}