<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 19:38
 */
$header = $block->getFieldValue('header');
$title = $block->getFieldValue('title');
$subtitle = $block->getFieldValue('subtitle');
$pointed = $block->getFieldValue('pointed');
$points = $block->getFieldValue('points');
$files = $block->getFieldValue('files');
?>
<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <h2 class="block-1__title"><b><?= $header ?></h2>
            <h4 class="block-1__sub-title"><?=$title ?></h4>

            <div class="block-1__text"><?= $subtitle ?></div>

            <h5 class="block-1__text block-1__text_sub-title"><?= $pointed ?></h5>

            <ul class="block-1__items">
                <?php foreach ($points as $point): ?>
                <li class="block-1__item">
                    <div class="block-1__text"> <?= $point['text'] ?></div>
                </li>
                <?php endforeach; ?>
            <?php if($files){ ?>
                <div class="download block-1__download">
                    <?php foreach ($files as $file): ?>
                        <div class="download__container">
                            <div class="download__icon-box"><span class="download__ico"></span></div>
                            <div class="download__link-box"><a class="download__link" href="<?= $file['file_link']? str_replace(Yii::getAlias('@frontend').'/web', '', $file['file_link']): $file['file_link_text']?>" target="_blank"><?= $file['file_name'] ?></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php }?>
        </div>
    </div>
</div>
