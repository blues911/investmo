<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 19:58
 */
$title = $block->getFieldValue('title');
$subtitle = $block->getFieldValue('subtitle');
$email = $block->getFieldValue('email');
$button = $block->getFieldValue('button');
?>
<div class="subscribe p-news__subscribe subscribe_js_inited">
    <div class="subscribe__container">
        <div class="subscribe__titles">
            <div class="subscribe__title"><?= $title ?>
            </div>
            <div class="subscribe__sub-title"><?= $subtitle ?>
            </div>
        </div>
        <form class="subscribe__form" novalidate="novalidate">
            <div class="subscribe__links">
                <a class="subscribe__link subscribe__link_rss" href="javascript:void(0)">RSS подписка</a>
            </div>
            <div class="subscribe__inputs">
                <input class="subscribe__input" placeholder="<?= $email ?>" name="form-email" type="text">
                <input class="subscribe__subscribe" value="<?= $button ?>" type="submit">
            </div>
        </form>
    </div>
</div>
