<?php

/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:38
 */
$title = $block->getFieldValue('title');
$subTitle = $block->getFieldValue('sub_title');
$strategies = $block->getFieldValue('strategies'); ?>

<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <p class="block-1__sub-title"><?= $title ?></p>

            <p class="block-1__sub-title">&nbsp;</p>

            <h5 class="block-1__text block-1__text_sub-title"><?= $subTitle ?></h5>
            &nbsp;

            <ul class="block-1__items">
                <?php foreach ($strategies as $strategy): ?>
                <li class="block-1__item">
                    <div class="block-1__text"><?= $strategy['name'] ?></div>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</div>
