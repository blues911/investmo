<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 20:01
 */
$title = $block->getFieldValue('title');
$email = $block->getFieldValue('email');
$phones = $block->getFieldValue('phones');
$site = $block->getFieldValue('site');?>
<div class="press p-news__press">
    <div class="press__container">
        <h3 class="press__title"><?= $title ?>
        </h3>
        <ul class="press__contacts">
            <li class="press__contact"><a class="press__link press__link_mail" href="mailto:<?= $email ?>"><?= $email ?></a>
            </li>
            <li class="press__contact"><a class="press__link press__link_phone" href="tel:<?= $phones ?>"><?= $phones ?></a>
            </li>
            <li class="press__contact"><a class="press__link press__link_web" href="<?= $site ?>"><?= $site ?></a>
            </li>
        </ul>
    </div>
</div>
