<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 21.06.2017
 * Time: 14:29
 */
$data = json_decode($block->data);?>

<div class="content content_theme_training">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?= $data->header ?>
            </h2>
            <div class="seminars p-business__seminars">
                <div class="seminars__items">

                    <?php foreach ($data->slides as $slide):?>
                    <div class="seminar seminars__item">
                        <div class="seminar__top">
                            <div class="seminar__date"><?= $slide->date; ?>
                            </div>
                            <div class="seminar__text seminar__text_next"><?= $slide->next_flow; ?>
                            </div>
                        </div>
                        <div class="seminar__content">
                            <div class="seminar__title"><?= $slide->title; ?>
                            </div>
                            <div class="seminar__address">
                                <div class="seminar__text seminar__text_address"><?= $slide->address; ?>
                                </div>
                            </div>
                            <div class="seminar__desc"><?= $slide->description; ?>
                            </div>
                            <ul class="seminar__items">
                                <li class="seminar__item seminar__item_time">
                                    <div class="seminar__text"><?= $slide->duration; ?>
                                    </div>
                                </li>
                                <li class="seminar__item seminar__item_peoples">
                                    <div class="seminar__text"><?= $slide->people_amount; ?>
                                    </div>
                                </li>
                                <li class="seminar__item seminar__item_man">
                                    <div class="seminar__text"><?= $slide->organization; ?>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php endforeach; ?>

                </div>
                <div class="seminars__bottom"><a class="seminars__btn" href="javascript:void(0)">Посмотреть все</a>
                </div>
            </div>
        </div>
    </div>
</div>
