<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 15:50
 */
use frontend\widgets\InvestmentInfrastructureWidget;
$blocks = $block->getFieldValue('blocks');
echo InvestmentInfrastructureWidget::widget(['blocks' => $blocks]);
?>