<?php

/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:09
 */
$data = json_decode($block->data);?>
<div class="numbers-box p-stories__numbers-box">

    <div class="numbers-box__items">

        <?php foreach ($data->result as $result): ?>
        <div class="numbers-box__item">
            <div class="numbers-box__number"><?= $result->num ?><div class="numbers-box__unit"><?= $result->num_small ?></div></div>

            <div class="numbers-box__text"><?= $result->title ?></div>
        </div>
        <?php endforeach; ?>
    </div>
</div>
