<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 15:29
 */
use frontend\widgets\MeasureSupportWidget;

$investor = $block->getFieldValue('investor_type');
$title = $block->getFieldValue('title');
echo MeasureSupportWidget::widget(['investor' => $investor, 'title' => $title]);