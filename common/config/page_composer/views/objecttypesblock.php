<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 11:00
 */
use common\models\Object;

$title = $block->getFieldValue('title');
$sliders = $block->getField('sliders')->getValueFields(); ?>

<div class="content content_theme_projects">
    <div class="container">
        <div class="content__inner-no_bottom_padding">
            <h2 class="content__title"><?= $title ?>
            </h2>
            <div class="projects">
                <div class="slider slider_theme_default projects__slider"
                     data-settings='{"slidesToShow":3,"responsive":[{"breakpoint":751,"settings":{"slidesToShow":1,"variableWidth":false}}]}'>
                    <?php foreach ($sliders as $slider): ?>
                        <?php $type = $slider['slider_type']->getValue();
                        $amount = Object::find()->where(['object_type_id' => $type])->active()->filterRequestStatus()->count();
                        if($amount > 1000){
                            $d= explode('.',$amount/1000);
                            $t = \Yii::$app->language == 'ru' ? 'тыс.': 'K';
                            $s = isset($d[1]) ? substr($d[1],0,1) : 0;
                            $value = $d[0].'.'.$s.'<span style="font-size: 1.875rem;">'.$t.'</span>';
                        }else{
                            $value = $amount;
                        }
                        ?>
                        <div class="projects__item">
                            <div class="project" style="background-image: url(<?= $slider['slider_img']->getPath() ?>)">
                                <div class="project__content">
                                    <h3 class="project__title">
                                        <strong><?= $value ?></strong><?= $slider['slider_title']->getValue() ?>
                                    </h3>
                                    <div class="project__bottom">
                                        <p class="project__subtitle"><?= $slider['slider_description']->getValue() ?>
                                        </p><a class="project__btn" href="/map?section=<?= $type ?>"><?= $slider['button_text']->getValue() ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
