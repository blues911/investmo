<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 *Time: 11:28
 */

$header = $block->getFieldValue('header');
$blocks = $block->getFieldValue('blocks');
?>


<div class="content content_theme_conditions">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?= $header ?></h2>

            <div class="conditions">
                <ul class="conditions__list">

                    <?php foreach ($blocks as $block): ?>
                    <li class="conditions__item"><svg class="icon icon_tower conditions__icon" height="41px" width="34px"> <use xlink:href="/svg-symbols.svg#tower" xmlns:xlink="http://www.w3.org/1999/xlink"></use> </svg>

                        <div class="conditions__content">
                            <h3 class="conditions__title"><?= $block['name'] ?></h3>
                            <p class="conditions__desc"><?= $block['description'] ?></p>
                            <a class="conditions__link" href="<?= $block['link'] ?>"><?= $block['link_text'] ?></a></div>
                    </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
</div>
