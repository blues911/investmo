<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 27.06.2017
 * Time: 13:20
 */
$text = $block->getFieldValue('text');
$documents = $block->getFieldValue('documents');?>

<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <div class="block-1__content-text">
                <p class="block-1__text block-1__text_description"><?= $text ?></p>
                <div class="download block-1__download">

                    <?php foreach ($documents as $document): ?>
                    <div class="download__container">
                        <div class="download__icon-box"><span class="download__ico"></span></div>
                        <div class="download__link-box">
                            <a class="download__link" href="<?= $document['link'] ?>"><?= $document['name'] ?></a>
                        </div>
                        <!--<div class="download__type"> .pdf</div>-->
                    </div>
                   <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div>
