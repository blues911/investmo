<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.12.2017
 * Time: 17:54
 */

$title = $block->getFieldValue('title');
$text = $block->getFieldValue('text');
$files = $block->getFieldValue('files'); ?>

<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <p class="block-1__sub-title"><?= $title ?></p>


            <h5 class="block-1__text block-1__text_sub-title"><?= $text ?></h5>

                <?php if($files){ ?>
                    <div class="download block-1__download">
                <?php foreach ($files as $file){ ?>
                    <div class="download__container">
                        <div class="download__icon-box"><span class="download__ico"></span></div>
                        <div class="download__link-box">
                            <a class="download__link" href="<?=$file['doc_link']?>" target="_blank"><?=$file['doc_name']?></a>
                        </div>
                    </div>
                <?php }; ?>
                    </div>
               <?php } ?>
        </div>
    </div>
</div>