<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:50
 */
$title = $block->getFieldValue('title');
$subTitle = $block->getFieldValue('sub_title');
$description = $block->getFieldValue('description');
$files = $block->getFieldValue('files');?>
<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <h2 class="block-1__title"><b><?= $title ?></b></h2>
            <h4 class="block-1__sub-title"><?= $subTitle ?></h4>

            <div style="font-weight: initial"><?= $description ?></div>
            <?php if($files){ ?>
                <div class="download block-1__download">
                    <?php foreach ($files as $file): ?>
                        <div class="download__container">
                            <div class="download__icon-box"><span class="download__ico"></span></div>
                            <div class="download__link-box"><a class="download__link" href="<?= $file['file_link']? str_replace(Yii::getAlias('@frontend').'/web', '', $file['file_link']): $file['file_link_text']?>" target="_blank"><?= $file['file_name'] ?></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php }?>

            </div>
        </div>
    </div>
</div>
