<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 18:23
 */
use frontend\widgets\NewsWidget;

?>
<div class="container">
    <div class="content content_theme_news">
        <?php echo NewsWidget::widget(); ?>
    </div>
</div>