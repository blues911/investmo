<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 13:59
 */$data = json_decode($block->data);
$img = end(explode('/',$data->img));?>

<div class="content-box p-stories__content-box">
    <div class="content-box__content">
        <div class="content-box__title"><?=$data->header ?></div>

        <div class="content-box__container">
            <div class="content-box__text-box">
                <div class="content-box__sub-title"><?=$data->title ?></div>

                <div class="content-box__text"><?=$data->text ?></div>

            </div>

            <div class="content-box__pic"><img alt="" class="content-box__img" role="presentation" src="/uploads/page_composer/<?=$img?>"></div>
        </div>
    </div>
</div>
