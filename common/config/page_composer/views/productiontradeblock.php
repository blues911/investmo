<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:34
 */
$data = json_decode($block->data);
$img = end(explode('/',$data->img));?>

<div class="content-box content-box_reverse p-stories__content-box">
    <div class="content-box__content">
        <div class="content-box__container">
            <div class="content-box__text-box">
                <div class="content-box__sub-title"><?= $data->header ?></div>

                <?php foreach ($data->blocks as $block): ?>
                    <div class="content-box__item">
                        <div class="content-box__head"><?= $block->title ?></div>
                        <div class="content-box__text"><?= $block->text ?></div>
                    </div>
                <?php endforeach; ?>

            </div>

            <div class="content-box__pic"><img alt="" class="content-box__img" role="presentation"
                                               src="/uploads/page_composer/<?=$img?>"></div>
        </div>
    </div>
</div>
