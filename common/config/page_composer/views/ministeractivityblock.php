<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:41
 */
$title = $block->getFieldValue('title');
$subtitle = $block->getFieldValue('subtitle');
$first_point = $block->getFieldValue('first_point');
$points = $block->getFieldValue('points');
$second_point = $block->getFieldValue('second_point');?>
<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <h2 class="block-1__title"><?= $title ?></h2>

            <h4 class="block-1__sub-title"><?= $subtitle ?></h4>
            &nbsp;

            <ul class="block-1__items">
                <li class="block-1__item">
                    <div class="block-1__text"><?= $first_point ?></div>
                    &nbsp;

                    <ul class="block-1__items">
                        <?php foreach ($points as $point): ?>
                        <li class="block-1__item">
                            <div class="block-1__text"><?= $point['sub_point'] ?></div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <li class="block-1__item">
                    <div class="block-1__text"><?= $second_point ?></div>
                </li>
            </ul>
        </div>
    </div>
</div>
