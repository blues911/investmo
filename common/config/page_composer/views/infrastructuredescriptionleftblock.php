<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:27
 */
$title = $block->getFieldValue('title');
$subTitle = $block->getFieldValue('sub_title');
$description = $block->getFieldValue('description');
$results = $block->getFieldValue('results');
$img = $block->getField('img')->getPath(); ?>
<div class="block-2 block-2_reverse">
    <div class="block-2__container">
        <div class="block-2__content">
            <h4 class="block-2__title"><?= $title ?></h4>

            <div class="block-2__content-text">
                <p class="block-2__text block-2__text_description"><?= $description ?></p>

                <ul class="block-2__items">
                    <?php foreach ($results as $result): ?>
                        <li class="block-2__item">
                            <div class="block-2__text"><?= $result['name']; ?></div>
                        </li>
                    <?php endforeach; ?>


                </ul>
            </div>
        </div>

        <div class="block-2__pic"><img alt="" class="block-2__img" role="presentation" src="<?= $img ?>"></div>
    </div>
</div>
