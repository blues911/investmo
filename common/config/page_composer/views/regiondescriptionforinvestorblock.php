<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 15:58
 */
$data = json_decode($block->data); ?>
<div class="container">
    <div class="info-box p-investor__info-box">
        <div class="info-box__content">
            <h2 class="info-box__title"><?= $data->title ?>
            </h2>
            <p class="info-box__description"><?= $data->subtitle ?>
            </p>
            <div class="info-box__items">

                <?php foreach ($data->regions as $region): ?>
                    <?php $img = end(explode('/',$region->img)) ?>
                    <div class="info-box__item">
                        <div class="info-box__item-content"><span class="info-box__ico"><img
                                        src="/uploads/page_composer/<?=$img?>" alt="" role="presentation"></span>
                            <div class="info-box__sub-title"><?= $region->name ?>
                            </div>
                            <div class="info-box__text"><?= $region->description ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
        <div class="info-box__bottom"><a class="btn btn_theme_light info-box__btn" href="<?=$data->btn_link?>"><span
                        class="btn__text"><?=$data->btn_text?></span></a>
        </div>
    </div>
</div>