<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 16:10
 */
$header = $block->getFieldValue('header');
$link = $block->getFieldValue('link');
$icon = $block->getField('icon')->getPath();

$name1 = $block->getFieldValue('name1');
$icon1 = $block->getField('icon1')->getPath();
$actions1 = $block->getFieldValue('actions1');


$name2 = $block->getFieldValue('name2');
$icon2 = $block->getField('icon2')->getPath();
$actions2 = $block->getFieldValue('actions2');

$name3 = $block->getFieldValue('name3');
$icon3 = $block->getField('icon3')->getPath();
$actions3 = $block->getFieldValue('actions3');


?>
<div class="sequence p-investor__sequence content__inner-padding_bottom">
    <div class="sequence__container">
        <div class="sequence__top">
            <h3 class="sequence__title"><?= $header ?>
            </h3>
            <div class="sequence__contacts">
                <div class="sequence__ico" style="background-image:url(/static/img/assets/sequence/i1.svg);"> </div>
                <a class="sequence__link sequence__link_contacts" href="<?= $link ?>">Посмотреть <br> контактные
                    <br> данные</a>
            </div>
        </div>
        <div class="sequence__steps">
            <div class="step sequence__step">
                <div class="step__number-box">
                    <div class="step__number">01
                    </div>
                    <div class="step__title">
                        <div class="step__desc"><?= $name1 ?>
                        </div>
                        <div class="step__ico step__ico_i1" style="background:url(<?= $icon2 ?>)">
                        </div>
                    </div>
                </div>
                <div class="step__content">
                    <?php foreach ($actions1 as $action): ?>

                        <div class="step__text"><?= $action['text'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="step sequence__step">
                <div class="step__number-box">
                    <div class="step__number">02
                    </div>
                    <div class="step__title">
                        <div class="step__desc"><?= $name2 ?>
                        </div>
                        <div class="step__ico step__ico_i1" style="background:url(<?= $icon3 ?>)">
                        </div>
                    </div>
                </div>
                <div class="step__content">
                    <?php foreach ($actions2 as $action): ?>
                        <div class="step__text"><?= $action['text'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="step sequence__step">
                <div class="step__number-box">
                    <div class="step__number">03
                    </div>
                    <div class="step__title">
                        <div class="step__desc"><?= $name3 ?>
                        </div>
                        <div class="step__ico step__ico_i1" style="background:url(<?= $icon2 ?>)">
                        </div>
                    </div>
                </div>
                <div class="step__content">
                    <?php foreach ($actions3 as $action): ?>
                        <div class="step__text"><?= $action['text'] ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
