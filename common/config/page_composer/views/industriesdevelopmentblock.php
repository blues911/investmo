<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 12:39
 */
use frontend\widgets\IndustriesDevelopmentWidget;
$blocks = $block->getFieldValue('content');
echo IndustriesDevelopmentWidget::widget(['blocks' => $blocks]);?>