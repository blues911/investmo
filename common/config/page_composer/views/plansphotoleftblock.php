<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 19:42
 */

$title = $block->getFieldValue('title');
$description = $block->getFieldValue('description');
$results = $block->getFieldValue('results');
$img = $block->getField('img')->getPath();?>
<div class="block-2 block-2_reverse content__box">
    <div class="block-2__container">
        <div class="block-2__content">
            <h2 class="block-1__title"><b><?= $title ?></b></h2>
            <div class="block-2__text"><?= $description ?>
            </div>

            <ul class="block-2__items">
                <?php foreach ($results as $result): ?>
                    <li class="block-2__item">
                        <div class="block-2__text"><?= $result['name']; ?></div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <div class="block-2__pic"><img alt="" class="block-2__img" role="presentation"
                                       src="<?= $img ?>">
        </div>
    </div>
</div>
