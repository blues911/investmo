<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 12:47
 */
$header = $block->getFieldValue('header');
$indicators = $block->getFieldValue('indicators');
?>
<div class="container">
    <div class="info-box p-business__info-box" id="info">
        <div class="info-box__content">
            <h2 class="info-box__title"><?= $header ?></h2>

            <div class="info-box__items">
                <?php foreach ($indicators as $indicator): ?>
                    <div class="info-box__item"><span
                                class="info-box__statistic"><b><?= $indicator['indicator_value'] ?></b> <span><?= $indicator['indicator_unit'] ?></span></span>

                        <div class="info-box__sub-title">
                            <?= $indicator['indicator_name'] ?>
                        </div>

                        <div class="info-box__text">
                            <?= $indicator['indicator_description'] ?>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </div>
</div>