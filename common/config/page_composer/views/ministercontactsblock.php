<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:44
 */
$name = $block->getFieldValue('name');
$position = $block->getFieldValue('position');
$contacts = $block->getFieldValue('contacts');
$address = $block->getFieldValue('address');
$img = $block->getField('img')->getPath();
?>
<div class="block-2 block-2_reverse">
    <div class="block-2__container">
        <div class="block-2__content">
            <h4 class="block-2__title"><?= $name ?></h4>

            <div class="block-2__content-text">
                <h5 class="block-2__text block-2__text_sub-title"><?= $position ?></h5>

                <p class="block-2__text block-2__text_description"><b>Контактная информация:</b><br>
                    <?= $contacts ?>
                    <br>
                    <b>Адрес:</b><br>
                    <?= $address ?></p>
            </div>
        </div>

        <div class="block-2__pic"><img alt="" class="block-2__img" role="presentation"
                                       src="<?= $img ?>"></div>
    </div>
</div>
