<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:09
 */

$title = $block->getFieldValue('title');
$strategies = $block->getField('strategies')->getValueFields();
$buttonName = $block->getFieldValue('button_text');
$buttonLink = $block->getFieldValue('button_link');
$buttonType = $block->getFieldValue('button_type');
//$icon = $block->getField('icon')->getPath();
?>
<div class="container">
    <div class="content__inner">
        <h2 class="content__title"><?= $title ?></h2>

        <div class="investclimate">
            <ul class="investclimate__list">
                <?php foreach ($strategies as $strategy): ?>
                <li class="investclimate__item">
                    <div class="investclimate__inner">
                        <h3 class="investclimate__title"><?= $strategy['name']->getValue() ?></b></h3>

                        <p class="investclimate__desc"><?= $strategy['description']->getValue() ?></p>

                        <ul class="investclimate__files">
                            <li class="investclimate__files-item"><a class="investclimate__file" href="<?= $strategy['document_link']->getValue()?>" target="_blank"><span class="investclimate__file-name"><?= $strategy['document_name']->getValue() ?><br>
		 &nbsp;</span></a></li>
                        </ul>
                    </div>
                </li>
                <?php endforeach; ?>

            </ul>
            <div class="investclimate__bottom"><a class="investclimate__btn <?= $buttonType ?>" href="<?= $buttonLink ?>" target="_blank"><?= $buttonName ?></a></div>
        </div>
    </div>
</div>
