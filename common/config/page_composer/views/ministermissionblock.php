<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:46
 */
$name = $block->getFieldValue('name');
$points = $block->getFieldValue('points');
?>
<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <h4 class="block-1__sub-title"><?= $name ?></h4>
            &nbsp;

            <ul class="block-1__items">

                <?php foreach ($points as $point): ?>
                <li class="block-1__item">
                    <div class="block-1__text"><?= $point['point'] ?></div>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</div>
