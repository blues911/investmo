<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:12
 */
$header = $block->getFieldValue('header');
$points = $block->getFieldValue('points');
$buttonText = $block->getFieldValue('button_text');
$buttonType = $block->getFieldValue('button_type');

?>
<div class="content content_theme_conditions" id="competition">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?= $header ?></h2>

            <div class="conditions">
                <ul class="conditions__list">
                    <?php foreach ($points as $point): ?>
                    <li class="conditions__item">
                        <div class="conditions__numeric">&nbsp;</div>

                        <div class="conditions__content">
                            <h3 class="conditions__title"><?= $point['point_name'];?> </h3>

                            <p class="conditions__desc"><?= $point['point_description'];?></p>
                            <a href="<?= $point['point_link'];?>"><?= $point['point_text'];?></a></div>
                    </li>
                    <?php endforeach; ?>

                </ul>
            </div>
            <div class="investclimate__bottom"><a class="investclimate__btn <?= $buttonType ?>" href="/page/konkur" target="_blank"><?= $buttonText ?></a></div>
        </div>
    </div>
</div>
