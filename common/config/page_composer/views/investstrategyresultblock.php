<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:41
 */
$name = $block->getFieldValue('name');
$results = $block->getFieldValue('results');
$documents = $block->getFieldValue('documents');
?>

<div class="block-1 block-1_mod">
    <div class="block-1__container">
        <div class="block-1__content">
            <h4 class="block-1__sub-title"><?= $name ?></h4>
            &nbsp;

            <ul class="block-1__items">

                <?php foreach ($results as $result): ?>
                    <li class="block-1__item">
                        <div class="block-1__text"><?= $result['name'] ?></div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php if($documents){ ?>
                <div class="download block-1__download">
                        <?php foreach ($documents as $document){ ?>
                            <div class="download__container">
                            <div class="download__icon-box"><span class="download__ico"></span>
                            </div>
                            <div class="download__link-box"><a class="download__link" href="<?= $document['file_link'] ? str_replace(Yii::getAlias('@frontend').'/web', '', $document['file_link']) : $document['file_link_text'] ?>"
                                                               target="_blank"><?= $document['file_name'] ?></a>
                            </div>
                    </div>
                       <?php } ?>
                </div>
            <?php } ?>

        </div>
    </div>
</div>
