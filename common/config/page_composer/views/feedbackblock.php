<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 13:20
 */
use frontend\widgets\FeedBackWidget;

$title = $block->getFieldValue('title');
$sub_title = $block->getFieldValue('sub_title');
$form_type = $block->getFieldValue('form_type');
$switchable = $block->getFieldValue('switchable');
$points = $block->getFieldValue('points');
?>


<div class="content content_theme_support">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><strong><?= $title ?>
            </h2>
            <div class="support">
                <div class="support__row">
                    <div class="support__left">
                        <p class="support__subtitle"><?= $sub_title ?>
                        </p>
                        <ul class="support__list">
                            <?php foreach ($points as $point): ?>
                                <li class="support__item">
                                    <div class="support__icon-wrap">
                                        <svg class="icon icon_search support__icon" width="42px" height="42px">
                                            <use xlink:href="<?= $point['icon']; ?>"></use>
                                        </svg>

                                    </div>
                                    <p class="support__text"><?= $point['description']; ?>
                                    </p>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                    <div class="support__right">
                        <?= FeedBackWidget::widget(['selector' => $switchable, 'type' => $form_type]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
