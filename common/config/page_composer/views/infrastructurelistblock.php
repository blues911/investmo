<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:24
 */
$title = $block->getFieldValue('title');
$subTitle = $block->getFieldValue('sub_title');
$points = $block->getFieldValue('points');
$results = $block->getFieldValue('created'); ?>
<div class="block-1">
    <div class="block-1__container">
        <div class="block-1__content">
            <h2 class="block-1__title"><?= $title ?></h2>

            <ul class="block-1__items">
                <?php foreach ($points as $point): ?>
                <li class="block-1__item">
                    <div class="block-1__text"><?= $point['name'] ?></div>
                </li>
                <?php endforeach; ?>

            </ul>

            <div class="block-1__badge"><?= $subTitle ?></div>

            <ul class="block-1__items">

                <?php foreach ($results as $result): ?>
                <li class="block-1__item">
                    <div class="block-1__text"><?= $result['name'] ?></div>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
