<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 13:39
 */
$data = json_decode($block->data);?>

<div class="items-box p-stories__items-box">
    <div class="items-box__container">
        <div class="items-box__title"><?= $data->header ?></div>

        <div class="items-box__items">

            <?php foreach ($data->measures as $measure): ?>
            <div class="items-box__item">
                <div class="items-box__sub-title"><?= $measure->title ?></div>

                <div class="items-box__mini-title">Получено:</div>

                <div class="items-box__text"><?= $measure->text ?></div>
                <a class="items-box__link" href="<?= $measure->link ?>">Подробнее о мере</a>
            </div>
            <?php endforeach; ?>


        </div>
    </div>
</div>
