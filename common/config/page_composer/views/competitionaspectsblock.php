<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 20:09
 */
$lists = $block->getFieldValue('list');
?>
<div class="container">
    <div class="content__inner">

        <div class="conditions">
            <ul class="conditions__list">

                <?php foreach ($lists as $list ): ?>
                <li class="conditions__item">
                    <div class="conditions__numeric">&nbsp;</div>
                    <div class="conditions__content">
                        <h3 class="conditions__title"><?= $list['list_title'] ?></h3>
                        <p class="conditions__desc"><?= $list['list_description'] ?></p>
                        <a href="<?= $list['list_link'] ?>">Подробнее</a></div>
                </li>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>
</div>
