<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 10:54
 */
use yii\helpers\Url;
$data = json_decode($block->data);
$img = end(explode('/',$data->photo));?>

<div class="content content_theme_quote">
    <div class="container">
        <div class="content__inner">
            <div class="content__left">
                <h2 class="content__title"><strong><?= $data->header ?></strong></h2>

                <p class="content__subtitle"><?= $data->cite ?></p>
                <a class="btn <?= $data->button_view ?>" href="<?= Url::to($data->button_link ,true)?>"><?= $data->button_text ?></a>
            </div>

            <div class="content__right">
                <figure><img alt="" class="content__img" role="presentation"
                             src="/uploads/page_composer/<?=$img?>">
                    <figcaption class="content__caption"><strong><?= $data->name ?></strong><em><?= $data->position ?></em></figcaption>
                </figure>
            </div>
        </div>
    </div>
</div>