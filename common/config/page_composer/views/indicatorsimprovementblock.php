<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:48
 */
$data = json_decode($block->data); ?>

<div class="diagrams-box p-stories__diagrams-box diagrams-box_js_inited">
    <div class="diagrams-box__container">
        <div class="diagrams-box__top">
            <div class="diagrams-box__title"><?= $data->header ?></div>

            <div class="diagrams-box__top-items">
                <div class="diagrams-box__top-item">
                    <div class="diagrams-box__top-color" style="background-color: #344a8e">&nbsp;</div>

                    <div class="diagrams-box__top-content">
                        <div class="diagrams-box__year"><?= $data->start_year ?></div>

                        <div class="diagrams-box__period">год запуска</div>
                    </div>
                </div>

                <div class="diagrams-box__top-item">
                    <div class="diagrams-box__top-color" style="background-color: #3777e8">&nbsp;</div>

                    <div class="diagrams-box__top-content">
                        <div class="diagrams-box__year"><?= $data->current_year ?></div>

                        <div class="diagrams-box__period">текущий год</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="diagrams-box__items">
            <?php foreach ($data->blocks as $block): ?>
            <div class="diagrams-box__item">
                <div class="diagrams-box__diagrams">
                    <div class="diagrams-box__diagram" data-height="<?=$block->height * 1.47?>" data-height-mobile="62"
                         style="background-color: rgb(52, 74, 142); height: 9.1875rem;">
                        <div class="diagrams-box__count"><?= $block->previous ?>
                            <div class="diagrams-box__unit"><?= $block->measure ?></div>
                        </div>
                    </div>

                    <div class="diagrams-box__diagram" data-height="147"
                         style="background-color: rgb(55, 119, 232); height: 9.1875rem;">
                        <div class="diagrams-box__count"><?= $block->current ?>
                            <div class="diagrams-box__unit"><?= $block->measure ?></div>
                        </div>
                    </div>
                </div>

                <div class="diagrams-box__text"><?= $block->label ?>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</div>
</div>
