<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 19:10
 */

use frontend\widgets\StoryWidget;

$investor = json_decode($block->data, true)['title']; ?>
<div class="container">
    <div class="content__inner">
<?php echo StoryWidget::widget(['investor' => $investor]); ?>
    </div>
</div>
