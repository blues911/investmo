<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:15
 */
$title = $block->getFieldValue('title');
$subTitle = $block->getFieldValue('sub_title');
$subTitle2 = $block->getFieldValue('sub_title2');
$figureValue = $block->getFieldValue('figure_value');
$figureUnit = $block->getFieldValue('figure_unit');
$figureName = $block->getFieldValue('figure_name');
$countries = $block->getField('countries')->getValueFields();
?>

<div class="content content_theme_collaboration hash-block" id="foreign">
    <div class="container">
        <div class="content__inner">
            <h2 class="content__title"><?= $title ?></h2>

            <div class="collaboration">
                <p class="collaboration__desc"><?= $subTitle ?></p>

                <h3 class="collaboration__title"><?= $subTitle2 ?></h3>

                <div class="investors collaboration__investors">
                    <div class="investors__container">
                        <div class="investors__left">
                            <div class="investors__percents">
                                <div class="investors__percents-val"><?= $figureValue ?></div>

                                <div class="investors__percents-title"><?= $figureName ?></div>
                            </div>

                            <ul class="investors__list">
                                <?php foreach ($countries as $country): ?>
                                    <li class="investors__item"><a class="investors__link"href="javascript:void(0)"><img al="" alt="" class="investors__flag" role="presentation" src="<?= $country['img']->getPath()?>"><span class="investors__country"><?= $country['name']->getValue()?> </span></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="investors__right"><img alt="" class="investors__map" role="presentation" src="/static/img/assets/investors/map.svg"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>