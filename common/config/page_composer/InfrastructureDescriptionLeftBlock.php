<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:27
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;

class InfrastructureDescriptionLeftBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Составляющие инфраструктуры (описание, фото слева)');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new TextareaField('description', 'Описание'),
            new FileField('img', 'Фото'),
            new RepeaterField('results', 'Результаты', [
                new TextareaField('name', 'Текст пункта')])

        ];
    }

}