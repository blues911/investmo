<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:32
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\TextareaField;


class ProductionTradeBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('История успеха - Произодство и реализация');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new FileField('img', 'Изображение'),
            new RepeaterField('blocks', 'Блоки', [
                new StringField('title', 'Заголовок пункта'),
                new TextareaField('text', 'Текст'),
            ]),

        ];;
    }

}