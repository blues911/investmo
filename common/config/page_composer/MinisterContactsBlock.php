<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:43
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\StringField;

class MinisterContactsBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Министр (контакты)');
    }

    public function getFields()
    {
        return [
            new StringField('name', 'Имя'),
            new StringField('position', 'Должность'),
            new StringField('contacts', 'Контакты'),
            new StringField('address', 'Адрес'),
            new FileField('img', 'Фото'),
        ];
    }

}