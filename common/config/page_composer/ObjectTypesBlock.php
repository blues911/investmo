<?php

/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 10:58
 */

namespace common\config\page_composer;

use common\models\Object;
use common\models\ObjectType;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\SelectField;
use common\modules\PageComposer\fields\FileField;
use yii\helpers\ArrayHelper;

class ObjectTypesBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Типы объектов (удобно выбрать, просто начать)');
        $this->select = ArrayHelper::map(ObjectType::find()->all(), 'id', 'name');
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new RepeaterField('sliders', 'Слайдер', [
                new StringField('slider_title', 'Заголовок Слайдера'),
                new SelectField('slider_type', 'Тип объекта', $this->select),
                new StringField('slider_description', 'Описание Слайдера'),
                new FileField('slider_img', 'Фото слайда'),
                new StringField('button_text', 'Текст кнопки'),
            ]),
        ];
    }

}