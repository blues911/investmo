<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:35
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\TextareaField;
use common\modules\PageComposer\fields\WidgetField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;

class MinisterActivityBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Деятельность Министерства');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('subtitle', 'Подзаголовок'),
            new TextareaField('first_point', 'Текст пункта'),
            new RepeaterField('points', 'Пункты',[
                new TextareaField('sub_point','Текст подпункта')
            ]),
            new StringField('second_point', 'Второй пункт'),
        ];
    }

}