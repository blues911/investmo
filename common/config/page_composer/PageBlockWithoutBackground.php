<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.12.2017
 * Time: 17:55
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\TextareaField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;

class PageBlockWithoutBackground extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Блок внутренней страницы (без фона)');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new TextareaField('text', 'Текст'),
            new RepeaterField('files', 'Файлы',[
                new StringField('doc_name', 'Имя файла'),
                new StringField('doc_link', 'Ссылка на файл'),
            ]),
        ];
    }
}