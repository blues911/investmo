<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 19:53
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;

class NewsSubscriptionBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Подписка на новости');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('subtitle', 'Подзаголовок'),
            new StringField('email', 'E-mail'),
            new StringField('button', 'Текст кнопки'),
        ];
    }

}