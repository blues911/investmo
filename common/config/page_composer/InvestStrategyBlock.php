<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:37
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;


class InvestStrategyBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Инвест стратегия (описание)');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('sub_title', 'Подзаголовок'),
            new RepeaterField('strategies', 'Файлы',[
                new TextareaField('name', 'Текст пункта'),
            ]),
        ];
    }

}