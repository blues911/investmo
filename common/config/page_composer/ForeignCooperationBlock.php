<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:14
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;


class ForeignCooperationBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Иностранное сотрудничество');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('sub_title', 'Подзаголовок'),
            new StringField('sub_title2', 'Подзаголовок 2'),
            new StringField('figure_value', 'Значение показателя (число инвестиций)'),
            new StringField('figure_name', 'Название показателя'),
            new RepeaterField('countries', 'Страны', [
                new StringField('name', 'Название'),
                new FileField('img', 'Флаг')]),
        ];
    }
}