<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:20
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\TextareaField;


class InfrastructureListBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Составляющие инфраструктуры (перечень)');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new RepeaterField('points', 'Составляюще', [
                new TextareaField('name', 'Текст пункта')]),
            new StringField('sub_title', 'Подзаголовок'),
            new RepeaterField('created', 'Создано', [
                new TextareaField('name', 'Текст пункта')])
        ];
    }
}