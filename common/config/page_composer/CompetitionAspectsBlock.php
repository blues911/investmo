<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 20:08
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;

class CompetitionAspectsBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Аспекты развития конкуренции');
    }

    public function getFields()
    {
        return [
            new RepeaterField('list', 'Список',
                [
                    new StringField('list_title', 'Заголовок'),
                    new StringField('list_description', 'Описание'),
                    new StringField('list_link', 'Ссылка'),
                ]),
        ];
    }
}