<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 15:46
 */

namespace common\config\page_composer;


use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\WidgetField;
use common\modules\PageComposer\config\AbstractBlock;

class RegionDescriptionForInvestorBlock extends AbstractBlock
{

    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Описание региона для типа инвестора');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('subtitle', 'Позаголовок'),
            new RepeaterField('regions', 'Области', [
                new StringField('name', 'Имя'),
                new StringField('description', 'Описание'),
                new FileField('img', 'Иконка')
            ]),
            new StringField('btn_text', 'Текст кнопки'),
            new StringField('btn_link', 'Ссылка кнопки'),
        ];
    }

}