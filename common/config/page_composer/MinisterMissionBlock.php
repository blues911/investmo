<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:45
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;

class MinisterMissionBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Задача Министерства');
    }

    public function getFields()
    {
        return [
            new StringField('name', 'Заголовок'),
            new RepeaterField('points', 'Пункты', [
                new TextareaField('point', 'Текст пункта')
            ]),
        ];
    }

}