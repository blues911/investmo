<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:06
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\SelectField;


class InvestSupportBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Поддержка инвестклимата');
        $this->select = ['btn_theme_light' => 'светлая тема', 'btn_theme_default' => 'темная тема'];
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new RepeaterField('strategies', 'Стратегии', [
                new StringField('name', 'Название'),
                new StringField('description', 'Описание'),
                new StringField('document_name', 'Имя документа'),
                new StringField('document_link', 'Ссылка документа'),
            ]),
            new StringField('button_text', 'Текст кнопки'),
            new StringField('button_link', 'Ссылка кнопки'),
            new SelectField('button_type', 'Тип кнопки', $this->select),
        ];
    }

}