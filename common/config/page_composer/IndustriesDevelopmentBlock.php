<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 12:38
 */

namespace common\config\page_composer;

use common\models\Direction;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\WidgetField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\SelectField;
use yii\helpers\ArrayHelper;

class IndustriesDevelopmentBlock extends AbstractBlock
{
    private $select;
    public function __construct()
    {


        $this->setName(__CLASS__);
        $this->setDescription('Развитие отраслей/Приоритетные направления');
        $types = Direction::find()->all();
        $array = ArrayHelper::toArray($types);
        $this->select = ArrayHelper::map($array, 'id', 'name');
        parent::__construct();
    }

    public function getFields()
    {
        return [

            new RepeaterField('content', 'Контент', [
                new SelectField('direction', 'Тип объекта', $this->select),
                new StringField('invest', 'Направление(список через ";")'),
            ])
        ];
    }

}