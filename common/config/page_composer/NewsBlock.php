<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 18:18
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\WidgetField;

class NewsBlock extends AbstractBlock
{

    public function __construct()
    {
        parent::__construct();
        
        $this->setName(__CLASS__);
        $this->setDescription('Новости инвестиций');
    }

    public function getFields()
    {
        return [
            new WidgetField('name', 'Имя'),
        ];
    }
}