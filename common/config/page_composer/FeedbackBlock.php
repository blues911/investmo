<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 11:38
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\CheckboxField;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\SelectField;
use common\modules\PageComposer\fields\StringField;
use Yii;


class FeedbackBlock extends AbstractBlock
{
    private $select = [];

    public function __construct()
    {

        $this->setName(__CLASS__);
        $this->setDescription('Обратная связь');
        $this->select = [1 => 'реализовать инвестиционный проект',
            2 => 'подать жалобу',
            3 => 'задать вопрос',];
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('sub_title', 'Подзаголовок'),
            new RepeaterField('points', 'Пункт', [
                new StringField('description', 'Описание'),
                new FileField('icon', 'Иконка')]),
            new SelectField('form_type', 'Тип формы', $this->select),
            new CheckboxField('switchable', 'Переключаемая форма'),

        ];
    }

}