<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 10:53
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\SelectField;

class QuoteBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Цитата (Область комфортного инвестклимата)');
        $this->select = ['btn_theme_light' => 'светлая тема', 'btn_theme_default' => 'темная тема'];
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new StringField('cite', 'Цитата'),
            new StringField('name', 'Имя'),
            new StringField('position', 'Должность'),
            new FileField('photo', 'Фото'),
            new StringField('button_text', 'Текст кнопки'),
            new StringField('button_link', 'Ссылка кнопки'),
            new SelectField('button_view', 'Тип кнопки', $this->select),
        ];
    }

}