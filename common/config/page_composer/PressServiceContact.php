<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 20:00
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;

class PressServiceContact extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Контакты пресс-службы');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('email', 'Email'),
            new StringField('phones', 'Телефоны'),
            new StringField('site', 'Веб сайт'),
        ];
    }

}