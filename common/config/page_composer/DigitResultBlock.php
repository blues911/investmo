<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 14:06
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\RepeaterField;


class DigitResultBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('История успеха - Результат в цифрах');
    }

    public function getFields()
    {
        return [
            new RepeaterField('result', 'Результаты', [
                new StringField('num', 'Число'),
                new StringField('num_small', 'Мелкий шрифт'),
                new StringField('title', 'Текст')])

        ];
    }

}