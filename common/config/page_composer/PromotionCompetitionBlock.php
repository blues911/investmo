<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 13:11
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\SelectField;


class PromotionCompetitionBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Содействие развитию конкуренции');
        $this->select = ['btn_theme_light' => 'светлая тема', 'btn_theme_default' => 'темная тема'];
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new RepeaterField('points', 'Пункты', [
                new StringField('point_name', 'Пункт списка: название'),
                new StringField('point_description', 'Пункт списка: описание'),
                new StringField('point_link', 'Пункт списка: ссылка для перехода'),
                new StringField('point_text', 'Пункт списка: текст кнопки для перехода'),
            ]),
            new StringField('button_text', 'Текст кнопки'),
            new SelectField('button_type', 'Тип кнопки', $this->select),
        ];
    }
}