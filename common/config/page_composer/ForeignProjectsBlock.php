<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 16.06.2017
 * Time: 14:44
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\WidgetField;

class ForeignProjectsBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Проекты с иностранным капиталом');
    }

    public function getFields()
    {
        return [
            new WidgetField('name', 'Имя'),
        ];
    }

}