<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 12:40
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;

class ProvidedSupportMeasuresBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('История успеха - Оказанные меры поддержки');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок блока'),
            new RepeaterField('measures', 'Меры', [
                new StringField('title', 'Заголовок пункта'),
                new TextareaField('text', 'Текст пункта'),
                new StringField('link', 'Ссылка'),
            ])
        ];;
    }
}