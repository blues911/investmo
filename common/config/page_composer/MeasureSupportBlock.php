<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 13:20
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\MultiselectField;
use common\modules\PageComposer\fields\SelectField;
use common\modules\PageComposer\fields\StringField;
use common\models\InvestorType;
use yii\helpers\ArrayHelper;


class MeasureSupportBlock extends AbstractBlock
{

    private $select;

    public function __construct()
    {       
        $this->setName(__CLASS__);
        $this->setDescription('Меры поддержки');
        $types = InvestorType::find()->asArray()->all();
        $this->select[0] = 'Все инвесторы';
        if($types){
            foreach ($types as $type)
                $this->select[] = $type['text'];
        }
        
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new MultiselectField('investor_type', 'Тип инвестора', $this->select),
        ];
    }
}