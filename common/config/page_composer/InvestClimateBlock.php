<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 14:49
 */
namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\InfoField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\TextareaField;


class InvestClimateBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Работы по улучшению инвестклимата');
    }

    public function getFields()
    {
        return [
            new StringField('title', 'Заголовок'),
            new StringField('sub_title', 'Подзаголовок'),
            new TextareaField('description', 'Описание'),
            new InfoField('Вы можете либо указать ссылку на документ, либо загрузить документ. Если документ загружен, ссылка на документ будет игнорироваться.',null),
            new RepeaterField('files', 'Файлы',[
                new StringField('file_name', 'Название документа'),
                new StringField('file_link_text', 'Ссылка на Документ'),
                new FileField('file_link', 'Файл'),
            ]),
        ];
    }
}