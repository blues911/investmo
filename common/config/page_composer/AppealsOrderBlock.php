<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 22.06.2017
 * Time: 16:08
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\FileField;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\WidgetField;

class AppealsOrderBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Порядок обращений');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new StringField('link', 'Ссылка для перехода к контактам'),
            new FileField('icon', 'Иконка'),

            new StringField('name1', 'Название шага 1'),
            new FileField('icon1', 'Иконка 1'),
            new RepeaterField('actions1','Перечень действий 1',[new StringField('text','Текст')]),

            new StringField('name2', 'Название шага 2'),
            new FileField('icon2', 'Иконка 2'),
            new RepeaterField('actions2','Перечень действий 2',[new StringField('text','Текст')]),

            new StringField('name3', 'Название шага 3'),
            new FileField('icon3', 'Иконка 3'),
            new RepeaterField('actions3','Перечень действий 3',[new StringField('text','Текст')]),

        ];
    }

}