<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 27.06.2017
 * Time: 13:18
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\RepeaterField;
use common\modules\PageComposer\fields\StringField;

class AuthorizedBodyBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Уполномоченный орган');
    }

    public function getFields()
    {
        return [
            new StringField('text', 'Текст'),
            new RepeaterField('documents', 'Список',
                [
                    new StringField('name', 'Название документа'),
                    new StringField('link', 'Ссылка документа'),
                ]),
        ];
    }

}