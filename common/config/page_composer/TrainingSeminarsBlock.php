<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 21.06.2017
 * Time: 14:20
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;

class TrainingSeminarsBlock extends AbstractBlock
{
    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Обучающие Семинары');
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new RepeaterField('slides', 'Слайды', [
                new StringField('date', 'Дата'),
                new StringField('next_flow', 'Следующий поток'),
                new StringField('title', 'Тайтл'),
                new StringField('address', 'Адрес'),
                new StringField('description', 'Описание'),
                new StringField('duration', 'Продолжительность'),
                new StringField('people_amount', 'Кол-во человек'),
                new StringField('organization', 'Организация'),
            ])
        ];
    }


}