<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 19:08
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\SelectField;
use common\models\InvestorType;
use yii\helpers\ArrayHelper;

class SuccessStoryBlock extends AbstractBlock
{
    private $select;

    public function __construct()
    {
        $this->setName(__CLASS__);
        $this->setDescription('Истории успеха');
        $types = InvestorType::find()->all();
        $array = ArrayHelper::toArray($types);
        $this->select = ArrayHelper::map($array, 'id', 'text');
        parent::__construct();
    }

    public function getFields()
    {
        return [
            new SelectField('title', 'Тип инвестора', $this->select)];
    }

}