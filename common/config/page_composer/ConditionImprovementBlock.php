<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 11:27
 */

namespace common\config\page_composer;
use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\WidgetField;
use common\modules\PageComposer\fields\RepeaterField;

class ConditionImprovementBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Улучшение условий');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new RepeaterField('blocks','Блок',[
                new StringField('name','Название'),
                new StringField('description','Описание'),
                new StringField('text','Текст'),
                new StringField('link_text','Текст ссылки'),
                new StringField('link','Ссылка'),
            ]),
        ];
    }
}