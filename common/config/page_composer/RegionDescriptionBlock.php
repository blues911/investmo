<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 15.06.2017
 * Time: 12:47
 */

namespace common\config\page_composer;

use common\modules\PageComposer\config\AbstractBlock;
use common\modules\PageComposer\fields\StringField;
use common\modules\PageComposer\fields\RepeaterField;


class RegionDescriptionBlock extends AbstractBlock
{
    public function __construct()
    {
        parent::__construct();

        $this->setName(__CLASS__);
        $this->setDescription('Описание региона');
    }

    public function getFields()
    {
        return [
            new StringField('header', 'Заголовок'),
            new RepeaterField('indicators', 'Показатели', [
                new StringField('indicator_value', 'Значение показателя'),
                new StringField('indicator_unit', 'Единица измерения показателя'),
                new StringField('indicator_name', 'Название показателя'),
                new StringField('indicator_description', 'Подпись к показателю'),
            ]),
        ];
    }
}