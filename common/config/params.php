<?php
return [
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'adminEmail' => 'admin@example.com',
    'email' => 'ShevchukES@mosreg.ru',
    'emailFrom' => 'noreply.investmo@gmail.com'
];
