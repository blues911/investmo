<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ObjectPhotoFixture extends ActiveFixture
{
    public $modelClass = 'common\models\ObjectPhoto';
    public $depends = ['common\fixtures\ObjectFixture'];
}