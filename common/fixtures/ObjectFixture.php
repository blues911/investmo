<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ObjectFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Object';
}
