<?php
return [
    'user1' => [
        'username' => 'user',
        'email' => 'user@yandex.ru',
        'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('user'),
        'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
        'status' => \common\models\User::ROLE_USER
    ],
    'user2' => [
        'username' => 'admin',
        'email' => 'admin@yandex.ru',
        'password_hash' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
        'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
        'status' => \common\models\User::ROLE_ADMIN
    ]
];