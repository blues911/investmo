<?php

use common\components\MoveFile;

return [
    [
        "id" => 1,
        "object_id" => 1,
        "img" => MoveFile::upload("1.jpg", 1),
        "tmp_id" => ""
    ],
    [
        "id" => 2,
        "object_id" => 2,
        "img" => MoveFile::upload("1.jpg", 2),
        "tmp_id" => ""
    ],
    [
        "id" => 3,
        "object_id" => 3,
        "img" => MoveFile::upload("1.jpg", 3),
        "tmp_id" => ""
    ],
    [
        "id" => 4,
        "object_id" => 4,
        "img" => MoveFile::upload("1.jpg", 4),
        "tmp_id" => ""
    ],
    [
        "id" => 5,
        "object_id" => 5,
        "img" => MoveFile::upload("1.jpg", 5),
        "tmp_id" => ""
    ],
    [
        "id" => 6,
        "object_id" => 6,
        "img" => MoveFile::upload("1.jpg", 6),
        "tmp_id" => ""
    ]
];