<?php
use yii\db\Expression;

return [
    [
        'title' => 'Инвесторам',
        'is_active' => true,
        'url' => '/investors',
        'sort' => '1',
        'created_at' => new Expression('NOW()'),
        'updated_at' => new Expression('NOW()'),
    ],
    [
        'title' => 'Девелоперам',
        'is_active' => true,
        'url' => '/developers',
        'sort' => '1',
        'created_at' => new Expression('NOW()'),
        'updated_at' => new Expression('NOW()'),
    ],
    [
        'title' => 'Владельцам',
        'is_active' => true,
        'url' => '/owners',
        'sort' => '1',
        'created_at' => new Expression('NOW()'),
        'updated_at' => new Expression('NOW()'),
    ],
    [
        'title' => 'Малому бизнессу',
        'is_active' => true,
        'url' => '/smallbissness',
        'sort' => '1',
        'created_at' => new Expression('NOW()'),
        'updated_at' => new Expression('NOW()'),
    ],
    [
        'title' => 'Об области',
        'is_active' => true,
        'url' => '/aboutregion',
        'sort' => '1',
        'created_at' => new Expression('NOW()'),
        'updated_at' => new Expression('NOW()'),
    ],
];