<?php

return [
    'Создать' => 'Создать',
    'The value must be a string.' => 'Значение должно быть строкой.',
    'Save' => 'Сохранить',
    'Cancel' => 'Отменить',
    'Update {modelClass}: ' => 'Update {modelClass}: ',
    '{property}: {message}.' => '{property}: {message}.',
];