<?php

return [
    'Delete' => 'Delete',
    'feed-back/create' => 'feed-back/create',
    'Are you sure you want to delete this item?' => 'Are you sure you want to delete this item?',
    'Update' => 'Update',
    'View' => 'View',
    'Success Stories' => 'Success Stories',
    'Create Success Story' => 'Create Success Story',
    'Create' => 'Create',
    'Search' => 'Search',
    'The value must be a string.' => 'The value must be a string.',
    '{n,number} is spelled as {n, spellout}' => '{n,number} is spelled as {n, spellout}',
    'The value must be a valid JSON string.' => 'The value must be a valid JSON string.',
    'hello' => 'hello',
    'Reset' => 'Reset',
    'Update {modelClass}: ' => 'Update {modelClass}: ',
    '{property}: {message}.' => '{property}: {message}.',
    'Save' => 'Save',
    'Cancel' => 'Cancel',
];