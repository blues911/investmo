<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SuccessStory */

$this->title = 'Update Success Story: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Success Stories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="success-story-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'blocks' => $blocks
    ]) ?>

</div>
