<?php
use yii\helpers\Html;
use common\modules\PageComposer\widgets\RenderFormWidget;

?>
<li class="form-block bg-success list-group-item" id="<?= $block->id ?>">
    <button type="button" class="close" id="block-delete" data-dismiss="modal" aria-hidden="true">×</button>
    <?= $block->block->getDescription(); ?><br>
    <? foreach ($block->getBlock()->fields as $key => $field) {
        echo $field->getFieldHtml('Page[blocks][' . $block->id . ']');
    } ?>
    <input type="hidden" class="form-control" name="Page[blocks][<?= $block->id ?>][type]>" value="<?= $block->type ?>">
    <input type="hidden" class="form-control" id="form-sort" name="Page[blocks][<?= $block->id ?>][sort]" value="<?= $block->sort ?>">
</li>