<?php

use common\models\SuccessStory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use backend\widgets\CKEditor\MyCKEditor;
use common\modules\PageComposer\widgets\BlockButtonWidget;

/* @var $this yii\web\View */
/* @var $model common\models\SuccessStory */
/* @var $form yii\widgets\ActiveForm */
use common\models\InvestorType;
use yii\helpers\ArrayHelper;
?>

<div class="success-story-form">

    <?php  $investorTypes = ArrayHelper::map(InvestorType::find()->all(),'id','text')  ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_active')->checkbox() ?>

    <?= $form->field($model, 'investor_type_id')->dropDownList($investorTypes) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php  if ($model->logo):?>
        <img src="<?= $model->getLogoImg() ?>" style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>

    <?= $form->field($model, 'logo')->fileInput(['accept' => 'image/*']) ?>

    <?php  if ($model->main_img):?>
        <img src="<?= $model->getMainImg() ?>" style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>
    
    <?= $form->field($model, 'main_img')->fileInput(['accept' => 'image/*']) ?>

    <?php  if ($model->background_img):?>
        <img src="<?= $model->getBackgroundImg() ?>" style="max-width: 100px; max-height: 100px;"/>
    <?php endif; ?>

    <?= $form->field($model, 'background_img')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'investments')->textInput() ?>

    <?= $form->field($model, 'currency')->radioList($model::CURRENCY) ?>

    <?= $form->field($model, 'date_of_start')->textInput() ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'investment_goal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'full_desc')->textarea(['rows' => 6]) ?>

    <?php if(false):?>
    <?= $form->field($model, 'html')->widget(MyCKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
        'customStyles' => [
            'page' => 'js/ckeditor_style.js'
        ],
        'clientOptions' => [
            'allowedContent' => true,
            'removeFormatAttributes' => '',
            //'extraPlugins' => 'youtube',
            'filebrowserUploadUrl' => Url::to(['/ckeditor/upload']),
        ],
    ]) ?>
    <?php endif; ?>

    <ul id="sortable" class="list-group">
        <?php if (isset($blocks)): ?>
            <?php foreach ($blocks as $block) : ?>
                <li class="form-block bg-success list-group-item" id="<?= $block->id ?>">
                    <button type="button" class="close" id="block-delete" data-dismiss="modal" aria-hidden="true">×</button>
                    <?= $block->block->getDescription(); ?><br>
                    <? foreach ($block->getBlock()->fields as $key => $field) {
                        echo $field->getFieldHtml('Page[blocks][' . $block->id . ']');
                    } ?>
                    <input type="hidden" class="form-control" name="Page[blocks][<?= $block->id ?>][type]>" value="<?= $block->type ?>">
                    <input type="hidden" class="form-control" id="form-sort" name="Page[blocks][<?= $block->id ?>][sort]" value="<?= $block->sort ?>">
                </li>
            <? endforeach; ?>
        <?php endif; ?>
    </ul>
    <?= BlockButtonWidget::widget() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
