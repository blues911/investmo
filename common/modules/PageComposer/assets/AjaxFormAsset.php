<?php

namespace common\modules\PageComposer\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AjaxFormAsset extends AssetBundle
{
    public $sourcePath = '@common';
    public $js = ['modules/PageComposer/web/js/ajax_form.js', 'modules/PageComposer/web/js/jquery-ui-1.12.1.custom/jquery-ui.min.js'];
    public $depends = ['yii\web\JqueryAsset'];

}
