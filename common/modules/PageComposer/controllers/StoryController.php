<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 19.06.2017
 * Time: 13:23
 */

namespace common\modules\PageComposer\controllers;

use Yii;
use common\models\SuccessStory;
use common\models\search\SuccessStorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use common\modules\PageComposer\models\PageBlock;
use common\modules\PageComposer\PageComposer;

class StoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SuccessStory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SuccessStorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SuccessStory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SuccessStory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SuccessStory();
        $logoImage = UploadedFile::getInstance($model, 'logo');
        $backgroundImage = UploadedFile::getInstance($model, 'background_img');
        $mainImage = UploadedFile::getInstance($model, 'main_img');

        if (!empty($logoImage)) {
            $oldLogoImage = $model->logo;
            $model->logoImage = UploadedFile::getInstance($model, 'logo');;
        }

        if (!empty($backgroundImage)) {
            $oldBackgroundImage = $model->background_img;
            $model->backgroundImage = UploadedFile::getInstance($model, 'background_img');;
        }

        if (!empty($mainImage)) {
            $oldMainImage = $model->main_img;
            $model->mainImage = UploadedFile::getInstance($model, 'main_img');;
        }

        if ($model->load(Yii::$app->request->post())) {

            if (empty($logoImage)) {
                unset($model->logo);
            } else {
                $model->upload($oldLogoImage);
            }

            if (empty($backgroundImage)) {
                unset($model->background_img);
            } else {
                $model->upload($oldBackgroundImage);
            }

            if (empty($mainImage)) {
                unset($model->main_img);
            } else {
                $model->upload($oldMainImage);
            }

            $model->save();

            if (isset($model->logoImage) || isset($model->backgroundImage)) {
                $model->upload();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SuccessStory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $logoImage = UploadedFile::getInstance($model, 'logo');
        $backgroundImage = UploadedFile::getInstance($model, 'background_img');
        $mainImage = UploadedFile::getInstance($model, 'main_img');

        $blocks = PageBlock::find()->where(['page_id' => $id])->andWhere(['class' => SuccessStory::className()])->orderBy('sort')->all();


        if (!empty($logoImage)) {
            $oldLogoImage = $model->logo;
            $model->logoImage = UploadedFile::getInstance($model, 'logo');;
        }

        if (!empty($backgroundImage)) {
            $oldBackgroundImage = $model->background_img;
            $model->backgroundImage = UploadedFile::getInstance($model, 'background_img');;
        }

        if (!empty($mainImage)) {
            $oldMainImage = $model->main_img;
            $model->mainImage = UploadedFile::getInstance($model, 'main_img');;
        }

        if ($model->load(Yii::$app->request->post())) {

            if (empty($logoImage)) {
                unset($model->logo);
            } else {
                $model->upload($oldLogoImage);
            }
            if (empty($backgroundImage)) {
                unset($model->background_img);
            } else {
                $model->upload($oldBackgroundImage);
            }
            if (empty($mainImage)) {
                unset($model->main_img);
            } else {
                $model->upload($oldMainImage);
            }

            $blocksRequest = Yii::$app->request->post('Page')['blocks'];
            if (isset($blocksRequest)) {
                foreach ($blocksRequest as $blockId => $data) {
                    $block = PageComposer::getInstance()->getBlock($blockId);
                    $block->setBlockValue($data);
                    $block->setFields($data, $model);
                    $block->page_id = $model->id;
                    $block->save();
                }
            }

            PageBlock::setHash($model->id, SuccessStory::className());

            $model->save();

            if (isset($model->logoImage) || isset($model->backgroundImage)) {
                $model->upload();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'blocks' => $blocks
            ]);
        }
    }

    /**
     * Deletes an existing SuccessStory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->removeImage();
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SuccessStory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SuccessStory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SuccessStory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRenderForm()
    {
        $class = Yii::$app->request->post('class');
        $pageBlock = new PageBlock($class);
        $pageBlock->class = SuccessStory::className();
        $pageBlock->save();
        return $this->renderAjax('renderform', [
            'block' => $pageBlock,
        ]);
    }

    public function actionDeleteBlock()
    {
        $blockId = Yii::$app->request->post('id');
        $block = PageBlock::findOne($blockId);
        $pageId = $block->page_id;
        $block->delete();
        return PageBlock::setHash($pageId, SuccessStory::className());
    }

}