<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 06.06.2017
 * Time: 14:36
 */

namespace common\modules\PageComposer\config;


abstract class AbstractBlock
{
    private $name;
    private $description;
    private $id;
    public $fields = [];

    abstract protected function getFields();
           
    public function __construct() {
        $this->createFields();
    }
    
    private function createFields()
    {
        foreach ($this->getFields() as $field) {
            $this->fields[$field->getName()] = $field;
        }
    }
        
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function setValue($value)
    {
        
        foreach ($this->fields as $key => $field) {
            if (isset($value[$key])) {
                $field->setValue($value[$key]);
            }
        }
    }
    
    public function getValue()
    {
        $result = [];
        foreach ($this->fields as $key => $field) {
            $result[$key] = $field->getValue();
        }
        
        return $result;
    }
    
    public function getFieldValue($field)
    {
        if (isset($this->fields[$field])) {
            return $this->fields[$field]->getValue();
        }
        
        return false;
    }

    public function getField($field)
    {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }

        return false;
    }
}