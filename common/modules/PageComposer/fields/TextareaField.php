<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.12.2017
 * Time: 18:23
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class TextareaField extends Field
{
    public function getFieldHtml($namePrefix = false)
    {
        return Html::label(
                $this->getCode(),
                $this->getName(),
                ['class' => 'control-label']
            )
            .
            Html::textarea(
                $namePrefix.'[' . $this->getName() . ']',
                $this->getValue(),
                ['class' => 'form-control res-v', 'rows' => 6]
            );
    }
}