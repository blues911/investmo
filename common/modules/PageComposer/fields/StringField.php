<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 05.06.2017
 * Time: 15:06
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class StringField extends Field
{
    public function getFieldHtml($namePrefix = false)
    {
        return Html::label(
            $this->getCode(),
            $this->getName(),
            ['class' => 'control-label pull-left']
        )
        .
        Html::textInput(
            $namePrefix.'[' . $this->getName() . ']',
            $this->getValue(),            
            ['class' => 'form-control']
        );
    }
}