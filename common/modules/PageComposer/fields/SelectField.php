<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 14.06.2017
 * Time: 13:47
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class SelectField extends Field
{
    private $select = [];


    function __construct($name, $code, $select, $value = false)
    {
        parent::__construct($name, $code, $value);

        if (is_array($select)) {
            foreach ($select as $key => $value) {
                $this->select[$key.''] = $value;
            }
        }
    }

    public function copy($withValue = false)
    {
        $class = get_called_class();
        if ($withValue) {
            return new $class($this->name, $this->code, $this->value);
        }
        return new $class($this->name, $this->code, $this->select);
    }

    public function getFieldHtml($namePrefix = false)
    {
        return
        Html::label(
            $this->getCode(),
            $this->getName(),
            ['class' => 'control-label pull-left']
        )
        .
        Html::dropDownList(
            $namePrefix.'['.$this->getName().']',
            $this->getValue(),
            $this->select,
            ['class' => 'form-control']
        );
    }

}