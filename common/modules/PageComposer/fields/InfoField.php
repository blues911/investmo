<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 20.12.2017
 * Time: 16:49
 */

namespace common\modules\PageComposer\fields;


use yii\helpers\Html;

class InfoField extends Field
{
    public function getFieldHtml($namePrefix = false)
    {
        return '<p>'.
                $this->getName().'</p>';
    }
}