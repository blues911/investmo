<?php

namespace common\modules\PageComposer\fields;
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 05.06.2017
 * Time: 15:02
 */
abstract class Field
{
    protected $name;
    protected $value;
    protected $code;
    protected $blockId;
    protected $json;

    function __construct($name, $code, $value = false)
    {
        $this->name = $name;
        $this->code = $code;
        $this->value = $value;
    }
    
    public function copy($withValue = false)
    {
        $class = get_called_class();
        if ($withValue) {
            return new $class($this->name, $this->code, $this->value);
        }
        return new $class($this->name, $this->code);
    }


    public function parseValue() {

    }
    /**
     * @return mixed
     */
    abstract public function getFieldHtml($namePrefix = false);


    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $blockId
     */
    public function setBlockId($blockId)
    {
        $this->blockId = $blockId;
    }

    /**
     * @return mixed
     */
    public function getBlockId()
    {
        return $this->blockId;
    }
}