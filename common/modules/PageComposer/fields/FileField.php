<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 09.06.2017
 * Time: 8:31
 */

namespace common\modules\PageComposer\fields;

use common\models\PictureTool;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use Yii;

class FileField extends Field
{
    public function setValue($value)
    {
        $namePrefix = $value;
        $uploadedFile = UploadedFile::getInstanceByName($value);
        if ($uploadedFile) {
            $this->uploadFile($uploadedFile, $namePrefix);
        } else if (empty($value) || $this->value && !file_exists($this->value)) {
            $this->value = null;
        } else if ($value == 'delete') {
            $this->deleteFile();
        } else if (file_exists($this->value)) {
            $this->value = $this->value;
        } else {
            $this->value = $value;
        }
        
        if (!file_exists($this->value) && $this->value) {
            $this->value = null;
        }
    }

    private function uploadFile(UploadedFile $uploadedFile, $namePrefix = false)
    {
        
        $dir = Yii::getAlias('@frontend').'/web/uploads/page_composer/';

        if (!file_exists($dir)) {
            FileHelper::createDirectory($dir);
        }
        
        $filename = md5(time().'_'.$namePrefix).'.'.$uploadedFile->extension;
        $filename = $dir.$filename;

        if ($filename != $this->value) {
            $this->deleteFile();
        }
        
        $uploadedFile->saveAs($filename);    
        
        $this->value = $filename;
    }
    
    private function deleteFile()
    {
        if (file_exists($this->value)) {
            unlink($this->value);
        }
    }
    
    public function getPath()
    {
        $value = $this->getValue();
        if (!$value) {
            return null;
        }
        
        return str_replace(Yii::getAlias('@frontend').'/web', '', $value);
    }
    
    public function getFieldHtml($namePrefix = false)
    {
        $ext = null;
        $file = null;
        if(file_exists($this->getValue())){
            $ext = strtolower(PictureTool::getExtension($this->getPath()));
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif' || $ext == 'bmp'){
                $file = Html::img($this->getPath(), ['style' => 'max-width: 100px; max-height: 100px;', 'data-name' => $namePrefix . '[' . $this->getName() . ']'])
                    .
                    '<span class="file-field-clear glyphicon glyphicon-remove" data-name="'.$namePrefix . '[' . $this->getName() . ']'.'"></span>';
            }else{
                $file = '<input type="text" disabled class="lite-font info-input" value="'.$this->getValue().'"/>';
            }
        }

        return Html::label(
                $this->getCode(),
                $this->getName(),
                ['class' => 'control-label pull-left']
            )
            .
            Html::hiddenInput(
                $namePrefix . '[' . $this->getName() . ']',
                $namePrefix . '[' . $this->getName() . ']',
                ['class' => 'form-control file-input-hidden']
            )
            .
            Html::fileInput(
                $namePrefix . '[' . $this->getName() . ']',
                $this->getValue(),
                ['class' => 'form-control file-input']
            ).
            $file;
    }
}