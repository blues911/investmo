<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 05.06.2017
 * Time: 15:16
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class RepeaterField extends Field
{
    private $fields = [];
    private $valueFields = [];

    function __construct($name, $code, $fields, $value = false)
    {
        parent::__construct($name, $code, $value);

        $this->fields = $fields;

    }

    private function createValuesFields()
    {
        $newItem = [];

        foreach ($this->fields as $key => $field) {
            $newItem[$field->getName()] = $field->copy();
        }
        return $newItem;
    }
    
    public function getValueFields()
    {
        return $this->valueFields;
    }
    
    
    public function getFields()
    {
        return $this->fields;
    }

    public function setFields($field, $code)
    {
        $this->fields[$code] = $field;
    }

    public function getFieldHtml($namePrefix = false)
    {
        if (empty($this->valueFields)) {
            $this->valueFields[0] = $this->createValuesFields();
        }
  
        $namePrefix .= '[' . $this->getName() . ']';
        $html = '';
        foreach ($this->valueFields as $key => $fields) {
            
            
            $html .= '<div class="repeater jumbotron">';
            $html .= Html::hiddenInput($namePrefix.'['.$key.'][remove-item]', 'false', ['class' => 'remove-item']);
            $html .= '<span class="remove-repeated-field pull-right glyphicon glyphicon-remove"></span>';
            foreach ($fields as $field) {
                $html .= $field->getFieldHtml($namePrefix.'['.$key.']');
            }
            if ($key == count($this->valueFields) - 1) {
                $html .= '<span class="add-repeated-field glyphicon glyphicon-plus"></span>';
            }
            $html .= '</div>';


        }
        
        return $html;
    }
    
    public function setValue($value)
    {
        foreach ($value as $key => $valueItem) {
            if (!isset($this->valueFields[$key])) {
                $this->valueFields[$key] = $this->createValuesFields();
            }
            
            if (isset($valueItem['remove-item']) && $valueItem['remove-item'] == 'true') {
                if (isset($this->valueFields[$key])) {
                    unset($this->valueFields[$key]);
                }
                continue;
            }
            
            foreach ($this->valueFields[$key] as $fieldKey => $field) {
                if (isset($valueItem[$fieldKey])) {
                    $this->valueFields[$key][$fieldKey]->setValue($valueItem[$fieldKey]);
                }
            }
        }        
    }
    
    public function getValue()
    {
        $result = [];
        foreach ($this->valueFields as $key => $fields) {
            $result[$key] = [];
            foreach ($fields as $fieldKey => $field) {
                $result[$key][$fieldKey] = $field->getValue();
            }
        }
        
        return $result;
    }
}
