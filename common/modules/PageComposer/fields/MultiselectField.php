<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 21.06.2017
 * Time: 12:47
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class MultiselectField extends Field
{
    private $select = [];

    function __construct($name, $code, $select, $value = false)
    {
        parent::__construct($name, $code, $value);

        if (is_array($select)) {
            foreach ($select as $key => $value) {
                $this->select[$key.''] = $value;
            }
        }
    }

    public function getFieldHtml($namePrefix = false)
    {
        return
        Html::label(
            $this->getCode(),
            $this->getName(),
            ['class' => 'control-label pull-left']
        )
        .
        Html::listBox(
            $namePrefix.'['.$this->getName().']',
            $this->getValue(),
            $this->select,
            ['class' => 'form-control','multiple' => 'true']
        );
    }

}