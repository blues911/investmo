<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 23.06.2017
 * Time: 14:20
 */

namespace common\modules\PageComposer\fields;

use yii\helpers\Html;

class CheckboxField extends Field
{
    public function getFieldHtml($namePrefix = false)
    {
        return Html::label(
                $this->getCode(),
                $this->getName(),
                ['class' => 'control-label pull-left']
            ) .
            Html::radioList(
                $namePrefix . '[' . $this->getName() . ']',
                [$this->getValue()],
                [1 => 'Да',
                    0 => 'Нет']
            );

    }

}