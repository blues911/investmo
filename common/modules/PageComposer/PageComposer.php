<?php

namespace common\modules\PageComposer;

use common\config\page_composer;
use common\modules\PageComposer\models\PageBlock;

/**
 * page_composer module definition class
 */
class PageComposer extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\modules\PageComposer\controllers';

    public $configFolder = '/config/page_composer';

    public $blocks = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $configFolder = dirname(dirname(__DIR__)) . $this->configFolder;

        $dir = scandir($configFolder);
        foreach ($dir as $block) {
            if (strpos($block, '.php')) {
                $className = 'common\config\page_composer\\' . str_replace('.php', '', $block);
                $this->blocks[$block] = new $className;
            }
        }
    }
    public function prepareBlockSelect()
    {
        if (!empty($this->blocks)) {
            $select = [];
            foreach ($this->blocks as $block) {
                $select[$block->getName()] = $block->getDescription();
            }
            return $select;
        } else {
            throwException('Create Block Classes first in ' . $this->configFolder);
        }

    }

    public function getBlock($blockId)
    {
        return PageBlock::findOne($blockId);
    }
}
