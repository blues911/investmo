<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 13.06.2017
 * Time: 15:47
 */

namespace common\modules\PageComposer\widgets;

use yii\base\Widget;


class BlockViewWidget extends Widget
{
    public $block;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        $template = strtolower(end(explode('\\', $this->block->type)));
        return $this->render('@common/config/page_composer/views/' . $template, ['block' => $this->block]);
    }

}