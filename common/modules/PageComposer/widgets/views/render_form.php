<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 06.06.2017
 * Time: 17:41
 */
use yii\helpers\Html;

$data = json_decode($block->data, true);
$object = new $block->type;
?>

<li class="list-group-item form-block bg-success" id="<?= $block->id ?>">
    <button id="block-delete" type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <?php foreach ($data as $key => $field): ?>
        <?php if (is_array($field)): ?>
            <?php $k = 1;
            $count = count($field); ?>
            <?php foreach ($field as $repeater): ?>
                <div class="repeater jumbotron">
                    <?php foreach ($repeater as $repeaterName => $repeaterField): ?>
                        <?php if ($repeaterName == 'img'): ?>
                            <div class="repeater-image">
                                <button type="button" class="close" id="image-delete" data-dismiss="modal"
                                        aria-hidden="true">×
                                </button><?= Html::img('/uploads/page_composer/' . $model->id . '/' . $block->id . '/' . $repeaterField) ?>
                            </div>
                            <label class="control-label" for="<?= $repeaterName ?>"><?= $repeaterName ?></label>
                            <input class="form-control"
                                   name="Page[blocks][<?= $block->id ?>][<?= $key ?>][<?= $k - 1 ?>][file]"
                                   value="<?= $repeaterField ?>" type="file">
                        <?php else: ?>
                            <label class="control-label" for="<?= $repeaterName ?>"><?= $repeaterName ?></label>
                            <input class="form-control"
                                   name="Page[blocks][<?= $block->id ?>][<?= $key ?>][<?= $k - 1 ?>][<?= $repeaterName ?>]"
                                   value="<?= $repeaterField ?>" type="">
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <?php if ($k == $count): ?>
                        <span class="add-repeated-field glyphicon glyphicon-plus"></span>
                    <?php endif;
                    $k++; ?>
                </div>
            <?php endforeach; ?>

        <?php else: ?>
            <label class="control-label" for="<?= $key ?>"><?= $key ?></label>
            <input class="form-control" name="Page[blocks][<?= $block->id ?>][<?= $key ?>]>"
                   value="<?= $field ?>"
                   type="name">
        <?php endif; ?>
    <?php endforeach; ?>
    <input class="form-control" name="Page[blocks][<?= $block->id ?>][type]>" value="<?= $block->type ?>">
    <input class="form-control" id="form-sort" name="Page[blocks][<?= $block->id ?>][sort]" value="<?= $block->sort ?>">
</li>

