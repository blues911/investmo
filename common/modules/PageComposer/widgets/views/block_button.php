<?php
/**
 * Created by PhpStorm.
 * User: Santa
 * Date: 06.06.2017
 * Time: 12:17
 */
use yii\helpers\Html;
use common\modules\PageComposer\assets\AjaxFormAsset;
use common\modules\PageComposer\PageComposer;

AjaxFormAsset::register($this);
$check_module = PageComposer::getInstance();
if($check_module != null){
    $select = $check_module->prepareBlockSelect();
}else{
    Yii::$app->loadedModules['PageComposer'] = new common\modules\PageComposer\PageComposer('PageComposer');
    $select = Yii::$app->loadedModules['PageComposer']->prepareBlockSelect();
}
asort($select);
reset($select);
?>
<div class="row form-group field-page-header required">
    <div class="col-lg-6">
        <?= Html::dropDownList('types', null, $select, ['id' => 'types-select', 'class' => 'form-control']) ?>
    </div>
    <div class="col-lg-6">
        <?= Html::submitButton('Добавить блок', ['class' => 'btn btn-success', 'id' => 'block-button', 'name' => 'block-button']) ?>  
    </div>
</div>