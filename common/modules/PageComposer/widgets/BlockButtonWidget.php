<?php

namespace common\modules\PageComposer\widgets;

use yii\base\Widget;

class BlockButtonWidget extends Widget
{

    public function init()
    {
        parent::init();

    }

    public function run()
    {
        return $this->render('block_button', []);
    }
}