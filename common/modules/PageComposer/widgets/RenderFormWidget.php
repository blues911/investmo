<?php

namespace common\modules\PageComposer\widgets;

use yii\base\Widget;

class RenderFormWidget extends Widget
{
    public $block;
    public $model;
    //private $namePrefix;

    public function init()
    {
        parent::init();

    }

    public function run()
    {
       return $this->render('render_form', ['block' => $this->block, 'model' => $this->model]);
    }
}