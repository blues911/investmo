$(document).ready(function () {

    //Adding new PageBlock
    $("#block-button").click(function (event) {
        event.preventDefault();
        var type = $("#types-select").val();
        $.ajax({
            url: 'render-form',
            data: {class: type},
            type: 'POST',
            dataType: 'html',
            success: function (html) {
                $('#sortable').append(html);
                $('#sortable li #form-sort').each(function (i) {
                    $(this).val(i);
                });
            }

        });
    });

    $(document).on('click', '.remove-repeated-field', function () {
        var $parent = $(this).parent();
        var $input = $parent.find('input.remove-item');
        $input.attr('value', 'true');
        
        $input.parent().after($input);
        
        var addSpan = false;
        if ($parent.find('.add-repeated-field').length == 1) {
            addSpan = true;
        }

        if ($parent.parent().find('div.repeater').length == 1) {
            $parent.parent().find('.add-repeated-field').click();
        }
        
        if (addSpan) {
            $parent.parent().find('div.repeater').eq(-2).append('<span class="add-repeated-field glyphicon glyphicon-plus"></span>');
        }
        
        $(this).parent().remove();
    });
    
    $(document).on('click', '.file-field-clear', function () {
        var $parent = $(this).parent();
        var name = $(this).attr('data-name');
        
        $parent.find('input[name="' + name +'"]').attr('value', 'delete');
        $parent.find('img[data-name="' + name +'"]').remove();
        $(this).remove();
    });
    

    //Adding repeater field
    $(document).on('click', '.add-repeated-field', function () {
        var repeater = $(this).parent().html();
        var numMatches = repeater.match(/Page\[blocks\]\[[0-9]+\]\[[\w]+\]\[([0-9]+)\]\[[\w]+\]/);
        num = +numMatches[1] + 1;
        repeater = repeater.replace(/(Page\[blocks\]\[[0-9]+\]\[[\w]+\]\[)([0-9]+)(\]\[[\w]+\])/g, "$1" + num + "$3");
        //repeater = repeater.replace(/(value=")(?:[^"](?:\\")?)*(")/g, "$1" + "$2");
        repeater = repeater.replace(/(<img[^>]*>)/g, "");
        repeater = '<div class ="repeater jumbotron">' + repeater + '</div>';
        var $repeater = $(repeater);
        $repeater.find('input').each(function () {
            var $this = $(this);
            if ($this.hasClass('file-input-hidden')) {
                $this.attr('value', $this.attr('name'));
            } else {
                $this.attr('value', '');
            }
        });
        
        $(this).parent().after($repeater);
        $(this).remove();
    });


    //Sorting of PageBlocks
    $(function () {
        $("#sortable").sortable({
            beforeStop: function (event, ui) {
                var order = $('#sortable').sortable("toArray");
                for (var i = 0; i < order.length; i++) {
                    $('#' + order[i] + ' #form-sort').val(i);
                }
            }
        });
        $("#sortable").disableSelection();
    });


    //Ajax-deletion of PageBlock
    $(document).on('click', '#block-delete', function () {
        var id = $(this).parent().attr('id');
        $.ajax({
            url: 'delete-block',
            data: {id: id},
            type: 'POST',
            dataType: 'html',
            success: function (html) {
                $('#' + id).remove();
                $('#sortable li #form-sort').each(function (i) {
                    $(this).val(i);
                });
            }
        });
    });

    //Ajax-deletion of Image
    $(document).on('click', '#image-delete', function () {
        var id = $(this).parents('.form-block').attr('id');
        var repeaterImg = $(this).parent();
        var img = $(this).next().attr('src');
        var imageName = img.split('/');
        imageName = imageName[imageName.length - 1];
        $.ajax({
            url: 'delete-image',
            data: {
                blockId: id,
                imageName: imageName
            },
            type: 'POST',
            dataType: 'html',
            success: function (html) {
                $(repeaterImg).remove();
            }
        });
    });

});
