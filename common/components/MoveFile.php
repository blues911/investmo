<?php

namespace common\components;

class MoveFile {
    public static function upload($file, $id)
    {
        $pathfile = pathinfo($file);
        $dir = __DIR__ . '\\..\\fixtures\\data\\photo\\';
        $dir_out = __DIR__ . '\\..\\..\\frontend\\web\\uploads\\objects\\' . $id;
        if (!is_dir($dir_out)) {
            mkdir($dir_out);
        }
        $hash = md5(time());
        $file_out = $hash . '.' . $pathfile['extension'];
        if(copy($dir . '\\' . $file, $dir_out . '\\' . $file_out));
        return $file_out;
    }
}