<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 24.07.2017
 * Time: 19:45
 */
$formatter = \Yii::$app->formatter;
$i = 0; foreach ($model as $new){
    if($i < 1)
        $top_new = $new;
    else $news[] = $new;
        $i++;
}
$top_new_title = explode(' ', $top_new->title);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        @media screen and (max-width: 659px) {
            .news__cnt {
                display: block !important;
                float: none !important;
                width: 100% !important;
                padding-left: 0;
            }
            .news__pic {
                display: block !important;
                float: none !important;
                width: 100% !important;
            }
            .btn_light {
                display: block !important;
                margin: 0 auto !important;
                max-width: 320px;
                text-align: center;
            }
        }


        @media screen and (min-width: 660px) {
            .sitename {
                font-size: 19px !important;
            }
            .newAction span {
                font-size: 16px !important;
            }
            .hero__cnt .h1 {
                padding-right: 190px !important;
            }
        }
    </style>
</head>

<body style="background-color: #f7f8fa; font-family: Verdana, Geneva, Tahoma, sans-serif; margin: 0;">
<div class="header" style="-ms-text-size-adjust: 100%; -webkit-font-smoothing: subpixel-antialiased; -webkit-text-size-adjust: 100%; background-color: #f7f8fa; border: 0; margin: 0 10px; padding-bottom: 20px; padding-top: 20px; table-layout: fixed; text-align: center;">
    <!--[if (gte mso 9)|(IE)]>
    <table width="600" align="center">
        <tr>
            <td>
    <![endif]-->
    <table class="outer" cellpadding="0" align="center" style="background-color: #fff; border-collapse: collapse; margin: 0 auto; max-width: 820px; width: 100%;">
        <tr>
            <td class="one-column preheader" style="-webkit-font-smoothing: subpixel-antialiased; font-size: 0; text-align: center;">
                <div class="column webmail" style="-webkit-font-smoothing: subpixel-antialiased; background-color: #f7f8fa; display: inline-block; font-size: 14px; max-width: 820px; text-align: left; vertical-align: top; width: 100%;">
                    <table width="100%" cellpadding="10" style="border-collapse: collapse;">
                        <tr>
                            <td style="-webkit-font-smoothing: subpixel-antialiased;">
                                <div class="paragraph" style="-webkit-font-smoothing: subpixel-antialiased; color: #939393; font-size: 14px; font-weight: 300; line-height: 20px; text-align: center;">
                                    Если письмо отображается некорректно, нажмите <a href="http://google.com" target="_blank" style="color: #3777e8 !important; text-decoration: none !important;">здесь</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
</div>

<div class="middle" style="-ms-text-size-adjust: 100%; -webkit-font-smoothing: subpixel-antialiased; -webkit-text-size-adjust: 100%; background-color: #f7f8fa; border: 0; margin: 0 10px; padding-bottom: 33px; table-layout: fixed; text-align: left;">
    <!--[if (gte mso 9)|(IE)]>
    <table width="600" align="center">
        <tr>
            <td>
    <![endif]-->
    <table class="outer" cellpadding="0" align="center" style="background-color: #fff; border-collapse: collapse; box-shadow: 0 10px 30px rgba(47, 64, 76, 0.15); margin: 0 auto; max-width: 820px; width: 100%;">
        <tr>
            <td class="one-column" style="-webkit-font-smoothing: subpixel-antialiased; font-size: 0; text-align: center;">
                <div class="column" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; font-size: 14px; max-width: 820px; padding-bottom: 13px; padding-top: 10px; text-align: left; vertical-align: top; width: 100%;">
                    <table width="100%" cellpadding="0" style="border-collapse: collapse;">
                        <tr>
                            <td style="-webkit-font-smoothing: subpixel-antialiased; padding: 0 3%;">
                                <div class="header__wrap" style="-webkit-font-smoothing: subpixel-antialiased;">
                                    <div class="header__logo" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; min-width: 300px; vertical-align: middle; width: 60%;">
                                        <a class="logo" href="<?=$host?>" target="_blank" style="color: #3777e8 !important; display: inline-block; text-decoration: none !important;">
                                            <img src="<?=$host?>/logo.jpg" width="36" height="50" alt="" style="border: 0;"/>
                                        </a>
                                        <div class="header-title" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; padding-left: 10px;">
                                            <span class="sitename" style="color: #282828; display: block; font-size: 16px; font-weight: 700;">Инвестиционный портал </span>
                                            <span class="sitesubname" style="color: #282828; display: block; font-size: 12px; font-weight: 400;">Московской области</span>
                                        </div>
                                    </div>
                                    <div class="newAction" style="-webkit-font-smoothing: subpixel-antialiased; color: #000000; display: inline-block; font-size: 14px; font-weight: 300; margin: 15px 0; min-width: 165px; padding-right: 2%; text-align: right; vertical-align: middle; width: 35%;">
                                        <img width="18" src="<?=$host?>/romb.jpg" alt="" style="display: inline-block; vertical-align: middle;">
                                        <span style="display: inline-block; margin-left: 9px; vertical-align: middle;">Новые события</span>
                                        </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="cover" style="-webkit-font-smoothing: subpixel-antialiased;">
                <table cellpadding="0" cellspacing="0" border="0" height="440" width="100%" style="border-collapse: collapse;">
                    <tr>
                        <td background="<?= $host.'/uploads/news/' . $top_new->id . '/' . $top_new->img ?>" bgcolor="#fffcf3" valign="top" style="-webkit-font-smoothing: subpixel-antialiased;background-size: cover">
                            <!--[if gte mso 9]>
                            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="mso-width-percent:1000;height:440px;">
                                <v:fill type="tile" src="<?= $host.'/uploads/news/' . $top_new->id . '/' . $top_new->img ?>" color="#fffcf3" />
                                <v:textbox inset="0,0,0,0">
                            <![endif]-->
                            <div class="hero__cnt" style="-webkit-font-smoothing: subpixel-antialiased; padding-left: 5%; padding-right: 5%; padding-top: 90px;">
                                <span class="hero__date" style="color: #282828; font-size: 16px; font-weight: 300;"><?= $formatter->asDate($top_new->created_at, 'dd  MMMM yyyy') ?></span>
                                <h1 class="h1" style="color: #000000; font-size: 27px; font-weight: 400; line-height: 40px; margin-bottom: 35px;"><b style="font-weight: 600;"><?= implode(' ',array_slice($top_new_title,0,3)) ?></b><br><?= implode(' ',array_slice($top_new_title,3)) ?></br></h1>
                                <a class="btn btn_main" href="<?=$host.'/news/'.$top_new->id?>" style="background-color: #3777e8; box-shadow: 0 6px 16px rgba(55, 119, 232, 0.6); color: #ffffff !important; display: inline-block; font-size: 14px; font-weight: 400; margin: auto; padding: 17px 8.5%; text-align: center; text-decoration: none !important; text-transform: uppercase; width: auto !important;">Подробнее</a>
                            </div>
                            <!--[if gte mso 9]>
                            </v:textbox>
                            </v:rect>
                            <![endif]-->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="one-column news-list" style="-webkit-font-smoothing: subpixel-antialiased; font-size: 0; padding: 40px 3% 30px; text-align: center;">
                <!--[if (gte mso 9)|(IE)]>
                <table width="100%">
                    <tr>
                        <td width="50%" valign="top">
                <![endif]-->
                <?php if(isset($news)){
                    foreach ($news as $new){ ?>
                    <div class="column" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; font-size: 14px; margin-bottom: 38px; max-width: 820px; text-align: left; vertical-align: top; width: 100%;">
                        <table width="100%" style="border-collapse: collapse;">
                            <tr style="vertical-align: top;">
                                <td style="-webkit-font-smoothing: subpixel-antialiased;">
                                    <div class="news__wrap" style="-webkit-font-smoothing: subpixel-antialiased;">
                                        <div class="news__pic" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; float: left; max-width: 220px; min-width: 220px; padding-right: 3%; vertical-align: top;">
                                            <a href="<?=$host.'/news/'.$new->id?>" target="_blank" style="color: #3777e8 !important; display: block; text-decoration: none !important;">
                                                <img width="220" src="<?=$host.'/uploads/news/' . $top_new->id . '/' . $top_new->img ?>" style="border: 0; width: 100%;" alt=""/>
                                            </a>
                                        </div>
                                        <div class="news__cnt" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; float: left; max-width: 400px; min-width: 300px; vertical-align: top;">
                                            <div class="timedate" style="-webkit-font-smoothing: subpixel-antialiased; color: #a4a4a4; font-size: 14px; font-weight: 300; line-height: 28px; margin-top: 6px;"><?= $formatter->asDate($new->created_at, 'dd  MMMM yyyy') ?></div>
                                            <div class="h2" style="-webkit-font-smoothing: subpixel-antialiased; color: #333333; font-size: 18px; line-height: 24px; margin-top: 4px; padding-right: 28px; text-align: left;"><a href="<?=$host.'/news/'.$new->id?>" target="_blank" style="color: #3777e8 !important; text-decoration: none !important;"><span style="color: #000000 !important; font-size: 15px; font-weight: 600; line-height: 22px;"><?= $new->title ?></span></a></div>
                                            <div class="paragraph" style="-webkit-font-smoothing: subpixel-antialiased; color: #353434; font-size: 14px; font-weight: 300; line-height: 22px; margin-top: 12px; padding-right: 27px; text-align: left;"><?= $new->short_desc ?></div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php }
                } ?>
                <div class="column" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; font-size: 14px; margin-bottom: 38px; max-width: 820px; text-align: left; vertical-align: top; width: 100%;">
                    <table width="100%" style="border-collapse: collapse;">
                        <tr style="vertical-align: top;">
                            <td style="-webkit-font-smoothing: subpixel-antialiased;">
                                <a href="<?=$host.'/news'?>" class="btn btn_light" style="background-color: #ffffff; border: 1px solid #000000; box-shadow: 0 6px 16px rgba(5, 11, 21, 0.1); color: #000000 !important; display: inline-block; font-size: 14px; font-weight: 400; line-height: 24px; margin: auto; padding: 13px 9%; text-decoration: none !important; text-transform: uppercase; width: auto !important;">Посмотреть все новости</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
            </td>
        </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
</div>
<div class="footer" style="-ms-text-size-adjust: 100%; -webkit-font-smoothing: subpixel-antialiased; -webkit-text-size-adjust: 100%; background-color: #f7f8fa; border: 0; margin: 0 10px; table-layout: fixed; text-align: center;">
    <!--[if (gte mso 9)|(IE)]>
    <table width="600" align="center">
        <tr>
            <td>
    <![endif]-->
    <table class="outer" cellpadding="0" align="center" style="background-color: #f7f8fa !important; border-collapse: collapse; margin: 0 auto; max-width: 820px; width: 100%;">
        <tr>
            <td class="one-column credits" style="-webkit-font-smoothing: subpixel-antialiased; font-size: 0; text-align: center;">
                <div class="column" style="-webkit-font-smoothing: subpixel-antialiased; display: inline-block; font-size: 14px; max-width: 820px; text-align: left; vertical-align: top; width: 100%;">
                    <table width="100%" cellpadding="20" style="border-collapse: collapse;">
                        <tr>
                            <td style="-webkit-font-smoothing: subpixel-antialiased;">
                                <div class="paragraph" style="-webkit-font-smoothing: subpixel-antialiased; color: #939393; font-size: 14px; font-weight: 400; line-height: 28px; text-align: center;">
                                    Вы получили это письмо, так как были подписаны на новостную рассылку портала <a href="<?=$host?>" target="_blank" style="color: #3777e8 !important; text-decoration: none !important;"><?=$host?></a>. Если вы не хотите получать обновления, вы можете <a href="<?=$host?>/news/unsubscribe?email=<?=$email?>&key=<?=$key?>" target="_blank" style="color: #3777e8 !important; text-decoration: none !important;"><span style="color: #3777e8;">отписаться от рассылки.</span></a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
    </td>
    </tr>
    </table>
    <![endif]-->
</div>

</body>

</html>
