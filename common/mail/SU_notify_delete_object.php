<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 19.10.2017
 * Time: 13:33
 */
/**
 *  @var $user \common\models\User
 * @var $object \common\models\Object
 * */

?>
<h1>Оповещение об удалении объекта с Инвестиционного портала пользователем</h1>
<br>
<p>Пользователь Инвестиционного портала Московской области удалил свои объекты из Личного кабинета, либо они были удалены из-за неактуальности.</p>
<?php
    $i = 0;
    foreach ($data['object'] as $user_id => $objects){
        $object_ids = '<span>Объекты:</span>';
        $object_res = null;

        $user = \common\models\User::findOne($user_id); ?>
        <p>Пользователь: <?=$user->getFullUserName()?></p>
        <p>E-mail: <?=$user->email?></p>

    <?php
    $n = 1;
        foreach ($objects as $object){
            $object_ids .= '<span> ID'.$object->id.' </span>';
            $address = $object->address_address ? ', адрес: '.$object->address_address : null;
            $object_res .= '<p>'.$n.'. Тип: '.$object->getObjectTypeName().', название: '.$object->name . $address .'.</p>';
        $n++;
        }
        echo $object_ids;
        echo $object_res;
    $i++;
}?>


<br>
<a href="http://<?=$data['host']?>">Перейти на главную</a>
<a href="http://<?=$data['host']?>/map">Перейти на карту</a>
