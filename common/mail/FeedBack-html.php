<?php
use common\models\FeedBack;
?>
<h2>
<?= $model->type == FeedBack::TYPE_REQUEST ? 'Новое обращение (заявка)' : '' ?>
<?= $model->type == FeedBack::TYPE_QUESTION ? 'Новое обращение (вопрос)' : '' ?>
<?= $model->type == FeedBack::TYPE_ARGUE ? 'Новое обращение (жалоба)' : '' ?>
</h2>
Имя: <?= $model->lastname ?> <?= $model->name ?> <?= $model->middlename ?><br />
Email: <?= $model->email ?><br />
Телефон: <?= $model->phone ?><br />

<h3>Данные обращения</h3><br />
<? foreach (json_decode($model->data) as $key => $value) : ?>
    <?
        if (is_array($value)) {
            $value = implode('<br/>', $value);
        }
    ?>
    <?= $model->getDataTranslation($key) ?> : <?= $value ?><br/>
<? endforeach; ?>