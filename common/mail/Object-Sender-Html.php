<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 08.08.2017
 * Time: 22:06
 */
use common\models\ObjectType;
?>
<h1>Новые инвестиционные объекты</h1>
<br>
<p>На Инвестиционном портале Московской области появились интересующие Вас объекты (<?=count($model)?>):</p>
<?php
    foreach ($model as $object){ ?>
        <?php
        $type_name = ObjectType::findOne($object->object_type_id)->name;
        if($object->object_type_id == 9)
            $section = 9;
        else
            $section = '0&main-'.$object->object_type_id.'=on';
        ?>
        <div><b>Название объекта: </b><span><?=$object->name?></span></div>
        <div><b>Тип объекта: </b><span><?=$type_name?></span></div>
        <?php if($object->address_address){ ?>
            <div><b>Адрес объекта: </b><span><?=$object->address_address?></span></div>
        <?php   }?>
        <br>
        <div><a href="<?= $host . '/map?ObjectMapSearch%5Baddress_address%5D=&ObjectMapSearch%5Bowner%5D%5B%5D=1&ObjectMapSearch%5Bowner%5D%5B%5D=2&ObjectMapSearch%5Bowner%5D%5B%5D=3&ObjectMapSearch%5Bbargain_type%5D%5B%5D=1&ObjectMapSearch%5Bbargain_type%5D%5B%5D=2&ObjectMapSearch%5Bbargain_price%5D%5B%5D=&ObjectMapSearch%5Bbargain_price%5D%5B%5D=&ObjectMapSearch%5Bsquare%5D%5B%5D=&ObjectMapSearch%5Bsquare%5D%5B%5D=&ObjectMapSearch%5Bcadastral_number%5D=&ObjectMapSearch%5Bdistance_to_moscow%5D%5B%5D=&ObjectMapSearch%5Bdistance_to_moscow%5D%5B%5D=&ObjectMapSearch%5Bsquare_target%5D=&ObjectMapSearch%5Bland_category%5D%5B%5D=1&ObjectMapSearch%5Bland_category%5D%5B%5D=2&ObjectMapSearch%5Bland_category%5D%5B%5D=3&ObjectMapSearch%5Bhas_buildings%5D%5B%5D=1&ObjectMapSearch%5Bhas_buildings%5D%5B%5D=0&ObjectMapSearch%5Bhas_road_availability%5D%5B%5D=1&ObjectMapSearch%5Bhas_road_availability%5D%5B%5D=0&ObjectMapSearch%5Bhas_railway_availability%5D%5B%5D=1&ObjectMapSearch%5Bhas_railway_availability%5D%5B%5D=0&ObjectMapSearch%5Bhas_charge%5D%5B%5D=1&ObjectMapSearch%5Bhas_charge%5D%5B%5D=0&ObjectMapSearch%5Bis_special_ecological_zone%5D%5B%5D=1&ObjectMapSearch%5Bis_special_ecological_zone%5D%5B%5D=0&ObjectMapSearch%5Bdanger_class%5D%5B%5D=1&ObjectMapSearch%5Bdanger_class%5D%5B%5D=2&ObjectMapSearch%5Bdanger_class%5D%5B%5D=3&ObjectMapSearch%5Bdanger_class%5D%5B%5D=4&ObjectMapSearch%5Bfree_land%5D%5B%5D=1&ObjectMapSearch%5Bfree_land%5D%5B%5D=0&ObjectMapSearch%5Bfree_room%5D%5B%5D=1&ObjectMapSearch%5Bfree_room%5D%5B%5D=0&ObjectMapSearch%5Bokved%5D=&ObjectMapSearch%5Bflat%5D%5B%5D=&ObjectMapSearch%5Bflat%5D%5B%5D=&ObjectMapSearch%5Bflat_height%5D%5B%5D=&ObjectMapSearch%5Bflat_height%5D%5B%5D=&section='.$section.'&id='.$object->id?>">Перейти к объекту</a></div>
        <br>
   <?php }

?>
<p>Вы получили это письмо, так как были подписаны на рассылку инвестиционных объектов портала <a href="<?=$host?>"><?=$host?></a>.</p>
</br>
<p>Если это были не Вы, можете отписаться от рассылки <a href="<?=$host?>/map-feed-back/unsubscribe?email=<?=$map_feed_back->email?>&key=<?=$map_feed_back->key?>"><?=$host?>/map-feed-back/unsubscribe?email=<?=$map_feed_back->email?>&key=<?=$map_feed_back->key?></a>.</p>
