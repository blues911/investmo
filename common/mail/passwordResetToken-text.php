<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<h1>Здраствуйте, <?= $user->username ?></h1>

<p>Воспользуйтесь этой ссылкой для восстановления пароля</p>

<?= $resetLink ?>
