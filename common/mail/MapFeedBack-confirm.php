<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 18.07.2017
 * Time: 16:27
 */
?>
<h1>Подписка на новые объекты Инвестиционного портала оформлена</h1>

<p>Вы получили это письмо, так как были подписаны на рассылку инвестиционных объектов портала <a href="<?=Yii::$app->urlManager->hostInfo?>"><?=Yii::$app->urlManager->hostInfo?></a>.</p>
</br>
<p>Если это были не Вы, можете отписаться от рассылки <a href="<?=Yii::$app->urlManager->hostInfo?>/map-feed-back/unsubscribe?email=<?=$model->email?>&key=<?=$model->key?>"><?=Yii::$app->urlManager->hostInfo?>/map-feed-back/unsubscribe?email=<?=$model->email?>&key=<?=$model->key?></a>.</p>