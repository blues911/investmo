<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 28.07.2017
 * Time: 18:27
 */
use yii\helpers\Html;
$link = 'http://'.Yii::$app->request->hostName.'/site/activation?email='.$data['email'].'&key='. $data['key'];
?>
<h1>Необходимо подтвердить регистрацию на Инвестиционном портале МО</h1>
<p>
    Вы успешно зарегистрировались на Инвестиционном портале Московской области.
    Для подтверждения регистрации и активации аккаунта Вам необходимо пройти по ссылке <a href="<?=$link?>"><?=$link?></a>
</p>
