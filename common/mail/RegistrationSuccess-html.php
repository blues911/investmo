<?php
/**
 * Created by PhpStorm.
 * User: ADDEQUATTE
 * Date: 29.07.2017
 * Time: 16:13
 */
use yii\helpers\Html;
?>
<h1>Ваша регистрация на Инвестиционном портале МО подтверждена</h1>
<p>
    Ваша регистрация на Инвестиционном портале Московской области подтверждена, Ваш аккаунт активирован
</p>
<a href="<?=Yii::$app->request->hostName?>">Перейти на главную</a>
<br>
<a href="<?=Yii::$app->request->hostName.'/cabinet'?>">Перейти в Личный Кабинет</a>
