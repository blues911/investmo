
<h1>Подписка на новости Инвестиционного портала оформлена</h1>

<p>Вы получили это письмо, так как были подписаны на новостную рассылку портала <a href="<?=Yii::$app->urlManager->hostInfo?>"><?=Yii::$app->urlManager->hostInfo?></a>.</p>
</br>
<p>Если это были не Вы, можете отписаться от рассылки <a href="<?=Yii::$app->urlManager->hostInfo?>/news/unsubscribe?email=<?=$data->email?>&key=<?=$data->key?>"><?=Yii::$app->urlManager->hostInfo?>/news/unsubscribe?email=<?=$data->email?>&key=<?=$data->key?></a>.</p>
