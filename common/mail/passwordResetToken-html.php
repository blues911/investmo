<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $data->password_reset_token]);
?>
<h1>Восстановление пароля на Инвестиционном портале МО</h1>

<div class="password-reset">

    <p>Пожалуйста, пройдите по этой ссылке для восстановления пароля на Инвестиционном портале Московской области:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
